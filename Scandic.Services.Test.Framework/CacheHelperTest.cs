﻿// <copyright file="CacheHelperTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.Framework
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.Framework;

    /// <summary>
    /// This is a test class for CacheHelperTest and is intended
    /// to contain all CacheHelperTest Unit Tests
    /// </summary>
    [TestClass]
    public class CacheHelperTest
    {
        /// <summary>
        /// Test Context Instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        /// <summary>
        /// A test for Flush
        /// </summary>
        [TestMethod]
        public void FlushTest()
        {
            try
            {
                CacheHelper.Flush();
                Assert.IsTrue(true, "Flushing did not result in an exception");
            }
            catch
            {
                Assert.Fail("Flushing resulted in an exception");
            }
        }

        /// <summary>
        /// A test for Get
        /// </summary>
        [TestMethod]
        public void GetTest()
        {
            try
            {
                string key = "mykey";
                object value = "hello";
                CacheHelper.PutTillDailyRefresh(key, value, "Cache Manager");
                Assert.AreEqual(CacheHelper.Get(key, "Cache Manager"), value);
            }
            catch
            {
                Assert.Fail("Flushing resulted in an exception");
            }
        }

        /// <summary>
        /// A test for PutTillWeeklyRefresh
        /// </summary>
        [TestMethod]
        public void PutTillWeeklyRefreshTest()
        {
            try
            {
                string key = "mykey";
                object value = "hello";
                CacheHelper.PutTillWeeklyRefresh(key, value);
                Assert.AreEqual(CacheHelper.Get(key), value);
            }
            catch
            {
                Assert.Fail("Flushing resulted in an exception");
            }
        }

        /// <summary>
        /// A test for PutTillWeeklyRefresh
        /// </summary>
        [TestMethod]
        public void PutTillWeeklyRefreshTestWithManager()
        {
            try
            {
                string key = "mykey";
                object value = "hello";
                CacheHelper.PutTillWeeklyRefresh(key, value, "Cache Manager");
                Assert.AreEqual(CacheHelper.Get(key, "Cache Manager"), value);
            }
            catch
            {
                Assert.Fail("Flushing resulted in an exception");
            }
        }

        /// <summary>
        /// A test for PutTillDailyRefresh
        /// </summary>
        [TestMethod]
        public void PutTillDailyRefreshTest()
        {
            try
            {
                string key = "mykey";
                object value = "hello";
                CacheHelper.PutTillDailyRefresh(key, value, "Cache Manager");
                if (CacheHelper.IsAvailable(key, "Cache Manager"))
                {
                    Assert.AreEqual(CacheHelper.Get(key, "Cache Manager"), value);
                }
            }
            catch
            {
                Assert.Fail("Flushing resulted in an exception");
            }
        }

        /// <summary>
        /// A test for Put
        /// </summary>
        [TestMethod]
        public void PutTest()
        {
            string key = "key";
            object value = "hello";
            string cacheManager = "Cache Manager";
            CacheDuration duration = CacheDuration.Hour;
            CacheHelper.Put(key, value, duration, cacheManager);
            duration = CacheDuration.HalfDay;
            CacheHelper.Put(key, value, duration, cacheManager);
            duration = CacheDuration.Day;
            CacheHelper.Put(key, value, duration, cacheManager);
            duration = CacheDuration.Week;
            CacheHelper.Put(key, value, duration, cacheManager);
            Assert.AreEqual(CacheHelper.Get(key, cacheManager), value);
        }

        /// <summary>
        /// A test for Put
        /// </summary>
        [TestMethod]
        public void PutTest1()
        {
            string key = "key";
            object value = "hello";
            long minutes = 5; // TODO: Initialize to an appropriate value
            CacheHelper.Put(key, value, minutes);
            Assert.AreEqual(CacheHelper.Get(key), value);
        }

        /// <summary>
        /// A test for Put
        /// </summary>
        [TestMethod]
        public void PutTest2()
        {
            string key = "key";
            object value = "hello";
            CacheDuration duration = CacheDuration.Hour;
            CacheHelper.Put(key, value, duration);
            duration = CacheDuration.HalfDay;
            CacheHelper.Put(key, value, duration);
            duration = CacheDuration.Day;
            CacheHelper.Put(key, value, duration);
            duration = CacheDuration.Week;
            CacheHelper.Put(key, value, duration);
            Assert.AreEqual(CacheHelper.Get(key), value);
        }

        /// <summary>
        /// A test for Put
        /// </summary>
        [TestMethod]
        public void PutTest3()
        {
            string key = "key";
            object value = "hello";
            long minutes = 5; // TODO: Initialize to an appropriate value
            string cacheManager = "Cache Manager";
            CacheHelper.Put(key, value, minutes, cacheManager);
            Assert.AreEqual(CacheHelper.Get(key, cacheManager), value);
        }
    }
}
