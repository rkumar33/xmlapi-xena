﻿// <copyright file="LogHelperTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.Framework
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.Framework;

    /// <summary>
    /// This is a test class for LogHelperTest and is intended
    /// to contain all LogHelperTest Unit Tests
    /// </summary>
    [TestClass]
    public class LogHelperTest
    {
        /// <summary>
        /// Test Context Instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        /// <summary>
        /// A test for LogException
        /// </summary>
        [TestMethod]
        public void LogExceptionTest()
        {
            Exception ex = new Exception("Test Exception");
            LogCategory category = LogCategory.Availability;
            LogHelper.LogException(ex, category);
            Assert.IsTrue(true); // Test case is a success if this doesn't result in exception
        }

        /// <summary>
        /// A test for LogError
        /// </summary>
        [TestMethod]
        public void LogErrorTest()
        {
            string message = "This is some error";
            LogCategory category = LogCategory.Content;
            LogHelper.LogError(message, category);
            Assert.IsTrue(true); // Test case is a success if this doesn't result in exception
        }

        /// <summary>
        /// A test for LogInfo
        /// </summary>
        [TestMethod]
        public void LogInfoTest()
        {
            string message = "This is some info";
            LogCategory category = LogCategory.Content;
            LogHelper.LogInfo(message, category);
            Assert.IsTrue(true); // Test case is a success if this doesn't result in exception
        }

        /// <summary>
        /// A test for LogWarning
        /// </summary>
        [TestMethod]
        public void LogWarningTest()
        {
            string message = "This is some warning";
            LogCategory category = LogCategory.Service;
            LogHelper.LogWarning(message, category);
            Assert.IsTrue(true); // Test case is a success if this doesn't result in exception
        }
    }
}
