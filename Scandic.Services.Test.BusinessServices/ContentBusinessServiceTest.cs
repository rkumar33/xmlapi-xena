﻿// <copyright file="ContentBusinessServiceTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessServices
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessServices;   

    /// <summary>
    /// This is a test class for ContentBusinessServiceTest and is intended
    /// to contain all ContentBusinessServiceTest Unit Tests
    /// </summary>
    [TestClass]
    public class ContentBusinessServiceTest
    {
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// A test for ContentBusinessService Constructor
        /// </summary>
        [TestMethod]
        public void ContentBusinessServiceConstructorTest()
        {
            ContentBusinessService target = new ContentBusinessService();
            Assert.IsInstanceOfType(target, typeof(IContentBusinessContract));
        }

        /// <summary>
        /// A test for GetHotelNetwork
        /// </summary>
        [TestMethod]
        public void GetHotelNetworkTest()
        {
            ContentBusinessService target = new ContentBusinessService();
            string expected = "Europe";
            string actual;
            actual = target.GetHotelNetwork().Continents[0].Name;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for Language
        /// </summary>
        [TestMethod]
        public void LanguageTest()
        {
            ContentBusinessService target = new ContentBusinessService();
            string expected = "en";
            string actual;
            target.Language = expected;
            actual = target.Language;
            Assert.AreEqual(expected, actual);
        }
    }
}
