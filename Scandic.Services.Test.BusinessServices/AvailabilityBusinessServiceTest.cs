﻿// <copyright file="AvailabilityBusinessServiceTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessServices
{
    using System;
    using System.Collections.Generic;     
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessServices;
    using Scandic.Services.BusinessEntity;
     
    
    /// <summary>
    /// This is a test class for AvailabilityBusinessServiceTest and is intended
    /// to contain all AvailabilityBusinessServiceTest Unit Tests
    /// </summary>
    [TestClass()]
    public class AvailabilityBusinessServiceTest
    {
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }              

         /// <summary>
        /// A test for GetRoomAndRates
        /// </summary>
        [TestMethod()]
        public void GetRoomAndRatesTest()
        {
            HotelSearchRoomEntity roomSearch1 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 2, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch2 = new HotelSearchRoomEntity() { AdultsPerRoom = 1, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch3 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 2 };
            List<HotelSearchRoomEntity> listsearch = new List<HotelSearchRoomEntity>();
            listsearch.Add(roomSearch1);
            listsearch.Add(roomSearch2);
            listsearch.Add(roomSearch3);
            List<string> selectedHotelCode = new List<string>();
            selectedHotelCode.Add("830");
            HotelSearchEntity hotelSearch = new HotelSearchEntity() { NoOfNights = 2, ArrivalDate = DateTime.Today, DepartureDate = DateTime.Today.AddDays(2), ListRooms = listsearch, SelectedHotelCode = selectedHotelCode };            
            AvailabilityBusinessService target = new AvailabilityBusinessService();
            string expected = "830";
            string actual;
            actual = target.GetRoomAndRates(hotelSearch).HotelId.ToString();
            Assert.Equals(expected, actual);
        }               
    }
}
