﻿// <copyright file="AvailabilityManagerTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessServices
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.BusinessServices;             
    
    /// <summary>
    /// This is a test class for AvailabilityManagerTest and is intended
    /// to contain all AvailabilityManagerTest Unit Tests
    /// </summary>
    [TestClass()]
    public class AvailabilityManagerTest
    {
        /// <summary>
        /// Gets or sets the testContextInstance which provides
        /// information about and functionality for the current test run.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
               this.testContextInstance = value;
            }
        }   

        /// <summary>
        /// A test for GetRoomsandRates
        /// </summary>
        [TestMethod()]
        public void GetRoomsandRatesTest()
        {
            HotelSearchRoomEntity roomSearch1 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 2, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch2 = new HotelSearchRoomEntity() { AdultsPerRoom = 1, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch3 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 2 };
            List<HotelSearchRoomEntity> listsearch = new List<HotelSearchRoomEntity>();
            listsearch.Add(roomSearch1);
            listsearch.Add(roomSearch2);
            listsearch.Add(roomSearch3);
            List<string> selectedHotelCode = new List<string>();
            selectedHotelCode.Add("830");
            HotelSearchEntity hotelSearch = new HotelSearchEntity() { NoOfNights = 2, ArrivalDate = DateTime.Today, DepartureDate = DateTime.Today.AddDays(2), ListRooms = listsearch, SelectedHotelCode = selectedHotelCode };
            string language = "en"; 
            string provider = "Scandic"; 
            AvailabilityManager target = new AvailabilityManager(language, provider);            
            RoomsAndRates expected = new RoomsAndRates();
            expected.HotelId = 830;
            RoomsAndRates actual = new RoomsAndRates();
            actual.HotelId = target.GetRoomsandRates(hotelSearch).HotelId;
            Assert.Equals(expected, actual);
        }
              
        /// <summary>
        /// A test for GetAvailableHotelList
        /// </summary>
        [TestMethod()]
        public void GetAvailableHotelListTest()
        {
            HotelSearchRoomEntity roomSearch1 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 2, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch2 = new HotelSearchRoomEntity() { AdultsPerRoom = 1, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch3 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 2 };
            List<HotelSearchRoomEntity> listsearch = new List<HotelSearchRoomEntity>();
            listsearch.Add(roomSearch1);
            listsearch.Add(roomSearch2);
            listsearch.Add(roomSearch3);
            List<string> selectedHotelCode = new List<string>();
            selectedHotelCode.Add("830");
            HotelSearchEntity hotelSearch = new HotelSearchEntity() { NoOfNights = 2, ArrivalDate = DateTime.Today, DepartureDate = DateTime.Today.AddDays(2), ListRooms = listsearch, SelectedHotelCode = selectedHotelCode };
            string language = "en";
            string provider = "Scandic"; 
            AvailabilityManager target = new AvailabilityManager(language, provider);
            List<Hotel> bookableHotelList = new List<Hotel>();
            Hotel hotel = new Hotel();
            hotel.Id = "830";
            hotel.IsBookable = true;
            hotel.Name = "Scandic Alvik";
            hotel.PropertyType = "Room";
            hotel.PostalCity = "StockHolm";
            bookableHotelList.Add(hotel);

            string expected = "Europe";
            AvailableHotelDetailsList actual;
            actual = target.GetAvailableHotelList(hotelSearch, bookableHotelList);
            Assert.Equals(expected, actual.Continent);            
        }

        /// <summary>
        /// A test for GetAvailableHotelDetails
        /// </summary>        
        [TestMethod()]
        public void GetAvailableHotelDetailsTest()
        {
            string language = "en";
            string provider = "Scandic";
            HotelSearchRoomEntity roomSearch1 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 2, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch2 = new HotelSearchRoomEntity() { AdultsPerRoom = 1, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 3 };
            HotelSearchRoomEntity roomSearch3 = new HotelSearchRoomEntity() { AdultsPerRoom = 2, ChildrenOccupancyPerRoom = 1, ChildrenPerRoom = 2 };
            List<HotelSearchRoomEntity> listsearch = new List<HotelSearchRoomEntity>();
            listsearch.Add(roomSearch1);
            listsearch.Add(roomSearch2);
            listsearch.Add(roomSearch3);
            List<string> selectedHotelCode = new List<string>();
            selectedHotelCode.Add("830");
            HotelSearchEntity hotelSearch = new HotelSearchEntity() { NoOfNights = 2, ArrivalDate = DateTime.Today, DepartureDate = DateTime.Today.AddDays(2), ListRooms = listsearch, SelectedHotelCode = selectedHotelCode };
            AvailabilityManager target = new AvailabilityManager(language, provider);
            string hotelId = "830";
            string expected = "Europe";
            AvailableHotelDetails actual;
            actual = target.GetAvailableHotelDetails(hotelSearch, hotelId);
            Assert.Equals(expected, actual.Continent);
        }
    }
}
