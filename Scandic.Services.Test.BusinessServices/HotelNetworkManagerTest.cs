﻿// <copyright file="HotelNetworkManagerTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessServices
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessServices;
    
    /// <summary>
    /// This is a test class for HotelNetworkManagerTest and is intended
    /// to contain all HotelNetworkManagerTest Unit Tests
    /// </summary>
    [TestClass]
    public class HotelNetworkManagerTest
    {
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// A test for HotelNetworkManager Constructor
        /// </summary>
        [TestMethod]
        public void HotelNetworkManagerConstructorTest()
        {
            string language = "en";
            string provider = "Scandic";
            HotelNetworkManager target = new HotelNetworkManager(language, provider);
            Assert.IsInstanceOfType(target, typeof(IEntityManager));
        }

        /// <summary>
        /// A test for Network
        /// </summary>
        [TestMethod]
        [DeploymentItem("Scandic.Services.BusinessServices.dll")]
        public void NetworkTest()
        {
            string language = "en";
            string provider = "Scandic";
            HotelNetworkManager target = new HotelNetworkManager(language, provider);
            string expected = "Europe";
            string actual = target.Network.Continents[0].Name; // Replace with some valid LINQ
            Assert.AreEqual(expected, actual);
        }
    }
}
