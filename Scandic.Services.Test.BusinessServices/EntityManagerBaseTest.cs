﻿// <copyright file="EntityManagerBaseTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessServices
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessServices;
    using Scandic.Services.Framework;
   
    /// <summary>
    /// This is a test class for EntityManagerBaseTest and is intended
    /// to contain all EntityManagerBaseTest Unit Tests
    /// </summary>
    [TestClass]
    public class EntityManagerBaseTest
    {
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// A test for EntityManagerBase Constructor
        /// </summary>
        [TestMethod]
        public void EntityManagerBaseConstructorTest()
        {
            EntityManagerBase target = new EntityManagerBase();
            Assert.IsInstanceOfType(target, typeof(EntityManagerBase));
        }

        /// <summary>
        /// A test for ExpireCache
        /// </summary>
        [TestMethod]
        public void ExpireCacheTest()
        {
            EntityManagerBase target = new EntityManagerBase { CachedValue = "hello" };
            target.ExpireCache();
            Assert.IsNull(target.CachedValue);
        }

        /// <summary>
        /// A test for CachedValue
        /// </summary>
        [TestMethod]
        public void CachedValueTest()
        {
            object expected = "Hello";
            EntityManagerBase target = new EntityManagerBase { CachedValue = expected };
            object actual;
            actual = target.CachedValue;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for IsCacheValid
        /// </summary>
        [TestMethod]
        public void IsCacheValidTest()
        {
            EntityManagerBase target = new EntityManagerBase { CachedValue = "hello" };
            Assert.IsTrue(target.IsCacheValid);
        }

        /// <summary>
        /// A test for IsCached
        /// </summary>
        [TestMethod]
        public void IsCachedTest()
        {
            EntityManagerBase target = new EntityManagerBase();
            bool expected = ConfigHelper.GetValue(ConfigKeys.IsCached).ToUpperInvariant().Equals("TRUE", StringComparison.CurrentCultureIgnoreCase);
            Assert.AreEqual(expected, target.IsCached);
        }
    }
}
