﻿// <copyright file="AvailabiltyBusinessFactory.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessFactory
{
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessServices;    

    /// <summary>
    /// Implements a factory for IContentContract
    /// </summary>    
   public static class AvailabilityBusinessFactory
    {
        /// <summary>
        /// Returns a IContentContract based on the key specified.
        /// This factory doesn't have a switch currently as there is only
        /// one concrete implementation.
        /// </summary>
        /// <param name="provider">Provider - default: Scandic</param>
        /// <param name="language">Language - default: English</param>
        /// <param name="partnerApiKey">Partner Api Key.Unique Key generated for the partners</param>
        /// <returns>Returns a IContentContract based on the key specified</returns>
        public static IAvailabilityBusinessContract CreateInstance(string provider, string language, string partnerApiKey)
        {
            return new AvailabilityBusinessService { Provider = provider, Language = language, PartnerApiKey = partnerApiKey };
        }
    }
}
