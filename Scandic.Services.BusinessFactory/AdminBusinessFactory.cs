﻿// <copyright file="AdminBusinessFactory.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessFactory
{
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessServices;

    /// <summary>
    /// Implements a factory for IContentContract
    /// </summary>    
    public static class AdminBusinessFactory 
    {
        /// <summary>
        /// Gets the admin business service.
        /// </summary>
        /// <param name="key">The Key for Creating Instance</param>
        /// <returns> returns business contract </returns>       
        public static IAdminBusinessContract CreateInstance(string key)
        {
            return new AdminBusinessService();
        }
    }
}
