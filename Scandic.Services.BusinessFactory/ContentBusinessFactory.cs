﻿// <copyright file="ContentBusinessFactory.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessFactory
{
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessServices;

    /// <summary>
    /// Implements a factory for IContentContract
    /// </summary>
    public static class ContentBusinessFactory
    {
        /// <summary>
        /// Returns a IContentContract based on the key specified.
        /// This factory doesn't have a switch currently as there is only
        /// one concrete implementation.
        /// </summary>
        /// <param name="provider">Provider - default: Scandic</param>
        /// <param name="language">Language - default: English</param>
        /// <returns>Returns a IContentContract based on the key specified</returns>
        public static IContentBusinessContract CreateInstance(string provider, string language)
        {
            return new ContentBusinessService { Provider = provider, Language = language };
        }
    }
}
