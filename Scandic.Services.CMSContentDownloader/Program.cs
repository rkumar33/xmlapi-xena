﻿using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Services.ContentDownloader;
using Scandic.Services.BusinessServices;
using Scandic.Services.Framework;
using System.IO;

namespace Scandic.Services.CMSContentDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IContentDownloader> itemDownloaders = new List<IContentDownloader>();
            itemDownloaders.Add(new RoomTypeContentDownloader());
            itemDownloaders.Add(new RateTypeContentDownloader()); //Freq
            itemDownloaders.Add(new PartnerContentDownloader()); //Freq
            itemDownloaders.Add(new HotelNetworkContentDownloader());
            itemDownloaders.Add(new HotelDetailsContentDownloader());

            // Default frequency is supposed to be daily. If argument for "FREQUENT" is passed,
            // then the frequency is considered to be FREQUENT
            DownloadFrequency frequencyToRun = DownloadFrequency.DAILY;

            if (args.Length > 0 && args[0] == "FREQUENT")
            {
                frequencyToRun = DownloadFrequency.FREQUENT;
            }

            LogHelper.LogInfo(string.Format("Running for frequency {0}", frequencyToRun.ToString()), LogCategory.ContentDownloader);

            try
            {
                // Get all content that needs to be downloaded frequently.
                List<IContentDownloader> downloadersToRun = itemDownloaders.FindAll(delegate(IContentDownloader match)
                {
                    return match.ContentDownloadFrequency == frequencyToRun;
                });

                LogHelper.LogInfo(string.Format("Downloading content started at {0}", DateTime.Now), LogCategory.ContentDownloader);

                // If an error occurs while downloading any content, the whole download should be considered unsuccessful.
                // So, the cache should not be refreshed and no timestamp file is updated.

                // Start content download
                foreach (IContentDownloader item in downloadersToRun)
                {
                    LogHelper.LogInfo(string.Format("Downloading content for {0}", item.ToString()), LogCategory.ContentDownloader);
                    item.DowloadContent();
                }

                LogHelper.LogInfo(string.Format("Downloading content ended at {0}", DateTime.Now), LogCategory.ContentDownloader);

                // Update the timestamp file
                string timeStampFile = string.Empty;

                if (frequencyToRun == DownloadFrequency.FREQUENT)
                {
                    timeStampFile = Path.Combine(ConfigHelper.GetValue(ConfigKeys.AppPath), ConfigHelper.GetValue(ConfigKeys.TimeStampFileFrequent));
                }
                else
                {
                    timeStampFile = Path.Combine(ConfigHelper.GetValue(ConfigKeys.AppPath), ConfigHelper.GetValue(ConfigKeys.TimeStampFileRegular));
                }

                LogHelper.LogInfo(string.Format("Writing to timestamp file {0}", timeStampFile), LogCategory.ContentDownloader);

                using (StreamWriter file = new StreamWriter(timeStampFile, true))
                {
                    file.WriteLine(DateTime.Now.ToString());
                }

                LogHelper.LogInfo(string.Format("Timestamp file {0} channged", timeStampFile), LogCategory.ContentDownloader);

            }
            catch (Exception ex)
            {
                LogHelper.LogError("Downloading content failed", LogCategory.ContentDownloader);
                LogHelper.LogException(ex, LogCategory.ContentDownloader);
            }
        }
    }
}
