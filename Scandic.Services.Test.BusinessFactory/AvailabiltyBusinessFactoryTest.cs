﻿// <copyright file="AvailabiltyBusinessFactoryTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessFactory
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.BusinessServices;   

    /// <summary>
    /// This is a test class for AvailabiltyBusinessFactoryTest and is intended
    /// to contain all AvailabiltyBusinessFactoryTest Unit Tests
    /// </summary>
    [TestClass]
    public class AvailabiltyBusinessFactoryTest
    {
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

         /// <summary>
        /// Tests the interface of type returned
        /// </summary>
        [TestMethod]
        public void GetAvailabiltyServiceTest01()
        {
            string key = string.Empty;
            object actual;
            actual = AvailabilityBusinessFactory.CreateInstance(key, "en");
            Assert.IsInstanceOfType(actual, typeof(IAvailabilityBusinessContract));
        }

         /// <summary>
        /// Tests the concrete type returned
        /// </summary>
        [TestMethod]
        public void GetAvailabiltyServiceTest02()
        {
            string key = string.Empty;
            object actual;
            actual = AvailabilityBusinessFactory.CreateInstance("Scandic", "en");
            Assert.IsInstanceOfType(actual, typeof(IAvailabilityBusinessContract));
        }
    }
}
