﻿// <copyright file="ContentBusinessFactoryTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.BusinessFactory
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.BusinessServices;

    /// <summary>
    /// This is a test class for ContentBusinessFactoryTest and is intended
    /// to contain all ContentBusinessFactoryTest Unit Tests
    /// </summary>
    [TestClass]
    public class ContentBusinessFactoryTest
    {
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// Tests the interface of type returned
        /// </summary>
        [TestMethod]
        public void GetContentServiceTest01()
        {
            string key = string.Empty;
            object actual;
            actual = ContentBusinessFactory.CreateInstance(key, "en");
            Assert.IsInstanceOfType(actual, typeof(IContentBusinessContract));
        }

        /// <summary>
        /// Tests the concrete type returned
        /// </summary>
        [TestMethod]
        public void GetContentServiceTest02()
        {
            string key = string.Empty;
            object actual;
            actual = ContentBusinessFactory.CreateInstance("Scandic", "en");
            Assert.IsInstanceOfType(actual, typeof(ContentBusinessService));
        }
    }
}
