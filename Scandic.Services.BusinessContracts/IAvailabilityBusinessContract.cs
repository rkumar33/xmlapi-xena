﻿// <copyright file="IAvailabilityBusinessContract.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessContracts
{
    using System; 
    using System.Collections;
    using System.Collections.Generic;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    
    /// <summary>
    /// Base contract for all Availabilty related classes
    /// </summary>
    public interface IAvailabilityBusinessContract
    {
        /// <summary>
        /// Gets or sets the language in which entities
        /// are to be returned, where applicable
        /// </summary>
        string Language { get; set; }

        /// <summary>
        /// Gets or sets the provider (i.e. Scandic) for which entities
        /// are to be returned, where applicable
        /// </summary>
        string Provider { get; set; }

        /// <summary>
        /// Gets or sets the partner API key.
        /// </summary>
        /// <value>
        /// The partner API key.
        /// </value>
        string PartnerApiKey { get; set; }

        /// <summary>
        /// Get the Room and Rates available in a hotel
        /// </summary>
        /// <param name="hotelId">The hotel Identifier.</param>
        /// <param name="arrivalDate">The arrival Date</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="adultCounts">The adult count.</param>
        /// <param name="childCounts">The child count.</param>
        /// <param name="childrenAges">The child ages.</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Rooms And Rates available for booking in a hotel
        /// </returns>
        RoomsAndRates GetRoomAndRates(string hotelId, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id);
        

        /// <summary>
        /// Get the Room and Rates available in a hotel
        /// </summary>
        /// <param name="hotelId">The hotel Identifier.</param>
        /// <param name="arrivalDate">The arrival Date</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="adultCounts">The adult count.</param>
        /// <param name="childCounts">The child count.</param>
        /// <param name="childrenAges">The child ages.</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Rooms And Rates available for booking for multiple hotel
        /// </returns>
        HotelRoomsAndRates GetMultipleRoomAndRates(string[] hotelId, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id);


        /// <summary>
        /// Gets the availability for city.
        /// </summary>
        /// <param name="city">The city Name.</param>
        /// <param name="arrivalDate">The arrival Date</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="adultCounts">The adult count.</param>
        /// <param name="childCounts">The child count.</param>
        /// <param name="childrenAges">The child ages.</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Available HotelDetails List
        /// </returns>
        AvailableHotelDetailsList GetAvailabilityForCity(string city, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id);

        /// <summary>
        /// Gets the availability for certain hotels.
        /// </summary>
        /// <param name="hotelIds">The hotel Identifier.</param>
        /// <param name="arrivalDate">The arrival Date</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="adultCounts">The adult count.</param>
        /// <param name="childCounts">The child count.</param>
        /// <param name="childrenAges">The child ages.</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request</param>
        /// <returns>
        /// Available HotelDetails List
        /// </returns>
        AvailableHotelDetailsList GetAvailabilityForCertainHotels(string[] hotelIds, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id);

        /// <summary>
        /// Gets the lowest rate for city.
        /// </summary>
        /// <param name="city">The city search.</param>
        /// <param name="id">The id of the request</param>
        /// <returns>
        /// Available HotelDetails List
        /// </returns>
        AvailableHotelDetailsList GetLowestRateForCity(string city, string id);

        /// <summary>
        /// Refreshes all cached entities
        /// </summary>
        void RefreshCache();
    }
}
