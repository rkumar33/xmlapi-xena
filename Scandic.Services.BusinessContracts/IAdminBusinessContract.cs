﻿// <copyright file="IAdminBusinessContract.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessContracts
{
    using System.Collections.Generic;
    using Scandic.Services.BusinessEntity;

    /// <summary>
    /// Interface for admin business contract
    /// </summary>  
    public interface IAdminBusinessContract
    {
        /// <summary>
        /// Gets a value indicating whether the availability of OWS
        /// </summary>
        bool IsOwsAvailabilityServiceUp { get; }

        /// <summary>
        /// Gets the partner details.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        /// <returns> returns partner details </returns>        
        Partner GetPartnerDetails(string apiKey);

        /// <summary>
        /// Gets a list of all partners
        /// </summary>
        /// <returns>List of partners</returns>
        IList<Partner> PartnerList();

        /// <summary>
        /// Refreshes all cached entities
        /// </summary>
        void RefreshCache();
    }
}
