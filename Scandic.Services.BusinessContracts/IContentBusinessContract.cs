﻿// <copyright file="IContentBusinessContract.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessContracts
{
    using Scandic.Services.BusinessEntity;

    /// <summary>
    /// Base contract for all content/CMS related classes
    /// </summary>
    public interface IContentBusinessContract
    {
        /// <summary>
        /// Gets or sets the language in which entities
        /// are to be returned, where applicable
        /// </summary>
        string Language { get; set; }

        /// <summary>
        /// Gets or sets the provider (i.e. Scandic) for which entities
        /// are to be returned, where applicable
        /// </summary>
        string Provider { get; set; }

        /// <summary>
        /// Get the whole hotel network meta
        /// </summary>
        /// <returns>Hotel Network as an object tree</returns>
        HotelNetwork GetHotelNetwork();

        /// <summary>
        /// Get list of hotels and details
        /// </summary>
        /// <returns>List of hotels with details</returns>
        HotelDetailsList GetHotelDetails();

        /// <summary>
        /// Returns details of one partner, identified by API Key
        /// </summary>
        /// <param name="apiKey">API Key of the partner</param>
        /// <returns>Details of the partner</returns>
        Partner GetPartnerDetails(string apiKey);

        /// <summary>
        /// Gets the room types.
        /// </summary>
        /// <returns>List of the Room categories and Room Codes configured in CMS</returns>
        RoomTypes GetRoomTypes();

        /// <summary>
        /// Gets the rate types.
        /// </summary>
        /// <returns>List of the Rate categories and Rate Code configured in CMS</returns>
        RateTypes GetRateTypes();

        /// <summary>
        /// Refreshes all cached entities
        /// </summary>
        void RefreshCache();
    }
}
