﻿// <copyright file="GlobalSuppressions.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// All suppressions should be approved by the architect. 

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]

// These are heavy methods with logic, and not appropriate to be exposed as a Property
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Scandic.Services.BusinessContracts.IContentBusinessContract.#GetHotelNetwork()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Scandic.Services.BusinessContracts.IContentBusinessContract.#GetRateTypes()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Scandic.Services.BusinessContracts.IContentBusinessContract.#GetRoomTypes()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Scandic.Services.BusinessContracts.IContentBusinessContract.#GetHotelDetails()")]

// Performance related decision
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "Scandic.Services.BusinessContracts.IAdminBusinessContract.#GetPartners()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "Scandic.Services.BusinessContracts.IAdminBusinessContract.#PartnerList()")]

// These are fixed size inner arrays and doesn't waste space. Jagged array doesn't provide any advantage in this case.
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "5#", Scope = "member", Target = "Scandic.Services.BusinessContracts.IAvailabilityBusinessContract.#GetAvailabilityForCertainHotels(System.String[],System.DateTime,System.DateTime,System.Int32[],System.Int32[],System.Int32[,],System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "5#", Scope = "member", Target = "Scandic.Services.BusinessContracts.IAvailabilityBusinessContract.#GetAvailabilityForCity(System.String,System.DateTime,System.DateTime,System.Int32[],System.Int32[],System.Int32[,],System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "5#", Scope = "member", Target = "Scandic.Services.BusinessContracts.IAvailabilityBusinessContract.#GetRoomAndRates(System.String,System.DateTime,System.DateTime,System.Int32[],System.Int32[],System.Int32[,],System.String)")]
