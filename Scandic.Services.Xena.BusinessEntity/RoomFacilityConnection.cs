namespace Scandic.Services.Xena.BusinessEntity
{
	public class RoomFacilityConnection
	{
		public bool AvailableOnlyInSomeRooms { get; set; }
		public RoomFacility RoomFacility { get; set; }
	}
}