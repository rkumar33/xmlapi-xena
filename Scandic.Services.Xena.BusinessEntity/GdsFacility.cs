﻿using System;
using System.Collections.Generic;


namespace Scandic.Services.Xena.BusinessEntity
{
	public class GdsFacility : HotelEntity //, IEquatable<GdsFacility>
	{		
		public string Code { get; set; }
		public bool ApplyToAllHotels { get; set; }
        public bool Public { get; set; }
        //public bool Equals(GdsFacility other)
        //{
        //    //Check whether the compared object is null.
        //    if (Object.ReferenceEquals(other, null)) return false;

        //    //Check whether the compared object references the same data.
        //    if (Object.ReferenceEquals(this, other)) return true;

        //    //Check whether the GDSFacilitiy' properties are equal.
        //    string compare1 = Code ?? string.Empty;
        //    string compare2 = other.Code ?? string.Empty;
        //    return compare1.Equals(compare2) && Name.Equals(other.Name);
        //}

		// If Equals() returns true for a pair of objects 
		// then GetHashCode() must return the same value for these objects.

        //public override int GetHashCode()
        //{

        //    //Get hash code for the Name field if it is not null.
        //    int hashProductName = Name == null ? 0 : Name.GetHashCode();

        //    //Get hash code for the Code field.
        //    int hashProductCode = Code==null? 0 : Code.GetHashCode();

        //    //Calculate the hash code for the product.
        //    return hashProductName ^ hashProductCode;
        //}
	}
}