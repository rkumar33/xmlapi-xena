using System.ComponentModel;

namespace Scandic.Services.Xena.BusinessEntity.Enums
{
	public enum ParkingPayment
	{
		[Description("No information")]
		NoInformation  = 0,

		[Description("Parking can be paid at the hotel")]
		CanBePaidAtTheHotel = 1,

		[Description("Parking must be paid at the hotel")]
		MustBePaidAtTheHotel = 2,
	}

	
}