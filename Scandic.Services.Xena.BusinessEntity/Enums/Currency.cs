namespace Scandic.Services.Xena.BusinessEntity.Enums
{
	// ReSharper disable InconsistentNaming
	public enum Currency
	{
		//None = 0,
		DKK = 1,
		EUR = 2,
		PLN = 3,
		NOK = 4,
		SEK = 5
	}
	// ReSharper restore InconsistentNaming
}