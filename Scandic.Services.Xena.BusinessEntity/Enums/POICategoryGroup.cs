namespace Scandic.Services.Xena.BusinessEntity.Enums
{
	public enum POICategoryGroup
	{
		None,
		Pin,
		Hotel,
		Airport,
		Trains,
		Bus,
		Taxi,
		Entertainment,
		Theatre,
		Star,
		Shopping,
		City,
		Company
	}
}