namespace Scandic.Services.Xena.BusinessEntity.Enums
{
	public enum CancellationRule
	{
		CancellableBefore6PM = 0,
		NotCancellable = 1
	}
}