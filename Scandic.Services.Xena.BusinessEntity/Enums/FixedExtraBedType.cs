﻿namespace Scandic.Services.Xena.BusinessEntity.Enums
{
	public enum FixedExtraBedType
	{
		None = 0,
		WallBed = 1,
		SofaBed = 2,
		PulloutBed = 3
	}
}