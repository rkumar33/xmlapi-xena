namespace Scandic.Services.Xena.BusinessEntity.Enums
{
    public enum RestaurantType
    {
        Restaurant,
        Bar,
        Cafe,
        SkyBar,
        RooftopBar,
        OutdoorTerrace,
		Other
    }
}