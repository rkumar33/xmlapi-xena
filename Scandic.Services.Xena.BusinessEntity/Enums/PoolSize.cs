﻿
namespace Scandic.Services.Xena.BusinessEntity.Enums
{
    public enum PoolSize
    {
        Small,
        Medium,
        Large
    }
}
