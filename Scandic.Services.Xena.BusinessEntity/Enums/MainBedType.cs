﻿namespace Scandic.Services.Xena.BusinessEntity.Enums
{
	public enum MainBedType
	{
		None = 0,
		Single = 1,
		Twin =  2,
		Queen = 3,
		King = 4,
		CustomOccupancy = 5
	}
}