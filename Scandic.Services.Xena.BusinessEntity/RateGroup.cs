using System.Collections.Generic;
using Scandic.Services.Xena.BusinessEntity.Enums;

namespace Scandic.Services.Xena.BusinessEntity
{
	public class RateGroup
	{
		public int Id { get; set; }
		public bool BreakfastIncluded { get; set; }
		public string Header { get; set; }
		public bool GetNameFromOpera { get; set; }
		public CancellationRule CancellationRule { get; set; }
		public bool GuaranteeForLateArrival { get; set; }
		public IEnumerable<string> GeneralTerms { get; set; }
		public int? OvertakenGroupId { get; set; }
		public IEnumerable<string> RateCodes { get; set; }
		public string GuaranteePrepaymentInformation { get; set; }
		public string AboutOurRates { get; set; }
		public CancellationPolicy CancellationPolicy { get; set; }

		public bool IsPublished { get; set; }
		public string LanguageCode { get; set; }
	}

	public class CancellationPolicy
	{
		public string PreBooking { get; set; }
		public string PostBooking { get; set; }
	}
}
