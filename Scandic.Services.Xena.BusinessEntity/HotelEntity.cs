namespace Scandic.Services.Xena.BusinessEntity
{
	public abstract class HotelEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}