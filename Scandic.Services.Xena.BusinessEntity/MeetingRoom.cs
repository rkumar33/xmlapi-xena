﻿using System.Collections.Generic;

namespace Scandic.Services.Xena.BusinessEntity
{
	public class MeetingRoom : HotelEntity
	{
		public string BriefInformation { get; set; }
		public double? Size { get; set; }
		public double? AccessSizeHeight { get; set; }
		public double? AccessSizeWidth { get; set; }
		public double? DimensionLength { get; set; }
		public double? DimensionHeight { get; set; }
		public double? DimensionWidth { get; set; }
		public int? FloorNumber { get; set; }
		public LightingType Lighting { get; set; }
		public int? UShapeCapacity { get; set; }
		public int? ClassroomCapacity { get; set; }
		public int? BoardroomCapacity { get; set; }
		public int? TheatreCapacity { get; set; }
		public int? StandingTableCapacity { get; set; }
		public int? CabaretSeatingCapacity { get; set; }
		public int? HalfCircleTableCapacity { get; set; }
		public int? FullCircleTableCapacity { get; set; }
		public IEnumerable<int> MediaIds { get; set; }
		public string ContentDescriptionShort { get; set; }
		public string ContentDescriptionMedium { get; set; }
	}

    public enum LightingType
    {
        WindowsNaturalDaylightBlackoutFacilities,
        WindowsNaturalDaylight
    }
}