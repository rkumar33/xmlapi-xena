﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable]
    public class Location
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public bool HasPosition
        {
            get
            {
                if (Latitude == null || Longitude == null)
                    return false;
                return true;
            }
        }

        //public static string FormatDistance(double distanceInKm)
        //{
        //    if (distanceInKm < 1.0)
        //    {
        //        // Show distance in meter with two significant digits
        //        return string.Format(CultureInfo.InvariantCulture, "{0:##}0 m", distanceInKm * 100);
        //    }

        //    return string.Format(CultureInfo.InvariantCulture, "{0:0.#} km", distanceInKm);
        //}

        /// <summary>
        /// Returns the distance between the latitude and longitude coordinates (In meters)
        /// </summary>
        /// <param name="other">The location to calculate the distance to.</param>
        /// <returns>The distance between the two coordinates, in meters.</returns>
        public double GetDistanceTo(Location other)
        {
            if (!this.HasPosition || other == null || !other.HasPosition)
            {
                return 0;
            }

            double latitude1 = this.Latitude.Value;
            double longitude1 = this.Longitude.Value;
            double latitude2 = other.Latitude.Value;
            double longitude2 = other.Longitude.Value;
            double d1 = latitude1 * (Math.PI / 180.0);
            double d2 = latitude2 * (Math.PI / 180.0);
            double num1 = d2 - d1;
            double num2 = (longitude2 - longitude1) * (Math.PI / 180.0);
            double d3 = Math.Pow(Math.Sin(num1 / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
            return 12753000.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3));
        }

        public Location()
        {
        }

        public Location(double? lat, double? lon)
        {
            Latitude = lat;
            Longitude = lon;
        }
    }
}
