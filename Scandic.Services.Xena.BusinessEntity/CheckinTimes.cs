﻿using System;
namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable]
    public class CheckinTimes
    {
        public string Checkin { get; set; }

        public string Checkout { get; set; }
    }
}