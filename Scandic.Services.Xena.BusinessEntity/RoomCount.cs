﻿using System;
namespace Scandic.Services.Xena.BusinessEntity
{
   [Serializable]
    public class RoomCount
    {
        public int? Total { get; set; }
        public int? ForDisabled { get; set; }
        public int? NonSmoking { get; set; }
		public int? ForAllergics { get; set; }
		public int? PetRooms { get; set; }
		public int? ConnectedRooms { get; set; }
		public int? WithExtrabeds { get; set; }
	}
}