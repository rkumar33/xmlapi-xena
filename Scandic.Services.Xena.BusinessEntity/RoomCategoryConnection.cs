using System.Collections.Generic;
using System.Linq;

namespace Scandic.Services.Xena.BusinessEntity
{
	public class RoomCategoryConnection 
	{
		/// <summary>
		/// Id of the RoomCategoryConnectorBlock
		/// </summary>
		public int Id { get; set; }

		public string Description { get; set; }
		public string UpsellMessage { get; set; }
		public IEnumerable<int> MediaIds { get; set; }

		public string ContentDescriptionShort { get; set; }
		public string ContentDescriptionMedium { get; set; }

		public RoomCategory RoomCategory { get; set; }

		public IEnumerable<RoomFacilityConnection> RoomFacilities { get; set; }
		public IEnumerable<RoomTypeConnection> RoomTypes { get; set; }

		public RoomCategoryConnection()
		{
			RoomFacilities = Enumerable.Empty<RoomFacilityConnection>();
			RoomTypes = Enumerable.Empty<RoomTypeConnection>();
		}
	}
}