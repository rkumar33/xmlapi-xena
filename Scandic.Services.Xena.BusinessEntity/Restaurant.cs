﻿using System.Collections.Generic;
using Scandic.Services.Xena.BusinessEntity.Enums;
namespace Scandic.Services.Xena.BusinessEntity
{
    public class Restaurant : HotelEntity
    {
        public string RestaurantId { get; set; }
        public RestaurantType RestaurantType { get; set; }
        public string BriefDescription { get; set; }
        public double? BreakfastPriceForExternalGuests { get; set; }
        public Currency Currency { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FacebookPage { get; set; }
        public string InstagramPage { get; set; }
        public string TwitterAccount { get; set; }
        public string ExternalSiteLink { get; set; }
        public IEnumerable<int> MediaIds { get; set; }
        public string ContentDescriptionShort { get; set; }
        public string ContentDescriptionMedium { get; set; }

    }
}