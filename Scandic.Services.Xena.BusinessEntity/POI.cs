﻿using System.Collections.Generic;

namespace Scandic.Services.Xena.BusinessEntity
{
	public class POI : HotelEntity
	{		
		public string Code { get; set; }
		public Location Coordinates { get; set; }
		public List<POICategory> Categories { get; set; }
	    
	    public POI()
		{
			Categories = new List<POICategory>();
			Coordinates = new Location();
		}
	}
}