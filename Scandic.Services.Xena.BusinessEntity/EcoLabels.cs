﻿using System;
namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable]
    public class EcoLabels
    {
        public bool NordicEcoLabel { get; set; }
        public bool EuEcoLabel { get; set; }
        public bool GreenGlobeLabel { get; set; }

        public bool HasEcoLabel()
        {
            return NordicEcoLabel || EuEcoLabel || GreenGlobeLabel;
        }
    }
}