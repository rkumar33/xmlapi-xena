﻿using System;

namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable]
    public class RewardNight
	{
		public int Points { get; set; }
		public DateTime CampaignStart { get; set; }
		public DateTime CampaignEnd { get; set; }
		public int CampaignPoints { get; set; }
	}
}