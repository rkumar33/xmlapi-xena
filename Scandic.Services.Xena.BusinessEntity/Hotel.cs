﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable]
    public class Hotel : HotelEntity
    {
        public int OperaId { get; set; }
        public int CityId { get; set; }
        public bool IsPublished { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string HotelPageUrl { get; set; }
        public Address Address { get; set; }
        public int? YearBuilt { get; set; }
        public int? NumberOfFloors { get; set; }
        public int? NumberOfBeds { get; set; }
        public int? NumberOfCribs { get; set; }
        public Location Location { get; set; }
        public string FamilyConceptLevel { get; set; }
        public bool PartnerHotel { get; set; }
        public bool OnlineCheckOut { get; set; }
        public RoomCount NumberOfRooms { get; set; }
        public EcoLabels EcoLabels { get; set; }
        //public RefurbishingData Refurbishing { get; set; }
        public CheckinTimes CheckinTimes { get; set; }
        public IEnumerable<string> Microsites { get; set; }
        public string BriefFacts { get; set; }
        //public OpeningHours ReceptionHours { get; set; }
        public int? HotelExteriorImageId { get; set; }
        public SocialMediaLinks SocialMediaLinks { get; set; }
        //public TripAdvisorData TripAdvisorData { get; set; }
        public RewardNight RewardNight { get; set; }
        public string ContentDescriptionShort { get; set; }
        public int? DistanceToCityCentre { get; set; }
        public Hotel()
        {
            // Create empty instances so we don't have to handle null values everywhere
            Address = new Address();
            Location = new Location();
            CheckinTimes = new CheckinTimes();
            SocialMediaLinks = new SocialMediaLinks();
            RewardNight = new RewardNight();
        }
    }
}
