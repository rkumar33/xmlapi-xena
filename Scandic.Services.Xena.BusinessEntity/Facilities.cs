﻿using System;
using Scandic.Services.Xena.BusinessEntity.Enums;

namespace Scandic.Services.Xena.BusinessEntity
{
    public class Facilities
    {
        public PoolFacilities PoolFacilities { get; set; }
        public JacuzziFacility JacuzziFacility { get; set; }
        public GymFacility GymFacility { get; set; }
        public SaunaFacility SaunaFacility { get; set; }
        public RelaxFacility RelaxFacility { get; set; }
        public String Tips { get; set; }
    }

    public class PoolFacilities
    {
        public PoolFacility OutdoorPool { get; set; }
        public PoolFacility IndoorPool { get; set; }
    }

    public class FacilitiesBase
    {
        public virtual bool Available { get; set; }
        public OpeningHoursWithDetails OpeningHoursWithDetails { get; set; }
        public String Tips { get; set; }
        public String ContentDescriptionShort { get; set; }
        public String ContentDescriptionMedium { get; set; }
        public int? FacilityImageId { get; set; }
    }

    public class PoolFacility : FacilitiesBase
    {
        public virtual Month OpenMonthFrom { get; set; }
        public virtual Month OpenMonthTo { get; set; }
        public PoolSize PoolSize { get; set; }
    }

    public class JacuzziFacility : FacilitiesBase
    {
    }

    public class GymFacility : FacilitiesBase
    {
        public virtual bool ExternalGym { get; set; }
        public virtual String NameOfExternalGym { get; set; }
        public virtual int? DistanceToExternalGym { get; set; }
    }

    public class SaunaFacility : FacilitiesBase
    {
        public virtual bool SeparateMenAndWomen { get; set; }
    }

    public class RelaxFacility : FacilitiesBase
    {
    }
}