
namespace Scandic.Services.Xena.BusinessEntity
{
	public class OpeningHours
	{
		public bool AlwaysOpen { get; set; }

		//[RegularExpression(ValidationRegexes.Time, ErrorMessage = ValidationErrorMessages.TimeFormatError)]
		public string OpeningTime { get; set; }

		//[RegularExpression(ValidationRegexes.Time, ErrorMessage = ValidationErrorMessages.TimeFormatError)]
		public string ClosingTime { get; set; }
	}
}