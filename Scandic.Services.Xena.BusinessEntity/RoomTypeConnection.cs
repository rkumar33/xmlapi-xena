﻿
namespace Scandic.Services.Xena.BusinessEntity
{
	public class RoomTypeConnection
	{
		public RoomType RoomType { get; set; }
		
		public int? DisplayOrderInGDS { get; set; }
		//[Required]
		public int? AdditionalExtraBedCount { get; set; }
		//[Required]
		public int? RoomCount { get; set; }
		//[Required]
		public int? RefurbishedRoomCount { get; set; }
		
        public int? RefurbishedYear { get; set; }
		//[Required]
		public int? MaxOccupancy { get; set; }
		//[Required]
		public int? MaxOccupancyAdults { get; set; }
		//[Required]
		public int? MaxOccupancyChildren { get; set; }
		///// <summary>
		///// The room size in m².
		///// </summary>
		public Range RoomSize { get; set; }
		///// <summary>
		///// Width range of the main bed (Entity.MainBedType).
		///// </summary>
		public Range MainBedSize { get; set; }
		///// <summary>
		///// Width range of the fixed extra bed (Entity.FixedExtraBedType).
		///// </summary>
		public Range FixedExtraBedSize { get; set; }

		public string BriefAdditionalBedTypeDescription { get; set; }
	}
}