﻿namespace Scandic.Services.Xena.BusinessEntity
{
	public class HotelMeetingRoomData
	{
		public string Description { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public int? MaxNumberOfPersonsInLargestRoom { get; set; }
	}
}