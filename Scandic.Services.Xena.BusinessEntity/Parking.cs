﻿using Scandic.Services.Xena.BusinessEntity.Enums;

namespace Scandic.Services.Xena.BusinessEntity
{
    public class Parking
    {
        public ParkingData OutdoorParking { get; set; }
        public ParkingData GarageParking { get; set; }
        public string WhereCanYouRecommendGuestsToPark { get; set; }
    }

    public class ParkingData
    {
        public bool Selected { get; set; }
        public int? NumberOfParkingSpots { get; set; }
        public int? NumberOfChargingSpaces { get; set; }
        public int? DistanceToHotel { get; set; }
        public string LocationDescription { get; set; }
        public bool FreeParking { get; set; }
        public bool CanMakeReservation { get; set; }
        public ParkingPayment ParkingPayment { get; set; }
        public double? PriceFrom { get; set; }
        public double? PriceTo { get; set; }
        public Currency Currency { get; set; }
        public ParkingPrice StandardPrices { get; set; }
        public ParkingPrice WeekendPrices { get; set; }

        public string ParkingName { get; set; }
        public string Address { get; set; }
        public string Comments { get; set; }
        public string TelephoneNumber { get; set; }

    }

    public class ParkingPrice
    {
        public PriceInInterval PricePerHour { get; set; }
        public PriceInInterval PricePerDay { get; set; }
        public PriceInInterval PricePerNight { get; set; }
        public double? PricePer24Hours { get; set; }
    }

    public class PriceInInterval
    {
        public double? Price { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}