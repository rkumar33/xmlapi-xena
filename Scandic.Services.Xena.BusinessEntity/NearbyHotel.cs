﻿
namespace Scandic.Services.Xena.BusinessEntity
{
	public class NearbyHotel : HotelEntity
	{
		public string Direction { get; set; }
		public double Distance { get; set; }
		public int DrivingTime { get; set; }
		public int SortSequence { get; set; }
		public Location Location { get; set; }
	}
}