using System.Collections.Generic;

namespace Scandic.Services.Xena.BusinessEntity
{
	public class RoomCategory : HotelEntity
	{
		public IEnumerable<RoomType> AvailableRoomTypes;
	}
}