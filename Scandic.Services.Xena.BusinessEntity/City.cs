﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.Xena.BusinessEntity
{
    public class City : HotelEntity
    {
        public string CityIdentifier { get; set; }

        public string CountryId { get; set; }

        public string DescriptionMedium { get; set; }

        public string DescriptionShort { get; set; }

        public string ImageId { get; set; }

        public Location Location { get; set; }

        public string TimeZoneId { get; set; }

    }
}
