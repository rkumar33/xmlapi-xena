﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.Xena.BusinessEntity
{
    public class Range
    {
        public int Max { get; set; }
        public int Min { get; set; }
    }
}
