﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable, XmlRoot("CountryList")]
    public class Country //: HotelEntity
    {
        public string Id { get; set; }

        public string Name { get; set; }
        
        public string ImageId { get; set; }

        public string DescriptionShort { get; set; }

        public string DescriptionMedium { get; set; }

        public double VAT { get; set; }

        public string Continent { get; set; }

        public string CityID { get; set; }

    }
}
