using Scandic.Services.Xena.BusinessEntity.Enums;
namespace Scandic.Services.Xena.BusinessEntity
{
	public class POICategory
	{
		public string Name { get; set; }
		public POICategoryGroup CategoryGroup { get; set; }
	}
}