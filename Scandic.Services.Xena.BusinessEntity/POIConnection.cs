
namespace Scandic.Services.Xena.BusinessEntity
{
	public class POIConnection
	{
		public string Direction { get; set; }
		public double Distance { get; set; }
		public int DrivingTime { get; set; }
		public POI POI { get; set; }
		public bool IsHighlighted { get; set; }
        
		public POIConnection()
		{
			Direction = "Unknown";
		}
	}
}