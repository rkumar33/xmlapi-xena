﻿using System;
namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable]
    public class SocialMediaLinks
    {
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string GooglePlus { get; set; }
    }
}