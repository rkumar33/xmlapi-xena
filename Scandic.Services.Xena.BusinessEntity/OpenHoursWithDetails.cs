﻿
namespace Scandic.Services.Xena.BusinessEntity
{
	public class OpeningHoursWithDetails
	{
		public OpeningHoursType OpeningHoursType { get; set; }
		public string OpeningHoursDetails { get; set; }

		public OpeningHours OpeningHoursOrdinary { get; set; }

		public OpeningHours OpeningHoursWeekends { get; set; }

		public OpeningHoursWithDetails()
		{
			OpeningHoursType = OpeningHoursType.SetWeekDayAndWeekEndSchema;
			OpeningHoursOrdinary = new OpeningHours() {AlwaysOpen = true};
			OpeningHoursWeekends = new OpeningHours() { AlwaysOpen = true };
			OpeningHoursDetails = string.Empty;
		}
	}

    public enum OpeningHoursType
    {
        SetWeekDayAndWeekEndSchema
    }
}