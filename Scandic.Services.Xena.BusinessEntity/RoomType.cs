﻿using Scandic.Services.Xena.BusinessEntity.Enums;

namespace Scandic.Services.Xena.BusinessEntity
{
	public class RoomType : HotelEntity
	{
		public string ScorpioCode { get; set; }
		public MainBedType MainBedType { get; set; }
		public FixedExtraBedType FixedExtraBedType { get; set; }
	}
}