﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.Xena.BusinessEntity
{
    public class Meeting
    {
        public HotelMeetingRoomData GeneralData { get; set; }
        public List<MeetingRoom> MeetingRooms { get; set; }
    }
}
