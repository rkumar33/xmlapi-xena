﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Scandic.Services.Xena.BusinessEntity
{
    [Serializable, XmlRoot("PartnerList")]
    public class Partner
    {
        public string Name { get; set; }

        public string PartnerProfileCode { get; set; }

        public string IsPartnerEnabled { get; set; }

        public string PromoCodes { get; set; }

        public string ArbCodes { get; set; }

        public string ApiKey { get; set; }

        public string ValidityStartDate { get; set; }

        public string ValidityEndDate { get; set; }

        public string TrackingCode { get; set; }

        public string IsTrackingCodeDisabled { get; set; }

        public string InterfaceList { get; set; }
    }
}