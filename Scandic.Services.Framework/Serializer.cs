﻿// <copyright file="Serializer.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Framework
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Class that holds methods to Serialize and Deserialize
    /// </summary>
    public class Serializer
    {
        private static object syncLock = new object();
        /// <summary>
        /// Serializes an object and stores in a file
        /// </summary>
        /// <typeparam name="T">Type of the object being serialized</typeparam>
        /// <param name="filename">Path of file to store the object</param>
        /// <param name="objectToSerialize">Object to be serialized</param>
        public static void ObjectToFile<T>(string filename, T objectToSerialize)
        {
            lock (syncLock)
            {
                using (Stream stream = File.Open(filename, FileMode.Create))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(stream, objectToSerialize);
                }                 
            }            
        }

        /// <summary>
        /// Deserializes a file content into object
        /// </summary>
        /// <typeparam name="T">Type of the object to be returned</typeparam>
        /// <param name="filename">Path of file to deserialize</param>
        /// <returns>An object of type T</returns>
        public static T FileToObject<T>(string filename)
        {
            T objectFromFile;
            using (Stream stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                objectFromFile = (T)binaryFormatter.Deserialize(stream);
            }
            return objectFromFile;
        }
    }
}
