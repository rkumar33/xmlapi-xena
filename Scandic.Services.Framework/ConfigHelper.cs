﻿// <copyright file="ConfigHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Framework
{
    using System.Configuration;

    /// <summary>
    /// Helper class for getting config entries
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// Gets a configured value
        /// </summary>
        /// <param name="key">Key of the config entry</param>
        /// <returns>Configured value corresponding to the Key</returns>
        public static string GetValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
