﻿// <copyright file="PerformanceHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Framework
{
    using System.Diagnostics;

    /// <summary>
    /// Helper class for performance diagnostics
    /// </summary>
    public class PerformanceHelper
    {
        #region Local Variables & Constants
        /// <summary>
        /// Constant for counter category
        /// </summary>
        private const string OWSCATEGORY = "OWS Performance";

        /// <summary>
        /// Constant for counter category
        /// </summary>
        private const string CMSCATEGORY = "CMS Performance";

        /// <summary>
        /// Constant for number of calls counter
        /// </summary>
        private const string NUMCALLS = "Number of Calls";

        /// <summary>
        /// Constant for rate of calls counter
        /// </summary>
        private const string RATE = "Rate of Calls/Second";

        /// <summary>
        /// Constant for average time counter
        /// </summary>
        private const string AVERAGE = "Average Time per Call";

        /// <summary>
        /// Constant for base counter
        /// </summary>
        private const string BASECOUNTER = "Base Counter";

        /// <summary>
        /// Private variable for number of calls counter
        /// </summary>
        private static PerformanceCounter totalOwsCalls;

        /// <summary>
        /// Private variable for rate of calls counter
        /// </summary>
        private static PerformanceCounter perSecondOwsCalls;

        /// <summary>
        /// Private variable for average time counter
        /// </summary>
        private static PerformanceCounter averageOwsTime;

        /// <summary>
        /// Private variable for base counter
        /// </summary>
        private static PerformanceCounter baseOwsCounter;

        /// <summary>
        /// Private variable for number of calls counter
        /// </summary>
        private static PerformanceCounter totalCmsCalls;

        /// <summary>
        /// Private variable for rate of calls counter
        /// </summary>
        private static PerformanceCounter perSecondCmsCalls;

        /// <summary>
        /// Private variable for average time counter
        /// </summary>
        private static PerformanceCounter averageCmsTime;

        /// <summary>
        /// Private variable for base counter
        /// </summary>
        private static PerformanceCounter baseCmsCounter;

        /// <summary>
        /// Gets a value indicating whether
        /// Opera related counters and diagnostics are enable or disabled
        /// </summary>
        public static bool Enabled
        {
            get
            {
                return bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableDiagnostics));
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the number of calls counter
        /// </summary>
        private static PerformanceCounter TotalOwsCalls
        {
            get
            {
                if (totalOwsCalls == null)
                {
                    totalOwsCalls = new PerformanceCounter
                    {
                        CategoryName = OWSCATEGORY,
                        CounterName = NUMCALLS,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return totalOwsCalls;
            }
        }

        /// <summary>
        /// Gets the rate of calls counter
        /// </summary>
        private static PerformanceCounter PerSecondOwsCalls
        {
            get
            {
                if (perSecondOwsCalls == null)
                {
                    perSecondOwsCalls = new PerformanceCounter
                    {
                        CategoryName = OWSCATEGORY,
                        CounterName = RATE,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return perSecondOwsCalls;
            }
        }

        /// <summary>
        /// Gets the average time counter
        /// </summary>
        private static PerformanceCounter AverageOwsTime
        {
            get
            {
                if (averageOwsTime == null)
                {
                    averageOwsTime = new PerformanceCounter
                    {
                        CategoryName = OWSCATEGORY,
                        CounterName = AVERAGE,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return averageOwsTime;
            }
        }

        /// <summary>
        /// Gets the base counter
        /// </summary>
        private static PerformanceCounter BaseOwsCounter
        {
            get
            {
                if (baseOwsCounter == null)
                {
                    baseOwsCounter = new PerformanceCounter
                    {
                        CategoryName = OWSCATEGORY,
                        CounterName = BASECOUNTER,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return baseOwsCounter;
            }
        }

        /// <summary>
        /// Gets the number of calls counter
        /// </summary>
        private static PerformanceCounter TotalCmsCalls
        {
            get
            {
                if (totalCmsCalls == null)
                {
                    totalCmsCalls = new PerformanceCounter
                    {
                        CategoryName = CMSCATEGORY,
                        CounterName = NUMCALLS,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return totalCmsCalls;
            }
        }

        /// <summary>
        /// Gets the rate of calls counter
        /// </summary>
        private static PerformanceCounter PerSecondCmsCalls
        {
            get
            {
                if (perSecondCmsCalls == null)
                {
                    perSecondCmsCalls = new PerformanceCounter
                    {
                        CategoryName = CMSCATEGORY,
                        CounterName = RATE,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return perSecondCmsCalls;
            }
        }

        /// <summary>
        /// Gets the average time counter
        /// </summary>
        private static PerformanceCounter AverageCmsTime
        {
            get
            {
                if (averageCmsTime == null)
                {
                    averageCmsTime = new PerformanceCounter
                    {
                        CategoryName = CMSCATEGORY,
                        CounterName = AVERAGE,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return averageCmsTime;
            }
        }

        /// <summary>
        /// Gets the base counter
        /// </summary>
        private static PerformanceCounter BaseCmsCounter
        {
            get
            {
                if (baseCmsCounter == null)
                {
                    baseCmsCounter = new PerformanceCounter
                    {
                        CategoryName = CMSCATEGORY,
                        CounterName = BASECOUNTER,
                        ReadOnly = false,
                        MachineName = "."
                    };
                }

                return baseCmsCounter;
            }
        }
        #endregion

        /// <summary>
        /// Registers one availability call by sending the elapsed ticks
        /// </summary>       
        public static void RegisterAvailabilityCall()
        {
            if (Enabled)
            {
                TotalOwsCalls.Increment();
                PerSecondOwsCalls.Increment();
            }
        }

        /// <summary>
        /// Registers one availability call by sending the elapsed ticks
        /// </summary>
        /// <param name="elapsedTicks">Ticks elapsed making this availability call</param>       
        public static void RegisterAvailabilityBurst(long elapsedTicks)
        {
            if (Enabled)
            {
                AverageOwsTime.IncrementBy(elapsedTicks);
                BaseOwsCounter.Increment();
            }
        }

        /// <summary>
        /// Registers one content call by sending the elapsed ticks
        /// </summary>     
        public static void RegisterContentCall()
        {
            if (Enabled)
            {
                TotalCmsCalls.Increment();
                PerSecondCmsCalls.Increment();
            }
        }

        /// <summary>
        /// Registers one content call by sending the elapsed ticks
        /// </summary>
        /// <param name="elapsedTicks">Ticks elapsed making this content call</param>      
        public static void RegisterContentBurst(long elapsedTicks)
        {
            if (Enabled)
            {
                AverageCmsTime.IncrementBy(elapsedTicks);
                BaseCmsCounter.Increment();
            }
        }
    }
}
