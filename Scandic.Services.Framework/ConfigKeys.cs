﻿// <copyright file="ConfigKeys.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Framework
{
    /// <summary>
    /// Keys for configuration entries in the
    /// application configuration file
    /// </summary>
    public struct ConfigKeys
    {
        #region General
        /// <summary>
        /// Key for default language setting
        /// </summary>
        public const string DefaultLanguage = "DefaultLanguage";

        /// <summary>
        /// Key for default language setting
        /// </summary>
        public const string ValidLanguages = "ValidLanguages";

        /// <summary>
        /// Key for Valid Russian Domain setting
        /// </summary>
        public const string ValidLanguageForRussianDomain = "ValidLanguageForRussianDomain";

        /// <summary>
        /// Key for default language setting
        /// </summary>
        public const string LanguageDomains = "LanguageDomains";

        /// <summary>
        /// Key for the provider name setting
        /// </summary>
        public const string Provider = "ProviderName";

        /// <summary>
        /// If multiple threads should be enabled
        /// for back-end service calls
        /// </summary>
        public const string EnableAvailabilityThreads = "EnableAvailabilityThreads";

        /// <summary>
        /// If multiple threads should be enabled
        /// for back-end service calls
        /// </summary>
        public const string EnableContentThreads = "EnableContentThreads";

        /// <summary>
        /// Log messages to console, if set to true
        /// </summary>
        public const string LogToConsole = "LogToConsole";

        /// <summary>
        /// Enable Opera related counters and diagnostics
        /// </summary>
        public const string EnableDiagnostics = "EnableDiagnostics";

        /// <summary>
        /// Logs Hotel availability Details used to compute Lowest rates, if set to true
        /// </summary>
        public const string LogLowestRate = "LogLowestRate";

        /// <summary>
        /// App path
        /// </summary>
        public const string AppPath = "AppPath";

        #endregion

        #region Service
        /// <summary>
        ///  DeepLink Domain Name
        /// </summary>
        public const string DebugMode = "DebugMode";

        /// <summary>
        /// Indicates if API Key validation is on or off.
        /// </summary>
        public const string KeyValidation = "EnableKeyValidation";

        /// <summary>
        /// Indicates if API analytics tracking is on or off
        /// </summary>
        public const string Tracking = "EnableTracking";

        /// <summary>
        /// Maximum allowed days to book in advance
        /// </summary>
        public const string AllowedDaysForDateOfArrival = "AllowedDaysForDateOfArrival";

        /// <summary>
        /// hotels to exclude in XML API
        /// </summary>
        public const string HotelsToExclude = "HotelsToExclude";
        #endregion

        #region Cache
        /// <summary>
        /// Key for the setting to turn the cache on or off
        /// </summary>
        public const string IsCached = "IsCached";

        /// <summary>
        /// Key for daily expiry time
        /// </summary>
        public const string DailyExpiry = "DailyExpiry";

        /// <summary>
        /// Key for weekly expiry time
        /// </summary>
        public const string WeeklyExpiry = "WeeklyExpiry";

        /// <summary>
        /// Key specifying if the cache should be pre-loaded
        /// </summary>
        public const string PreloadCache = "PreloadCache";

        /// <summary>
        /// key for sheduling time for Ows monitor.
        /// </summary>
        public const string OwsSheduledRefresh = "OwsSheduledRefresh";

        /// <summary>
        /// Timestamp file for objects in cache that gets refreshed more frequently
        /// </summary>
        public const string TimeStampFileFrequent = "TimeStampFileFrequent";

        /// <summary>
        /// Timestamp file for objects in cache that gets refreshed less frequently
        /// </summary>
        public const string TimeStampFileRegular = "TimeStampFileRegular";

        #endregion

        #region OWS
        /// <summary>
        /// Key for the ows chain code.
        /// </summary>
        public const string OwSChainCode = "OwS.Chain.Code";

        /// <summary>
        /// Key for the Ows Origin Entity Id
        /// </summary>
        public const string OwsOriginEntityId = "Ows.Origin.EntityId";

        /// <summary>
        /// Key for the Ows Origin system type
        /// </summary>
        public const string OwsOriginSystemType = "Ows.Origin.SystemType";

        /// <summary>
        ///  Key for the Ows destination entity type
        /// </summary>
        public const string OwsDesitinationEntityId = "Ows.Desitination.EntityId";

        /// <summary>
        /// Key for the Ows Desitination System Type
        /// </summary>
        public const string OwSDesitinationSystemType = "OwS.Desitination.SystemType";

        /// <summary>
        /// Key for Session Desitination System Type 
        /// </summary>
        public const string OwsSessionDesitinationSystemType = "Ows.Session.Desitination.SystemType";

        /// <summary>
        /// Key for Ows Default Source Code
        /// </summary>
        public const string OwsDefaultSourceCode = "Ows.Default.SourceCode";

        /// <summary>
        /// Ows Session Origin System Type
        /// </summary>
        public const string OwsSessionOriginSystemType = "Ows.Session.Origin.SystemType";

        /// <summary>
        /// Ows Session Intermediaries System Type1
        /// </summary>
        public const string OwsSessionIntermediariesSystemType1 = "Ows.Session.Intermediaries.SystemType1";

        /// <summary>
        /// Ows Session Intermediaries System Type2
        /// </summary>
        public const string OwsSessionIntermediariesSystemType2 = "Ows.Session.Intermediaries.SystemType2";

        /// <summary>
        /// Ows Availability Service
        /// </summary>
        public const string OwsAvailabilityService = "Ows.Availability.Service";

        /// <summary>
        /// Default service timeout for OWS requests
        /// </summary>
        public const string OwsServiceTimeOut = "OWSServiceTimeOut";

        /// <summary>
        /// Time out for all Rooms and rate's Availability OWS calls.
        /// </summary>
        public const string OWSRoomsRateServiceTimeOut = "OWSRoomsRateServiceTimeOut";

        /// <summary>
        /// Time out for all for certain hotels Availability OWS calls.
        /// </summary>
        public const string OWSForCertainHotelsServiceTimeOut = "OWSForCertainHotelsServiceTimeOut";

        /// <summary>
        /// Time out for all loweset rate Availability OWS calls.
        /// </summary>
        public const string OWSLowestRateRateServiceTimeOut = "OWSLowestRateRateServiceTimeOut";

        /// <summary>
        /// Time out for all for city Availability OWS calls.
        /// </summary>
        public const string OWSForCityServiceTimeOut = "OWSForCityServiceTimeOut";

        /// <summary>
        /// Throw Exception On ServiceTimeOut.
        /// </summary>
        public const string ThrowExceptionOnServiceTimeOut = "ThrowExceptionOnServiceTimeOut";

        /// <summary>
        /// Message to be displayed On ServiceTimeOut.
        /// </summary>
        public const string ServiceTimeoutMessage = "ServiceTimeOutMessage";

        /// <summary>
        /// Indicates the number of ougoing asynchronous OWS requests
        /// </summary>
        public const string OWSAsyncRequestGroupCount = "OWSAsyncRequestGroupCount";

        #endregion

        #region Lowest Rate
        /// <summary>
        ///  The Lead time
        /// </summary>
        public const string LeadTime = "LeadTime";

        /// <summary>
        /// The Start Day
        /// </summary>
        public const string StartDay = "StartDay";

        /// <summary>
        /// Number of Days
        /// </summary>
        public const string NoOfDays = "NoOfDays";

        /// <summary>
        /// No Of Adults
        /// </summary>
        public const string NoOfAdults = "NoOfAdults";

        /// <summary>
        /// Number of Rooms
        /// </summary>
        public const string NoOfRooms = "NoOfRooms";

        /// <summary>
        /// No Of Children
        /// </summary>
        public const string NoOfChildren = "NoOfChildren";

        /// <summary>
        /// No Of Nights
        /// </summary>
        public const string NoOfNights = "NoOfNights";
        #endregion

        #region CMS
        /// <summary>
        /// Base URL for ImageVault images
        /// </summary>
        public const string ImageBaseUri = "ImageBaseUri";

        /// <summary>
        /// Page reference ID of the Room category Container in CMS
        /// </summary>
        public const string UseOfflineContent = "UseOfflineContent";

        /// <summary>
        /// Page reference ID of the Room category Container in CMS
        /// </summary>
        public const string RoomContainerID = "RoomContainerPageReferenceID";

        /// <summary>
        /// Page reference ID of the Rate category Container in CMS
        /// </summary>
        public const string RateContainerID = "RateTypeContainerPageReferenceID";

        /// <summary>
        /// Page Type Id of Room Category 
        /// </summary>
        public const string RoomCategoryPageTypeID = "RoomCategoryPageTypeID";

        /// <summary>
        /// Page Type Id of Room Type
        /// </summary>
        public const string RoomTypePageTypeID = "RoomTypePageTypeID";

        /// <summary>
        /// Page Type Id of Rate Category
        /// </summary>
        public const string RateCategoryPageTypeID = "RateCategoryPageTypeID";

        /// <summary>
        /// Page Type Id of Rate Type
        /// </summary>
        public const string RateTypePageTypeID = "RateTypePageTypeID";

        /// <summary>
        /// PageReferenceID for the Root Page
        /// </summary>
        public const string RootPageReferenceID = "RootPageReferenceID";

        /// <summary>
        /// PageReferenceID for the blockContainerPage
        /// </summary>
        public const string BlockContainerPageReferenceID = "blockContainerPageReferenceID";

        /// <summary>
        /// Page Type Id for the BlockRate
        /// </summary>
        public const string BlockRatePageTypeID = "blockRatePageTypeID";

        /// <summary>
        /// PageReferenceID for the Country Container Page
        /// </summary>
        public const string CountryContainerPageReferenceID = "CountryContainerPageReferenceID";

        /// <summary>
        /// Page Type Id of CountryPage
        /// </summary>
        public const string CountryPageTypeID = "CountryPageTypeID";

        /// <summary>
        /// Page Type Id of City Page
        /// </summary>
        public const string CityPageTypeID = "CityPageTypeID";

        /// <summary>
        /// Page Type Id of Hotel Page
        /// </summary>
        public const string HotelPageTypeID = "HotelPageTypeID";

        /// <summary>
        /// Page Type Id of HotelRoomDescription Page
        /// </summary>
        public const string HotelRoomDescriptionPageTypeID = "HotelRoomDescriptionPageTypeID";

        /// <summary>
        /// Page Type Id of Alternative Hotel Page
        /// </summary>
        public const string AlternativeHotelPageTypeID = "AlternativeHotelPageTypeID";

        /// <summary>
        /// CmsPageStoreService Url 
        /// </summary>
        public const string CmsPageStoreServiceUrl = "CmsPageStoreServiceUrl";

        /// <summary>
        ///  Cms UserName
        /// </summary>
        public const string CmsUserName = "CmsUserName";

        /// <summary>
        /// Cms Password 
        /// </summary>
        public const string CmsPassword = "CmsPassword";

        /// <summary>
        /// ImageVault ImageUrlTemplate
        /// </summary>
        public const string ImageUrlTemplate = "ImageUrlTemplate";

        /// <summary>
        /// PrepaidGuaranteeTypes
        /// </summary>
        public const string PrepaidGuaranteeTypes = "PrepaidGuaranteeTypes";

        /// <summary>
        /// Prepaid PaymentType
        /// </summary>
        public const string PrepaidPayment = "PrepaidPaymentType";

        /// <summary>
        /// Postpaid PaymentType
        /// </summary>
        public const string PostpaidPayment = "PostpaidPaymentType";

        #endregion

        #region Partner
        /// <summary>
        /// Page Type Id of PartnerCategoryPageTypeId
        /// </summary>
        public const string PartnerContainerId = "PartnerContainerPageReferenceID";

        /// <summary>
        /// Page Type Id of partnerTypePageTypeId
        /// </summary> 
        public const string PartnerTypePageTypeId = "PartnerTypePageTypeId";

        /// <summary>
        /// Page Type Id of PartnerContainerID
        /// </summary>
        public const string PartnerPageTypeID = "PartnerPageTypeID";

        #endregion partner

        #region DeepLink

        /// <summary>
        /// Find A Hotel Page Id
        /// </summary>
        public const string FindHotelLink = "FindHotelLink";

        /// <summary>
        /// Link template for Select Rate page
        /// </summary>
        public const string SelectRateLink = "SelectRateLink";

        /// <summary>
        /// Link template for Select Rate page
        /// </summary>
        public const string SelectHotelLink = "SelectHotelLink";

        /// <summary>
        /// Link template for Select Rate page
        /// </summary>
        public const string HotelLandingLink = "HotelLandingLink";

        /// <summary>
        /// Link template for Lowest rate hotel landing page.
        /// </summary>
        public const string LowestRateHotelLandingLink = "LowestRateHotelLandingLink";

        /// <summary>
        /// Link template for Lowest rate find a hotel page.
        /// </summary>
        public const string LowestRateFindHotelLink = "LowestRateFindHotelLink";

        #endregion


        #region siteCatalyst
        /// <summary>
        /// Report Suite Id Test Env
        /// </summary>
        public const string ReportSuiteId = "ReportSuiteId";

        /// <summary>
        /// Url To Site Catalyst
        /// </summary>
        public const string UrlToSiteCatalyst = "UrlToSiteCatalyst";
        #endregion

        #region Ows Monitor

        /// <summary>
        /// Hotel Id For OwsMonitor
        /// </summary>
        public const string HotelIdForOwsMonitor = "HotelIdForOwsMonitor";

        /// <summary>
        /// API Service Visibility
        /// </summary>
        public const string ApiServiceVisibility = "ApiServiceVisibility";

        #endregion


        #region Xena Deeplink

        public const string XenaFindHotelLink = "XenaFindHotelLink";

        /// <summary>
        /// Link template for Select Rate page
        /// </summary>
        public const string XenaSelectRateLink = "XenaSelectRateLink";

        /// <summary>
        /// Link template for Select Rate page
        /// </summary>
        public const string XenaSelectHotelLink = "XenaSelectHotelLink";

        /// <summary>
        /// Link template for Select Rate page
        /// </summary>
        public const string XenaHotelLandingLink = "XenaHotelLandingLink";

        /// <summary>
        /// Link template for Lowest rate hotel landing page.
        /// </summary>
        public const string XenaLowestRateHotelLandingLink = "XenaLowestRateHotelLandingLink";

        /// <summary>
        /// Link template for Lowest rate find a hotel page.
        /// </summary>
        public const string XenaLowestRateFindHotelLink = "XenaLowestRateFindHotelLink";

        /// <summary>
        /// Link template for EnterDetails
        /// </summary>
        public const string XenaEnterDetailsLink = "XenaEnterDetailsLink";

        #endregion


        # region Xena
        public const string XenaAPIBaseURL = "XenaAPIBaseURL";
        public const string XenaAPIHotels = "XenaAPIHotels";
        public const string XenaAPICities = "XenaAPICities";
        public const string XenaAPICountries = "XenaAPICountries";
        public const string XenaAPIRates = "XenaAPIRates";
        public const string XenaContinent = "XenaContinent";
        public const string XenaOperator = "XenaOperator";
        public const string XenaCountryXML = "XenaCountryXML";
        public const string XenaPartnerXML = "XenaPartnerXML";
        public const string XenaFacilities = "XenaFacilities";
        public const string XenaRestaurantBar = "XenaRestaurantBar";
        public const string XenaParking = "XenaParking";
        public const string XenaMeetingFacility = "XenaMeetingFacility";
        public const string XenaPOIConnection = "XenaPOIConnection";
        public const string XenaGDSFacilities = "XenaGDSFacilities";
        public const string XenaRoomCategories = "XenaRoomCategories";
        public const string XenaNearByHotels = "XenaNearByHotels";
        public const string XenaImageVaultAuthTokenURL = "XenaImageVaultAuthTokenURL";
        public const string XenaImageVaultMediaServiceURL = "XenaImageVaultMediaServiceURL";
        public const string XenaImageVaultMediaFormatType = "XenaImageVaultMediaFormatType";
        public const string XenaImageVaultMediaFormatEffectType = "XenaImageVaultMediaFormatEffectType";
        public const string XenaImageVaultMediaFormatHeight = "XenaImageVaultMediaFormatHeight";
        public const string XenaImageVaultMediaFormatWidth = "XenaImageVaultMediaFormatWidth";
        public const string XenaImageVaultPublishIdentifier = "XenaImageVaultPublishIdentifier";
        public const string XenaImageVaultUserName = "XenaImageVaultUserName";
        public const string XenaImageVaultPassword = "XenaImageVaultPassword";
        public const string XenaBreakfastNotIncludedHotels = "XenaBreakfastNotIncludedHotels";
        public const string ReadCountriesFromXML = "ReadCountriesFromXML";
        #endregion
    }
}
