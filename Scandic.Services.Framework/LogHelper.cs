﻿// <copyright file="LogHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Framework
{
    #region References

    using System;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using System.ServiceModel.Web;
   
    #endregion  

    #region Enum
    /// <summary>
    /// Represents the exception categories
    /// </summary>
    public enum LogCategory
    {
        /// <summary>
        /// This category represents the Opera OWS errors
        /// </summary>
        Availability,

        /// <summary>
        /// This category represents the CMS errors
        /// </summary>
        Content,

        /// <summary>
        /// This category represents the errors occured within the application
        /// </summary>
        Service,

        /// <summary>
        /// This category represents the hotel availability obtained to calculate lowest rates
        /// </summary>
        LowestRate,

        /// <summary>
        /// 
        /// </summary>
        XMLAPIForCertainHotels,

        /// <summary>
        /// This category represents the availability for Certain Hotels, used to determine the session timeout for OWS calls from this API
        /// </summary>
        OWSForCertainHotels,

        /// <summary>
        /// This category represents the hotel availability obtained to calculate lowest rates
        /// </summary>
        XMLAPIForCity,

        /// <summary>
        /// This category represents the availability for a City, used to determine the session timeout for OWS calls from this API
        /// </summary>
        OWSForCity,

        /// <summary>
        /// This category represents the hotel availability obtained to calculate lowest rates
        /// </summary>
        XMLAPIRoomsRate,

        /// <summary>
        /// This category represents the Rooms and Rates, used to determine the session timeout for OWS calls from this API
        /// </summary>
        OWSRoomsRate,

        /// <summary>
        /// This category represents the hotel availability obtained to calculate lowest rates
        /// </summary>
        XMLAPILowestRate,

        /// <summary>
        /// This category represents the availability for Lowest Rates, used to determine the session timeout for OWS calls from this API
        /// </summary>
        OWSLowestRate,

        /// <summary>
        /// This category represents the content downloader
        /// </summary>
        ContentDownloader
    }
    #endregion

    /// <summary>
    /// Helper class to provide easy to use
    /// static methods to log events and exceptions
    /// </summary>
    public static class LogHelper
    {
        #region Exposed Methods
        /// <summary>
        /// Logs an exception, to standard log output
        /// </summary>
        /// <param name="ex">The exception to be logged</param>
        /// <param name="category">The category to which the exception belongs</param>
        public static void LogException(Exception ex, LogCategory category)
        {
            if (ex != null && !string.Equals(ex.Message, "No Rooms and rates matched your query", StringComparison.InvariantCultureIgnoreCase))
            {
                if (WebOperationContext.Current != null && WebOperationContext.Current.IncomingRequest != null && WebOperationContext.Current.IncomingRequest.UriTemplateMatch != null)
                    LogMessage(string.Format("Message: {0}\n RequestedUrl : {1}\nStack Trace:\n{2}", ex.Message, WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri, ex.StackTrace), TraceEventType.Critical, category);
                else
                    LogMessage(string.Format("Message: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace), TraceEventType.Critical, category);
            }
        }

        /// <summary>
        /// Logs an error message, to standard log output
        /// </summary>
        /// <param name="message">The message to be logged</param>
        /// <param name="category">The category to which the error belongs</param>
        public static void LogError(string message, LogCategory category)
        {
            LogMessage(message, TraceEventType.Error, category);
        }

        /// <summary>
        /// Logs a warning message, to standard log output
        /// </summary>
        /// <param name="message">The message to be logged</param>
        /// <param name="category">The category to which the warning belongs</param>
        public static void LogWarning(string message, LogCategory category)
        {
            LogMessage(message, TraceEventType.Warning, category);
        }

        /// <summary>
        /// Logs an informational message, to standard log output
        /// </summary>
        /// <param name="message">The message to be logged</param>
        /// <param name="category">The category to which the info belongs</param>
        public static void LogInfo(string message, LogCategory category)
        {
            LogMessage(message, TraceEventType.Information, category);
        }
        #endregion

        #region Private Helpers
        /// <summary>
        /// Logs a message of specified severity, to standard log output
        /// </summary>
        /// <param name="message">Message to be logged</param>
        /// <param name="type">Severity level of the message</param>
        /// <param name="category">The category to which the exception belongs</param>
        private static void LogMessage(string message, TraceEventType type, LogCategory category)
        {
            LogEntry log = new LogEntry();
            log.Message = System.Security.SecurityElement.Escape(message);
            log.Categories.Add(category.ToString());
            log.Severity = type;
            StackFrame sf = new StackFrame(2);
            if(sf != null)
            {
                log.Title = System.Security.SecurityElement.Escape(sf.GetMethod() != null ? sf.GetMethod().Name : string.Empty);
            }
            Logger.Write(log);
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.LogToConsole)))
            {
                Console.WriteLine(message);
            }
        }
        #endregion
    }
}
