﻿// <copyright file="IContentService.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Operation
{
    #region References
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using Scandic.Services.Contracts.Data;
    #endregion

    /// <summary>
    /// All content (CMS) related operations come under this service
    /// </summary>
    [ServiceContract(
        Name = "IContentService",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public interface IContentService
    {
        /// <summary>
        /// Get a list of countries where Scandic has operations
        /// </summary>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>List of countries where Scandic has operations</returns>
        [OperationContract]
        [WebGet(
            UriTemplate = "/countries?language={language}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Data.ServiceFault))]
        CountryList GetCountryList(string language);

        /// <summary>
        /// Get a list of cities where Scandic has operations,
        /// in the specified country
        /// </summary>
        /// <param name="country">ID of Country in which cities are to be listed</param>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>Collection of cities</returns>
        [OperationContract]
        [WebGet(
            UriTemplate = "/cities?language={language}&country={country}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Data.ServiceFault))]
        CityList GetCityList(string country, string language);

        /// <summary>
        /// Searches for Scandic hotels in a city
        /// </summary>
        /// <param name="city">Name of the city in which hotels are to be searched</param>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>List of hotels matching the search</returns>
        [OperationContract]
        [WebGet(
            UriTemplate = "/hotels?language={language}&city={city}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Data.ServiceFault))]
        HotelList GetHotelList(string city, string language);

        /// <summary>
        /// Gets detailed information on a specified hotel
        /// </summary>
        /// <param name="hotel">ID of the hotel for which details are required</param>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>Detailed information on the speified hotel</returns>
        [OperationContract]
        [WebGet(
            UriTemplate = "/hotel?language={language}&id={hotel}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Data.ServiceFault))]
        HotelInformation GetHotelInformation(string hotel, string language);
    }
}
