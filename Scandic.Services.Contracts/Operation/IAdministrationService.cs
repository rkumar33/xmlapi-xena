﻿// <copyright file="IAdministrationService.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Operation
{
    #region References
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using Scandic.Services.Contracts.Data;
    #endregion

    /// <summary>
    /// All administration related operations come under this service
    /// </summary>
    [ServiceContract(
        Name = "IAdministrationService",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public interface IAdministrationService
    {
        /// <summary>
        /// Gets the partner details.
        /// </summary>
        /// <returns> returns partner details </returns>        
        [OperationContract]
        [WebGet(
            UriTemplate = "/validate",
            ResponseFormat = WebMessageFormat.Xml)]
        [FaultContract(typeof(Data.ServiceFault))]
        PartnerDetails GetPartnerDetails();

        /// <summary>
        /// Clears an item from cache
        /// </summary>
        /// <param name="key">Key of the item to be cleared</param>
        /// <returns> Clears an item from cache, key=ALL flushes </returns>        
        [OperationContract]
        [WebGet(
            UriTemplate = "/resetcache?key={key}",
            ResponseFormat = WebMessageFormat.Xml)]
        [FaultContract(typeof(Data.ServiceFault))]
        string ResetCache(string key);
    }
}
