﻿// <copyright file="IAvailabilityService.cs" company="Scandic Hotels">
//  Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Operation
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using Scandic.Services.Contracts.Data;

    /// <summary>
    /// All availability related operations come under this service
    /// </summary>
    [ServiceContract(Name = "IAvailabilityService", Namespace = @"http://api.scandichotels.com/schemas") ]
    public interface IAvailabilityService
    {
        /// <summary>
        /// Availabilities for Cities
        /// </summary>
        /// <param name="city">The City Id</param>
        /// <param name="arrivalDate">The arrival date</param>
        /// <param name="arrivalMonth">The arrival Month</param>
        /// <param name="arrivalYear">The arrival Year</param>
        /// <param name="departureDate">The departure Date</param>
        /// <param name="departureMonth">The departure Month</param>
        /// <param name="departureYear">The departure Year</param>
        /// <param name="adultNumber1">The Adult Count in Room1</param>
        /// <param name="adultNumber2">The Adult Count in Room2</param>
        /// <param name="adultNumber3">The Adult Count in Room3</param>
        /// <param name="adultNumber4">The Adult Count in Room4</param>
        /// <param name="childNumber1">The Child Count in Room1</param>
        /// <param name="childNumber2">The Child Count in Room2</param>
        /// <param name="childNumber3">The Child Count in Room3</param>
        /// <param name="childNumber4">The Child Count in Room4</param>
        /// <param name="language">The language</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified city
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "/ForCity?city={city}&AD={arrivalDate}&AM={arrivalMonth}&AY={arrivalYear}&DD={departureDate}&DM={DepartureMonth}&DY={departureYear}&AN1={adultNumber1}&AN2={adultNumber2}&AN3={adultNumber3}&AN4={adultNumber4}&CN1={childNumber1}&CN2={childNumber2}&CN3={childNumber3}&CN4={childNumber4}&language={language}&bookingCode={bookingCode}&id={id}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        AvailabilityForCity AvailabilityForCity(string city, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id);

        /// <summary>
        /// Availabilities for certain hotels.
        /// </summary>
        /// <param name="hotels">The hotel id</param>
        /// <param name="arrivalDate">The arrival date</param>
        /// <param name="arrivalMonth">The arrival month</param>
        /// <param name="arrivalYear">The arrival year</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="departureMonth">The departure month</param>
        /// <param name="departureYear">The departure year</param>
        /// <param name="adultNumber1">The Adult Count in Room1</param>
        /// <param name="adultNumber2">The Adult Count in Room2</param>
        /// <param name="adultNumber3">The Adult Count in Room3</param>
        /// <param name="adultNumber4">The Adult Count in Room4</param>
        /// <param name="childNumber1">The Child Count in Room1</param>
        /// <param name="childNumber2">The Child Count in Room2</param>
        /// <param name="childNumber3">The Child Count in Room3</param>
        /// <param name="childNumber4">The Child Count in Room4</param>
        /// <param name="language">The language</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "/ForCertainHotels?hotels={hotels}&AD={arrivalDate}&AM={arrivalMonth}&AY={arrivalYear}&DD={departureDate}&DM={DepartureMonth}&DY={departureYear}&AN1={adultNumber1}&AN2={adultNumber2}&AN3={adultNumber3}&AN4={adultNumber4}&CN1={childNumber1}&CN2={childNumber2}&CN3={childNumber3}&CN4={childNumber4}&language={language}&bookingCode={bookingCode}&id={id}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        AvailabilityForCertainHotels AvailabilityForCertainHotels(string hotels, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id);

        /// <summary>
        /// Lowests the rates for city.
        /// </summary>
        /// <param name="city">The city id.</param>
        /// <param name="language">The language.</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "/LowestRatesForCity?city={city}&language={language}&id={id}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        LowestRatesForCity LowestRatesForCity(string city, string language, string id);

        /// <summary>
        /// Rooms and Rates available in a hotel.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="arrivalDate">The arrival date</param>
        /// <param name="arrivalMonth">The arrival month</param>
        /// <param name="arrivalYear">The arrival year</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="departureMonth">The departure month</param>
        /// <param name="departureYear">The departure year</param>
        /// <param name="adultNumber1">The Adult Count in Room1</param>
        /// <param name="adultNumber2">The Adult Count in Room2</param>
        /// <param name="adultNumber3">The Adult Count in Room3</param>
        /// <param name="adultNumber4">The Adult Count in Room4</param>
        /// <param name="childNumber1">The Child Count in Room1</param>
        /// <param name="childNumber2">The Child Count in Room2</param>
        /// <param name="childNumber3">The Child Count in Room3</param>
        /// <param name="childNumber4">The Child Count in Room4</param>
        /// <param name="language">The language</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging </param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationContract]
        [WebGet(
            UriTemplate = "/ForOneHotel?hotel={hotel}&AD={arrivalDate}&AM={arrivalMonth}&AY={arrivalYear}&DD={departureDate}&DM={DepartureMonth}&DY={departureYear}&AN1={adultNumber1}&AN2={adultNumber2}&AN3={adultNumber3}&AN4={adultNumber4}&CN1={childNumber1}&CN2={childNumber2}&CN3={childNumber3}&CN4={childNumber4}&language={language}&bookingCode={bookingCode}&id={id}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Data.ServiceFault))]
        RoomsAndRates GetRoomAndRates(string hotel, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id);

        /// <summary>
        /// Rooms and Rates available in a hotel.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="arrivalDate">The arrival date</param>
        /// <param name="arrivalMonth">The arrival month</param>
        /// <param name="arrivalYear">The arrival year</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="departureMonth">The departure month</param>
        /// <param name="departureYear">The departure year</param>
        /// <param name="adultNumber1">The Adult Count in Room1</param>
        /// <param name="adultNumber2">The Adult Count in Room2</param>
        /// <param name="adultNumber3">The Adult Count in Room3</param>
        /// <param name="adultNumber4">The Adult Count in Room4</param>
        /// <param name="childNumber1">The Child Count in Room1</param>
        /// <param name="childNumber2">The Child Count in Room2</param>
        /// <param name="childNumber3">The Child Count in Room3</param>
        /// <param name="childNumber4">The Child Count in Room4</param>
        /// <param name="language">The language</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging </param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationContract]
        [WebGet(
            UriTemplate = "/AvailabilityForHotels?hotels={hotels}&AD={arrivalDate}&AM={arrivalMonth}&AY={arrivalYear}&DD={departureDate}&DM={DepartureMonth}&DY={departureYear}&AN1={adultNumber1}&AN2={adultNumber2}&AN3={adultNumber3}&AN4={adultNumber4}&CN1={childNumber1}&CN2={childNumber2}&CN3={childNumber3}&CN4={childNumber4}&language={language}&bookingCode={bookingCode}&id={id}",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Data.ServiceFault))]
        AvailabilitiesForRoomsAndRates GetMultipleRoomsAndRates(string hotels, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id);

    }
}