﻿// <copyright file="RoomImage.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// This class has fields to hold the Images of the hotel
    /// </summary>   
    [DataContract(
        Name = "Image",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class RoomImage
    {
        /// <summary>           
        /// Gets or sets the Latitude 
        /// </summary>  
        [DataMember(
            Name = "ImageURL",
            Order = 0,
            IsRequired = true)]
        public string ImageURL { get; set; } 
    }
}
