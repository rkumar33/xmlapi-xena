﻿// <copyright file="CountryList.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Reply object for the GetCountryList service
    /// </summary>
    [DataContract(
        Name = "CountryList",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class CountryList
    { 
        /// <summary>
        /// Gets or sets the Name of the Language 
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 0,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Operator",
            Order = 1,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the list of cities matching the query
        /// </summary>
        [DataMember(
            Name = "Countries",
            EmitDefaultValue = false,
            Order = 4,
            IsRequired = true)]
        public IEnumerable<Country> Countries { get; set; }
    }
}
