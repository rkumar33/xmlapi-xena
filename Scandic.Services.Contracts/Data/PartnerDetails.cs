﻿// <copyright file="PartnerDetails.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// class PartnerDetails which provides partner information.
    /// </summary>    
    [DataContract(
        Name = "APIValidityInformation",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class PartnerDetails
    {
        /// <summary>
        /// Gets or sets the operator.
        /// </summary>
        /// <value>The operator.</value>       
        [DataMember(
        Name = "Operator",
        Order = 0,
        IsRequired = true)]
        public string Operator { get; set; }

         /// <summary>
        /// Gets or sets the name of the partner.
        /// </summary>
        /// <value>The name of the partner.</value>       
        [DataMember(
        Name = "PartnerName",
        Order = 1,
        IsRequired = true)]
        public string PartnerName { get; set; }

        /// <summary>
        /// Gets or sets the validity from date.
        /// </summary>
        /// <value>The validity from date.</value>        
        [DataMember(
        Name = "ValidityFromDate",
        Order = 2,
        IsRequired = true)]
        public DateTime ValidityFromDate { get; set; }

        /// <summary>
        /// Gets or sets the validity end date.
        /// </summary>
        /// <value>The validity end date.</value>        
        [DataMember(
        Name = "ValidityEndDate",
        Order = 3,
        IsRequired = true)]
        public DateTime ValidityEndDate { get; set; }

        /// <summary>
        /// Gets or sets the interface list.
        /// </summary>
        /// <value>The interface list.</value>       
        [DataMember(
           Name = "AccessToInterfaces",
           EmitDefaultValue = false,
           Order = 4,
           IsRequired = true)]
        public IEnumerable<InterfaceDetails> InterfaceList { get; set; }
    }
}
