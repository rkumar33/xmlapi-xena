﻿// <copyright file="Tax.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Scandic.Services.Contracts.Data
{
    [Serializable]
    [DataContract(
         Name = "Tax",
         Namespace = @"http://api.scandichotels.com/schemas")]
    public class Tax
    {
        /// <summary>
        /// Type of Tax
        /// </summary>
        [DataMember(
            Name = "type",
            Order = 1,
            IsRequired = false)]
        public string type { get; set; }

        /// <summary>
        /// Amount of Tax
        /// </summary>
        [DataMember(
           Name = "amount",
           Order = 0,
           IsRequired = false)]
        public double amount { get; set; }
    }
}
