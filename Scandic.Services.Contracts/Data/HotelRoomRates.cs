﻿// <copyright file="HotelRoomRates.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Scandic.Services.Contracts.Data
{
    [DataContract(
   Name = "Hotel",
   Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelRoomRates
    {
        [DataMember(
            Name = "Id",
            Order = 0,
            IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the list of hotels and their rooms and rates matching the query
        /// </summary>
        [DataMember(
            Name = "RoomsAndRates",
            EmitDefaultValue = false,
            Order = 1,
            IsRequired = true)]
        public RoomsAndRates RoomsAndRates { get; set; }

        


    }
}
