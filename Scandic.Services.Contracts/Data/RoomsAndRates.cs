﻿// <copyright file="RoomsAndRates.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System; 
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using Biz = Scandic.Services.BusinessEntity;
      
    /// <summary>
    /// Detailed information on a RoomsAndRates
    /// </summary>    
    [DataContract(
        Name = "RoomsAndRates",
        Namespace = @"http://api.scandichotels.com/schemas")]
   public class RoomsAndRates
    {
        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The hotelid.</value>       
        /* [DataMember(
            Name = "HotelId",
            Order = 1,
            IsRequired = true)]*/
        public int HotelId { get; set; }

         /// <summary>
         /// Gets or sets the Name of the Continent
         /// </summary>
         /// <value>The hotelid.</value>         
         [DataMember(
            Name = "HotelName",
            Order = 2,
            IsRequired = true)]
         public string HotelName { get; set; }

         /// <summary>
         /// Gets or sets the rate description.
         /// </summary>
         /// <value>
         /// The rate description.
         /// </value>
         [DataMember(
             Name = "DeeplinkURLHotelPage",
             Order = 3,
             IsRequired = true)]
         public string DeeplinkURLHotelPage { get; set; }


         /// <summary>
         /// Gets or sets the Name of the Continent
         /// </summary>
         /// <value> The Language </value>         
         [DataMember(
            Name = "Language",
            Order = 4,
            IsRequired = true)]
         public string Language { get; set; }

         /// <summary>
         /// Gets or sets the Name of the Operator
         /// </summary>
         /// <value> The Operator </value>         
         [DataMember(
            Name = "Operator",
            Order = 5,
            IsRequired = true)]
         public string Operator { get; set; }

         /// <summary>
         /// Gets or sets the Name of the Continent
         /// </summary>
         /// <value> Fetch Continent </value>         
         [DataMember(
            Name = "Continent",
            Order = 6,
            IsRequired = true)]
         public string Continent { get; set; }

         /// <summary>
         /// Gets or sets the Name of the Rooms
         /// </summary>
         /// <value> Fetch the Rooms defined Properties  </value>                   
         [DataMember(
           Name = "Rooms",
           Order = 7,
           IsRequired = true)]
         public IList<Biz.Room> Rooms { get; set; }       

         /// <summary>
         /// Gets or sets RoomAndRate
         /// </summary>
         /// <value> Room And Rate </value>               
         [DataMember(
           Name = "RoomAndRates",
           Order = 8,
           IsRequired = true)]
         public IList<RoomAndRate> RoomAndRates { get; set; } 
    }
}
