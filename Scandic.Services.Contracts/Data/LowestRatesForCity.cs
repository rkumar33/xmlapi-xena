﻿// <copyright file="LowestRatesForCity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using Biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// Reply Object for AvailableHotelDetails
    /// </summary>    
    [DataContract(Name = "LowestRates", Namespace = @"http://api.scandichotels.com/schemas")]
    public class LowestRatesForCity
    {
        /// <summary>
        /// Gets or sets the city Id
        /// </summary>
        [DataMember(Name = "CityId", Order = 0, IsRequired = true)]
        public string CityId { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 1,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Operator",
            Order = 2,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the continent.
        /// </summary>
        /// <value>
        /// The continent.
        /// </value>
        [DataMember(
            Name = "Continent",
            Order = 3,
            IsRequired = true)]
        public string Continent { get; set; }

         /// <summary>
        /// Gets or sets the deep link URL find hotel page.
        /// </summary>
        /// <value>
        /// The deep link URL find hotel page.
        /// </value>
        [DataMember(Name = "DeepLinkURLFindHotelPage", Order = 6, IsRequired = true)]
        public string DeepLinkURLFindHotelPage { get; set; }
 
        /// <summary>
        /// Gets or sets the list of cities matching the query
        /// </summary>
        [DataMember(
            Name = "Hotels",
            EmitDefaultValue = false,
            Order = 7,
            IsRequired = true)]
        public IList<AvailableLowestRateHotelDetails> Hotels { get; set; }

        /// <summary>
        /// Implicit casting conversion operator
        /// </summary>
        /// <param name="hotelList">HotelDetails business entity</param>
        /// <returns>Converted HotelDetails data contract</returns>
        public static implicit operator LowestRatesForCity(Biz.AvailableHotelDetailsList hotelList)
        {
            if (hotelList == null)
            {
                return null;
            }

            LowestRatesForCity thisHotel = new LowestRatesForCity
            {
              Operator = hotelList.Provider,
              Hotels = new List<AvailableLowestRateHotelDetails>(),
              CityId = hotelList.CityId,
              Continent = hotelList.Continent,
              Language = hotelList.Language,
              DeepLinkURLFindHotelPage = hotelList.DeepLinkUrlFindHotelPage
            };

            foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
            {
                thisHotel.Hotels.Add(new AvailableLowestRateHotelDetails
                {
                    HotelId = h.HotelId,
                    HotelName = h.HotelName,
                    HotelDescription = h.HotelDescription,
                    HotelAddress = h.HotelAddress,
                    FromRate = new FromRateForLowestRate
                    {
                        CurrencyCode = (h.FromRate != null) ? h.FromRate.CurrencyCode : string.Empty,
                        PricePerNight = (h.FromRate != null) ? h.FromRate.PricePerNight : 0,
                        PricePerStay = (h.FromRate != null) ? h.FromRate.PricePerStay : 0,
                        RateDescription = (h.FromRate != null) ? h.FromRate.RateDescription : string.Empty,
                        RateName = (h.FromRate != null) ? h.FromRate.RateName : string.Empty,
                        BreakfastIncluded = (h.FromRate != null) ? h.FromRate.BreakfastIncluded : false,
                        FreeCancellation = (h.FromRate != null) ? h.FromRate.FreeCancellation : false,
                        Payment = (h.FromRate != null) ? h.FromRate.Payment : string.Empty
                    },
                    Image = h.Image,
                    DeeplinkURLHotelPage = h.DeeplinkURLHotelPage
                });
            }

            return thisHotel;
        }
    }
}
