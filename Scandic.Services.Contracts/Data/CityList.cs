﻿// <copyright file="CityList.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Reply object for the GetCityList service
    /// </summary>
    [DataContract(
        Name = "CityList",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class CityList
    {  
        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 0,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Operator",
            Order = 1,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent 
        /// </summary>
        [DataMember(
            Name = "Continent",
            Order = 2,
            IsRequired = true)]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the Name of the CountryId
        /// </summary>
        [DataMember(
            Name = "CountryId",
            Order = 3,
            IsRequired = true)]
        public string CountryId { get; set; }

        /// <summary>
        /// Gets or sets the list of cities matching the query
        /// </summary>
        [DataMember(
            Name = "MarketingCities",
            EmitDefaultValue = false,
            Order = 4,
            IsRequired = true)]
        public IEnumerable<City> Cities { get; set; }
    }
}
