﻿// <copyright file="ServiceFault.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The exception message that is returned if there
    /// is any errors or exceptions that hamper meaningful
    /// processing ofa request. A fault is returned in case
    /// of empty responses also, in some cases.
    /// </summary>
    [DataContract(
        Name = "ServiceFault",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class ServiceFault
    {
        /// <summary>
        /// Gets or sets the
        /// Closest HTTP error that matches this fault
        /// </summary>
        [DataMember]
        public string HttpErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the
        /// Scandic error code for this fault
        /// </summary>
        [DataMember]
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the
        /// Descriptive message with details about the fault
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
