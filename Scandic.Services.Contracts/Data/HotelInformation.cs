﻿// <copyright file="HotelInformation.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Detailed information on a Hotel.
    /// </summary>    
    [DataContract(
        Name = "HotelInformation",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelInformation
    {
        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        /// <value>The language.</value>        
        [DataMember(
            Name = "Language",
            Order = 0,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        /// <value>The operator.</value>       
        [DataMember(
            Name = "Operator",
            Order = 1,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The continent.</value>        
        [DataMember(
            Name = "Continent",
            Order = 2,
            IsRequired = true)]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the details of the hotel
        /// </summary>
        /// <value>The hotel.</value>        
        [DataMember(
            Name = "Hotel",
            Order = 3,
            IsRequired = true)]
        public HotelDetails Hotel { get; set; }
    }
}
