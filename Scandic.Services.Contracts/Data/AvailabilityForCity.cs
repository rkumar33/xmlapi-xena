﻿// <copyright file="AvailabilityForCity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using Biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// Reply Object for AvailableHotelDetails
    /// </summary>    
    [DataContract(Name = "CityAvailability", Namespace = @"http://api.scandichotels.com/schemas")]
    public class AvailabilityForCity
    {
        /// <summary>
        /// Gets or sets the city Id
        /// </summary>
        [DataMember(Name = "MarketingCityId", Order = 0, IsRequired = true)]
        public string CityId { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 1,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Operator",
            Order = 2,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the continent.
        /// </summary>
        /// <value>
        /// The continent.
        /// </value>
        [DataMember(
            Name = "Continent",
            Order = 3,
            IsRequired = true)]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the deeplink URL hotel list.
        /// </summary>
        /// <value>
        /// The deeplink URL hotel list.
        /// </value>
        [DataMember(Name = "DeeplinkUrlHotelList", Order = 5, IsRequired = true)]
        public string DeeplinkUrlHotelList { get; set; }

        /// <summary>
        /// Gets or sets the list of cities matching the query
        /// </summary>
        [DataMember(
            Name = "Hotels",
            Order = 6,
            IsRequired = true)]
        public IList<AvailableCityHotelDetails> Hotels { get; set; }

        /// <summary>
        /// Implicit casting conversion operator
        /// </summary>
        /// <param name="hotelList">HotelDetails business entity</param>
        /// <returns>Converted HotelDetails data contract</returns>
        public static implicit operator AvailabilityForCity(Biz.AvailableHotelDetailsList hotelList)
        {
            if (hotelList == null)
            {
                return null;
            }

            AvailabilityForCity thisHotel = new AvailabilityForCity
            {
              Operator = hotelList.Provider,
              Hotels = new List<AvailableCityHotelDetails>(),
              CityId = hotelList.CityId,
              Continent = hotelList.Continent,
              Language = hotelList.Language,
              DeeplinkUrlHotelList = hotelList.DeeplinkUrlHotelList
            };

            foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
            {
                thisHotel.Hotels.Add(new AvailableCityHotelDetails
                {
                    HotelId = h.HotelId,
                    HotelName = h.HotelName,
                    HotelDescription = h.HotelDescription,
                    HotelAddress = h.HotelAddress,
                    FromRate = h.FromRate,
                    Image = h.Image,
                    DeeplinkURLHotelPage = h.DeeplinkURLHotelPage
                });
            }

            return thisHotel;
        }
    }
}
