﻿// <copyright file="HotelDetails.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// Detailed information on a Hotel.
    /// </summary>    
    [DataContract(
        Name = "Hotel",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelDetails
    {
        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The hotel id.</value>       
        [DataMember(
            Name = "HotelId",
            Order = 0,
            IsRequired = true)]
        public string HotelId { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The name of the hotel.</value>        
        [DataMember(
            Name = "HotelName",
            Order = 1,
            IsRequired = true)]
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The hotel address.</value>       
        [DataMember(
            Name = "HotelAddress",
            Order = 2,
            IsRequired = true)]
        public HotelAddress HotelAddress { get; set; }

        /// <summary>
        /// Gets or sets the Coordinates of the Hotel
        /// </summary>
        /// <value>The hotel coordinates.</value>        
        [DataMember(
            Name = "HotelCoordinates",
            Order = 3,
            IsRequired = true)]
        public HotelCoordinates HotelCoordinates { get; set; }

        /// <summary>
        /// Gets or sets the Coordinates of the Hotel
        /// </summary>
        /// <value>The hotel phone.</value>        
        [DataMember(
            Name = "HotelPhone",
            Order = 4,
            IsRequired = true)]
        public string HotelPhone { get; set; }

        /// <summary>
        /// Gets or sets the Coordinates of the Hotel
        /// </summary>
        /// <value>The hotel fax.</value>       
        [DataMember(
            Name = "HotelFax",
            Order = 5,
            IsRequired = true)]
        public string HotelFax { get; set; }

        /// <summary>
        /// Gets or sets the Coordinates of the Hotel
        /// </summary>
        /// <value>The hotel email.</value>       
        [DataMember(
            Name = "HotelEmail",
            Order = 6,
            IsRequired = true)]
        public string HotelEmail { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The hotel description.</value>        
        [DataMember(
            Name = "HotelDescription",
            Order = 7,
            IsRequired = true)]
        public HotelDescription HotelDescription { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent
        /// </summary>
        /// <value>The hotel facilities.</value>      
        [DataMember(
            Name = "HotelFacilities",
            Order = 8,
            IsRequired = true)]
        public HotelFacilities HotelFacilities { get; set; }

        /// <summary>
        /// Gets the Name of the Continent
        /// </summary>
        /// <value>The room facilities.</value>      
        [DataMember(
            Name = "RoomFacilities",
            Order = 9,
            IsRequired = true)]
        public IList<RoomFacilities> RoomFacilitiesList { get; private set; }

        /// <summary>
        /// Gets the Name of the Continent
        /// </summary>
        /// <value>The images.</value>       
        [DataMember(
            Name = "Images",
            Order = 10,
            IsRequired = true)]
        public IList<Image> Images { get; private set; }

        /// <summary>
        /// Implicit casting conversion operator
        /// </summary>
        /// <param name="hotel">HotelDetails business entity</param>
        /// <returns>Converted HotelDetails data contract</returns>
        public static implicit operator HotelDetails(Biz.HotelDetails hotel)
        {
            if (hotel == null)
            {
                return null;
            }

            HotelDetails thisHotel = new HotelDetails
            {
                HotelId = hotel.HotelId.ToString(),
                HotelName = hotel.HotelName,
                HotelEmail = hotel.HotelEmail,
                HotelFax = hotel.HotelFax,
                HotelPhone = hotel.HotelPhone,
                HotelAddress = new HotelAddress
                {
                    Country = hotel.HotelAddress.Country,
                    MarketingCityName = hotel.HotelAddress.MarketingCityName,
                    PostalCity = hotel.HotelAddress.PostalCity,
                    PostalCode = hotel.HotelAddress.PostalCode,
                    StreetAddress = hotel.HotelAddress.StreetAddress
                },
                HotelCoordinates = new HotelCoordinates
                {
                    Latitude = hotel.HotelCoordinate.Latitude,
                    Longitude = hotel.HotelCoordinate.Longitude
                },
                HotelDescription = new HotelDescription
                {
                    HotelFacilitiesDescription = hotel.HotelDescription.HotelFacilitiesDescription,
                    HotelIntro = hotel.HotelDescription.HotelIntro
                },
                HotelFacilities = new HotelFacilities
                {
                    AirportName = hotel.HotelFacilities.AirportName,
                    DistanceToAirportInKm = hotel.HotelFacilities.DistanceToAirportInKM,
                    DistanceToCityCenterInKm = hotel.HotelFacilities.DistanceToCityCenterInKM,
                    DistanceToTrainStationInKm = hotel.HotelFacilities.DistanceToTrainStationInKM,
                    EcoLabeledHotel = hotel.HotelFacilities.EcoLabeledHotel,
                    Garage = hotel.HotelFacilities.Garage,
                    MeetingFacilities = hotel.HotelFacilities.MeetingFacilities,
                    NonSmokingRooms = hotel.HotelFacilities.NonsmokingRooms,
                    NumberOfRooms = hotel.HotelFacilities.NumberOfRooms,
                    OutdoorParking = hotel.HotelFacilities.OutdoorParking,
                    RelaxCenter = hotel.HotelFacilities.RelaxCenter,
                    RestaurantOrBar = hotel.HotelFacilities.RestaurantOrBar,
                    RoomsForDisabled = hotel.HotelFacilities.RoomsForDisabled,
                    Shop = hotel.HotelFacilities.Shop,
                    WirelessInternetAccess = hotel.HotelFacilities.WirelessInternetAccess
                },
                RoomFacilitiesList = new List<RoomFacilities>(),
                Images = new List<Image>()
            };

            foreach (Biz.Image image in hotel.Images)
            {
                thisHotel.Images.Add(new Image
                {
                    ImageURL = image.ImageUrl,
                    IsMainImage = image.IsMainImage,
                    Title = image.Title
                });
            }

            foreach (Biz.RoomFacilities facility in hotel.RoomFacilitiesList)
            {
                thisHotel.RoomFacilitiesList.Add(new RoomFacilities
                {
                    Name = facility.Name,
                    Description = facility.Description,
                    Image = new RoomImage
                    {
                        ImageURL = facility.Image.ImageUrl,                       
                    }
                });
            }

            return thisHotel;
        }
    }      
}
