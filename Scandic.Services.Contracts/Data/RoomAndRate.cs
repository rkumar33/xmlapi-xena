﻿// <copyright file="RoomAndRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using Biz = Scandic.Services.BusinessEntity;

    [DataContract(
    Name = "RoomAndRate",
    Namespace = @"http://api.scandichotels.com/schemas")]

    /// <summary>
    /// Class to hold Room and Rate information
    /// </summary>
    public class RoomAndRate
    {
        /// <summary>
        /// Field for the roomRates property
        /// </summary>
        private IList<Biz.RoomRate> roomRates;
        /// <summary>
        /// Gets or sets RoomRates
        /// </summary>
        [DataMember(
            Name = "RoomRates",
            Order = 0,
            IsRequired = true)]
        public IList<Biz.RoomRate> RoomRates
        {
            get
            {
                if (this.roomRates == null)
                {
                    this.roomRates = new List<Biz.RoomRate>();
                }

                return this.roomRates;
            }

            set
            {
                this.roomRates = value;
            }
        }

        /// <summary>
        /// Gets or sets TotalPricePerStay
        /// </summary>
        [DataMember(
       Name = "TotalPricePerStay",
       Order = 1,
       IsRequired = true)]
        public double TotalPricePerStay { get; set; }

        /// <summary>
        /// Gets or sets TotalPricePerNight
        /// </summary>
        [DataMember(
        Name = "TotalPricePerNight",
        Order = 2,
        IsRequired = true)]
        public double TotalPricePerNight { get; set; }

        /// <summary>
        /// Gets or sets CurrencyCode
        /// </summary>
        [DataMember(
          Name = "CurrenyCode",
          Order = 6,
          IsRequired = true)]
        public string CurrenyCode { get; set; }

        /// <summary>
        /// Gets or sets DeepLinkUrl
        /// </summary>
        [DataMember(
        Name = "DeepLinkUrl",
        Order = 7,
        IsRequired = true)]
        public string DeepLinkUrl { get; set; }
    }
}
