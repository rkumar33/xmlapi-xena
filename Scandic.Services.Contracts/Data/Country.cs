﻿// <copyright file="Country.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Details about a Country
    /// </summary>
    [DataContract(
        Name = "Country",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class Country
    {
        /// <summary>
        /// Gets or sets the Name of the CountryId
        /// </summary>
        [DataMember(
            Name = "CountryId",
            Order = 1,
            IsRequired = true)]
        public string CountryId { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Country, as per ISO3166
        /// </summary>
        [DataMember(
            Name = "CountryName",
            Order = 2,
            IsRequired = true)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent, the country belongs to
        /// </summary>
        [DataMember(
            Name = "Continent",
            Order = 3,
            IsRequired = true)]
        public string Continent { get; set; }        
    }
}
