﻿// <copyright file="FromRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// FromRate class
    /// </summary>
    [DataContract(
        Name = "FromRate",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class FromRate
    {
        /// <summary>
        /// Gets or sets the price per night.
        /// </summary>
        /// <value>
        /// The price per night.
        /// </value>
        [DataMember(
            Name = "PricePerNight",
            Order = 0,
            IsRequired = true)]
        public double PricePerNight { get; set; }

        /// <summary>
        /// Gets or sets the price per stay.
        /// </summary>
        /// <value>
        /// The price per stay.
        /// </value>
        [DataMember(
            Name = "PricePerStay",
            Order = 1,
            IsRequired = true)]
        public double PricePerStay { get; set; }

        /// <summary>
        /// Gets or sets the Currency Code
        /// </summary>
        [DataMember(
            Name = "CurrencyCode",
            Order = 2,
            IsRequired = true)]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the rate.
        /// </summary>
        /// <value>
        /// The name of the rate.
        /// </value>
        [DataMember(
            Name = "RateName",
            Order = 3,
            IsRequired = true)]
        public string RateName { get; set; }

        /// <summary>
        /// Gets or sets the rate description.
        /// </summary>
        /// <value>
        /// The rate description.
        /// </value>
        [DataMember(
            Name = "RateDescription",
            Order = 4,
            IsRequired = true)]
        public string RateDescription { get; set; }

        /// <summary>
        /// Gets or sets the rate description.
        /// </summary>
        /// <value>
        /// The rate description.
        /// </value>
        [DataMember(
            Name = "DeepLinkURL",
            Order = 5,
            IsRequired = true)]
        public string DeepLinkURL { get; set; }

		 /// <summary>
        /// Gets or sets the BreakfastIncluded.
        /// </summary>
        /// <value>
        /// The BreakfastIncluded.
        /// </value>
        [DataMember(
            Name = "BreakfastIncluded",
            Order = 6,
            IsRequired = true)]
        public bool BreakfastIncluded { get; set; }

        /// <summary>
        /// Gets or sets the FreeCancellation.
        /// </summary>
        /// <value>
        /// The FreeCancellation.
        /// </value>
        [DataMember(
            Name = "FreeCancellation",
            Order = 7,
            IsRequired = true)]
        public bool FreeCancellation { get; set; }

        /// <summary>
        /// Gets or sets GuaranteeType
        /// </summary>
        [DataMember(
            Name = "Payment",
            Order = 8,
            IsRequired = true)]
        public string Payment { get; set; }
    }
}
