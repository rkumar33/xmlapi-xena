﻿// <copyright file="City.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Details about a Country
    /// </summary>
    [DataContract(
        Name = "MarketingCity",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class City
    {
        /// <summary>
        /// Gets or sets the ID of the City
        /// </summary>
        [DataMember(
            Name = "MarketingCityId",
            Order = 0,
            IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the Name of the City
        /// </summary>
        [DataMember(
            Name = "MarketingCityName",
            Order = 1,
            IsRequired = true)]
        public string Name { get; set; }
    }
}
