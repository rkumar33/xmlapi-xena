﻿// <copyright file="InterfaceDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class to provide interfacedetails
    /// </summary>    
    [DataContract(
        Name = "Interface",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class InterfaceDetails
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value> Gets or sets the interface name. </value>       
            [DataMember(
            Name = "Name",
            Order = 1,
            IsRequired = true)]
        public string Name { get; set; }
    }
}
