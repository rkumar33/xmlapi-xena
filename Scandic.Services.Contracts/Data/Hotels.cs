﻿// <copyright file="HotelRoomsAndRates.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Scandic.Services.Contracts.Data
{
    /// <summary>
    /// Reply Object for AvailabilityForHotels
    /// </summary>    
    [DataContract(Name = "AvailabilitiesForRoomsAndRates", Namespace = @"http://api.scandichotels.com/schemas")]
    public class AvailabilitiesForRoomsAndRates
    {

        /// <summary>
        /// Gets or sets the list of hotels and their rooms and rates matching the query
        /// </summary>
        [DataMember(
            Name = "Hotels",
            EmitDefaultValue = false,
            Order = 1,
            IsRequired = true)]
        public List<HotelRoomRates> Hotels { get; set; }

        
    }
}
