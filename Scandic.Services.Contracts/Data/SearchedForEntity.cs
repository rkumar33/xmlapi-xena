﻿
// <copyright file="HotelSearchEntity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;

    /// <summary>
    /// The searched for Entity containing the details of what user has searched for
    /// i.e, city or hotel
    /// </summary>
    public class SearchedForEntity
    {
        /// <summary>
        /// Enumeration containing the different kinds of locations types
        /// user can search for.
        /// Currently only two are user
        /// 1. Hotel
        /// 2. City
        /// The other option 3.Region is added for future user if we include it.
        /// </summary>
        public enum LocationSearchType
        {
            /// <summary>
            /// Hotel value to be set
            /// </summary>
            Hotel,

            /// <summary>
            /// City vale to be set
            /// </summary>
            City,

            /// <summary>
            /// Region value to set
            /// </summary>
            Region
        }

        /// <summary>
        /// Gets or sets the search string.
        /// </summary>
        /// <value>The search string.</value>
        /// <remarks></remarks>
        public string SearchString { get; set; }

        /// <summary>
        /// Gets or sets the type of the user search.
        /// </summary>
        /// <value>The type of the user search.</value>
        /// <remarks></remarks>
        public LocationSearchType UserSearchType { get; set; }

        /// <summary>
        /// Gets or sets the search code.
        /// </summary>
        /// <value>The search code.</value>
        /// <remarks></remarks>
        public string SearchCode { get; set; }
    }
}