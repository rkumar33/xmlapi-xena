﻿// <copyright file="HotelCoordinates.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// THis class has fields to hold the coordinates of the hotel
    /// </summary> 
      [DataContract(
        Name = "HotelCoordinates",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelCoordinates
    {
        /// <summary>
        /// Gets or sets the Longitude
        /// </summary>
        /// <value>The longitude.</value>        
        [DataMember(
            Name = "Longitude",
            Order = 0,
            IsRequired = true)]
        public string Longitude { get; set; }

        /// <summary>
        /// Gets or sets the Latitude
        /// </summary>
        /// <value>The latitude.</value>       
         [DataMember(
            Name = "Latitude",
            Order = 1,
            IsRequired = true)]
        public string Latitude { get; set; }
    }
}
