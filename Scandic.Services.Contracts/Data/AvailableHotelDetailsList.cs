﻿// <copyright file="AvailableHotelDetailsList.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using Biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// Reply Object for AvailableHotelDetails
    /// </summary>    
    [DataContract(Name = "Hotels", Namespace = @"http://api.scandichotels.com/schemas")]
    public class AvailableHotelDetailsList
    {
        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 0,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Operator",
            Order = 1,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the continent.
        /// </summary>
        /// <value>
        /// The continent.
        /// </value>
        [DataMember(
            Name = "Continent",
            Order = 2,
            IsRequired = true)]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the name of the MessageOnSingleHotelSearchNotAvailable
        /// </summary>
        [DataMember(Name = "MessageonOnSingleHotelSearchNotAvailable", Order = 3, IsRequired = true)]
        public string MessageonOnSingleHotelSearchNotAvailable { get; set; }

        /// <summary>
        /// Gets or sets the city Id
        /// </summary>
        [DataMember(Name = "CityId", Order = 4, IsRequired = true)]
        public string CityId { get; set; }

        /// <summary>
        /// Gets or sets the deeplink URL hotel list.
        /// </summary>
        /// <value>
        /// The deeplink URL hotel list.
        /// </value>
        [DataMember(Name = "DeeplinkUrlHotelList", Order = 5, IsRequired = true)]
        public string DeeplinkUrlHotelList { get; set; }

        /// <summary>
        /// Gets or sets the deep link URL find hotel page.
        /// </summary>
        /// <value>
        /// The deep link URL find hotel page.
        /// </value>
        [DataMember(Name = "DeepLinkURLFindHotelPage", Order = 6, IsRequired = true)]
        public string DeepLinkURLFindHotelPage { get; set; }
 
        /// <summary>
        /// Gets or sets the list of cities matching the query
        /// </summary>
        [DataMember(
            Name = "Hotel",
            EmitDefaultValue = false,
            Order = 7,
            IsRequired = true)]
        public List<AvailableHotelDetails> Hotels { get; set; }

        /// <summary>
        /// Implicit casting conversion operator
        /// </summary>
        /// <param name="hotelList">HotelDetails business entity</param>
        /// <returns>Converted HotelDetails data contract</returns>
        public static implicit operator AvailableHotelDetailsList(Biz.AvailableHotelDetailsList hotelList)
        {
            if (hotelList == null)
            {
                return null;
            }

            AvailableHotelDetailsList thisHotel = new AvailableHotelDetailsList
            {
              Operator = hotelList.Provider,
              Hotels = new List<AvailableHotelDetails>(),
              CityId = hotelList.CityId,
              Continent = hotelList.Continent,
              Language = hotelList.Language,
              MessageonOnSingleHotelSearchNotAvailable = hotelList.MessageonOnSingleHotelSearchNotAvailable,
              DeeplinkUrlHotelList = hotelList.DeeplinkUrlHotelList,
              DeepLinkURLFindHotelPage = hotelList.DeepLinkURLFindHotelPage              
            };

            foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
            {
                thisHotel.Hotels.Add(new AvailableHotelDetails
                {
                    HotelId = h.HotelId,
                    HotelName = h.HotelName,
                    HotelDescription = h.HotelDescription,
                    HotelAddress = h.HotelAddress,
                    FromRate = h.FromRate,
                    Image = h.Image
                });
            }

            return thisHotel;
        }
    }
}
