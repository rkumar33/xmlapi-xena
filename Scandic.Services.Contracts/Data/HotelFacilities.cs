﻿// <copyright file="HotelFacilities.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// This class has fields to hold the Facilities available in the hotel
    /// </summary>
    [DataContract(
        Name = "HotelFacilities",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelFacilities
    {
        /// <summary> 
        /// Gets or sets the NumberOfRooms 
        /// </summary>  
           [DataMember(
            Name = "NumberOfRooms",
            Order = 1,
            IsRequired = true)]
        public string NumberOfRooms { get; set; }

        /// <summary>     
        /// Gets or sets the NonSmokingRooms 
        /// </summary>  
          [DataMember(
            Name = "NonSmokingRooms",
            Order = 2,
            IsRequired = true)]
        public string NonSmokingRooms { get; set; }

        /// <summary>     
        /// Gets or sets the RoomsForDisabled 
        /// </summary>  
          [DataMember(
            Name = "RoomsForDisabled",
            Order = 3,
            IsRequired = true)]
        public string RoomsForDisabled { get; set; }

        /// <summary>     
        /// Gets or sets the RelaxCenter 
        /// </summary> 
           [DataMember(
            Name = "RelaxCenter",
            Order = 4,
            IsRequired = true)]
        public string RelaxCenter { get; set; }

        /// <summary>     
        /// Gets or sets the RestaurantOrBar 
        /// </summary>  
          [DataMember(
            Name = "RestaurantOrBar",
            Order = 5,
            IsRequired = true)]
        public string RestaurantOrBar { get; set; }

        /// <summary>           
        /// Gets or sets the Garage 
        /// </summary>  
         [DataMember(
            Name = "Garage",
            Order = 6,
            IsRequired = true)]
        public string Garage { get; set; }

        /// <summary>           
        /// Gets or sets the OutdoorParking 
        /// </summary>  
          [DataMember(
            Name = "OutdoorParking",
            Order = 7,
            IsRequired = true)]
        public string OutdoorParking { get; set; }

        /// <summary>           
        /// Gets or sets the Shop 
        /// </summary>  
         [DataMember(
            Name = "Shop",
            Order = 8,
            IsRequired = true)]
        public string Shop { get; set; }

        /// <summary>           
        /// Gets or sets the MeetingFacilities 
        /// </summary> 
        [DataMember(
            Name = "MeetingFacilities",
            Order = 9,
            IsRequired = true)]
        public string MeetingFacilities { get; set; }

        /// <summary>           
        /// Gets or sets the EcoLabeledHotel 
        /// </summary> 
        [DataMember(
            Name = "EcoLabeledHotel",
            Order = 10,
            IsRequired = true)]
        public string EcoLabeledHotel { get; set; }

        /// <summary>           
        /// Gets or sets the DistanceToCityCenterInKm 
        /// </summary> 
        [DataMember(
            Name = "DistanceToCityCenterInKm",
            Order = 11,
            IsRequired = true)]
        public string DistanceToCityCenterInKm { get; set; }

        /// <summary>           
        /// Gets or sets the DistanceToTrainStationInKm 
        /// </summary>  
        [DataMember(
            Name = "DistanceToTrainStationInKm",
            Order = 12,
            IsRequired = true)]
        public string DistanceToTrainStationInKm { get; set; }

        /// <summary>           
        /// Gets or sets the WirelessInternetAccess 
        /// </summary>  
          [DataMember(
            Name = "WirelessInternetAccess",
            Order = 13,
            IsRequired = true)]
        public string WirelessInternetAccess { get; set; }

        /// <summary>           
        /// Gets or sets the AirportName 
        /// </summary>  
           [DataMember(
            Name = "AirportName",
            Order = 14,
            IsRequired = true)]
        public string AirportName { get; set; }

        /// <summary>           
        /// Gets or sets the DistanceToAirportInKm 
        /// </summary>  
         [DataMember(
            Name = "DistanceToAirportInKm",
            Order = 15,
            IsRequired = true)]
        public string DistanceToAirportInKm { get; set; }
    }
}
