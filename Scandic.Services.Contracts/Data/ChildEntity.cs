﻿// <copyright file="ChildEntity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;

    /// <summary>
    /// Information of a single child is stored in 
    /// instances of this class.
    /// </summary>
    public class ChildEntity
    {
        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        /// <value> Fetch The Age value.</value>
        /// <remarks></remarks>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the accommodation string.
        /// </summary>
        /// <value>The accommodation string.</value>
        /// <remarks></remarks>
        public string AccommodationString { get; set; }
    }
}