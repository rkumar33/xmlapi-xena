﻿// <copyright file="HotelDescription.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// This class has fields to hold the description of the hotel
    /// </summary>    
   [DataContract(
        Name = "HotelDescription",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelDescription
    {
        /// <summary>
        /// Gets or sets the Latitude
        /// </summary>
        /// <value>The hotel intro.</value>        
        [DataMember(
            Name = "HotelIntro",
            Order = 0,
            IsRequired = true)]
        public string HotelIntro { get; set; }

        /// <summary>
        /// Gets or sets the Latitude
        /// </summary>
        /// <value>The hotel facilities description.</value>       
        [DataMember(
            Name = "HotelFacilitiesDescription",
            Order = 0,
            IsRequired = true)]
        public string HotelFacilitiesDescription { get; set; }
    }
}
