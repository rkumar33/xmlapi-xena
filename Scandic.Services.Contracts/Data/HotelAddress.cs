﻿// <copyright file="HotelAddress.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// This class holds the address of the hotel
    /// </summary>    
    [DataContract(
        Name = "HotelAddress",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelAddress
    {
        /// <summary>
        /// Gets or sets the streetaddress of the hotel
        /// </summary>
        /// <value>The street address.</value>        
      [DataMember(
            Name = "StreetAddress",
            Order = 0,
            IsRequired = true)]
        public string StreetAddress { get; set; }

        /// <summary>
        /// Gets or sets the Postal Code of the hotel
        /// </summary>
        /// <value>The postal code.</value>       
        [DataMember(
            Name = "PostalCode",
            Order = 1,
            IsRequired = true)]
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the postalcity of the city
        /// </summary>
        /// <value>The postal city.</value>       
        [DataMember(
            Name = "PostalCity",
            Order = 2,
            IsRequired = true)]
        public string PostalCity { get; set; }

        /// <summary>
        /// Gets or sets the MarketingCityName
        /// </summary>
        /// <value>The name of the marketing city.</value>        
        [DataMember(
            Name = "MarketingCityName",
            Order = 3,
            IsRequired = true)]
        public string MarketingCityName { get; set; }

        /// <summary>
        /// Gets or sets the Name of the country
        /// </summary>
        /// <value>The country.</value>        
        [DataMember(
            Name = "Country",
            Order = 4,
            IsRequired = true)]
        public string Country { get; set; }
    }
}
