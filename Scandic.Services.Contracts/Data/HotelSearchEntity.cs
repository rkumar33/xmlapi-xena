﻿// <copyright file="HotelSearchEntity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;  
    using System.Text;

    /// <summary>
    /// Basic information on a HotelSearchEntity.
    /// </summary>
    public class HotelSearchEntity
    {
        /// <summary>
        /// Gets or sets the searchedfor.
        /// </summary>
        /// <value>The searched for</value>
        /// <remarks></remarks>
        public SearchedForEntity SearchedFor { get; set; }

        /// <summary>
        /// Gets or sets the arrival date.
        /// </summary>
        /// <value>The arrival date.</value>
        /// <remarks></remarks>
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// Gets or sets the no of nights.
        /// </summary>
        /// <value>The no of nights.</value>
        /// <remarks></remarks>
        public int NoOfNights { get; set; }

        /// <summary>
        /// Gets or sets the rooms per night.
        /// </summary>
        /// <value>The rooms per night.</value>
        /// <remarks></remarks>
        public int RoomsPerNight { get; set; }

        /// <summary>
        /// Gets or sets the adults per room.
        /// </summary>
        /// <value>The adults per room.</value>
        /// <remarks></remarks>
        public int AdultsPerRoom { get; set; }

        /// <summary>
        /// Gets or sets the children per room.
        /// </summary>
        /// <value>The children per room.</value>
        /// <remarks></remarks>
        public int ChildrenPerRoom { get; set; }

        /// <summary>
        /// Gets or sets the children occupancy per room.
        /// </summary>
        /// <value>The children occupancy per room.</value>
        /// <remarks></remarks>
        public int ChildrenOccupancyPerRoom { get; set; }

        /// <summary>
        /// Gets or sets the selected hotel code.
        /// </summary>
        /// <value>The selected hotel code.</value>
        /// <remarks></remarks>
        public string SelectedHotelCode { get; set; }

        /// <summary>
        /// Gets or sets the list rooms.
        /// </summary>
        /// <value>The list rooms.</value>
        /// <remarks></remarks>
        private IList<HotelSearchRoomEntity> ListRooms { get; set; }

        /// <summary>
        /// Gets or sets the hotel country code.
        /// </summary>
        /// <value>The hotel country code.</value>
        /// <remarks></remarks>
        private string HotelCountryCode { get; set; }
    }
}