﻿// <copyright file="HotelSearchRoomEntity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;

    /// <summary>
    /// Number of Adults per room user has selected
    /// </summary>
    public class HotelSearchRoomEntity
    {
        /// <summary>
        /// Gets or sets the adults per room.
        /// </summary>
        /// <value>The adults per room.</value>
        /// <remarks></remarks>
        public int AdultsPerRoom { get; set; }

        /// <summary>
        /// Gets or sets the children per room.
        /// </summary>
        /// <value>The children per room.</value>
        /// <remarks></remarks>       
        public int ChildrenPerRoom { get; set; }

        /// <summary>
        /// Gets or sets the children occupancy per room.
        /// </summary>
        /// <value> The children occupancy per room. </value>
        /// <remarks></remarks>
        public int ChildrenOccupancyPerRoom { get; set; }
    }
}