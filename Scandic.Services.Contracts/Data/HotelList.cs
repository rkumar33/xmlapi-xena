﻿// <copyright file="HotelList.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Reply object for the GetHotelList service
    /// </summary>
    [DataContract(
        Name = "HotelList",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelList
    {
        /// <summary>
        /// Gets or sets the Name of the Language
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 0,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
        Name = "Operator",
        Order = 1,
        IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent 
        /// </summary>
        [DataMember(
            Name = "Continent",
            Order = 2,
            IsRequired = true)]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the CityId
        /// </summary>
        [DataMember(
            Name = "MarketingCityId",
            Order = 3,
            IsRequired = true)]
        public string MarketingCityId { get; set; }

        /// <summary>
        /// Gets or sets the list of Hotels under the cityId passed In
        /// </summary>
        [DataMember(
            Name = "Hotels",
            EmitDefaultValue = false,
            Order = 4,
            IsRequired = true)]
        public IEnumerable<Hotel> Hotels { get; set; }
    }
}
