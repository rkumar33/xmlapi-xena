﻿// <copyright file="Images.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{    
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// This class has fields to hold the images collesction in the hotel
    /// </summary>  
     [DataContract(
        Name = "Images",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class Images
    {
        /// <summary>
        /// Gets or sets the Images 
        /// </summary> 
          [DataMember(
            Name = "ImageList",
            Order = 0,
            IsRequired = true)]
        public List<Image> ImageList { get; set; }

        /// <summary>
        /// Gets or sets the Title 
        /// </summary> 
             [DataMember(
            Name = "Title",
            Order = 0,
            IsRequired = true)]
        public string Title { get; set; }
    }    
}
