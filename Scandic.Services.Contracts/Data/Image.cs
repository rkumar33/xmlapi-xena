﻿// <copyright file="Image.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// This class has fields to hold the Images of the hotel
    /// </summary>   
    [DataContract(
        Name = "Image",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class Image
    {
        /// <summary>           
        /// Gets or sets the Latitude 
        /// </summary>  
        [DataMember(
            Name = "ImageURL",
            Order = 0,
            IsRequired = true)]
        public string ImageURL { get; set; }

         /// <summary>
         /// Gets or sets the IsMainImage
         /// </summary>
         /// <value>The is main image.</value>        
         [DataMember(
             Name = "Title",
             Order = 2,
             IsRequired = true)]
         public string Title { get; set; }

         /// <summary>
         /// Gets or sets the IsMainImage
         /// </summary>
         /// <value>The is main image.</value>         
         [DataMember(
             Name = "IsMainImage",
             Order = 3,
             IsRequired = true)]
         public string IsMainImage { get; set; }
    }
}
