﻿// <copyright file="AvailableLowestRateHotelDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// Holds information related to availability in a Hotel
    /// </summary>    
    [DataContract(
        Name = "Hotel",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class AvailableLowestRateHotelDetails
    {
        /// <summary>
        /// Gets or sets the Name of the HotelId
        /// </summary>
        /// <value>The hotel id.</value>       
        [DataMember(
            Name = "HotelId",
            Order = 1,
            IsRequired = true)]
        public string HotelId { get; set; }

        /// <summary>
        /// Gets or sets the Name of the HotelName
        /// </summary>
        /// <value>The name of the hotel.</value>     
        [DataMember(
            Name = "HotelName",
            Order = 2,
            IsRequired = true)]
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Hotel Address
        /// </summary>
        /// <value>The hotel address.</value>        
        [DataMember(
            Name = "HotelAddress",
            Order = 3,
            IsRequired = true)]
        public HotelAddress HotelAddress { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Hotel Description
        /// </summary>
        /// <value>Hotel Description.</value>       
        [DataMember(
            Name = "HotelDescription",
            Order = 4,
            IsRequired = true)]
        public HotelDescription HotelDescription { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Image
        /// </summary>
        /// <value>Room Image.</value>        
        [DataMember(
            Name = "HotelImage",
            Order = 5,
            IsRequired = true)]
        public RoomImage Image { get; set; }

        /// <summary>
        /// Gets or sets the FromRate
        /// </summary>
        /// <value>From Rate.</value>        
        [DataMember(
            Name = "FromRate",
            Order = 6,
            IsRequired = true)]
        public FromRateForLowestRate FromRate { get; set; }

        /// <summary>
        /// Gets or sets the DeeplinkURLHotelPage
        /// </summary>
        /// <value>
        /// The DeeplinkURLHotelPage.
        /// </value>
        [DataMember(
            Name = "DeeplinkURLHotelPage",
            Order = 7,
            IsRequired = true)]
        public string DeeplinkURLHotelPage { get; set; }

        /// <summary>
        /// Implicit casting conversion operator
        /// </summary>
        /// <param name="hotel">HotelDetails business entity</param>
        /// <returns>Converted HotelDetails data contract</returns>
        public static implicit operator AvailableLowestRateHotelDetails(biz.AvailableHotelDetails hotel)
        {
            if (hotel == null)
            {
                return null;
            }

            AvailableLowestRateHotelDetails thisHotel = new AvailableLowestRateHotelDetails
            {
                HotelId = hotel.HotelId,
                HotelName = hotel.HotelName,
                DeeplinkURLHotelPage = hotel.Deeplinkurlhotellangingpage,
                HotelAddress = new HotelAddress
                {
                    Country = (hotel.HotelAddress != null) ? hotel.HotelAddress.Country : string.Empty,
                    MarketingCityName = (hotel.HotelAddress != null) ? hotel.HotelAddress.MarketingCityName : string.Empty,
                    PostalCity = (hotel.HotelAddress != null) ? hotel.HotelAddress.PostalCity : string.Empty,
                    PostalCode = (hotel.HotelAddress != null) ? hotel.HotelAddress.PostalCode : string.Empty,
                    StreetAddress = (hotel.HotelAddress != null) ? hotel.HotelAddress.StreetAddress : string.Empty
                },

                HotelDescription = new HotelDescription
                {
                    HotelFacilitiesDescription = (hotel.HotelDescription != null) ? hotel.HotelDescription.HotelFacilitiesDescription : string.Empty,
                    HotelIntro = (hotel.HotelDescription != null) ? hotel.HotelDescription.HotelIntro : string.Empty
                },

                Image = new RoomImage
                {
                    ImageURL = (hotel.HotelBookingImage != null) ? hotel.HotelBookingImage.ImageUrl : string.Empty
                },

                FromRate = new FromRateForLowestRate
                {
                    RateName = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.RateName : string.Empty,
                    RateDescription = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.RateDescription : string.Empty,
                    PricePerNight = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.PricePerNight : 0,
                    PricePerStay = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.PricePerStay : 0,
                    CurrencyCode = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.PricePerNightCurrencyCode : string.Empty,
                    BreakfastIncluded = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.BreakfastIncluded : false,
                    FreeCancellation = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.FreeCancellation : false,
                    Payment = (hotel.HotelFromRate != null) ? hotel.HotelFromRate.Payment : string.Empty,
                }
            };

            return thisHotel;
        }
    }
}
