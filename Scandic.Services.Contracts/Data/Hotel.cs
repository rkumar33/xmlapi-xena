﻿// <copyright file="Hotel.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Basic information on a Hotel.
    /// </summary>
    [DataContract(
        Name = "Hotel",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class Hotel
    {
        /// <summary>
        /// Gets or sets the Hotel Id
        /// </summary>
        [DataMember(
            Name = "HotelId",
            Order = 0,
            IsRequired = true)]
        public string HotelId { get; set; }

        /// <summary>
        /// Gets or sets the Hotel Name
        /// </summary>
        [DataMember(
            Name = "HotelName",
            Order = 1,
            IsRequired = true)]
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the Hotel Type
        /// </summary>
        [DataMember(
            Name = "HotelType",
            Order = 2,
            IsRequired = true)]
        public string HotelType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is bookable.
        /// </summary>
        /// <value><c>true</c> if this instance is bookable; otherwise, <c>false</c>.</value>        
        [DataMember(
            Name = "IsBookable",
            Order = 3,
            IsRequired = true)]
        public bool IsBookable { get; set; }

        /// <summary>
        /// Gets or sets the Postal City to which the hotel belongs
        /// </summary>
        [DataMember(
            Name = "PostalCity",
            Order = 4,
            IsRequired = true)]
        public string PostalCity { get; set; }

        /// <summary>
        /// Gets the Tax Rates of the hotel
        /// </summary>
        public double APITaxRates { get; set; }
    }
}
