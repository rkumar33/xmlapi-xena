﻿// <copyright file="ExtendedHostFactory.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Host
{
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Description;
    using System.ServiceModel.Web;    
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Extensions;

    /// <summary>
    /// Extending WebServiceHostFactory to add behaviors.
    /// </summary>
    public class ExtendedHostFactory : WebServiceHostFactory
    {
        /// <summary>
        /// Overrides the base CreateServiceHost method to
        /// add the behaviors to service
        /// </summary>
        /// <param name="serviceType">The parameter is not used.</param>
        /// <param name="baseAddresses">The parameter is not used.</param>
        /// <returns>ServiceHost created by the factory</returns>
        protected override ServiceHost CreateServiceHost(System.Type serviceType, System.Uri[] baseAddresses)
        {
            ServiceHost host = base.CreateServiceHost(serviceType, baseAddresses) as WebServiceHost;

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.DebugMode)))
            {
                ServiceDebugBehavior debug = host.Description.Behaviors.Find<ServiceDebugBehavior>();
                if (debug == null)
                {
                    host.Description.Behaviors.Add(
                        new ServiceDebugBehavior()
                        {
                            IncludeExceptionDetailInFaults = true
                        });
                }
                else
                {
                    if (!debug.IncludeExceptionDetailInFaults)
                    {
                        debug.IncludeExceptionDetailInFaults = true;
                    }
                }
            }

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.KeyValidation)))
            {
                host.Description.Behaviors.Add(new ValidateApiKey());
            }

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.Tracking)))
            {
                host.Description.Behaviors.Add(new SendRequestToSiteCatalyst());
            }

            return host;
        }
    }
}