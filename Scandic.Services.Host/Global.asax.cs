﻿// <copyright file="Global.asax.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Host
{
    using System;
    using System.ServiceModel.Activation;
    using System.Web.Routing;    
    using Scandic.Services.Framework;
    using Scandic.Services.Operations;

    /// <summary>
    /// Global events at the web application level
    /// </summary>
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Application start event handler
        /// Adds routing tables
        /// </summary>
        /// <param name="sender">sender of event</param>
        /// <param name="e">event arguments</param>
        protected void Application_Start(object sender, EventArgs e)
        {
            ExtendedHostFactory factory = new ExtendedHostFactory();
            RouteTable.Routes.Add(new ServiceRoute(@"XMLAPI/v1/admin", factory, typeof(AdministrationService)));
            RouteTable.Routes.Add(new ServiceRoute(@"XMLAPI/v1/availability", factory, typeof(AvailabilityService)));
            RouteTable.Routes.Add(new ServiceRoute(@"XMLAPI/v1", factory, typeof(ContentService)));

            // Pre-load the cache
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.PreloadCache)))
            {
                CacheLoader.PreloadAll();
            }
        }
    }
}