using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;
using System.Net;
using System.Text;
using Scandic.Services.Framework;

namespace Scandic.Services.ServiceAgents
{
    // Define a SOAP Extension that traces the SOAP request and SOAP
    // response for the Web service method the SOAP extension is
    // applied to.
    public class TraceSoapExtension : SoapExtension 
    {
        Stream oldStream;
        Stream newStream;
        string filename;

        // Save the Stream representing the SOAP request or SOAP response into
        // a local memory buffer.
        public override Stream ChainStream( Stream stream ){
            oldStream = stream;
            newStream = new MemoryStream();
            return newStream;
        }

        // When the SOAP extension is accessed for the first time, the XML Web
        // service method it is applied to is accessed to store the file
        // name passed in, using the corresponding SoapExtensionAttribute.   
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute) 
        {
            return ((TraceSoapExtensionAttribute)attribute).Filename;
        }

        // The SOAP extension was configured to run using a configuration file
        // instead of an attribute applied to a specific Web service
        // method.
        public override object GetInitializer(Type WebServiceType) 
        {
          // Return a file name to log the trace information to, based on the
          // type.
          return "C:\\" + WebServiceType.FullName + ".log";    
        }

        // Receive the file name stored by GetInitializer and store it in a
        // member variable for this specific instance.
        public override void Initialize(object initializer) 
        {
            filename = (string) initializer;
        }

        //  If the SoapMessageStage is such that the SoapRequest or
        //  SoapResponse is still in the SOAP format to be sent or received,
        //  save it out to a file.
        public override void ProcessMessage(SoapMessage message) 
        {
            switch (message.Stage) {
            case SoapMessageStage.BeforeSerialize:
                break;
            case SoapMessageStage.AfterSerialize:
                WriteOutput(message);
                break;
            case SoapMessageStage.BeforeDeserialize:
                WriteInput(message);
                break;
            case SoapMessageStage.AfterDeserialize:
                break;
            default:
                 throw new Exception("invalid stage");
            }
        }

        public void WriteOutput(SoapMessage message){
            
            newStream.Position = 0;
            StringBuilder soapString = new StringBuilder();
            soapString.Append("-----");
            soapString.Append( (message is SoapServerMessage) ? "SoapResponse" : "SoapRequest" );
            soapString.Append(Environment.NewLine);
            TextReader reader = new StreamReader(newStream);
            soapString.Append(reader.ReadToEnd());
            newStream.Position = 0;
            Copy(newStream, oldStream);
            LogHelper.LogInfo(soapString.ToString(), LogCategory.Availability);
        }

        public void WriteInput(SoapMessage message){
            Copy(oldStream, newStream);
            StringBuilder soapString = new StringBuilder();
            soapString.Append("-----");
            soapString.Append( (message is SoapServerMessage) ? "SoapRequest" : "SoapResponse");
            soapString.Append(Environment.NewLine);
            newStream.Position = 0;
            TextReader reader = new StreamReader(newStream);
            soapString.Append(reader.ReadToEnd());            
            newStream.Position = 0;
            LogHelper.LogInfo(soapString.ToString(), LogCategory.Availability);
        }

        void Copy(Stream from, Stream to) 
        {
            TextReader reader = new StreamReader(from);
            TextWriter writer = new StreamWriter(to);
            writer.WriteLine(reader.ReadToEnd());
            writer.Flush();
        }
      }

       // Create a SoapExtensionAttribute for the SOAP Extension that can be
       // applied to a Web service method.
      [AttributeUsage(AttributeTargets.Method)]
      public class TraceSoapExtensionAttribute : SoapExtensionAttribute {

        //private string filename = ConfigurationManager.AppSettings["LogPath"].ToString() + "\\OWSRequestResponse.log";
        private string filename = "c:\\OWSRequestResponse.log";
        private int priority;

        public override Type ExtensionType {
            get { return typeof(TraceSoapExtension); }
        }

        public override int Priority {
            get { return priority; }
            set { priority = value; }
        }

        public string Filename {
            get {
                return filename;
            }
            set {
                filename = value;
            }
        }
    }
}
