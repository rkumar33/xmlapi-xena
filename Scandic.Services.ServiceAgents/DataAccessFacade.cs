﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Scandic.Services.Framework;

namespace Scandic.Services.ServiceAgents
{
    public class DataAccessFacade
    {
        public DataAccessFacade()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
            new RemoteCertificateValidationCallback(delegate(object sender2, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            });
        }

        public Task<string> GetStringAsync(Uri uri)
        {
            // create completion source  
            var tcs = new TaskCompletionSource<string>();

            // create a web client for downloading the string  
            var wc = new WebClient();

            // Set up variable for referencing anonymous event handler method. We  
            // need to first assign null, and then create the method so that we  
            // can reference the variable within the method itself.  
            DownloadStringCompletedEventHandler downloadCompletedHandler = null;

            // Set up an anonymous method that will handle the DownloadStringCompleted  
            // event.  
            downloadCompletedHandler = (s, e) =>
            {
                // Unsubscribe the event listener (to allow the WebClient to  
                // be garbage collected).  
                wc.DownloadStringCompleted -= downloadCompletedHandler;
                if (e.Error != null)
                {
                    // If the download failed, set the error on the task completion source  
                    tcs.TrySetException(e.Error);
                }
                else if (e.Cancelled)
                {
                    // If the download was cancelled, signal cancellation to the  
                    // task completion source.  
                    tcs.TrySetCanceled();
                }
                else
                {
                    // If the download was successful, set the result on the task completion  
                    // source  
                    tcs.TrySetResult(e.Result);
                }

                wc.Dispose();
            };

            // Subscribe to the completed event  
            wc.DownloadStringCompleted += downloadCompletedHandler;

            // Start the asynchronous download  
            wc.DownloadStringAsync(uri);

            // Return the Task object from the TaskCompletionSource. This object will be monitored  
            // by the calling code for the result, that is provided by the TrySetXxxx calls above.  
            return tcs.Task;
        }

        public IEnumerable<T> GetContentsInList<T>(string url)
        {
            LogHelper.LogWarning(string.Format("Downloading data from URL: {0}", url), LogCategory.Content);
            string downloadData = GetStringAsync(new Uri(string.Format("{0}/{1}", ConfigHelper.GetValue(ConfigKeys.XenaAPIBaseURL), url))).Result;
            var result = JsonConvert.DeserializeObject<IEnumerable<T>>(downloadData);
            return result;
        }


        public List<T> GetContentsInListByFilter<T>(string url, string filter)
        {
            LogHelper.LogWarning(string.Format("Downloading data from URL: {0}?{1}", url, filter), LogCategory.Content);
            string downloadData = GetStringAsync(new Uri(string.Format("{0}/{1}?{2}", ConfigHelper.GetValue(ConfigKeys.XenaAPIBaseURL), url, filter))).Result;
            var result = JsonConvert.DeserializeObject<List<T>>(downloadData);
            return result;
        }

        public T GetContentsByFilter<T>(string url, string filter)
        {
            LogHelper.LogWarning(string.Format("Downloading data from URL: {0}?{1}", url, filter), LogCategory.Content);
            string downloadData = GetStringAsync(new Uri(string.Format("{0}/{1}?{2}", ConfigHelper.GetValue(ConfigKeys.XenaAPIBaseURL), url, filter))).Result;
            var result = JsonConvert.DeserializeObject<T>(downloadData);
            return result;
        }

        public T GetContentsByID<T>(string url, string id, string filter)
        {
            LogHelper.LogWarning(string.Format("Downloading data Hote HotelID:{1}, from URL: {0}?{2}", url, id, filter), LogCategory.Content);
            string downloadData = GetStringAsync(new Uri(string.Format("{0}/{1}?{2}", ConfigHelper.GetValue(ConfigKeys.XenaAPIBaseURL), url, filter))).Result;
            var result = JsonConvert.DeserializeObject<T>(downloadData);
            return result;

        }

        public T GetContents<T>(string url)
        {
            string downloadData = GetStringAsync(new Uri(string.Format("{0}/{1}", ConfigHelper.GetValue(ConfigKeys.XenaAPIBaseURL), url))).Result;
            var result = JsonConvert.DeserializeObject<T>(downloadData);
            return result;
        }

        public List<T> GetDataFromXMLinList<T>(string xmlPath, string root)
        {
            var serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute() { ElementName = root });
            XmlDocument xmlFile = new XmlDocument();
            List<T> result = default(List<T>);
            if (File.Exists(xmlPath))
            {
                xmlFile.Load(xmlPath);
                MemoryStream rdr = new MemoryStream(Encoding.UTF8.GetBytes(xmlFile.InnerXml));
                result = (List<T>)serializer.Deserialize(rdr);
            }
            else
            {
                throw new ApplicationException(string.Format("Configuration file {0} not found.", xmlPath));
            }

            return result;
        }


        public T GetDataFromXML<T>(string xmlPath, string root)
        {
            var serializer = new XmlSerializer(typeof(T), new XmlRootAttribute() { ElementName = root });
            XmlDocument xmlFile = new XmlDocument();
            T result = default(T);
            if (File.Exists(xmlPath))
            {
                xmlFile.Load(xmlPath);
                MemoryStream rdr = new MemoryStream(Encoding.UTF8.GetBytes(xmlFile.InnerXml));
                result = (T)serializer.Deserialize(rdr);
            }
            else
            {
                throw new ApplicationException(string.Format("Configuration file {0} not found.", xmlPath));
            }

            return result;
        }

        public T GetEntityData<T>(string entityName, string hotelID, string language)
        {
            string apiURL = string.Format("{0}/{1}/{2}", ConfigHelper.GetValue(ConfigKeys.XenaAPIHotels), hotelID, entityName);
            T entity = GetContentsByFilter<T>(apiURL, "lang=" + language);
            return entity;
        }

        public List<T> GetEntityDataInList<T>(string entityName, string hotelID, string language)
        {
            string apiURL = string.Format("{0}/{1}/{2}", ConfigHelper.GetValue(ConfigKeys.XenaAPIHotels), hotelID, entityName);
            List<T> entity = GetContentsInListByFilter<T>(apiURL, "lang=" + language);
            return entity;
        }
    }
}
