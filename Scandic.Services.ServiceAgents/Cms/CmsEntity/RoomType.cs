﻿// <copyright file="RoomType.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.CmsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// class to store room type information.
    /// </summary>
    [Serializable]
    public class RoomType
    {
        /// <summary>
        /// Gets or sets the ID of the OperaRoomType
        /// </summary>
        public string OperaRoomTypeId { get; set; }

        /// <summary>
        /// Gets or sets the RoomDescriptione
        /// </summary>
        public string RoomDescription { get; set; }

        /// <summary>
        /// Gets or sets the RoomCategoryName
        /// </summary>
        public string RoomCategoryName { get; set; }
    }
}
