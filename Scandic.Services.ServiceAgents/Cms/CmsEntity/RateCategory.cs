﻿// <copyright file="RateCategory.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.ServiceAgents.CmsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Rate category Class
    /// </summary>
    [Serializable]
    public class RateCategory
    {
        /// <summary>
        /// Initializes a new instance of the RateCategory class.
        /// </summary>
        public RateCategory()
        {
            this.RateCodes = new List<RateCode>();
        }

        /// <summary>
        /// Gets or sets the RateCategoryId
        /// </summary>
        public string RateCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the OperaId
        /// </summary>
        public string OperaId { get; set; }

        /// <summary>
        /// Gets or sets the RateCategoryName
        /// </summary>
        public string RateCategoryName { get; set; }

        /// <summary>
        /// Gets or sets the RateCategoryDescription
        /// </summary>
        public string RateCategoryDescription { get; set; }

        /// <summary>
        /// Gets the RateCodes
        /// </summary>
        public IList<RateCode> RateCodes { get; private set; }

        /// <summary>
        /// Gets or sets the GuaranteeType
        /// </summary>
        public string GuaranteeType { get; set; }
    }
}
