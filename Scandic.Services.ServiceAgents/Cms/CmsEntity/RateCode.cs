﻿// <copyright file="RateCode.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.CmsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// class to store the details of ratecodes.
    /// </summary>
    [Serializable]
    public class RateCode
    {
        /// <summary>
        /// Gets or sets OperaRateId
        /// </summary>
        public string OperaRateId { get; set; }

        /// <summary>
        /// Gets or sets RateCodeDescription
        /// </summary>
        public string RateCodeDescription { get; set; }

        /// <summary>
        /// Gets or sets RateCategoryName
        /// </summary>
        public string RateCategoryName { get; set; }

        /// <summary>
        /// Gets or sets BreakfastIncluded
        /// </summary>
        public bool BreakfastIncluded { get; set; }

        /// <summary>
        /// Gets or sets IsCancellable
        /// </summary>
        public bool IsCancellable { get; set; }
    }
}
