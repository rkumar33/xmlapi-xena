﻿// <copyright file="RoomCategory.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.CmsEntity
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    #endregion
    /// <summary>
    /// class to store info about room category
    /// </summary>
    [Serializable]
    public class RoomCategory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomCategory"/> class.
        /// </summary>
        public RoomCategory()
        {
            List<RoomType> roomTypes = new List<RoomType>();
        }

        /// <summary>
        /// Gets or sets RoomCategoryId
        /// </summary>
        public string RoomCategoryId { get; set; }

        /// <summary>
        /// Gets or sets RoomCategoryName
        /// </summary>
        public string RoomCategoryName { get; set; }

        /// <summary>
        /// Gets or sets RoomCategoryDescription
        /// </summary>
        public string RoomCategoryDescription { get; set; }

        /// <summary>
        /// Gets or sets RoomCategoryDescription
        /// </summary>
        public string PageReferenceId { get; set; }

        /// <summary>
        /// Gets or sets RoomTypes
        /// </summary>
        public IList<RoomType> RoomTypes { get; set; }
    }
}
