// <copyright file="OWSException.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    #region References
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using Scandic.Services.Framework;
    #endregion

    /// <summary>
    /// custom exception for ows.
    /// </summary>
    public class OwsException : ApplicationException
    {
        /// <summary>
        /// Error Code
        /// </summary>
        private string errCode = string.Empty;

        /// <summary>
        /// Error MEssage
        /// </summary>
        private string errMessage = string.Empty;

        /// <summary>
        /// Translate Path
        /// </summary>
        private string translatePath = string.Empty;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OwsException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception</param>
        public OwsException(string message, Exception ex)
            : base(message, ex)
        {
            LogHelper.LogException(ex, LogCategory.Availability);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwsException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public OwsException(string message)
            : base(message)
        {
            this.errMessage = message;

            ArrayList parameters = new ArrayList();
            parameters.Add(AppConstants.OWSEXCEPTION);
            parameters.Add(string.Empty);
            parameters.Add(this.errMessage);
            string customMessage = " ERROR DESC: " + this.errMessage;

            LogHelper.LogException(this, LogCategory.Availability);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwsException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The error code.</param>
        public OwsException(string message, string code)
            : base(message)
        {
            this.errCode = code;
            this.errMessage = message;

            ArrayList parameters = new ArrayList();
            parameters.Add(AppConstants.OWSEXCEPTION);
            parameters.Add(this.errCode);
            parameters.Add(this.errMessage);

            string customMessage = "ERROR CODE: " + this.errCode + " ERROR DESC: " + this.errMessage;
            LogHelper.LogException(this, LogCategory.Availability);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwsException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The error code.</param>
        /// <param name="exceptionData">The exception data.</param>
        public OwsException(string message, string code, Hashtable exceptionData)
            : base(message)
        {
            this.errCode = code;
            this.errMessage = this.Message;
            ArrayList parameters = new ArrayList();
            parameters.Add(AppConstants.OWSEXCEPTION);
            parameters.Add(this.errCode);
            parameters.Add(this.errMessage);
            this.AddDataToException(exceptionData);
            string customMessage = "ERROR CODE: " + this.errCode + " ERROR DESC: " + this.errMessage;
            LogHelper.LogException(this, LogCategory.Availability);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OwsException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The error code.</param>
        /// <param name="exceptionData">The exception data.</param>
        /// <param name="paramCritical">if set to <c>true</c> [param critical].</param>
        public OwsException(string message, string code, Hashtable exceptionData, bool paramCritical)
            : base(message)
        {
            this.errCode = code.Trim();
            this.errMessage = message.Trim();

            ArrayList parameters = new ArrayList();
            parameters.Add(AppConstants.OWSEXCEPTION);
            parameters.Add(this.errCode);
            parameters.Add(this.errMessage);
            this.AddDataToException(exceptionData);
            string customMessage = "ERROR CODE: " + this.errCode + " ERROR DESC: " + this.errMessage;

            bool isCritical = paramCritical;
            switch (this.errCode)
            {
                case OwsExceptionConstants.PROMOTIONCODEINVALID:
                case OwsExceptionConstants.PROPERTYNOTAVAILABLE:
                case OwsExceptionConstants.INVALIDPROPERTY:
                case OwsExceptionConstants.PROPERTYRESTRICTED:
                    isCritical = false;
                    break;
            }

            if (isCritical)
            {
                LogHelper.LogException(this, LogCategory.Availability);
            }
            else
            {
                LogHelper.LogInfo(customMessage, LogCategory.Availability);
            }
        }

        #endregion Constructor

        /// <summary>
        /// Gets or sets the err code.
        /// </summary>
        /// <value>
        /// The err code.
        /// </value>
        public string ErrCode { get; set; }

        /// <summary>
        /// Gets or sets the err message.
        /// </summary>
        /// <value>
        /// The err message.
        /// </value>
        public string ErrMessage { get; set; }

        /// <summary>
        /// Gets or sets the translate path.
        /// </summary>
        /// <value>
        /// The translate path.
        /// </value>
        public string TranslatePath { get; set; }

        #region AddDataToException

        /// <summary>
        /// Adds the data to exception.
        /// </summary>
        /// <param name="exceptionData">The exception data.</param>
        private void AddDataToException(Hashtable exceptionData)
        {
            // Loop through all items of a Hashtable
            IDictionaryEnumerator en = exceptionData.GetEnumerator();
            string key = string.Empty;
            string value = string.Empty;

            while (en.MoveNext())
            {
                key = en.Key.ToString();
                if (en.Value != null)
                {
                    value = en.Value.ToString();
                }

                this.Data.Add(key, value);
            }
        }

        #endregion AddDataToException
    }
}
