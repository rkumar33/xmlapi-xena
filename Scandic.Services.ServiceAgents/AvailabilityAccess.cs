﻿// <copyright file="AvailabilityAccess.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    #region References
    using System.Diagnostics;
    using System.IO;
    using System.Xml.Serialization;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;
    using System.Collections.Generic;
   #endregion

    /// <summary>
    /// Availability Access Helpers/Agents
    /// </summary>
    public static class AvailabilityAccess
    {
        /// <summary>
        /// Gets the availability for multiple hotels.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <param name="logCategory">The log category.</param>
        /// <param name="id">The id of the request</param>
        /// <returns>List of AvailabilityResponse</returns>
        public static List<AvailabilityResponse> GetAvailabilityForMultipleHotels(List<AvailabilityRequest> requests, LogCategory logCategory, string id)
        {
            List<AvailabilityResponse> responses = AvailabilityDomain.GeneralAvailabilityForMultipleHotels(requests, logCategory, id);
            //AvailabilityResponse response = StubResponse(); 
            PerformanceHelper.RegisterAvailabilityCall();
            return responses;
        }

        /// <summary>
        /// Gets the availability response.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Availabilty Response</returns>
        public static List<AvailabilityResponse> GetAvailabilityResponse(List<AvailabilityRequest> requests, LogCategory logCategory, string id)
        {
            List<AvailabilityResponse> responses = AvailabilityDomain.GeneralAvailability(requests, logCategory,id); 
            //AvailabilityResponse response = StubResponse(); 
            PerformanceHelper.RegisterAvailabilityCall();
            return responses;
        }

        /// <summary>
        /// Gets the availability for OWS monitor.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns the availability for Opera </returns>
        public static bool GetAvailabilityForOWSMonitor(AvailabilityRequest request)
        {
            return AvailabilityDomain.GetAvailabilityForOWSMonitor(request);
        }

        /// <summary>
        /// Stubs the response.
        /// </summary>
        /// <returns>stubbed availability response</returns>
        private static AvailabilityResponse StubResponse()
        {
            string responseString = @"<AvailabilityResponse>
			<Result resultStatusFlag=""SUCCESS"" />
			<AvailResponseSegments>
				<AvailResponseSegment>
					<RoomStayList>
						<RoomStay>
							<RatePlans>
								<RatePlan ratePlanCategory=""BC"" ratePlanCode=""BC"" hold=""true"" suppressRate=""false"" ratePlanName=""BC"" commissionCode=""N"">
									<RatePlanDescription>
										<Text>Only valid for customers with prepaid Bonus Cheque vouchers. Buffet breakfast is included in the rate.</Text>
									</RatePlanDescription>
									<AdditionalDetails>
										<AdditionalDetail detailType=""MarketingInformation"">
											<AdditionalDetailDescription>
												<Text>Scandic, 130 hotels in the Nordic region</Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
										<AdditionalDetail detailType=""CommissionPolicy"">
											<AdditionalDetailDescription>
												<Text>No commission available</Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
										<AdditionalDetail detailType=""TaxInformation"">
											<AdditionalDetailDescription>
												<Text>Tax included Vat 12 PCT </Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
									</AdditionalDetails>
									<CancellationDateTime period=""-PT4H"">2011-10-18T18:00:00</CancellationDateTime>
								</RatePlan>
								<RatePlan ratePlanCode=""EA4"" hold=""false"" suppressRate=""false"" ratePlanName=""EA4"" commissionCode=""C"">
									<RatePlanDescription>
										<Text>Early:  Best deal at Scandic.  Book 7 days in advance and pay for the entire stay  Non-refundable.  Buffet Breakfast included.</Text>
									</RatePlanDescription>
									<AdditionalDetails>
										<AdditionalDetail detailType=""MarketingInformation"">
											<AdditionalDetailDescription>
												<Text>Scandic, 130 hotels in the Nordic region</Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
										<AdditionalDetail detailType=""CommissionPolicy"">
											<AdditionalDetailDescription>
												<Text>Commission policy for travel agents is 8%</Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
										<AdditionalDetail detailType=""TaxInformation"">
											<AdditionalDetailDescription>
												<Text>Tax included Vat 12 PCT </Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
									</AdditionalDetails>
									<CancellationDateTime period=""P329009DT14H"">1111-01-01T00:00:00</CancellationDateTime>
								</RatePlan>
								<RatePlan ratePlanCode=""RA4"" hold=""true"" suppressRate=""false"" ratePlanName=""RA4"" commissionCode=""C"">
									<RatePlanDescription>
										<Text>Flex: Best unrestricted rate. Buffet breakfast included.</Text>
									</RatePlanDescription>
									<AdditionalDetails>
										<AdditionalDetail detailType=""MarketingInformation"">
											<AdditionalDetailDescription>
												<Text>Scandic, 130 hotels in the Nordic region</Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
										<AdditionalDetail detailType=""CommissionPolicy"">
											<AdditionalDetailDescription>
												<Text>Commission policy for travel agents is 8%</Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
										<AdditionalDetail detailType=""TaxInformation"">
											<AdditionalDetailDescription>
												<Text>Tax included Vat 12 PCT </Text>
											</AdditionalDetailDescription>
										</AdditionalDetail>
									</AdditionalDetails>
									<CancellationDateTime period=""-PT4H"">2011-10-18T18:00:00</CancellationDateTime>
								</RatePlan>
							</RatePlans>
							<RoomTypes>
								<RoomType roomTypeCode=""SC"" numberOfUnits=""35"">
									<RoomTypeDescription>
										<Text>Cabin no window 1 single bed. Free WIFI, armchair, desk, iron</Text>
									</RoomTypeDescription>
								</RoomType>
								<RoomType roomTypeCode=""SR"" numberOfUnits=""12"">
									<RoomTypeDescription>
										<Text>STD 1 sgl bed. Free WIFI, armchair, desk, iron, 18-22 sqm</Text>
									</RoomTypeDescription>
								</RoomType>
								<RoomType roomTypeCode=""QR"" numberOfUnits=""14"">
									<RoomTypeDescription>
										<Text>STD 1 queen bed. Free WIFI, armchair, desk, iron, 18-22 sqm</Text>
									</RoomTypeDescription>
								</RoomType>
								<RoomType roomTypeCode=""TS"" numberOfUnits=""90"">
									<RoomTypeDescription>
										<Text>Superior 2sgl beds. Free WIFI, sofa, iron, parkview, large bathroom,26-28sqm</Text>
									</RoomTypeDescription>
								</RoomType>
								<RoomType roomTypeCode=""TR"" numberOfUnits=""25"">
									<RoomTypeDescription>
										<Text>STD 2 sgl beds. Free WIFI, armchair, desk, iron, 18-22 sqm</Text>
									</RoomTypeDescription>
								</RoomType>
								<RoomType roomTypeCode=""KS"" numberOfUnits=""7"">
									<RoomTypeDescription>
										<Text>Superior 1 king bed. Free WIFI, sofa, large bathroom, iron, parkview, 26-28 sqm</Text>
									</RoomTypeDescription>
								</RoomType>
								<RoomType roomTypeCode=""ZJ"" numberOfUnits=""11"">
									<RoomTypeDescription>
										<Text>JNR suite 1king bed. Free WIFI, sofa, dining area, iron, parkview, 35-44sqm</Text>
									</RoomTypeDescription>
								</RoomType>
							</RoomTypes>
							<RoomRates>
								<RoomRate roomTypeCode=""SC"" ratePlanCode=""BC"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1545</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1545</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""SR"" ratePlanCode=""BC"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1895</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1895</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""QR"" ratePlanCode=""BC"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1895</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1895</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""TS"" ratePlanCode=""BC"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2195</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2195</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""TR"" ratePlanCode=""BC"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1895</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1895</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""KS"" ratePlanCode=""BC"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2195</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2195</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""ZJ"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">3662</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">3662</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""SC"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1362</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1362</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""SR"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1862</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1862</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""QR"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1862</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1862</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""TS"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2162</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2162</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""TR"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1862</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1862</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""KS"" ratePlanCode=""EA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2162</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2162</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""ZJ"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">3990</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">3990</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""SC"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">1690</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">1690</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""SR"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2190</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2190</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""QR"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2190</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2190</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""TS"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2490</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2490</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""TR"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2190</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2190</Total>
								</RoomRate>
								<RoomRate roomTypeCode=""KS"" ratePlanCode=""RA4"" suppressRate=""false"">
									<Rates>
										<Rate rateOccurrence=""DAILY"" rateChangeIndicator=""false"">
											<Base currencyCode=""SEK"" decimals=""2"">2490</Base>
										</Rate>
									</Rates>
									<Total currencyCode=""SEK"" decimals=""2"">2490</Total>
								</RoomRate>
							</RoomRates>
							<HotelReference chainCode=""SH"" hotelCode=""810"">SCANDIC ANGLAIS</HotelReference>
							<HotelContact>
								<Addresses>
									<Address>
										<AddressLine>HUMLEGAARDSGATAN 23 BOX 5178</AddressLine>
										<cityName>STOCKHOLM</cityName>
										<countryCode>SE</countryCode>
										<postalCode>102 44</postalCode>
									</Address>
								</Addresses>
								<ContactEmails>
									<ContactEmail>Anglais@scandichotels.com</ContactEmail>
								</ContactEmails>
								<ContactPhones>
									<Phone phoneType=""FAX"">
										<PhoneNumber>0046851734011</PhoneNumber>
									</Phone>
									<Phone phoneType=""VOICE"">
										<PhoneNumber>0046851734000</PhoneNumber>
									</Phone>
								</ContactPhones>
								<HotelInformation>
									<HotelInfo hotelInfoType=""CHECKININFO"">
										<Text>
											<TextElement>14:00</TextElement>
										</Text>
									</HotelInfo>
									<HotelInfo hotelInfoType=""CHECKOUTINFO"">
										<Text>
											<TextElement>12:00</TextElement>
										</Text>
									</HotelInfo>
								</HotelInformation>
							</HotelContact>
						</RoomStay>
					</RoomStayList>
				</AvailResponseSegment>
			</AvailResponseSegments>
		</AvailabilityResponse>";

            XmlSerializer serializer = new XmlSerializer(typeof(AvailabilityResponse));
            StringReader rdr = new StringReader(responseString);
            return serializer.Deserialize(rdr) as AvailabilityResponse;
        }
    }
}
