



namespace Scandic.Services.ServiceAgents
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;

    public class OWSRequestConverter
    {

        #region Availability Request Getters

        public static OwsProxy.Availability.AvailabilityRequest GetGeneralAvailabilityRequest(HotelSearchEntity hotelSearch, HotelSearchRoomEntity roomSearch, OwsProxy.Availability.HotelReference hotelReference)
        {
            /*******************************************************************************************
            * Get the Request Object for General availability
            *******************************************************************************************/
            AvailRequestSegment requestSegment = new AvailRequestSegment();
            requestSegment.availReqType = AvailRequestType.Room;
            requestSegment.numberOfRoomsSpecified = true;
            requestSegment.numberOfRooms = 1;

            requestSegment.totalNumberOfGuestsSpecified = true;
            if (roomSearch != null)
            {
                requestSegment.totalNumberOfGuests = roomSearch.AdultsPerRoom;
            }

            requestSegment.numberOfChildrenSpecified = true;


            if (roomSearch != null)
            {
                requestSegment.numberOfChildren = roomSearch.ChildrenOccupancyPerRoom;
            }

            OwsProxy.Availability.TimeSpan dateRange = new OwsProxy.Availability.TimeSpan();
            dateRange.StartDate = hotelSearch.ArrivalDate;
            dateRange.Item = hotelSearch.DepartureDate;
            requestSegment.StayDateRange = dateRange;

            requestSegment.HotelSearchCriteria = new HotelSearchCriterion[1];
            requestSegment.HotelSearchCriteria[0] = new HotelSearchCriterion();
            requestSegment.HotelSearchCriteria[0].HotelRef = hotelReference;

            AvailabilityRequest request = new AvailabilityRequest();
            request.summaryOnly = true;
            request.AvailRequestSegment = new AvailRequestSegment[1];
            request.AvailRequestSegment[0] = requestSegment;

            RatePlanCandidate ratePlanCandidate = new RatePlanCandidate();
            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                    {
                        ratePlanCandidate.promotionCode = hotelSearch.CampaignCode;
                        requestSegment.RatePlanCandidates = new RatePlanCandidate[] { ratePlanCandidate };
                    }
                    break;

                case SearchType.CORPORATE:
                    if (AppConstants.BLOCK_CODE_QUALIFYING_TYPE == hotelSearch.QualifyingType)
                    {
                        RoomStayCandidate roomStayCandidate = new RoomStayCandidate();
                        roomStayCandidate.invBlockCode = hotelSearch.CampaignCode;
                        requestSegment.RoomStayCandidates = new RoomStayCandidate[] { roomStayCandidate };
                    }

                    break;

            }
            return request;
        }


        #endregion

    }
}
