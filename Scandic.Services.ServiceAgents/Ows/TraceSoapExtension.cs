// <copyright file="TraceSoapExtension.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    #region References
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using Scandic.Services.Framework;
    #endregion

    /// <summary>
    /// Define a SOAP Extension that traces the SOAP request and SOAP response for the Web service method the SOAP extension is applied to.
    /// </summary>
    public class TraceSoapExtension : SoapExtension, IDisposable
    {
        /// <summary>
        /// oldStream value
        /// </summary>
        private Stream oldStream;

        /// <summary>
        /// newStream value
        /// </summary>
        private Stream newStream;

        /// <summary>
        /// filename value
        /// </summary>
        private string filename;

       /// <summary>
        /// Save the Stream representing the SOAP request or SOAP response into local memory buffer.
       /// </summary>
        /// <param name="stream">stream object</param>
        /// <returns>Stream object</returns>
        public override Stream ChainStream(Stream stream) 
        {
            this.oldStream = stream;
            this.newStream = new MemoryStream();
            return this.newStream;
        }

        /// <summary>
        /// When the SOAP extension is accessed for the first time, the XML Web service method it is applied to is accessed to store the file name passed in, using the corresponding SoapExtensionAttribute.
        /// </summary>
        /// <param name="methodInfo"> method Info object </param>
        /// <param name="attribute"> attribute object</param>
        /// <returns>Object return value</returns>   
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute) 
        {
            return ((TraceSoapExtensionAttribute)attribute).Filename;
        }

        /// <summary>
        /// The SOAP extension was configured to run using a configuration file instead of an attribute applied to a specific Web service method.
        /// </summary>
        /// <param name="webServiceType">webServiceType of type</param>
        /// <returns>object return value</returns>
        public override object GetInitializer(Type webServiceType) 
        {
          // Return a file name to log the trace information to, based on the
          // type.
          return "C:\\" + webServiceType.FullName + ".log";    
        }

        /// <summary>
        /// Receive the file name stored by GetInitializer and store it in a member variable for this specific instance.
        /// </summary>
        /// <param name="initializer">initializer of object type</param>
        public override void Initialize(object initializer) 
        {
            this.filename = (string)initializer;
        }

       /// <summary>
       /// Process Message
       /// </summary>
        /// <param name="message">message of soapmessage type</param>
        public override void ProcessMessage(SoapMessage message) 
        {
            switch (message.Stage) 
            {
            case SoapMessageStage.BeforeSerialize:
                break;
            case SoapMessageStage.AfterSerialize:
                this.WriteOutput(message);
                break;
            case SoapMessageStage.BeforeDeserialize:
                this.WriteInput(message);
                break;
            case SoapMessageStage.AfterDeserialize:
                break;
            default:
                 throw new Exception("invalid stage");
            }
        }

        /// <summary>
        /// Writes the output.
        /// </summary>
        /// <param name="message">The message.</param>
        public void WriteOutput(SoapMessage message)
        {
            this.newStream.Position = 0;
            StringBuilder soapString = new StringBuilder();
            soapString.Append("-----");
            soapString.Append((message is SoapServerMessage) ? "SoapResponse" : "SoapRequest");
            soapString.Append(Environment.NewLine);
            TextReader reader = new StreamReader(this.newStream);
            soapString.Append(reader.ReadToEnd());
            this.newStream.Position = 0;
            this.Copy(this.newStream, this.oldStream);
            LogHelper.LogInfo(soapString.ToString(), LogCategory.Availability);
        }

        /// <summary>
        /// Writes the input.
        /// </summary>
        /// <param name="message">The message.</param>
        public void WriteInput(SoapMessage message)
        {
            this.Copy(this.oldStream, this.newStream);
            StringBuilder soapString = new StringBuilder();
            soapString.Append("-----");
            soapString.Append((message is SoapServerMessage) ? "SoapRequest" : "SoapResponse");
            soapString.Append(Environment.NewLine);
            this.newStream.Position = 0;
            TextReader reader = new StreamReader(this.newStream);
            soapString.Append(reader.ReadToEnd());
            this.newStream.Position = 0;
            LogHelper.LogInfo(soapString.ToString(), LogCategory.Availability);
        }

        /// <summary>
        /// Copies the specified from.
        /// </summary>
        /// <param name="from">From source</param>
        /// <param name="to">To destination</param>
        public void Copy(Stream from, Stream to) 
        {
            TextReader reader = new StreamReader(from);
            TextWriter writer = new StreamWriter(to);
            writer.WriteLine(reader.ReadToEnd());
            writer.Flush();
        }

        /// <summary>
        /// Dispose method since MemoryStream is used
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Dispose method since MemoryStream is used
        /// </summary>
        /// <param name="suppress">True, to suppress finalize</param>
        protected virtual void Dispose(bool suppress)
        {
            this.Dispose();
            if (suppress)
            {
                GC.SuppressFinalize(this);
            }
        }
    }

      /// <summary>
      ///  Create a SoapExtensionAttribute for the SOAP Extension that can be applied to a Web service method.
      /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class TraceSoapExtensionAttribute : SoapExtensionAttribute
    {
        /// <summary>
        /// ExtensionType which return type object
        /// </summary>
        public override Type ExtensionType
        {
            get { return typeof(TraceSoapExtension); }
        }

        /// <summary>
        /// When overridden in a derived class, gets or set the priority of the SOAP extension.
        /// </summary>
        /// <returns>The priority of the SOAP extension.</returns>
        public override int Priority { get; set; }

        /// <summary>
        /// Gets or sets the filename.
        /// </summary>
        /// <value>
        /// The filename.
        /// </value>
        public string Filename { get; set; }
    }
}
