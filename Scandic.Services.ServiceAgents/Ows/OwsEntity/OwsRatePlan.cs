﻿// <copyright file="OwsRatePlan.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.OwsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// class to store ows rate plan
    /// </summary>
    [Serializable]
    public class OwsRatePlan
    {
        /// <summary>
        /// Gets or sets ID of the Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets ID of the Description
        /// </summary>
        private string Description { get; set; }
    }
}
