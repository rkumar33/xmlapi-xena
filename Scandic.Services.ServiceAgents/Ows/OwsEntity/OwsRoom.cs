﻿// <copyright file="OwsRoom.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.OwsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// class to store information about Ows room. 
    /// </summary>
    [Serializable]
    public class OwsRoom
    {
        /// <summary>
        /// Initializes a new instance of the OwsRoom class.
        /// </summary>
        public OwsRoom()
        {
            this.RoomRates = new List<OwsRoomRate>();
        }

        /// <summary>
        /// Gets or sets RoomRates
        /// </summary>
        public List<OwsRoomRate> RoomRates { get; set; } 
    }
}
