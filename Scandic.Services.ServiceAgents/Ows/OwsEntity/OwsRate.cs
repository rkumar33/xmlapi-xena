﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.BusinessEntity.OwsEntities
{
    [Serializable]
    public class OwsRate
    {
        /// <summary>
        /// The Currency Code
        /// </summary>
        public string CurrencyCode{get; set;}
        /// <summary>
        /// The rate value
        /// </summary>
        public double RateValue { get;  set; }

       
    }
}
