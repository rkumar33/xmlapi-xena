﻿// <copyright file="OwsRoomType.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.OwsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

/// <summary>
    /// OwsRoomType class
/// </summary>
   [Serializable]
    public class OwsRoomType
    {
        /// <summary>
        /// Gets or sets ID of the Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        private string Description { get; set; }
    }
}
