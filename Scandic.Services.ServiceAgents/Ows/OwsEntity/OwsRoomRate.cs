﻿// <copyright file="OwsRoomRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents.OwsEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// class to store ows room rate information.
    /// </summary>
    [Serializable]
    public class OwsRoomRate
    {
        /// <summary>
        /// Gets or sets the RoomTypeCategory
        /// </summary>
        public string RoomTypeCategory { get; set; }

        /// <summary>
        /// Gets or sets the RateTypeCategory
        /// </summary>
        public string RateTypeCategory { get; set; }

        /// <summary>
        /// Gets or sets the RoomTypeCode
        /// </summary>
        public string RoomTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the RatePlanCode
        /// </summary>
        public string RatePlanCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is true or false
        /// </summary>
        public bool IsSpecialRate { get; set; }

        /// <summary>
        /// Gets or sets the BaseRate
        /// </summary>
        public double BaseRate { get; set; }

        /// <summary>
        /// Gets or sets the BaseRateCurrency
        /// </summary>
        public string BaseRateCurrency { get; set; }

        /// <summary>
        /// Gets or sets the BasePricePerNight
        /// </summary>
        //public double BasePricePerNight { get; set; }

        /// <summary>
        /// Gets or sets the BasePricePerStay
        /// </summary>
        //public double BasePricePerStay { get; set; }

        /// <summary>
        /// Gets or sets the TaxAmount
        /// </summary>
        //public string TaxAmount { get; set; }

        /// <summary>
        /// Gets or sets the TaxType
        /// </summary>
        //public string TaxType { get; set; }
        /// <summary>
        /// Gets or sets the TotalRate
        /// </summary>
        public double TotalRate { get; set; }

        /// <summary>
        /// Gets or sets the TotalRateCurrency
        /// </summary>
        public string TotalRateCurrency { get; set; }    
    }
}
