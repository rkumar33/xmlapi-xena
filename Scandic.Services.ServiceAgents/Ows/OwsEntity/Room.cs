﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Services.BusinessEntity.OwsEntities
{
    [Serializable]
    public class Room
    {
        public Room()
        {
            //this.RoomTypes = new List<OwsRoomType>();
            //this.RatePlans = new List<OwsRatePlan>();
            this.RoomRates = new List<OwsRoomRate>();
        }

        //public OwsRate MinPerNightRate { get; set; }

        //public OwsRate MinPerStayRate { get; set; }
        ///// <summary>
        ///// List of available room types in OWS
        ///// </summary>
        //public List<OwsRoomType> RoomTypes { get; set; }
        ///// <summary>
        ///// List of available rate plans for the selected dates and search criteria
        ///// </summary>
        //public List<OwsRatePlan> RatePlans { get; set; }
        /// <summary>
        /// List of available room rate combinations for the selected dates and search criteria
        /// </summary>
        public List<OwsRoomRate> RoomRates { get; set; }
 
    }
}
