// <copyright file="AvailabilityDomain.cs" company="Scandic Hotels">
// Copyright � Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    #region References
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Web.Services.Protocols;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;
   
    #endregion
    /// <summary>
    /// class containing availabilty service calls.
    /// </summary>
    public class AvailabilityDomain
    {
        #region GeneralAvailability

        /// <summary>
        /// Gets the timeout for each availabilty service based on it log category
        /// </summary>
        /// <param name="logCategory"></param>
        /// <returns></returns>
        private static int GetServiceTimeout(LogCategory logCategory)
        {
            int timeout = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OwsServiceTimeOut));
            switch (logCategory)
            {
                case LogCategory.OWSForCertainHotels:
                    timeout = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OWSForCertainHotelsServiceTimeOut));
                    break;

                case LogCategory.OWSForCity:
                    timeout = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OWSForCityServiceTimeOut));
                    break;

                case LogCategory.OWSRoomsRate:
                    timeout = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OWSRoomsRateServiceTimeOut));
                    break;

                case LogCategory.OWSLowestRate:
                    timeout = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OWSLowestRateRateServiceTimeOut));
                    break;
            }

            return timeout;
        }

        /// <summary>
        /// Generals the availability.
        /// </summary>
        /// <param name="request">The request.</param>  
        /// <returns>AvailabilityResponse object</returns>
        public static List<AvailabilityResponse> GeneralAvailability(List<AvailabilityRequest> requests, LogCategory logCategory, string id)
        {
            // Setting parameters for General Availability Search.
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();
            try
            {
                bool isException = false;
                bool throwExceptionOnServiceTimeout = Convert.ToBoolean(ConfigHelper.GetValue(ConfigKeys.ThrowExceptionOnServiceTimeOut));
                DateTime startDateTime = DateTime.Now;
                DateTime asyncRequestStartTime = DateTime.Now;
                DateTime waitStartTime = DateTime.Now;
                DateTime asyncResponseStartTime = DateTime.Now;
                DateTime asyncRequestEndTime = DateTime.Now;
                DateTime waitEndTime = DateTime.Now;
                DateTime asyncResponseEndTime = DateTime.Now;   
                int responseAbortedCount = 0;
                int completedResponseCount = 0;
                OwsProxy.Availability.ResultStatusFlag resultStatus = ResultStatusFlag.SUCCESS;               
                string errorMessage = string.Empty;
                AvailabilityService service = OwsUtility.GetAvailabilityService();
                List<IAsyncResult> asyncResults = new List<IAsyncResult>();
                try
                {
                    int serviceTimeOut = GetServiceTimeout(logCategory); 
                    WaitHandle[] wHandles = new WaitHandle[requests.Count];
                    int index = 0;
                    asyncRequestStartTime = DateTime.Now;
                    foreach (var request in requests)
                    {
                        IAsyncResult asyncResult = service.BeginAvailability(request, null, null);
                        asyncResults.Add(asyncResult);
                        wHandles[index] = asyncResult.AsyncWaitHandle;
                        index++;
                    }
                
                    asyncRequestEndTime = DateTime.Now;
                    waitStartTime = DateTime.Now;
                    WaitHandle.WaitAll(wHandles, serviceTimeOut);
                    waitEndTime = DateTime.Now;                    
                    asyncResponseStartTime = DateTime.Now;
                    index = 0;

                    foreach (var asyncResult in asyncResults)
                    {
                        if (asyncResult.IsCompleted)
                        {
                            completedResponseCount++;
                        }

                        if (resultStatus == ResultStatusFlag.SUCCESS && asyncResult.IsCompleted)
                        {
                            AvailabilityResponse response = service.EndAvailability(asyncResult);
                            resultStatus = response.Result.resultStatusFlag;
                            if (response.Result.resultStatusFlag != ResultStatusFlag.FAIL)
                            {
                                responses.Add(response);
                            }
                        }
                        else
                        {
                            if (!asyncResult.IsCompleted)
                            {
                                responseAbortedCount++;
                            }
                            var result = asyncResult as WebClientAsyncResult;
                            resultStatus = ResultStatusFlag.FAIL;
                            result.Abort();
                        }

                    }
                    asyncResponseEndTime = DateTime.Now;

                }
                catch (Exception ex)
                {
                    isException = true;
                    errorMessage = ex.Message;
                    throw ex;
                }
                finally
                {
                    //close all wait handles.
                    foreach (var asyncResult in asyncResults)
                    {
                        if (asyncResult != null && asyncResult.AsyncWaitHandle != null)
                        {
                            asyncResult.AsyncWaitHandle.Close();
                        }
                    }
                    service.Dispose();
                    DateTime endDateTime = DateTime.Now;
                    string timeDistributionLog = string.Format("{0},Time distribution,GeneralAvailability,{1},{2},{3},{4},{5},{6}", id,
                                                                    asyncRequestStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), asyncRequestEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"),
                                                                    waitStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), waitEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"),
                                                                    asyncResponseStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), asyncResponseEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"));
                    string logMessage = string.Format("{0},OWS time,GeneralAvailability,{1},{2},{3},{4},{5},{6},{7}", id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString(), errorMessage, requests.Count, completedResponseCount, responseAbortedCount);
                    LogHelper.LogInfo(timeDistributionLog, logCategory);
                    LogHelper.LogInfo(logMessage, logCategory);
                }


                if (responseAbortedCount > 0 && throwExceptionOnServiceTimeout)
                {
                    string errorMsg = ConfigHelper.GetValue(ConfigKeys.ServiceTimeoutMessage);
                    throw new ApplicationException(errorMsg);
                }

                LogHelper.LogInfo("Received the response: service.Availability(request)", LogCategory.Availability);

                if (resultStatus != OwsProxy.Availability.ResultStatusFlag.FAIL)
                {
                    LogHelper.LogInfo("OWSProxy.Availability.ResultStatusFlag: Success", LogCategory.Availability);
                    return responses;
                }
                else
                {
                    LogHelper.LogWarning("OWSProxy.Availability.ResultStatusFlag: Failed", LogCategory.Availability);
                    return null;
                }
            }
            catch (SoapException soapEx)
            {
                LogHelper.LogException(soapEx, LogCategory.Availability);
                return null;
            }
        }

        /// <summary>
        /// Gets the availability for OWS monitor.
        /// </summary>
        /// <param name="request">The  availability request.</param>
        /// <returns>        
        /// <c>true</c> if [the opera is avialble]; otherwise, <c>false</c>.
        /// </returns>
        public static bool GetAvailabilityForOWSMonitor(AvailabilityRequest request)
        {
            bool result = true;
            ////Setting parameters for General Availability Search.
            AvailabilityResponse response = default(AvailabilityResponse);
            try
            {
                AvailabilityService service = OwsUtility.GetAvailabilityService();

                //// Making web service call to General Availability.

                response = service.Availability(request);

                LogHelper.LogInfo("Received the response: service.Availability(request)", LogCategory.Availability);

                if (response.Result.resultStatusFlag != OwsProxy.Availability.ResultStatusFlag.FAIL)
                {
                    LogHelper.LogInfo("OWSProxy.Availability.ResultStatusFlag: Success", LogCategory.Availability);
                    result = true;
                }
                else
                {
                    /*
                    If the Response is FAIL and is returning "SYSTEM ERROR" then we consider it as the Availability service is down, & return false as a result.
                    But even if the response is FAIL, & its not a "SYSTEM ERROR"  we consider Availability service is up but with no 
                    availibility for the given date & destination, and return true as a result.
                     */
                    if ((response != null)
                        && (response.Result.resultStatusFlag == ResultStatusFlag.FAIL))
                    {
                        if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEMERROR))
                        {
                            result = false;
                        }
                        else if ((response.Result.GDSError != null) && (response.Result.GDSError.Value == AppConstants.SYSTEMERROR))
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (SoapException soapEx)
            {
                LogHelper.LogException(soapEx, LogCategory.Availability);
                result = false;
            }
            catch (System.Net.WebException ex)
            {
                LogHelper.LogException(ex, LogCategory.Availability);
                result = false;
            }
           
            return result;
        }

        /// <summary>
        /// This method fires batches of ows requests asyncronus for passed all ows requests,
        /// dependeing upon the request's start index and burst count for all requests.
        /// </summary>
        /// <param name="requests">Total number of request to processed.</param>
        /// <param name="serviceTimeOut">Wait time before batch of async requests are processed.</param>
        /// <param name="startIndex">Index from burst of async requests should start from all ows requests.</param>
        /// <param name="owsGroupCount">This maximum number to requests that could be fired in batch.</param>
        /// <param name="asyncResults">List where async handles of fired async requests will be saved.</param>
        /// <param name="resultStatuses">Dictionary item containing the proessing status of async responses for each hotels</param>
        /// <param name="asyncResultsHotelId">List containing hotelids of all fired async ows requests.</param>
        /// <param name="service">OWS service object that will be firing the async requests.</param>
        private static void FireOwsRequests(List<AvailabilityRequest> requests, int serviceTimeOut, int startIndex,
                                            int owsGroupCount, List<IAsyncResult> asyncResults,
                                            IDictionary<string, ResultStatusFlag> resultStatuses, List<string> asyncResultsHotelId,
                                            AvailabilityService service)
        {
            int limit = startIndex + owsGroupCount;
            int waitArrayLength = limit < requests.Count ? owsGroupCount : requests.Count - startIndex;
            WaitHandle[] wHandles = new WaitHandle[waitArrayLength];
            int waitHandelIndex = 0;
            for (int index = startIndex; index < limit; index++)
            {
                if (index >= requests.Count)
                    break;
                var request = requests[index];
                string hotelId = request.AvailRequestSegment[0].HotelSearchCriteria[0].HotelRef.hotelCode;
                if (!resultStatuses.Keys.Contains(hotelId))
                {
                    resultStatuses.Add(hotelId, ResultStatusFlag.SUCCESS);
                }
                IAsyncResult asyncResult = service.BeginAvailability(request, null, null);
                asyncResults.Add(asyncResult);
                asyncResultsHotelId.Add(hotelId);
                wHandles[waitHandelIndex] = asyncResult.AsyncWaitHandle;
                waitHandelIndex++;
            }
            WaitHandle.WaitAll(wHandles, serviceTimeOut);
        }

        /// <summary>
        /// This method process all passed ows request asyncronusly, in batch mode.
        /// </summary>
        /// <param name="requests">All OWS requests to be processed.</param>
        /// <param name="logCategory">It identify category of avialability service 
        /// for which OWS requests are fired.</param>
        /// <param name="id">This is the unique identifier associated with each XMLAPI availability 
        /// request.</param>
        /// <returns></returns>
        public static List<AvailabilityResponse> GeneralAvailabilityForMultipleHotels(List<AvailabilityRequest> requests, LogCategory logCategory, string id = "")
        {
            // Setting parameters for General Availability Search.
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();
            List<IAsyncResult> asyncResults = new List<IAsyncResult>();
            try
            {                
                DateTime startDateTime = DateTime.Now;
                DateTime asyncRequestStartTime = DateTime.Now;
                DateTime asyncResponseStartTime = DateTime.Now;
                DateTime asyncRequestEndTime = DateTime.Now;
                DateTime asyncResponseEndTime = DateTime.Now;
                int completeResponseCount = 0;
                int responseAbortedCount = 0;
                bool throwExceptionOnServiceTimeout = Convert.ToBoolean(ConfigHelper.GetValue(ConfigKeys.ThrowExceptionOnServiceTimeOut));
                int serviceTimeOut = GetServiceTimeout(logCategory);
                AvailabilityService service = OwsUtility.GetAvailabilityService();
                IDictionary<string, ResultStatusFlag> resultStatuses = new Dictionary<string, ResultStatusFlag>();
                //ResultStatusFlag resultStatus = ResultStatusFlag.SUCCESS;
                bool isException = false;
                string errorMessage = "";
                try
                {
                    //service.Timeout = serviceTimeOut;
                    //response = service.Availability(request);                    
                    List<string> asyncResultsHotelId = new List<string>();
                    int groupCount = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OWSAsyncRequestGroupCount));
                    int groupLimit = Convert.ToInt32(Math.Ceiling(decimal.Divide(requests.Count, groupCount)));

                    int lastBurstServiceTimeOut = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.OwsServiceTimeOut));

                    asyncRequestStartTime = DateTime.Now;
                    int startIndex = 0;
                    for (int groupIndex = 0; groupIndex < groupLimit; groupIndex++)
                    {
                        int timeout = groupIndex == (groupLimit - 1) ? lastBurstServiceTimeOut : serviceTimeOut;
                        int burstCount = (groupIndex == 0) && decimal.Remainder(requests.Count, groupCount) > 0 ? Convert.ToInt32(decimal.Remainder(requests.Count, groupCount)) : groupCount;                        
                        FireOwsRequests(requests, timeout, startIndex, burstCount, asyncResults, resultStatuses, asyncResultsHotelId, service);
                        startIndex = startIndex + burstCount;
                    }

                    asyncRequestEndTime = DateTime.Now;

                    int index = 0;
                    asyncResponseStartTime = DateTime.Now;
                    foreach (var asyncResult in asyncResults)
                    {                        
                        string hoteId = asyncResultsHotelId[index];
                        //for logging completed response count.
                        if (asyncResult.IsCompleted)
                        {
                            completeResponseCount++;
                        }
                        if (resultStatuses[hoteId] == ResultStatusFlag.SUCCESS && asyncResult.IsCompleted)
                        {
                            AvailabilityResponse response = service.EndAvailability(asyncResult);
                            resultStatuses[hoteId] = response.Result.resultStatusFlag;
                            if (response.Result.resultStatusFlag == ResultStatusFlag.SUCCESS)
                            {
                                responses.Add(response);
                            }
                        }
                        else
                        {
                            if (!asyncResult.IsCompleted)
                            {
                                responseAbortedCount++;
                            }
                            var result = asyncResult as WebClientAsyncResult;
                            resultStatuses[hoteId] = ResultStatusFlag.FAIL;
                            result.Abort();
                        }

                        index++;
                    }
                    asyncResponseEndTime = DateTime.Now;

                }
                catch (Exception ex)
                {
                    isException = true;
                    errorMessage = ex.Message;
                    throw ex;
                }
                finally
                {
                    //close all wait handles.
                    foreach (var asyncResult in asyncResults)
                    {
                        if (asyncResult != null && asyncResult.AsyncWaitHandle != null)
                        {
                            asyncResult.AsyncWaitHandle.Close();                            
                        }
                    }
                    service.Dispose();
                    DateTime endDateTime = DateTime.Now;
                    string timeDistributionLog = string.Format("{0},Time distribution,GeneralAvailabilityForMultipleHotels,{1},{2},{3},{4}", id,
                                                                    asyncRequestStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), asyncRequestEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"),
                                                                    asyncResponseStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), asyncResponseEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"));
                    string logMessage = string.Format("{0},OWS time,GeneralAvailabilityForMultipleHotels,{1},{2},{3},{4},{5},{6},{7}", id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString(), errorMessage, requests.Count, completeResponseCount, responseAbortedCount);
                    LogHelper.LogInfo(timeDistributionLog, logCategory);
                    LogHelper.LogInfo(logMessage, logCategory);
                }

                if (responseAbortedCount > 0 && throwExceptionOnServiceTimeout)
                {
                    string errorMsg = ConfigHelper.GetValue(ConfigKeys.ServiceTimeoutMessage);
                    throw new ApplicationException(errorMsg);
                }

                if (responses != null && responses.Count == 1 && responses[0].Result.resultStatusFlag == ResultStatusFlag.FAIL)
                {
                    responses = new List<AvailabilityResponse>();
                }

                if (responses.Count > 1)
                {

                    responses = (from rs in responses
                                 from st in resultStatuses
                                 where st.Value == ResultStatusFlag.SUCCESS && rs.AvailResponseSegments[0].RoomStayList[0].HotelReference.hotelCode == st.Key
                                 select rs).ToList<AvailabilityResponse>();

                }
                return responses;

            }
            catch (SoapException soapEx)
            {
                LogHelper.LogException(soapEx, LogCategory.Availability);
                return null;
            }
        }

        ///// <summary>
        ///// Generals the availability for multiple hotels.
        ///// </summary>
        ///// <param name="requests">The requests.</param>
        ///// <param name="logCategory">The log category.</param>
        ///// <param name="id">The id of the request</param>
        ///// <returns></returns>
        //public static List<AvailabilityResponse> GeneralAvailabilityForMultipleHotels(List<AvailabilityRequest> requests, LogCategory logCategory, string id = "")
        //{
        //    DateTime startDateTime = DateTime.Now;
        //    DateTime asyncRequestStartTime = DateTime.Now;
        //    DateTime waitStartTime = DateTime.Now;
        //    DateTime asyncResponseStartTime = DateTime.Now;
        //    DateTime asyncRequestEndTime = DateTime.Now;
        //    DateTime waitEndTime = DateTime.Now;
        //    DateTime asyncResponseEndTime = DateTime.Now;
        //    int completeResponseCount = 0;
        //    int responseAbortedCount = 0;
        //    // Setting parameters for General Availability Search.
        //    List<AvailabilityResponse> responses = new List<AvailabilityResponse>();
        //    try
        //    {             
        //        bool throwExceptionOnServiceTimeout = Convert.ToBoolean(ConfigHelper.GetValue(ConfigKeys.ThrowExceptionOnServiceTimeOut));
        //        int serviceTimeOut = GetServiceTimeout(logCategory);
        //        AvailabilityService service = OwsUtility.GetAvailabilityService();
        //        IDictionary<string, ResultStatusFlag> resultStatuses = new Dictionary<string, ResultStatusFlag>();
        //        //ResultStatusFlag resultStatus = ResultStatusFlag.SUCCESS;
        //        bool isException = false;
        //        string errorMessage = "";
        //        try
        //        {
        //            //service.Timeout = serviceTimeOut;
        //            //response = service.Availability(request);
        //            List<IAsyncResult> asyncResults = new List<IAsyncResult>();
        //            List<string> asyncResultsHotelId = new List<string>();
        //            int waitArrayLength = requests.Count > 64 ? 64 : requests.Count;
        //            WaitHandle[] wHandles = new WaitHandle[waitArrayLength];
        //            WaitHandle[] wHandles2 = new WaitHandle[requests.Count - waitArrayLength];
        //            int index = 0;
        //            asyncRequestStartTime = DateTime.Now;
        //            foreach (var request in requests)
        //            {
        //                string hotelId = request.AvailRequestSegment[0].HotelSearchCriteria[0].HotelRef.hotelCode;
        //                if (!resultStatuses.Keys.Contains(hotelId))
        //                {
        //                    resultStatuses.Add(hotelId, ResultStatusFlag.SUCCESS);
        //                }
        //                IAsyncResult asyncResult = service.BeginAvailability(request, null, null);
        //                asyncResults.Add(asyncResult);
        //                asyncResultsHotelId.Add(hotelId);
        //                if (index < 64)
        //                {
        //                    wHandles[index] = asyncResult.AsyncWaitHandle;
        //                }
        //                else
        //                {
        //                    wHandles2[index - 64] = asyncResult.AsyncWaitHandle;
        //                }
        //                index++;
        //            }
        //            asyncRequestEndTime = DateTime.Now;
        //            waitStartTime = DateTime.Now;
        //            //number of max async requests that can be monitored through WaitHandle is 64,
        //            //so only first 64 requests will monitered in wait handle.
        //            var stopWatch = Stopwatch.StartNew();
        //            WaitHandle.WaitAll(wHandles, serviceTimeOut);
        //            stopWatch.Stop();
        //            //in case there are more than 64 requests and still sometime is left before
        //            //total timeout then make thread wait for remaining time to allow further 
        //            //request to complete.
        //            if (asyncResults.Count > 63 && stopWatch.ElapsedMilliseconds < serviceTimeOut)
        //            {
        //                WaitHandle.WaitAll(wHandles2, (serviceTimeOut - (int)stopWatch.ElapsedMilliseconds));
        //            }

        //            waitEndTime = DateTime.Now;
        //            index = 0;
        //            asyncResponseStartTime = DateTime.Now;
        //            foreach (var asyncResult in asyncResults)
        //            {
        //                string hoteId = asyncResultsHotelId[index];
        //                //for logging completed response count.
        //                if (asyncResult.IsCompleted)
        //                {
        //                    completeResponseCount++;
        //                }
        //                if (resultStatuses[hoteId] == ResultStatusFlag.SUCCESS && asyncResult.IsCompleted)
        //                {
        //                    AvailabilityResponse response = service.EndAvailability(asyncResult);
        //                    resultStatuses[hoteId] = response.Result.resultStatusFlag;
        //                    if (response.Result.resultStatusFlag == ResultStatusFlag.SUCCESS)
        //                    {
        //                        responses.Add(response);
        //                    }
        //                }
        //                else
        //                {
        //                    if (!asyncResult.IsCompleted)
        //                    {
        //                        responseAbortedCount++;
        //                    }
        //                    var result = asyncResult as WebClientAsyncResult;
        //                    resultStatuses[hoteId] = ResultStatusFlag.FAIL;
        //                    result.Abort();
        //                }

        //                index++;
        //            }
        //            asyncResponseEndTime = DateTime.Now;

        //        }
        //        catch (Exception ex)
        //        {
        //            isException = true;
        //            errorMessage = ex.Message;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            service.Dispose();
        //            DateTime endDateTime = DateTime.Now;
        //            string timeDistributionLog = string.Format("{0},Time distribution,GeneralAvailabilityForMultipleHotels,{1},{2},{3},{4},{5},{6}", id,
        //                                                            asyncRequestStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), asyncRequestEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"),
        //                                                            waitStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), waitEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"),
        //                                                            asyncResponseStartTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), asyncResponseEndTime.ToString("MM/dd/yyyy HH:mm:ss.fff"));
        //            string logMessage = string.Format("{0},OWS time,GeneralAvailabilityForMultipleHotels,{1},{2},{3},{4},{5},{6},{7}", id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString(), errorMessage, requests.Count, completeResponseCount, responseAbortedCount);
        //            LogHelper.LogInfo(timeDistributionLog, logCategory);
        //            LogHelper.LogInfo(logMessage, logCategory);
        //        }

        //        if (responseAbortedCount > 0 && throwExceptionOnServiceTimeout)
        //        {
        //            string errorMsg = ConfigHelper.GetValue(ConfigKeys.ServiceTimeoutMessage);
        //            throw new ApplicationException(errorMsg);
        //        }

        //        if (responses != null && responses.Count == 1 && responses[0].Result.resultStatusFlag == ResultStatusFlag.FAIL)
        //        {
        //            responses = new List<AvailabilityResponse>();
        //        }

        //        if (responses.Count > 1)
        //        {
        //            responses = (from rs in responses
        //                         from st in resultStatuses
        //                         where st.Value == ResultStatusFlag.SUCCESS && rs.AvailResponseSegments[0].RoomStayList[0].HotelReference.hotelCode == st.Key
        //                         select rs).ToList<AvailabilityResponse>();

        //        }
        //        return responses;

        //    }
        //    catch (SoapException soapEx)
        //    {
        //        LogHelper.LogException(soapEx, LogCategory.Availability);
        //        return default(List<AvailabilityResponse>);
        //    }
        //}
        
        #endregion
    }
}
