// <copyright file="OWSUtility.cs" company="Scandic Hotels">
// Copyright � Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    
    /// <summary>
    ///  Ows Utility Class
    /// </summary>
    public class OwsUtility
    {
        #region Public Static Methods    
        /// <summary>
        /// Get the Availability Service Proxy
        /// </summary>       
        /// <returns>Name Service</returns>
        public static AvailabilityService GetAvailabilityService()
        {
            AvailabilityService service = new AvailabilityService();
            service.OGHeaderValue = GetAvailabilityHeader();
            service.Url = ConfigHelper.GetValue(ConfigKeys.OwsAvailabilityService);
            
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
               new RemoteCertificateValidationCallback(
               delegate(
                   object sender2,
                   X509Certificate certificate,
                   X509Chain chain,
                   SslPolicyErrors sslPolicyErrors)
               {
                   return true;
               });  
            return service;
        }

        #region RaiseOWSException

        /// <summary>
        /// Raises the OWS exception.
        /// </summary>
        /// <param name="exceptionMessage">The exception message.</param>
        /// <param name="generalException">The general exception.</param>
        public static void RaiseOWSException(string exceptionMessage, Exception generalException)
        {
            throw new OwsException(exceptionMessage, generalException);
        }

        /// <summary>
        /// Raises the regional avail OWS exception.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="exceptionData">The exception data.</param>
        public static void RaiseRegionalAvailOWSException(RegionalAvailabilityResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }

                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }

            throw new OwsException(errorMessage, errorCode, exceptionData, true);                    
        }

        /// <summary>
        /// Raises the general avail OWS exception.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="exceptionData">The exception data.</param>
        public static void RaiseGeneralAvailOWSException(AvailabilityResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }

                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }

            throw new OwsException(errorMessage, errorCode, exceptionData, true);
        }
      
        #endregion RaiseOWSException

        #endregion Public Static Methods

        #region Private Static Methods

        /// <summary>
        /// Get Transaction ID
        /// </summary> 
        /// <returns>Transaction ID</returns>
        private static string GetTransactionID()
        {
            ////Getting some random transaction ID

            byte[] tID = new byte[2];
            Random rnd = new Random(~unchecked((int)DateTime.Now.Ticks));
            rnd.NextBytes(tID);
            return tID[0].ToString() + tID[1].ToString();
        }

        /// <summary>
        /// Get Availabilty Header
        /// </summary>
        /// <returns>OGHeader value</returns>
        private static OGHeader GetAvailabilityHeader()
        {
            OGHeader soapHeader = new OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new Scandic.Services.ServiceAgents.OwsProxy.Availability.EndPoint();
            soapHeader.Origin.entityID = ConfigHelper.GetValue(ConfigKeys.OwsOriginEntityId);
            soapHeader.Origin.systemType = ConfigHelper.GetValue(ConfigKeys.OwsOriginSystemType); 

            soapHeader.Destination = new Scandic.Services.ServiceAgents.OwsProxy.Availability.EndPoint();
            soapHeader.Destination.entityID = ConfigHelper.GetValue(ConfigKeys.OwsDesitinationEntityId);
            soapHeader.Destination.systemType = ConfigHelper.GetValue(ConfigKeys.OwSDesitinationSystemType);

            return soapHeader;
        }

         #endregion Private Static Methods            
    }
  
}
