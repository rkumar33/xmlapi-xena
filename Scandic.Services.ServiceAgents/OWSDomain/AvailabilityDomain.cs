


namespace Scandic.Services.ServiceAgents
{

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System.Web.Services.Protocols;
    using Scandic.Services.ServiceAgents.OWSProxy.Availability;
    using Scandic.Services.Framework;
    using Scandic.Services.BusinessEntity;

    public class AvailabilityDomain
    {
      
        #region GeneralAvailability
   
        public static AvailabilityResponse GeneralAvailability(HotelSearchEntity hotelSearchEntity, HotelSearchRoomEntity roomSearch, string hotelCode, string chainCode)
        {
          
            // Setting parameters for General Availability Search.
            HotelReference hotelRef = new OWSProxy.Availability.HotelReference();
            hotelRef.chainCode = chainCode;
            hotelRef.hotelCode = hotelCode;

            try
            {                
                AvailabilityService service = OWSUtility.GetAvailabilityService();                
                AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(hotelSearchEntity, roomSearch, hotelRef);

                // Making web service call to General Availability.
                
                AvailabilityResponse response = service.Availability(request);

                LogHelper.LogInfo("Received the response: service.Availability(request)", LogCategory.Availability);

                if (response.Result.resultStatusFlag != OWSProxy.Availability.ResultStatusFlag.FAIL)
                {
                    LogHelper.LogInfo("OWSProxy.Availability.ResultStatusFlag: Success", LogCategory.Availability);
                    return response;
                }
                else
                {
                    LogHelper.LogInfo("OWSProxy.Availability.ResultStatusFlag: Failed", LogCategory.Availability);

                    AvailRequestSegment []RequestSegment = request.AvailRequestSegment;
                    
                    Hashtable ExceptionData = new Hashtable();
                    if (RequestSegment != null)
                    {
                        ExceptionData.Add("REQ:AvailReqType", RequestSegment[0].availReqType);
                        ExceptionData.Add("REQ:NoOfChildren", RequestSegment[0].numberOfChildren);
                        ExceptionData.Add("REQ:NumberOfRooms", RequestSegment[0].numberOfRooms);
                        ExceptionData.Add("REQ:TotalNoOfGuests", RequestSegment[0].totalNumberOfGuests);
                        ExceptionData.Add("REQ:StayDateRange", RequestSegment[0].StayDateRange);
                    }
                    if (RequestSegment[0].RatePlanCandidates != null)
                    {
                        ExceptionData.Add("REQ:PromotionCode", RequestSegment[0].RatePlanCandidates[0].promotionCode);
                        ExceptionData.Add("REQ:QualifyingIdType", RequestSegment[0].RatePlanCandidates[0].qualifyingIdType);
                        ExceptionData.Add("REQ:QualifyingIdValue", RequestSegment[0].RatePlanCandidates[0].qualifyingIdValue);
                    }
                    if (response.AvailResponseSegments != null)
                        ExceptionData.Add("RES:AvailableSegments Count", response.AvailResponseSegments.Length);
                    else
                        ExceptionData.Add("RES:AvailableSegments Count", "null");
                    ExceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());

                    OWSUtility.RaiseGeneralAvailOWSException(response, ExceptionData);
                }
            }
            catch (SoapException SoapEx)
            {
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
            return null;
        }

       
        #endregion


    }
}