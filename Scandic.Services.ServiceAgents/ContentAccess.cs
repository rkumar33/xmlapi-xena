﻿// <copyright file="ContentAccess.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using System.Security.Cryptography.X509Certificates;
    using System.Net.Security;

    /// <summary>
    /// Content Access Helpers/Agents
    /// </summary>    
    public static class ContentAccess
    {
        /// <summary>
        /// Constant for PageTypeId
        /// </summary>
        private const string PageTypeId = "PageTypeId";

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <returns>The service to access CMS</returns>
        public static PageStoreService GetService()
        {
            PageStoreService service = new PageStoreService();
            NetworkCredential netCredentials = new NetworkCredential(
                ConfigHelper.GetValue(ConfigKeys.CmsUserName),
                ConfigHelper.GetValue(ConfigKeys.CmsPassword));
            service.Credentials = netCredentials;
            service.PreAuthenticate = true;
            service.Timeout = -1;
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                new RemoteCertificateValidationCallback(
                delegate(
                    object sender2,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                });  
            return service;
        }

        /// <summary>
        /// Gets the page.
        /// </summary>
        /// <param name="pageReferenceID">The page reference ID.</param>
        /// <param name="language">The language.</param>
        /// <returns>Page Information corresponding to the page reference ID passed In</returns>
        public static RawPage GetPage(int pageReferenceID, string language)
        {
            RawPage page = default(RawPage);
            string key = string.Format("Page-{0}-{1}", pageReferenceID, language);
            if (CacheHelper.IsAvailable(key))
            {
                page = CacheHelper.Get(key) as RawPage;
            }
            else
            {             
                PageStoreService service = GetService();
                try
                {
                    PageReference pageReference = new PageReference();
                    pageReference.ID = pageReferenceID;
                    LanguageSelector selector = new LanguageSelector();
                    selector.LanguageBranch = language;
                    page = service.GetPage(pageReference, selector, AccessLevel.NoAccess);
                }
                catch (System.Web.Services.Protocols.SoapException ex)
                {
                    LogHelper.LogError(string.Format("page {0} is not available in master language {1}", pageReferenceID, language), LogCategory.ContentDownloader);
                }
                finally
                {
                    service.Dispose();                    
                }
                
                if (language.Equals(ConfigHelper.GetValue("DefaultLanguage"), System.StringComparison.InvariantCultureIgnoreCase) && page != null)
                {
                    CacheHelper.Put(key, page,CacheDuration.HalfDay);
                }

                LogHelper.LogWarning(string.Format("{0} was not in cache. Items in cache: {1}", key, CacheHelper.CacheCount()), LogCategory.Content);
                PerformanceHelper.RegisterContentCall();
            }

            return page;
        }

        /// <summary>
        /// Gets the pages with page type ID.
        /// </summary>
        /// <param name="pageTypeID">The page type ID.</param>
        /// <param name="containerPageReferenceID">The container page reference ID.</param>
        /// <param name="language">The language.</param>
        /// <returns>Page with page type ID</returns>
        public static RawPage[] GetPagesWithPageTypeID(string pageTypeID, int containerPageReferenceID, string language)
        {
            RawPage[] searchResults = default(RawPage[]);
            LogHelper.LogWarning(
                string.Format(
                "PageTypeID:{0,-5} | ContainerPageReferenceID:{1,-5} | Language: {2}",
                pageTypeID,
                containerPageReferenceID,
                language),
                LogCategory.Content);            
                PageStoreService service = GetService();
                try
                {
                    LanguageSelector selector = new LanguageSelector();
                    selector.LanguageBranch = language;

                    PageReference rootPageReference = new PageReference();
                    rootPageReference.ID = containerPageReferenceID;

                    PropertyCriteria pageTypeCriteria = new PropertyCriteria();
                    pageTypeCriteria.Condition = CompareCondition.Equal;
                    pageTypeCriteria.Name = PageTypeId;
                    pageTypeCriteria.Required = true;
                    pageTypeCriteria.Type = PropertyDataType.PageType;
                    pageTypeCriteria.Value = pageTypeID;
                    PropertyCriteria[] searchCriterias = new PropertyCriteria[1];
                    searchCriterias[0] = pageTypeCriteria;
                    searchResults = service.FindPagesWithCriteria(rootPageReference, searchCriterias, language, selector, AccessLevel.Read);
                    return searchResults;
                }
                finally
                {
                    service.Dispose();
                }
        }

        /// <summary>
        /// Gets the bonus cheque rate codes.
        /// </summary>
        /// <returns>List of GetBomus Cheque Rate</returns>
        public static List<string> GetBonusChequeRateCodes()
        {
            List<string> bonusChequeRateCodes = new List<string>();
            bonusChequeRateCodes.Add("BC");
            return bonusChequeRateCodes;
        }
    }

    class ContentSSLPolicy : ICertificatePolicy
    {
        public bool CheckValidationResult(
              ServicePoint srvPoint
            , X509Certificate certificate
            , WebRequest request
            , int certificateProblem)
        {

            //Return True to force the certificate to be accepted.
            return true;

        } 
    } 
}
