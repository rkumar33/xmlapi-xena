using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Scandic.Services.ServiceAgents.OWSProxy.Availability;


namespace Scandic.Services.ServiceAgents
{
    class OWSUtility
    {
        #region Public Static Methods    
        /// <summary>
        /// Get the Name Service Proxy
        /// </summary>
        /// <param name="primaryLangaugeID">Primary LanguageID</param>
        /// <returns>NameService</returns>
  

        public static AvailabilityService GetAvailabilityService()
        {
            AvailabilityService service = new AvailabilityService();
            service.OGHeaderValue = GetAvailabilityHeader();
            service.Url = AppConstants.OWS_AVAILABILITY_SERVICE;
            return service;
        }

       

        #region RaiseOWSException
        public static void RaiseOWSException(string exceptionMessage, Exception generalException)
        {
            //throw new OWSException(exceptionMessage, generalException);
        }

        public static void RaiseRegionalAvailOWSException(RegionalAvailabilityResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }

                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
           // throw new OWSException(errorMessage, errorCode, exceptionData, true);                    
        }

        public static void RaiseGeneralAvailOWSException(AvailabilityResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }

                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
           // throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }
      
        #endregion RaiseOWSException

        #endregion Public Static Methods

        #region Private Static Methods
        private static string GetTransactionID()
        {
            //Getting some random transaction ID
            byte[] tID = new Byte[2];
            Random rnd = new Random(~unchecked((int)DateTime.Now.Ticks));
            rnd.NextBytes(tID);
            return tID[0].ToString() + tID[1].ToString();

        }

      

       

        private static OGHeader GetAvailabilityHeader()
        {
            OGHeader soapHeader = new OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new Scandic.Services.ServiceAgents.OWSProxy.Availability.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new Scandic.Services.ServiceAgents.OWSProxy.Availability.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

 
        #endregion Private Static Methods        
    
        //internal static void RaiseIgnoreOWSException(IgnoreBookingResponse response, Hashtable exceptionData)
        //{
        //    string errorMessage = string.Empty;
        //    string errorCode = string.Empty;

        //    if (response.Result.GDSError != null)
        //    {
        //        if (response.Result.GDSError.Value != null)
        //        {
        //            errorMessage = response.Result.GDSError.Value;
        //        }
        //        if (response.Result.GDSError.errorCode != null)
        //        {
        //            errorCode = response.Result.GDSError.errorCode;
        //        }
        //    }
        //    else if (response.Result.Text != null)
        //    {
        //        if (response.Result.Text.Length > 0)
        //        {
        //            errorMessage = response.Result.Text[0].Value;
        //        }
        //    }
        //    throw new OWSException(errorMessage, errorCode, exceptionData);
        //}

        //internal static void RaiseConfirmOWSException(ConfirmBookingResponse response, Hashtable exceptionData)
        //{
        //    string errorMessage = string.Empty;
        //    string errorCode = string.Empty;

        //    if (response.Result.GDSError != null)
        //    {
        //        if (response.Result.GDSError.Value != null)
        //        {
        //            errorMessage = response.Result.GDSError.Value;
        //        }
        //        if (response.Result.GDSError.errorCode != null)
        //        {
        //            errorCode = response.Result.GDSError.errorCode;
        //        }
        //    }
        //    else if (response.Result.Text != null)
        //    {
        //        if (response.Result.Text.Length > 0)
        //        {
        //            errorMessage = response.Result.Text[0].Value;
        //        }
        //    }
        //    throw new OWSException(errorMessage, errorCode, exceptionData);
        //}
    }
}
