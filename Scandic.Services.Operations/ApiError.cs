﻿// <copyright file="ApiError.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    /// <summary>
    /// Constants related to Scandic Error codes and
    /// related HTTP error codes
    /// </summary>
    public static class ApiError
    {
        /// <summary>
        /// Scandic Error Code for Server Error
        /// </summary>
        public const string ServerError = "1001";

        /// <summary>
        /// Scandic Error Code for Auth Failed
        /// </summary>
        public const string AuthFailed = "1002";

        /// <summary>
        /// Scandic Error Code for Invalid API Key
        /// </summary>
        public const string InvalidApiKey = "1003";

        /// <summary>
        /// Scandic Error Code for Invalid Input
        /// </summary>
        public const string InvalidInput = "1004";

        /// <summary>
        /// Scandic Error Code for Missing Input
        /// </summary>
        public const string MissingInput = "1005";
        
        /// <summary>
        /// Scandic Error Code for Empty Result Set
        /// </summary>
        public const string EmptyResult = "1006";

        /// <summary>
        /// HTTP Error Code for Content Unavailable
        /// </summary>
        public const string HttpOk = "200";

        /// <summary>
        /// HTTP Error Code for Content Unavailable
        /// </summary>
        public const string HttpNoContent = "204";

        /// <summary>
        /// HTTP Error Code for Bad Request
        /// </summary>
        public const string HttpBadRequest = "400";

        /// <summary>
        /// HTTP Error Code for missing Login input
        /// </summary>
        public const string HttpUnauthorized = "401";

        /// <summary>
        /// HTTP Error Code for Access Denied
        /// </summary>
        public const string HttpForbidden = "403";

        /// <summary>
        /// HTTP Error Code for Not Found
        /// </summary>
        public const string HttpNotFound = "404";

        /// <summary>
        /// HTTP Error Code for Server Error
        /// </summary>
        public const string HttpServerError = "500";

        /// <summary>
        /// HTTP Error Code for Server Unavailable
        /// </summary>
        public const string HttpServerUnavailable = "503";
    }
}
