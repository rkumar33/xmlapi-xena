﻿// <copyright file="AdministrationService.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.Threading.Tasks;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Properties;
    using Biz = Scandic.Services.BusinessEntity;
    #endregion

    /// <summary>
    /// All Administration related operations come under this service
    /// </summary>    
    [ServiceBehavior(
        Name = "AdministrationService",
        Namespace = @"http://api.scandichotels.com/schemas",
        InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Multiple)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AdministrationService : IAdministrationService
    {
        #region IAdministrationService Members

        /// <summary>
        /// Gets the partner details.
        /// </summary>
        /// <returns> returns partner details</returns>       
        [OperationBehavior]
        public PartnerDetails GetPartnerDetails()
        {
            try
            {
                ApiRequest request = new ApiRequest();
                string apiKey = request.ApiKey;

                if (string.IsNullOrEmpty(apiKey))
                {
                    throw Validate.Fault(ApiError.InvalidApiKey, ApiError.HttpBadRequest, Messages.ApiKeyEmpty);
                }

                Biz.Partner partner = AdminBusinessFactory
                    .CreateInstance(string.Empty)
                    .GetPartnerDetails(apiKey);

                if (partner != null)
                {
                    List<InterfaceDetails> interfaceList = partner.InterfaceList
                            .Select(interfaceDetails => new InterfaceDetails() { Name = interfaceDetails.Name }).ToList<InterfaceDetails>();
                    return new PartnerDetails
                    {
                        Operator = partner.Operator,
                        PartnerName = partner.Name,
                        ValidityFromDate = partner.ValidityStartDate,
                        ValidityEndDate = partner.ValidityEndDate,
                        InterfaceList = interfaceList
                    };
                }
                else
                {
                    throw Validate.Fault(ApiError.InvalidApiKey, ApiError.HttpBadRequest, Messages.ApiKeyInvalid);
                }
            }
            catch (Exception ex)
            {
                throw Validate.ExceptionHandler(ex);
            }
        }

        /// <summary>
        /// Clears an item from cache
        /// </summary>
        /// <param name="key">Key of the item to be cleared</param>
        /// <returns> Clears an item from cache, key=ALL flushes </returns>        
        [OperationBehavior]
        public string ResetCache(string key)
        {
            try
            {
                if (key.Equals("ALL", System.StringComparison.OrdinalIgnoreCase))
                {
                    Task.Factory.StartNew(() => CacheLoader.PreloadAll());
                    return string.Format("Cache will be refreshed in the next 5-10 minutes. Please wait.");
                }
                else
                {
                    if (CacheHelper.IsAvailable(key))
                    {
                        CacheHelper.Clear(key);
                        return string.Format("{0} cleared from cache", key);
                    }
                    else
                    {
                        return string.Format("{0} not available in cache", key);
                    }
                }
            }
            catch (Exception ex)
            {
                throw Validate.ExceptionHandler(ex);
            }
        }
        #endregion
    }
}
