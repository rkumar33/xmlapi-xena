﻿// <copyright file="AvailabilityService.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Contracts.Operation;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Properties;
    using Api = Scandic.Services.Contracts.Data;
    using Biz = Scandic.Services.BusinessEntity;
    using System.Xml.Serialization;


    /// <summary>
    /// All content (CMS) related operations come under this service
    /// </summary>
    [ServiceBehavior(Name = "AvailibilityService",
        Namespace = @"http://api.scandichotels.com/schemas",
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AvailabilityService : IAvailabilityService
    {
        #region Properties & Constants
        /// <summary>
        /// Gets or sets the user request copy
        /// </summary>
        private ApiRequest Request { get; set; }
        #endregion

        #region Operation Behaviors
        /// <summary>
        /// Lowests the rates for city.
        /// </summary>
        /// <param name="city">The marketing city id.</param>
        /// <param name="language">The language.</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationBehavior]
        public Api.LowestRatesForCity LowestRatesForCity(string city, string language, string id)
        {
            DateTime startDateTime = DateTime.Now;
            bool isException = false;
            try
            {
                this.Request = new ApiRequest();
                this.Request.ValidateLanguage();
                this.Request.ValidateCity();
                //Validate.ValidateOwsAvailability();
                Api.LowestRatesForCity hotelList = AvailabilityBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language, this.Request.ApiKey)
                    .GetLowestRateForCity(this.Request.City, id);
                if (hotelList.Hotels.Count == 0)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoSearchResults);
                }

                return hotelList;
            }
            catch (Exception ex)
            {
                if (Validate.IsOperaDown(ex))
                {
                    isException = true;
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
                    throw Validate.RaiseServiceUnavailableException();
                }
                else
                {
                    isException = true;
                    throw Validate.ExceptionHandler(ex);
                }
            }
            finally
            {
                DateTime endDateTime = DateTime.Now;
                string logMessage = string.Format("{0},{1},{2},{3}",
                                                    id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                LogHelper.LogInfo(logMessage, LogCategory.XMLAPILowestRate);
            }
        }

        /// <summary>
        /// Availabilities for certain hotels.
        /// </summary>
        /// <param name="hotels">The marketing city id.</param>
        /// <param name="arrivalDate">Arrival Date.</param>
        /// <param name="arrivalMonth">Arrival Month.</param>
        /// <param name="arrivalYear">Arrival Year.</param>
        /// <param name="departureDate">Departure Date</param>
        /// <param name="departureMonth">Departure Month.</param>
        /// <param name="departureYear">Departure Year</param>
        /// <param name="adultNumber1">Adult Number1</param>
        /// <param name="adultNumber2">Adult Number2</param>
        /// <param name="adultNumber3">Adult Number3</param>
        /// <param name="adultNumber4">Adult Number4</param>
        /// <param name="childNumber1">Child Number1</param>
        /// <param name="childNumber2">Child Number2</param>
        /// <param name="childNumber3">Child Number3</param>
        /// <param name="childNumber4">Child Number4</param>
        /// <param name="language">The language.</param>
        /// <param name="bookingCode">The booking code.</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationBehavior]
        public Api.AvailabilityForCertainHotels AvailabilityForCertainHotels(string hotels, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id)
        {

            DateTime startDateTime = DateTime.Now;
            bool isException = false;
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    id = string.Format("{0}-{1}", "GetAvailabilityForCertainHotels", Guid.NewGuid().ToString());
                }
                this.Request = new ApiRequest();
                this.Request.ValidateLanguage();
                this.Request.ValidateDates();
                this.Request.ValidateAgesAndCounts();
                this.Request.ValidateHotelList();
                //Validate.ValidateOwsAvailability();
                Api.AvailabilityForCertainHotels hotelList = AvailabilityBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language, this.Request.ApiKey)
                    .GetAvailabilityForCertainHotels(
                    this.Request.Hotels,
                    this.Request.ArrivalDate,
                    this.Request.DepartureDate,
                    this.Request.AdultCounts,
                    this.Request.ChildCounts,
                    this.Request.ChildrenAges,
                    this.Request.BookingCode,
                    id);
                if (hotelList.MessageonOnSingleHotelSearchNotAvailable != null && (!string.IsNullOrEmpty(hotelList.MessageonOnSingleHotelSearchNotAvailable)))
                {
                    hotelList.MessageonOnSingleHotelSearchNotAvailable = string.Format(Messages.AlternateHotelSuggestion, hotelList.MessageonOnSingleHotelSearchNotAvailable);
                }

                if (hotelList.Hotels.Count == 0)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoSearchResults);
                }

                return hotelList;
            }
            catch (Exception ex)
            {
                if (Validate.IsOperaDown(ex))
                {
                    isException = true;
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
                    throw Validate.RaiseServiceUnavailableException();
                }
                else
                {
                    isException = true;
                    throw Validate.ExceptionHandler(ex);
                }

            }
            finally
            {
                DateTime endDateTime = DateTime.Now;
                string logMessage = string.Format("{0},{1},{2},{3}",
                                                    id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                LogHelper.LogInfo(logMessage, LogCategory.XMLAPIForCertainHotels);
            }
        }

        /// <summary>
        /// Availabilities for A city.
        /// </summary>
        /// <param name="city">The marketing city id.</param>
        /// <param name="arrivalDate">Arrival Date.</param>
        /// <param name="arrivalMonth">Arrival Month.</param>
        /// <param name="arrivalYear">Arrival Year.</param>
        /// <param name="departureDate">Departure Date</param>
        /// <param name="departureMonth">Departure Month.</param>
        /// <param name="departureYear">Departure Year</param>
        /// <param name="adultNumber1">Adult Number1</param>
        /// <param name="adultNumber2">Adult Number2</param>
        /// <param name="adultNumber3">Adult Number3</param>
        /// <param name="adultNumber4">Adult Number4</param>
        /// <param name="childNumber1">Child Number1</param>
        /// <param name="childNumber2">Child Number2</param>
        /// <param name="childNumber3">Child Number3</param>
        /// <param name="childNumber4">Child Number4</param>
        /// <param name="language">The language.</param>
        /// <param name="bookingCode">The booking code.</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationBehavior]
        public Api.AvailabilityForCity AvailabilityForCity(string city, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id)
        {
            DateTime startDateTime = DateTime.Now;
            bool isException = false;
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    id = string.Format("{0}-{1}", "GetAvailabilityForCity", Guid.NewGuid().ToString());
                }
                this.Request = new ApiRequest();
                this.Request.ValidateDates();
                this.Request.ValidateLanguage();
                this.Request.ValidateCity();
                this.Request.ValidateAgesAndCounts();
                //Validate.ValidateOwsAvailability();
                Api.AvailabilityForCity hotelList = AvailabilityBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language, this.Request.ApiKey)
                    .GetAvailabilityForCity(
                    this.Request.City,
                    this.Request.ArrivalDate,
                    this.Request.DepartureDate,
                    this.Request.AdultCounts,
                    this.Request.ChildCounts,
                    this.Request.ChildrenAges,
                    this.Request.BookingCode,
                    id);
                if (hotelList.Hotels.Count == 0)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoSearchResults);
                }

                return hotelList;
            }
            catch (Exception ex)
            {
                if (Validate.IsOperaDown(ex))
                {
                    isException = true;
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
                    throw Validate.RaiseServiceUnavailableException();
                }
                else
                {
                    isException = true;
                    throw Validate.ExceptionHandler(ex);
                }
            }
            finally
            {
                DateTime endDateTime = DateTime.Now;
                string logMessage = string.Format("{0},{1},{2},{3}",
                                                    id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                LogHelper.LogInfo(logMessage, LogCategory.XMLAPIForCity);
            }
        }

        /// <summary>
        /// Rooms and Rates available in a hotel.
        /// </summary>
        /// <param name="hotel">The hotel id</param>
        /// <param name="arrivalDate">The arrival date</param>
        /// <param name="arrivalMonth">The arrival month</param>
        /// <param name="arrivalYear">The arrival year</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="departureMonth">The departure month</param>
        /// <param name="departureYear">The departure year</param>
        /// <param name="adultNumber1">Adult Count in Room1</param>
        /// <param name="adultNumber2">Adult Count in Room2</param>
        /// <param name="adultNumber3">Adult Count in Room3</param>
        /// <param name="adultNumber4">Adult Count in Room4</param>
        /// <param name="childNumber1">Children Count in Room1</param>
        /// <param name="childNumber2">Children Count in Room2</param>
        /// <param name="childNumber3">Children Count in Room3</param>
        /// <param name="childNumber4">Children Count in Room4</param>
        /// <param name="language">The language</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information on the speified hotel
        /// </returns>
        [OperationBehavior]
        public Api.RoomsAndRates GetRoomAndRates(string hotel, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id)
        {
            DateTime startDateTime = DateTime.Now;
            bool isException = false;
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    id = string.Format("{0}-{1}", "GetRoomAndRates", Guid.NewGuid().ToString());
                }
                this.Request = new ApiRequest();
                this.Request.ValidateLanguage();
                this.Request.ValidateDates();
                this.Request.ValidateHotel();
                this.Request.ValidateAgesAndCounts();
                //Validate.ValidateOwsAvailability();
                Biz.RoomsAndRates roomsAndRates = AvailabilityBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language, this.Request.ApiKey)
                    .GetRoomAndRates(
                    hotel,
                    this.Request.ArrivalDate,
                    this.Request.DepartureDate,
                    this.Request.AdultCounts,
                    this.Request.ChildCounts,
                    this.Request.ChildrenAges,
                    this.Request.BookingCode,
                    id);
                Biz.HotelNetwork network = ContentBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language).GetHotelNetwork();

                if (roomsAndRates != null && roomsAndRates.Rooms != null && roomsAndRates.RoomAndRates.Count > 0)
                {
                    List<Api.RoomAndRate> roomAndRates = new List<Api.RoomAndRate>();
                    foreach (Biz.RoomAndRate roomRate in roomsAndRates.RoomAndRates)
                    {
                        Api.RoomAndRate roomAndRate = new Api.RoomAndRate()
                        {
                            CurrenyCode = roomRate.CurrenyCode,
                            DeepLinkUrl = roomRate.DeepLinkUrl,
                            TotalPricePerNight = roomRate.TotalPricePerNight,
                            TotalPricePerStay = roomRate.TotalPricePerStay,
                            RoomRates = roomRate.RoomRates
                        };
                        roomAndRates.Add(roomAndRate);
                    }

                    return new Api.RoomsAndRates
                    {
                        HotelId = roomsAndRates.HotelId,
                        HotelName = roomsAndRates.HotelName,
                        DeeplinkURLHotelPage = roomsAndRates.DeeplinkURLHotelPage,
                        Operator = Messages.Provider,
                        Language = language,
                        Continent = network.GetContinentForHotel(roomsAndRates.HotelId.ToString()),
                        Rooms = roomsAndRates.Rooms,
                        RoomAndRates = roomAndRates
                    };
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoRoomRateMatch);
                }
            }
            catch (Exception ex)
            {
                if (Validate.IsOperaDown(ex))
                {
                    isException = true;
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
                    throw Validate.RaiseServiceUnavailableException();
                }
                else
                {
                    isException = true;
                    throw Validate.ExceptionHandler(ex);
                }
            }
            finally
            {
                DateTime endDateTime = DateTime.Now;
                string logMessage = string.Format("{0},{1},{2},{3}",
                                                    id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                LogHelper.LogInfo(logMessage, LogCategory.XMLAPIRoomsRate);
            }
        }

        /// <summary>
        /// Rooms and Rates available for hotels.
        /// </summary>
        /// <param name="hotel">The hotel id</param>
        /// <param name="arrivalDate">The arrival date</param>
        /// <param name="arrivalMonth">The arrival month</param>
        /// <param name="arrivalYear">The arrival year</param>
        /// <param name="departureDate">The departure date</param>
        /// <param name="departureMonth">The departure month</param>
        /// <param name="departureYear">The departure year</param>
        /// <param name="adultNumber1">Adult Count in Room1</param>
        /// <param name="adultNumber2">Adult Count in Room2</param>
        /// <param name="adultNumber3">Adult Count in Room3</param>
        /// <param name="adultNumber4">Adult Count in Room4</param>
        /// <param name="childNumber1">Children Count in Room1</param>
        /// <param name="childNumber2">Children Count in Room2</param>
        /// <param name="childNumber3">Children Count in Room3</param>
        /// <param name="childNumber4">Children Count in Room4</param>
        /// <param name="language">The language</param>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// Detailed information for searched hotels
        /// </returns>
        [OperationBehavior]
        public Api.AvailabilitiesForRoomsAndRates GetMultipleRoomsAndRates(string hotels, string arrivalDate, string arrivalMonth, string arrivalYear, string departureDate, string departureMonth, string departureYear, string adultNumber1, string adultNumber2, string adultNumber3, string adultNumber4, string childNumber1, string childNumber2, string childNumber3, string childNumber4, string language, string bookingCode, string id)
        {
            DateTime startDateTime = DateTime.Now;
            bool isException = false;
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    id = string.Format("{0}-{1}", "GetMultipleRoomsAndRates", Guid.NewGuid().ToString());
                }
                this.Request = new ApiRequest();
                this.Request.ValidateLanguage();
                this.Request.ValidateDates();
                this.Request.ValidateHotelList();
                this.Request.ValidateAgesAndCounts();
                //Validate.ValidateOwsAvailability();
                Biz.HotelRoomsAndRates hotelRoomsAndRates = AvailabilityBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language, this.Request.ApiKey)
                    .GetMultipleRoomAndRates(
                    this.Request.Hotels,
                    this.Request.ArrivalDate,
                    this.Request.DepartureDate,
                    this.Request.AdultCounts,
                    this.Request.ChildCounts,
                    this.Request.ChildrenAges,
                    this.Request.BookingCode,
                    id);
                Biz.HotelNetwork network = ContentBusinessFactory.CreateInstance(Messages.Provider, this.Request.Language).GetHotelNetwork();

                Api.AvailabilitiesForRoomsAndRates dcHotelRoomsAndRates = new Api.AvailabilitiesForRoomsAndRates();
                dcHotelRoomsAndRates.Hotels = new List<Api.HotelRoomRates>();

                if (hotelRoomsAndRates != null && hotelRoomsAndRates.Hotel != null && hotelRoomsAndRates.Hotel.Count > 0)
                {
                    foreach (var RoomsRate in hotelRoomsAndRates.Hotel)
                    {
                        if (RoomsRate != null && RoomsRate.RoomsAndRates.RoomAndRates != null && RoomsRate.RoomsAndRates.RoomAndRates != null && RoomsRate.RoomsAndRates.RoomAndRates.Count > 0)
                        {
                            List<Api.RoomAndRate> roomAndRates = new List<Api.RoomAndRate>();
                            foreach (Biz.RoomAndRateWithTaxes roomRate in RoomsRate.RoomsAndRates.RoomAndRates)
                            {
                                List<Api.Tax> listTax = new List<Api.Tax>();

                                foreach (var tx in roomRate.Taxes)
                                {
                                    Api.Tax tax = new Api.Tax()
                                    {
                                        amount = tx.Amount,
                                        type = tx.Type
                                    };
                                    listTax.Add(tax);
                                }

                                Api.RoomAndRateWithTaxes roomAndRate = new Api.RoomAndRateWithTaxes()
                                {
                                    CurrenyCode = roomRate.CurrenyCode,
                                    DeepLinkUrl = roomRate.DeepLinkUrl,
                                    TotalPricePerNight = roomRate.TotalPricePerNight,
                                    TotalPricePerStay = roomRate.TotalPricePerStay,
                                    RoomRates = roomRate.RoomRates,
                                    TotalBaseRatePerStay = roomRate.TotalBaseRatePerStay,
                                    TotalBaseRatePerNight = roomRate.TotalBaseRatePerNight,
                                    Taxes = listTax
                                };
                                roomAndRates.Add(roomAndRate);
                            }

                            Api.RoomsAndRates RoomsandRates = new Api.RoomsAndRates
                            {
                                HotelId = RoomsRate.RoomsAndRates.HotelId,
                                HotelName = RoomsRate.RoomsAndRates.HotelName,
                                DeeplinkURLHotelPage = RoomsRate.RoomsAndRates.DeeplinkURLHotelPage,
                                Operator = Messages.Provider,
                                Language = language,
                                Continent = network.GetContinentForHotel(RoomsRate.RoomsAndRates.HotelId.ToString()),
                                Rooms = RoomsRate.RoomsAndRates.Rooms,
                                RoomAndRates = roomAndRates
                            };

                            Api.HotelRoomRates hotelRoomRate = new Api.HotelRoomRates()
                           {
                               Id = Convert.ToString(RoomsandRates.HotelId),
                               RoomsAndRates = RoomsandRates
                           };

                            dcHotelRoomsAndRates.Hotels.Add(hotelRoomRate);
                        }
                        else
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                            throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoRoomRateMatch);
                        }
                    }

                    return dcHotelRoomsAndRates;
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoRoomRateMatch);
                }
            }
            catch (Exception ex)
            {
                if (Validate.IsOperaDown(ex))
                {
                    isException = true;
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
                    throw Validate.RaiseServiceUnavailableException();
                }
                else
                {
                    isException = true;
                    throw Validate.ExceptionHandler(ex);
                }
            }
            finally
            {
                DateTime endDateTime = DateTime.Now;
                string logMessage = string.Format("{0},{1},{2},{3}",
                                                    id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                LogHelper.LogInfo(logMessage, LogCategory.XMLAPIRoomsRate);
            }
        }


        #endregion
    }
}
