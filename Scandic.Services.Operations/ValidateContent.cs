﻿// <copyright file="ValidateContent.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    using System;
    using System.ServiceModel;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Properties;
    using System.ServiceModel.Web;

    /// <summary>
    /// Validation helpers for the services
    /// </summary>    
    public static partial class Validate
    {
        /// <summary>
        /// Validates a language input, against a list of
        /// supported languages. Optionally throws a fault
        /// if the input is not valid.
        /// </summary>
        /// <param name="languageId">Language ID to be validated</param>
        /// <param name="raiseFault">Indicates if a fault is to be thrown</param>
        /// <returns>True for valid</returns>       
        public static bool Language(string languageId, bool raiseFault)
        {
            bool isValid = false;
            string[] strlang = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).ToLower().Split(',');
            if (strlang.Length > 0)
            {
                for (int i = 0; i < strlang.Length; i++)
                {
                    if (languageId.ToLower() == strlang[i].ToLower())
                    {
                        isValid = true;
                        break;
                    }
                }
            }

            //if (languageId.ToLower() != "ru" && languageId.ToLower() != "ru-" && languageId.ToLower() != "ru-r")
            //    isValid = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).ToLower().Contains(languageId.ToLower());

            if (!isValid && raiseFault)
            {
                throw InvalidInputFault(Messages.InvalidLanguage);
            }

            return isValid;
        }

        /// <summary>
        /// Validates a country input, and checks for
        /// null or empty. Optionally throws a fault
        /// if the input is not valid.
        /// </summary>
        /// <param name="countryId">Country ID to be validated</param>
        /// <param name="raiseFault">Indicates if a fault is to be thrown</param>
        /// <returns>True for valid</returns>      
        public static bool Country(string countryId, bool raiseFault)
        {
            bool isValid = !string.IsNullOrEmpty(countryId);
            if (!isValid && raiseFault)
            {
                throw MissingInputFault(Messages.MissingCountry);
            }

            if (!Network().IsCountryValid(countryId))
            {
                throw InvalidInputFault(Messages.InvalidCountry);
            }

            return isValid;
        }
        public static bool Country(string language, string countryId, bool raiseFault)
        {
            bool isValid = !string.IsNullOrEmpty(countryId);
            if (!isValid && raiseFault)
            {
                throw MissingInputFault(Messages.MissingCountry);
            }

            if (!Network(language).IsCountryValid(countryId))
            {
                throw InvalidInputFault(Messages.InvalidCountry);
            }

            return isValid;
        }

        /// <summary>
        /// Validates a city input, and checks for
        /// null or empty. Optionally throws a fault
        /// if the input is not valid.
        /// </summary>
        /// <param name="cityId">City ID to be validated</param>
        /// <param name="raiseFault">Indicates if a fault is to be thrown</param>
        /// <returns>True for valid</returns>      
        public static bool City(string cityId, bool raiseFault)
        {
            bool isValid = !string.IsNullOrEmpty(cityId);
            if (!isValid && raiseFault)
            {
                throw MissingInputFault(Messages.MissingCity);
            }

            if (!Network().IsCityValid(cityId))
            {
                throw InvalidInputFault(Messages.InvalidCity);
            }

            return isValid;
        }

        public static bool City(string language, string cityId, bool raiseFault)
        {
            bool isValid = !string.IsNullOrEmpty(cityId);
            if (!isValid && raiseFault)
            {
                throw MissingInputFault(Messages.MissingCity);
            }

            if (!Network(language).IsCityValid(cityId))
            {
                throw InvalidInputFault(Messages.InvalidCity);
            }

            return isValid;
        }
        /// <summary>
        /// Validates a hotel input, and checks for
        /// null or empty. Optionally throws a fault
        /// if the input is not valid.
        /// </summary>
        /// <param name="hotelId">Hotel ID to be validated</param>
        /// <param name="raiseFault">Indicates if a fault is to be thrown</param>
        /// <param name="checkBookable">Indicates if bookability of hotel should be checked</param>
        /// <returns>True for valid</returns>       
        public static bool Hotel(string hotelId, bool raiseFault, bool checkBookable = false)
        {
            bool isValid = !string.IsNullOrEmpty(hotelId);

            if (isValid)
            {
                if (checkBookable)
                {
                    isValid = Network().IsHotelValidAndBookable(hotelId);
                }
                else
                {
                    isValid = Network().IsHotelValid(hotelId);
                }
            }
            else
            {
                if (raiseFault)
                {
                    throw MissingInputFault(Messages.MissingHotel);
                }
            }

            if (!isValid && raiseFault)
            {
                throw InvalidInputFault(Messages.InvalidHotel);
            }

            return isValid;
        }



        /// <summary>
        /// Helper to throw a fault exception
        /// </summary>
        /// <param name="scandicError">Scandic error code</param>
        /// <param name="httpError">HTTP error code</param>
        /// <param name="message">Fault message</param>
        /// <returns>Fault exception object</returns>       
        public static FaultException<ServiceFault> Fault(string scandicError, string httpError, string message)
        {
            if (!string.Equals(message, "No Rooms and rates matched your query", StringComparison.InvariantCultureIgnoreCase))
                LogHelper.LogError(string.Format("{0} | {1}", scandicError, message), LogCategory.Service);
            return new FaultException<ServiceFault>(
                new ServiceFault { ErrorCode = scandicError, HttpErrorCode = httpError, ErrorMessage = message },
                new FaultReason(new FaultReasonText(message, "en-US")),
                new FaultCode(scandicError));
        }

        /// <summary>
        /// Helper to throw a fault exception
        /// </summary>
        /// <param name="message">Fault message</param>
        /// <returns>Fault exception object</returns>
        public static FaultException<ServiceFault> InvalidInputFault(string message)
        {
            //Fix for artf1280894
            return Fault(ApiError.InvalidInput, ApiError.HttpServerError, message);
        }

        /// <summary>
        /// Helper to throw a fault exception
        /// </summary>
        /// <param name="message">Fault message</param>
        /// <returns>Fault exception object</returns>
        public static FaultException<ServiceFault> MissingInputFault(string message)
        {
            return Fault(ApiError.MissingInput, ApiError.HttpServerError, message);
        }


        /// <summary>
        /// Generic exception handler for service unavailable
        /// and other unhandled exceptions
        /// </summary>
        /// <param name="ex">The unknown exception that was caught</param>
        /// <returns>FaultException to be returned to the user</returns>
        public static FaultException<ServiceFault> ExceptionHandler(Exception ex)
        {
            LogHelper.LogException(ex, LogCategory.Service);
            throw ex;
        }

        /// <summary>
        /// Servers the unavailable fault.
        /// </summary>
        /// <returns>ServiceFault for server unavailable</returns>
        private static FaultException<ServiceFault> ServerUnavailableFault()
        {
            return Fault(ApiError.HttpServerUnavailable, ApiError.HttpServerUnavailable, Messages.OwsUnavailable);
        }

        /// <summary>
        /// Availability service status
        /// </summary>
        /// <returns>Returns true if availability service is down</returns>
        public static bool IsOperaDown(Exception ex)
        {
            bool IsOperaDown = false;
            var exp = ex as System.Net.WebException;
            if (exp != null && ((System.Net.HttpWebResponse)exp.Response).StatusCode == System.Net.HttpStatusCode.ServiceUnavailable)
            {
                IsOperaDown = true;
            }

            //if (string.Equals(ex.Message, ConfigHelper.GetValue(ConfigKeys.ServiceTimeoutMessage), StringComparison.InvariantCultureIgnoreCase) ||
            //           string.Equals(ex.Message, "Unable to connect to the remote server", StringComparison.InvariantCultureIgnoreCase) ||
            //    string.Equals(ex.Message, "The request failed with HTTP status 503: Service Unavailable.", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    IsOperaDown = true;
            //}
            return IsOperaDown;
        }

        /// <summary>
        /// Availability Service Unavailable exception.
        /// </summary>
        /// <returns>Raise Availability service down exception</returns>
        public static FaultException<ServiceFault> RaiseServiceUnavailableException()
        {
            try
            {
                throw Validate.ServerUnavailableFault();
            }
            catch (Exception expn)
            {
                throw Validate.ExceptionHandler(expn);
            }
        }

        /// <summary>
        /// Servers the unavailable fault.
        /// </summary>
        /// <returns>ServiceFault for server unavailable</returns>
        private static FaultException<ServiceFault> ServiceUnavailableFault()
        {
            return Fault(ApiError.ServerError, ApiError.HttpServerUnavailable, Messages.ServiceUnavailable);
        }

        /// <summary>
        /// Default hotel network, to be used for ID validations only
        /// </summary>
        /// <returns>Hotel network with default initializers</returns>
        private static HotelNetwork Network()
        {
            return ContentBusinessFactory
                .CreateInstance(Messages.Provider, ConfigHelper.GetValue(ConfigKeys.DefaultLanguage))
                .GetHotelNetwork();
        }

        private static HotelNetwork Network(string language)
        {
            return ContentBusinessFactory
                .CreateInstance(Messages.Provider, language)
                .GetHotelNetwork();
        }
        /// <summary>
        /// Partner details for a given API Key
        /// </summary>
        /// <param name="apiKey">API Key to search partner for</param>
        /// <returns>Partner details for the given API Key</returns>
        private static Partner PartnerDetails(string apiKey)
        {
            return ContentBusinessFactory
                .CreateInstance(Messages.Provider, ConfigHelper.GetValue(ConfigKeys.DefaultLanguage))
                .GetPartnerDetails(apiKey);
        }
    }
}
