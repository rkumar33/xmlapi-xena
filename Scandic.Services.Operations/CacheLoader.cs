﻿// <copyright file="CacheLoader.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Properties;
   
    /// <summary>
    /// Cache Loader class
    /// </summary>
    public class CacheLoader
    {
        /// <summary>
        /// PreLaodall method
        /// </summary>
        public static void PreloadAll()
        {
          AdminBusinessFactory.CreateInstance(string.Empty).RefreshCache();
          ContentBusinessFactory.CreateInstance(string.Empty, string.Empty).RefreshCache();
          AvailabilityBusinessFactory.CreateInstance(string.Empty, string.Empty, string.Empty).RefreshCache();
        }
    }
}
