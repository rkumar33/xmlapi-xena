﻿// <copyright file="ContentService.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Channels;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Properties;
    using Biz = Scandic.Services.BusinessEntity;
    using System.ServiceModel.Web;

    #endregion

    /// <summary>
    /// All content (CMS) related operations come under this service
    /// </summary>
    [ServiceBehavior(
        Name = "ContentService",
        Namespace = @"http://api.scandichotels.com/schemas",
        InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Multiple)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ContentService : IContentService
    {
        #region Content Service
        /// <summary>
        /// The default language to fall back to
        /// in case a language is not specified
        /// </summary>
        private string defaultLanguage = ConfigHelper.GetValue(ConfigKeys.DefaultLanguage);

        /// <summary>
        /// Get a list of countries where Scandic has operations
        /// </summary>
        /// <param name="language">Code for the language in which results are sought (default: en)</param>
        /// <returns>List of countries where Scandic has operations</returns>
        [OperationBehavior]
        public CountryList GetCountryList(string language)
        {
            try
            {
                if (string.IsNullOrEmpty(language))
                {
                    language = this.defaultLanguage;
                }

                Validate.Language(language, true);
                Biz.HotelNetwork network = ContentBusinessFactory.CreateInstance(Messages.Provider, language).GetHotelNetwork();

                var countryList = network.AllCountries
                    .Select(Country => new Country() { CountryId = Country.Id, Name = Country.Name, Continent = Country.Continent }).ToList<Country>();

                if (countryList.Count > 0)
                {
                    return new CountryList
                    {
                        Operator = Messages.Provider,
                        Language = language,
                        Countries = countryList
                    };
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoCountryMatch);
                }
            }
            catch (Exception ex)
            {
                throw Validate.ExceptionHandler(ex);
            }
        }

        /// <summary>
        /// Get a list of cities where Scandic has operations,
        /// in the specified country
        /// </summary>
        /// <param name="country">ID of Country in which cities are to be listed</param>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>Collection of cities</returns>
        [OperationBehavior]
        public CityList GetCityList(string country, string language)
        {
            try
            {
                if (string.IsNullOrEmpty(language))
                {
                    language = this.defaultLanguage;
                }

                Validate.Language(language, true);
                Validate.Country(language, country, true);
                Biz.HotelNetwork network = ContentBusinessFactory.CreateInstance(Messages.Provider, language).GetHotelNetwork();

                var cityList = network.CitiesInCountry(country)
                    .Select(city => new City() { Id = city.Id, Name = city.Name }).ToList();
                if (cityList.Count > 0)
                {
                    return new CityList
                    {
                        Operator = Messages.Provider,
                        Language = language,
                        Continent = network.GetContinentForCountry(country),
                        CountryId = country,
                        Cities = cityList
                    };
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoCityMatch);
                }
            }
            catch (Exception ex)
            {
                throw Validate.ExceptionHandler(ex);
            }
        }

        /// <summary>
        /// Searches for Scandic hotels in a city
        /// </summary>
        /// <param name="city">Name of the city in which hotels are to be searched</param>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>List of hotels matching the search</returns>
        [OperationBehavior]
        public HotelList GetHotelList(string city, string language)
        {
            try
            {
                if (string.IsNullOrEmpty(language))
                {
                    language = this.defaultLanguage;
                }

                Validate.Language(language, true);
                Validate.City(language, city, true);
                Biz.HotelNetwork network = ContentBusinessFactory.CreateInstance(Messages.Provider, language).GetHotelNetwork();

                var hotelList = network.HotelsInCity(city)
                    .Select(Hotel => new Scandic.Services.Contracts.Data.Hotel() { HotelId = Hotel.Id, HotelName = Hotel.Name, HotelType = Hotel.PropertyType, IsBookable = Hotel.IsBookable, PostalCity = Hotel.PostalCity, APITaxRates = Hotel.APITaxRates }).ToList<Scandic.Services.Contracts.Data.Hotel>();
                if (hotelList.Count > 0)
                {
                    return new HotelList
                    {
                        Operator = Messages.Provider,
                        Language = language,
                        Continent = network.GetContinentForHotel(hotelList[0].HotelId),
                        MarketingCityId = city,
                        Hotels = hotelList
                    };
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoHotelMatch);
                }
            }
            catch (Exception ex)
            {
                throw Validate.ExceptionHandler(ex);
            }
        }

        /// <summary>
        /// Gets detailed information on a specified hotel
        /// </summary>
        /// <param name="hotel">ID of the hotel for which details are required</param>
        /// <param name="language">Code for the language in which results are sought (e.g. en)</param>
        /// <returns>Detailed information on the speified hotel</returns>
        [OperationBehavior]
        public HotelInformation GetHotelInformation(string hotel, string language)
        {
            try
            {
                if (string.IsNullOrEmpty(language))
                {
                    language = this.defaultLanguage;
                }

                Validate.Language(language, true);
                Validate.Hotel(hotel, true);
                Biz.HotelDetailsList hotels = BusinessFactory.ContentBusinessFactory.CreateInstance(Messages.Provider, language).GetHotelDetails();
                Biz.HotelNetwork network = ContentBusinessFactory.CreateInstance(Messages.Provider, language).GetHotelNetwork();
                if (hotels.Hotels.Count > 0)
                {
                    var oneHotel = hotels.Hotels.Single(htl => htl.HotelId == hotel);
                    if (oneHotel == null)
                    {
                        throw Validate.InvalidInputFault(Messages.InvalidHotel);
                    }
                    else
                    {
                        return new HotelInformation
                        {
                            Language = language,
                            Operator = Messages.Provider,
                            Continent = network.GetContinentForHotel(hotel),
                            Hotel = oneHotel
                        };
                    }
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpNotFound, Messages.NoHotelMatch);
                }
            }
            catch (Exception ex)
            {
                throw Validate.ExceptionHandler(ex);
            }
        }

        #endregion
    }
}
