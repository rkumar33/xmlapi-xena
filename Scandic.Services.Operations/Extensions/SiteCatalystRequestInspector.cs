﻿// <copyright file="SiteCatalystRequestInspector.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Operations.Extensions
{
    using System;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;
    using System.Threading;
    using System.Threading.Tasks;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Framework;
    using Biz = Scandic.Services.BusinessEntity; 

    /// <summary>
    /// Site Catalyst Request Inspector
    /// </summary>
    public class SiteCatalystRequestInspector : IDispatchMessageInspector
    {
        #region Properties & Constants

        /// <summary>
        /// constant for availability
        /// </summary>
        private const string AVAILABILITY = "availability";

        /// <summary>
        /// Constant for Lowest Rate for a city.
        /// </summary>
        private const string LOWESTRATEFORCITY = "lowestratesforcity";

        /// <summary>
        /// const for Event50. Total request event variable
        /// </summary>
        private const string EVENTFORMAT = "event50%2Cevent{0}";

        /// <summary>
        /// const for NS
        /// </summary>
        private const string NS = "scandichotelsab";

        private const string RESETCACHE = "resetcache";

        private const string V2 = "v2";

        /// <summary>
        /// const for C51
        /// </summary>
        private const string REQUIREDPARAMS = "ev={0}&gn={1}|{2}&c24={1}&v49={1}&vid={1}&ns=scandichotelsab&c25={2}&v50={2}&c3={3}&v3={3}&c5={4}&v22={4}&c6={5}&v23={5}&c51={6}&v51={6}";

        /// <summary>
        /// Mapping Events/APIs to IDs
        /// </summary>
        private NameValueCollection eventId = new NameValueCollection 
                                                            {
                                                             { "/xmlapi/v1/countries", "1" },
                                                             { "/xmlapi/v1/cities", "2" },
                                                             { "/xmlapi/v1/hotels", "3" },
                                                             { "/xmlapi/v1/hotel", "4" },
                                                             { "/xmlapi/v1/availability/forcity", "5" },
                                                             { "/xmlapi/v1/availability/forcertainhotels", "6" },
                                                             { "/xmlapi/v1/availability/foronehotel", "7" },
                                                             { "/xmlapi/v1/availability/lowestratesforcity", "8" },
                                                             { "/xmlapi/v1/availability/AvailabilityForHotels", "9" },
                                                             { "/xmlapi/v1/admin/validate", "12" }
                                                            };

        /// <summary>
        /// Mapping events/APIs to Names
        /// </summary>
        private string[] eventName = { "CountryList",
                                       "CityList",
                                       "HotelList",
                                       "GetHotelInformation",
                                       "AvailabilityForACity",
                                       "AvailabilityForCertainHotel",
                                       "RoomsAndRates",
                                       "LowestRatesForCity",
                                       "GetMultipleRoomsAndRates",
                                       "AvailabilityForACityWithBookingCode",
                                       "AvailabilityForCertainHotelWithBookingCode",
                                       "RoomsAndRatesWithBookingCode",
                                       "ValidateAPIKey",
                                       "NA"
                                      };       

        /// <summary>
        /// ApiRequest variable
        /// </summary>
        private ApiRequest apiRequest = null;

        #endregion

        #region IDispatchMessageInspector Members

        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.         
        /// </summary>
        /// <param name="request">The request message</param>
        /// <param name="channel">The incoming channel</param>
        /// <param name="instanceContext">The current service instance</param>
        /// <returns>The object used to correlate state.</returns>
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            this.apiRequest = new ApiRequest();
            if (!string.IsNullOrEmpty(this.apiRequest.ApiKey))
            {
                //Do not track RESET Cache.
                if (OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower().Contains(RESETCACHE) ||
                     OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower().Contains(V2))
                    return null;

                if (OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower().Contains(AVAILABILITY))
                {
                    if (!OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower().Contains(LOWESTRATEFORCITY))
                    {
                        this.apiRequest.ValidateDates();
                    }
                }

                this.SendRequestToSiteCatalyst();
            }

            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way</param>
        /// <param name="correlationState">The correlation object returned from the AfterReceiveRequest method.</param>
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
        }

        #endregion

        #region SiteCatalyst
        /// <summary>
        /// Gets the time of request.
        /// </summary>
        /// <param name="date">The date .</param>
        /// <returns>Returns the date time of the requested date. </returns>
        private static string GetTimeOfRequest(DateTime date)
        {
            DateTime roundDown = date;
            if (date.TimeOfDay.Minutes < 30)
            {
                return roundDown.AddMinutes(-roundDown.Minute).ToShortTimeString().Replace(" ", "%3A");
            }
            else
            {
                return roundDown.AddMinutes(-roundDown.Minute + 30).ToShortTimeString().Replace(" ", "%3A");
            }
        }

        /// <summary>
        /// Finds if weekend.
        /// </summary>
        /// <param name="date">The date of the request to service.</param>
        /// <returns>Returns if its weekend or weekday for the request date</returns>
        private static string FindIfWeekend(DateTime date)
        {
            return (date.DayOfWeek.Equals(DayOfWeek.Saturday) || date.DayOfWeek.Equals(DayOfWeek.Sunday)) 
                ? "Weekend" : "Weekday";
        }

        /// <summary>
        /// Gets the partner id.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        /// <returns>Returns the Partner Id</returns>
        private static string GetPartnerId(string apiKey)
        {
            Biz.Partner partner = AdminBusinessFactory
               .CreateInstance(string.Empty)
               .GetPartnerDetails(apiKey);
            return (partner != null && !partner.IsTrackingCodeDisabled) ? partner.TrackingCode : string.Empty;
        }

        /// <summary>
        /// Gets the name of the site catalyst event.
        /// </summary>
        /// <returns>
        /// returns site catalyst event name
        /// </returns>
        private int GetEventId()
        {
            string request = OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower();
            string id = this.eventId[request.Split('?')[0]];
            int eventId;
            if (Int32.TryParse(id, out eventId))
            {
                if (eventId > 4 && eventId < 8 && request.Contains("bookingcode"))
                {
                    eventId += 4;
                }
            }

            eventId = eventId == 0 ? 14 : eventId;
            return eventId;
        }

        /// <summary>
        /// Get version of the API
        /// </summary>
        /// <returns>Version of the API requested</returns>
        private string GetVersion()
        {
            string request = OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower();
            return request.Contains("/xmlapi/v") ?
                request.Substring(request.IndexOf("/xmlapi/v") + 9, 1) :
                string.Empty;
        }

        /// <summary>
        /// Sends the request to site catalyst.
        /// </summary>        
        private void SendRequestToSiteCatalyst()
        {
            string catalystUri = string.Format("{0}/b/ss/{1}/0?", ConfigHelper.GetValue(ConfigKeys.UrlToSiteCatalyst), ConfigHelper.GetValue(ConfigKeys.ReportSuiteId));
            string queryString = this.SetSiteCatalystVariables();
            Task.Factory.StartNew(() => this.CallSiteCatalyst(catalystUri + queryString));
        }

        /// <summary>
        /// Sets the site catalyst variables.
        /// </summary>
        /// <returns>Returns the Site Catalyst Variables</returns>
        private string SetSiteCatalystVariables()
        {
            return this.SetRequiredVariablesForSiteCatalyst() + this.SetOptionalVariablesForSiteCatalyst();
        }

        /// <summary>
        /// Sets the required variables for site catalyst.
        /// </summary>
        /// <returns>Returns the required variables for site catalyst</returns>
        private string SetRequiredVariablesForSiteCatalyst()
        {
            string partnerId = GetPartnerId(this.apiRequest.ApiKey);
            string weekEnd = FindIfWeekend(DateTime.Today);
            string day = DateTime.Today.DayOfWeek.ToString();
            string timeOfRequest = GetTimeOfRequest(DateTime.Now);
            int eventId = this.GetEventId();
            string requiredVariables = string.Format(
                CultureInfo.InvariantCulture,
                REQUIREDPARAMS,
                string.Format(EVENTFORMAT, eventId.ToString()),
                partnerId,
                this.eventName[eventId - 1],
                weekEnd,
                day,
                timeOfRequest,
                this.GetVersion());
            return requiredVariables;
        }

        /// <summary>
        /// Sets the other variables for site catalyst.
        /// </summary>
        /// <returns>Returns the other variables for site catalyst</returns>
        private string SetOptionalVariablesForSiteCatalyst()
        {
            string noOfnights = string.Empty;
            string state = string.Empty;
            string noOfRooms = string.Empty;
            string arrivalDate = string.Empty;
            string departureDate = string.Empty;
            string noOfAdults = string.Empty;
            string noOfChildern = string.Empty;
            string bookingCode = string.Empty;
            string language = string.Empty;
            string city = string.Empty;
            if (this.apiRequest != null)
            {
                arrivalDate = this.CreateDateString(this.apiRequest.ArrivalDate);
                departureDate = this.CreateDateString(this.apiRequest.DepartureDate);
                noOfAdults = this.apiRequest.AdultCount.ToString();
                if (noOfAdults.Equals("0"))
                {
                    noOfAdults = string.Empty;
                }

                noOfChildern = this.apiRequest.ChildCount.ToString();
                if (noOfChildern.Equals("0"))
                {
                    noOfChildern = string.Empty;
                }

                noOfRooms = this.apiRequest.NoOfRooms.ToString();
                if (noOfRooms.Equals("0"))
                {
                    noOfRooms = string.Empty;
                }

                noOfnights = this.GetNoOfNights();
                if (noOfnights.Equals("0"))
                {
                    noOfnights = string.Empty;
                }

                bookingCode = (this.apiRequest.BookingCode != null) ? this.apiRequest.BookingCode.ToString() : string.Empty;
                language = (this.apiRequest.Language != null) ? this.apiRequest.Language : "en";
                city = (this.apiRequest.City != null) ? this.apiRequest.City : string.Empty;
                state = (this.apiRequest.Country != null) ? this.apiRequest.Country : string.Empty;
            }

            string otherVariables = string.Format(
                CultureInfo.InvariantCulture, 
                "&c12={0}&v12={0}&c13={1}&v13={1}&c14={2}&v14={2}&c15={3}&v15={3}&c16={4}&v16={4}&c17={2}-{5}&v17={2}-{5}&state={6}&pl={7}&c26={8}&v2={8}&c27={9}&v47={9}&c28={10}&v48={10}",
                noOfnights,
                noOfRooms,
                arrivalDate,
                noOfAdults,
                noOfChildern,
                departureDate,
                state,
                this.FormatHotelIds(),
                bookingCode,
                language,
                city);
                
            return otherVariables;
        }

        /// <summary>
        /// Calls the site catalyst.
        /// </summary>
        /// <param name="urltositeCatalyst">The urltosite catalyst.</param>
        private void CallSiteCatalyst(string urltositeCatalyst)
        {
            try
            {
                HttpWebRequest catalystRequest = (HttpWebRequest)WebRequest.Create(urltositeCatalyst);
                catalystRequest.KeepAlive = false;
                catalystRequest.Headers.Add("X-Forwarded-For", "127.0.0.1");
                //old code
                //ThreadPool.QueueUserWorkItem(o => { catalystRequest.GetResponse(); });
                //New Async Code
                catalystRequest.BeginGetResponse(null,null);
                LogHelper.LogInfo(urltositeCatalyst, LogCategory.Service);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, LogCategory.Service);
            }
        }

        #region Helper Methods

        /// <summary>
        /// Gets the no of nights.
        /// </summary>
        /// <returns>Returns the number of nights</returns>
        private string GetNoOfNights()
        {
            return this.apiRequest.DepartureDate.Subtract(this.apiRequest.ArrivalDate).Days.ToString();
        }

        /// <summary>
        /// Creates the date string.
        /// </summary>
        /// <param name="date">The date of the request service.</param>
        /// <returns>The Date of the requested service</returns>
        private string CreateDateString(DateTime date)
        {
            if (date.Equals(default(DateTime)))
            {
                return string.Empty;
            }

            return string.Format(CultureInfo.InvariantCulture, "{0}-{1}-{2}", date.Year.ToString(), date.Month.ToString(), date.Day.ToString());
        }

        /// <summary>
        /// Formats the hotel ids.
        /// </summary>
        /// <returns>Returns the formatted hotel Ids</returns>
        private string FormatHotelIds()
        {
            if (this.apiRequest.Hotels != null)
            {
                return ";" + string.Join(",;", this.apiRequest.Hotels);
            }
            else if (!string.IsNullOrEmpty(this.apiRequest.Hotel))
            {
                return ";" + this.apiRequest.Hotel;
            }

            return string.Empty;
        }

        #endregion
      
        #endregion
    }
}
