﻿// <copyright file="RequestInspector.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Operations.Extensions
{
    using System;
    using System.Collections.Specialized;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;
    using System.ServiceModel.Web;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Operations.Properties;
    using Biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// class to inspect request header.
    /// </summary>    
    public class RequestInspector : IDispatchMessageInspector
    {    
        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)"/> method.</returns>        
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            if (!OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.ToLower().Contains("help"))
            {
                AuthenticateAndAuthorizePartner();
            }

            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)"/> method.</param>        
        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            //// throw new NotImplementedException();
        }

        #region Partner Authentication And Authorization

        /// <summary>
        /// Validates the API key.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        /// <param name="partner">The partner.</param>
        /// <returns>
        /// checks if the api key is valid
        /// </returns>
        private static bool ValidateAPIKey(string apiKey, Biz.Partner partner)
        {
            bool isAPIKeyValid = false;
            if (!string.IsNullOrEmpty(apiKey))
            {
                isAPIKeyValid = AdminBusinessFactory
                    .CreateInstance(string.Empty)
                    .GetPartnerDetails(apiKey) != null;
            }

            return isAPIKeyValid;
        }

        /// <summary>
        /// Authorizes the API access for partner.
        /// </summary>
        /// <param name="partner">The partner.</param>
        /// <returns>
        /// returns a boolean to indicate if the partner has access to the API
        /// </returns>
        private static bool AuthorizeApiAccessForPartner(Biz.Partner partner)
        {
            bool hasAccessToApi = false;
            //// OperationContext.Current.IncomingMessageHeaders.To = new System.Uri("http://devapi.scandichotels.com/XMLAPI/V1/availability/ForCity?city=STOCKHOLM&AD=15&AM=09&AY=2011&DD=16&DM=09&DY=2011&AN1=1&language=en&bookingCode=PROMO815");
            string apiName = OperationContext.Current.IncomingMessageHeaders.To.PathAndQuery.Remove(0, 1).Split('?')[0];
            if (!string.IsNullOrEmpty(apiName))
            {
                if (partner != null)
                {
                    hasAccessToApi = partner.HasAccessTo(apiName)
                        && partner.IsPartnerEnabled;
                }
            }   
                 
            return hasAccessToApi;
        }

        /// <summary>
        /// Validates the dates for partner.
        /// </summary>
        /// <param name="partner">The partner.</param>
        private static void ValidateDatesForPartner(Biz.Partner partner)
        {
            if (partner != null)
            {
                if ((partner.ValidityStartDate.Date != default(DateTime) && partner.ValidityStartDate > DateTime.Now) ||
                   (partner.ValidityEndDate.Date != default(DateTime) && partner.ValidityEndDate < DateTime.Now)) 
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Forbidden;
                    throw Validate.Fault(ApiError.InvalidApiKey, ApiError.HttpForbidden, Messages.ApiKeyInvalid);
                }                
            }
        }

        /// <summary>
        /// Validates the promo code access for partner.
        /// </summary>
        /// <param name="bookingCode">The booking code.</param>
        /// <param name="partner">The partner.</param>
        /// <returns>boolean indicating whether partner has access to Promo code</returns>
        private static bool ValidatePromoCodeAccessForPartner(string bookingCode, Biz.Partner partner)
        {
            return (partner != null) ? partner .HasAccessToPromoCode(bookingCode) : false;
        }

        /// <summary>
        /// Validates the block code access for partner.
        /// </summary>
        /// <param name="bookingCode">The booking code.</param>
        /// <param name="partner">The partner.</param>
        /// <returns>
        /// boolean indicating whether partner has access to Block code
        /// </returns>
        private static bool ValidateBlockCodeAccessForPartner(string bookingCode, Biz.Partner partner)
        {
            return (partner != null) ? partner.HasAccessToBlockCode(bookingCode) : false;
        }

        /// <summary>
        /// Gets the API key from request.
        /// </summary>
        /// <returns> returns a API key from the request </returns>       
        private static string GetAPIKeyFromRequest()
        {
            object prop;
            string headerValue = string.Empty;
            if (OperationContext.Current.IncomingMessageProperties.TryGetValue(HttpRequestMessageProperty.Name, out prop))
            {
                HttpRequestMessageProperty httpProp = (HttpRequestMessageProperty)prop;
                headerValue = httpProp.Headers["APIKey"];
                if (string.IsNullOrEmpty(headerValue))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                    throw Validate.Fault(ApiError.MissingInput, ApiError.HttpUnauthorized, Messages.ApiKeyEmpty);
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                throw Validate.Fault(ApiError.MissingInput, ApiError.HttpUnauthorized, Messages.ApiKeyEmpty);
            }

            return headerValue;
        }

        /// <summary>
        /// Authenticates the and authorize partner.
        /// </summary>        
        private static void AuthenticateAndAuthorizePartner()
        {
            string apiKey = GetAPIKeyFromRequest();
            if (!string.IsNullOrEmpty(apiKey))
            {
                  Biz.Partner partner = AdminBusinessFactory
                    .CreateInstance(string.Empty)
                    .GetPartnerDetails(apiKey);
                if (partner == null)
                {
                    //Error Code 403 for wrong/expired API Key
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Forbidden;
                    throw Validate.Fault(ApiError.InvalidApiKey, ApiError.HttpForbidden, Messages.ApiKeyInvalid);
                }

                if (!AuthorizeApiAccessForPartner(partner))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Forbidden;
                    throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpForbidden, Messages.ApiKeyNoAccess);
                }

                ValidateDatesForPartner(partner);
                NameValueCollection query = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters;
                if (query != null)
                {
                    string bookingCode = query.Get("bookingCode");
                    if (!string.IsNullOrEmpty(bookingCode))
                    {
                        if (bookingCode.ToUpperInvariant().StartsWith("B"))
                        {
                            if (!ValidateBlockCodeAccessForPartner(query.Get("bookingCode"), partner))
                            {
                                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.BadRequest;
                                throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpBadRequest, Messages.BlockCodeNoAccess);
                            }
                        }
                        else
                        {
                            if (!ValidatePromoCodeAccessForPartner(query.Get("bookingCode"), partner))
                            {
                                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.BadRequest;
                                throw Validate.Fault(ApiError.EmptyResult, ApiError.HttpBadRequest, Messages.PromoCodeNoAccess);
                            }
                        }
                    }
                }
           }
        }

        #endregion
    }
}
