﻿// <copyright file="SendRequestToSiteCatalystExtension.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Operations.Extensions
{
    using System;
    using System.ServiceModel.Configuration;

    /// <summary>
    /// Extension to send the request to site catalyst
    /// </summary>
    /// <remarks></remarks>
    public class SendRequestToSiteCatalystExtension : BehaviorExtensionElement
    {
        /// <summary>
        /// Initializes a new instance of the SendRequestToSiteCatalystExtension class.
        /// </summary>
        /// <remarks></remarks>
        public SendRequestToSiteCatalystExtension() 
        { 
        }

        /// <summary>
        /// Gets the type of behavior.
        /// </summary>
        /// <returns>A <see cref="T:System.Type"/>.</returns>
        /// <remarks></remarks>
        public override Type BehaviorType
        {
            get { return typeof(SendRequestToSiteCatalyst); }
        }

        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>The behavior extension.</returns>
        /// <remarks></remarks>
        protected override object CreateBehavior()
        {
            return new SendRequestToSiteCatalyst();
        }
    }
}
