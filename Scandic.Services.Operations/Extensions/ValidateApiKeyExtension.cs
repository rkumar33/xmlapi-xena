﻿// <copyright file="ValidateApiKeyExtension.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Operations.Extensions
{
    using System;
    using System.ServiceModel.Configuration;

    /// <summary>
    /// Extension for validate key
    /// </summary>
    /// <remarks></remarks>
    public class ValidateApiKeyExtension : BehaviorExtensionElement
    {
        /// <summary>
        /// Initializes a new instance of the ValidateApiKeyExtension class.
        /// </summary>
        /// <remarks></remarks>
        public ValidateApiKeyExtension() 
        { 
        }

        /// <summary>
        /// Gets the type of behavior.
        /// </summary>
        /// <returns>A <see cref="T:System.Type"/>.</returns>
        /// <remarks></remarks>
        public override Type BehaviorType
        {
            get { return typeof(ValidateApiKey); }
        }

        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>The behavior extension.</returns>
        /// <remarks></remarks>
        protected override object CreateBehavior()
        {
            return new ValidateApiKey();
        }
    }
}
