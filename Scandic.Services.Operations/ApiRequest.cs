﻿// <copyright file="ApiRequest.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    using System;
    using System.Collections.Specialized;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Web;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations.Properties;

    /// <summary>
    /// Helper class to deal with the request object
    /// </summary>
    public class ApiRequest
    {
        /// <summary>
        /// Initializes a new instance of the ApiRequest class
        /// </summary>
        public ApiRequest()
        {
            if (WebOperationContext.Current != null && WebOperationContext.Current.IncomingRequest.UriTemplateMatch != null)
            {
                this.Query = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters;
                if (this.Query != null)
                {
                    this.Language = this.Query.Get("language");
                    this.Hotel = this.Query.Get("hotel");
                    this.City = this.Query.Get("city");
                    if (!string.IsNullOrEmpty(this.City))
                    {
                        this.City = this.City.ToUpper();
                    }

                    this.Country = this.Query.Get("country");
                    string hotels = this.Query.Get("hotels");
                    if (this.Language == null || string.IsNullOrEmpty(this.Language.Trim()))
                    {
                        this.Language = ConfigHelper.GetValue(ConfigKeys.DefaultLanguage);
                    }
                    
                    if (string.Equals(this.Language, ConfigHelper.GetValue(ConfigKeys.ValidLanguageForRussianDomain),StringComparison.CurrentCultureIgnoreCase))
                        this.Language = ConfigHelper.GetValue(ConfigKeys.ValidLanguageForRussianDomain);

                    if (string.IsNullOrEmpty(this.Hotel))
                    {
                        this.Hotel = this.Query.Get("id");
                    }

                    if (!string.IsNullOrEmpty(hotels))
                    {
                        this.Hotels = hotels.Replace(" ", string.Empty).Split(',');
                    }

                    this.SetAgesAndCounts();
                    this.SetBookingCode();
                    this.SetApiKey();
                }
            }
        }

        #region Properties & Constants
        /// <summary>
        /// Gets the requested language
        /// </summary>
        public string Language { get; private set; }

        /// <summary>
        /// Gets the Number of rooms
        /// </summary>
        /// <returns>Returns the number of rooms</returns>
        public int NoOfRooms { get; private set; }

        /// <summary>
        /// Gets or sets the children ages
        /// </summary>
        public int[,] ChildrenAges { get; set; }

        /// <summary>
        /// Gets or sets the adult count
        /// </summary>
        public int[] AdultCounts { get; set; }

        /// <summary>
        /// Gets or sets the child count
        /// </summary>
        public int[] ChildCounts { get; set; }

        /// <summary>
        /// Gets the total child count across rooms
        /// </summary>
        public int ChildCount
        {
            get
            {
                int total = 0;
                if (this.ChildCounts != null)
                {
                   foreach (int count in this.ChildCounts)
                    {
                        total += count;
                    }
                }

               return total;
            }
        }

        /// <summary>
        /// Gets the total adult count across rooms
        /// </summary>
        public int AdultCount
        {
            get
            {
                int total = 0;
                if (this.AdultCounts != null)
                {
                    foreach (int count in this.AdultCounts)
                    {
                        total += count;
                    }
                }

               return total;
            }
        }

        /// <summary>
        /// Gets the Arrival Date
        /// </summary>
        public DateTime ArrivalDate { get; private set; }

        /// <summary>
        /// Gets the departure date
        /// </summary>
        public DateTime DepartureDate { get; private set; }

        /// <summary>
        /// Gets the country
        /// </summary>
        public string Country { get; private set; }

        /// <summary>
        /// Gets the city
        /// </summary>
        public string City { get; private set; }

        /// <summary>
        /// Gets the hotel
        /// </summary>
        public string Hotel { get; private set; }

        /// <summary>
        /// Gets the list of hotels
        /// </summary>
        public string[] Hotels { get; private set; }

        /// <summary>
        /// Gets the booking code
        /// </summary>
        public string BookingCode { get; private set; }

        /// <summary>
        /// Gets the API Key (from request header)
        /// </summary>
        public string ApiKey { get; private set; }

        /// <summary>
        /// Gets or sets the request query collection 
        /// </summary>
        private NameValueCollection Query { get; set; }
        #endregion

        #region Validations
        /// <summary>
        /// Validates the language
        /// </summary>
        public void ValidateLanguage()
        {
            Validate.Language(this.Language, true);
        }

        /// <summary>
        /// Validates the single hotel ID in request
        /// </summary>
        public void ValidateHotel()
        {
            Validate.Hotel(this.Hotel, true, true);
        }

        ///// <summary>
        ///// Validates the single hotel ID in request
        ///// </summary>
        //public void ValidateHotel(string language)
        //{
        //    Validate.Hotel(language,this.Hotel, true, true);
        //}
        /// <summary>
        /// Validates the list of hotels in request
        /// </summary>
        public void ValidateHotelList()
        {
            if (this.Hotels == default(string[]))
            {
                throw Validate.InvalidInputFault(Messages.MissingHotelIds);
            }

            if (this.Hotels.Length > 25)
            {
                throw Validate.InvalidInputFault(Messages.HotelCountExceeded);
            }

            bool isValid = true;
            bool invalidHotel = false;
            if (this.Hotels.Length == 1)
            {
                isValid = Validate.Hotel(this.Hotels[0], false, true);
            }
            else
            {
                for (int i = 0; i < this.Hotels.Length; i++)
                {
                    this.Hotels[i] = this.Hotels[i].Trim();
                    invalidHotel = !Validate.Hotel(this.Hotels[i], false, true);
                    isValid = isValid && invalidHotel;
                    if (invalidHotel)
                    {
                        this.Hotels[i] = string.Empty;
                    }
                }

                isValid = !isValid;
            }

            if (!isValid)
            {
                throw Validate.InvalidInputFault(Messages.InvalidHotel);
            }
        }

        /// <summary>
        /// Validates the city in request
        /// </summary>
        public void ValidateCity()
        {
            Validate.City(this.City, true);
        }

        public void ValidateCity(string language)
        {
            Validate.City(language, this.City, true);
        }
        /// <summary>
        /// Validates the country in request
        /// </summary>
        public void ValidateCountry()
        {
            Validate.Country(this.Country, true);
        }

        public void ValidateCountry(string language)
        {
            Validate.Country(language, this.Country, true);
        }
        /// <summary>
        /// Validates the ages and counts in request
        /// </summary>
        public void ValidateAgesAndCounts()
        {
            Validate.AgesAndCounts(this.AdultCounts, this.ChildCounts, this.ChildrenAges);
        }

        /// <summary>
        /// Validate the booking code
        /// </summary>
        public void ValidateBookingCode()
        {
            Validate.BookingCode(this.BookingCode, this.ApiKey, true);
        }

        /// <summary>
        /// Validate arival and departure dates
        /// </summary>
        public void ValidateDates()
        {
            Validate.Dates(this.Query.Get("AD"), this.Query.Get("AM"), this.Query.Get("AY"), this.Query.Get("DD"), this.Query.Get("DM"), this.Query.Get("DY"));
            this.SetDates();
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Creates a date out of the year, month and date input
        /// </summary>
        /// <param name="day">Day of the date</param>
        /// <param name="month">Month of the date</param>
        /// <param name="year">Year of the date</param>
        /// <returns>Date constructed from the given parameters</returns>
        private DateTime CreateDate(string day, string month, string year)
        {
            return new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
        }

        /// <summary>
        /// Sets the Adult Count, Child Count and Children Ages
        /// </summary>
        private void SetAgesAndCounts()
        {
            bool isOccupancyMissing = false;
            try
            {
                this.AdultCounts = new int[4];
                this.ChildCounts = new int[4];
                this.ChildrenAges = new int[4, 5] 
                    {
                        { -1, -1, -1, -1, -1 },
                        { -1, -1, -1, -1, -1 },
                        { -1, -1, -1, -1, -1 },
                        { -1, -1, -1, -1, -1 }
                    };
                int index;
                int childAgeCnt = 0;
                foreach (string key in this.Query.Keys)
                {
                    switch (key.Substring(0, 2))
                    {
                        case "AN":
                            index = int.Parse(key.Replace("AN", string.Empty)) - 1;
                            this.AdultCounts[index] = Convert.ToInt32(this.Query.Get(key));
                            break;
                        case "CN":
                            index = int.Parse(key.Replace("CN", string.Empty)) - 1;
                            this.ChildCounts[index] = int.Parse(this.Query.Get(key));
                            break;
                        case "CA":
                            index = int.Parse(key.Replace("CA", string.Empty));
                            this.ChildrenAges[(int)(index / 10) - 1, (index % 10) - 1] = int.Parse(this.Query.Get(key));
                            break;
                    }
                }

                for (int i = 0; i < this.AdultCounts.Length; i++)
                {
                    if (this.AdultCounts[i] == 0 && this.ChildCounts[i] > 0)
                    {
                        throw Validate.InvalidInputFault(Messages.InvalidInput);
                    }
                }
 
                for (int i = 0; i < this.ChildCounts.Length; i++)
                {
                    childAgeCnt = this.ChildrenAges[i, 0] >= 0 ? 1 : 0
                        + this.ChildrenAges[i, 1] >= 0 ? 1 : 0
                        + this.ChildrenAges[i, 2] >= 0 ? 1 : 0
                        + this.ChildrenAges[i, 3] >= 0 ? 1 : 0
                        + this.ChildrenAges[i, 4] >= 0 ? 1 : 0;
                    if (this.ChildCounts[i] < childAgeCnt)
                    {
                        throw Validate.InvalidInputFault(Messages.InvalidInput);
                    }
                }

                if (!string.IsNullOrEmpty(this.Query.Get("RN")))
                {
                    this.NoOfRooms = Convert.ToInt32(this.Query.Get("RN"));
                    if (this.NoOfRooms == 0)
                    {
                        throw Validate.InvalidInputFault(Messages.InvalidInput);
                    }

                    int actualAdultCount = 0;
                    foreach (int count in this.AdultCounts)
                    {
                       if (count > 0)
                       {
                           actualAdultCount++;
                       }
                    }
                   
                    if (actualAdultCount != this.NoOfRooms)
                    {
                        isOccupancyMissing = true;
                        throw Validate.MissingInputFault(Messages.MissingOccupancy);
                    }
                }
                else
                {
                    if (this.AdultCounts != null)
                    {
                        foreach (int count in this.AdultCounts)
                        {
                            if (count > 0)
                            {
                                this.NoOfRooms++;
                            }
                        }
                    }
                }
            }
            catch
            {
                if (isOccupancyMissing)
                {
                    throw Validate.MissingInputFault(Messages.MissingOccupancy);
                }
                else
                {
                    throw Validate.InvalidInputFault(Messages.InvalidInput);
                }
                
            }
        }

        /// <summary>
        /// Set arrival and departure dates
        /// </summary>
        private void SetDates()
        {
            this.ArrivalDate = this.CreateDate(this.Query.Get("AD"), this.Query.Get("AM"), this.Query.Get("AY"));
            this.DepartureDate = this.CreateDate(this.Query.Get("DD"), this.Query.Get("DM"), this.Query.Get("DY"));
        }

        /// <summary>
        /// Set the booking code
        /// </summary>
        private void SetBookingCode()
        {
            this.BookingCode = (this.Query.Get("bookingCode") != null) ? this.Query.Get("bookingCode").ToUpperInvariant() : this.Query.Get("bookingCode");
        }

        /// <summary>
        /// Gets the API key from request.
        /// </summary>
        private void SetApiKey()
        {
            object prop;
            string headerValue = string.Empty;
            if (OperationContext.Current.IncomingMessageProperties.TryGetValue(HttpRequestMessageProperty.Name, out prop))
            {
                HttpRequestMessageProperty httpProp = (HttpRequestMessageProperty)prop;
                headerValue = httpProp.Headers["APIKey"];
                //// headerValue = "8ca6b565-3428-4a22-86a8-6980e5400212";
            }

            this.ApiKey = headerValue;
        }
        #endregion
    }
}
