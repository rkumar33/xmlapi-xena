﻿// <copyright file="ValidateAvailability.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Operations
{
    using System;
    using System.Collections.Generic;
    using Scandic.Services.BusinessFactory;
    using Scandic.Services.Operations.Properties;
    using Scandic.Services.Framework;

    /// <summary>
    /// Validation helpers for the services
    /// </summary>  
    public static partial class Validate
    {
        /// <summary>
        /// Gets or sets the count
        /// </summary>
        public static int Count { get; set; }

        /// <summary>
        /// Validates the adult rooms.
        /// </summary>
        /// <param name="adultCount">The adult count.</param>
        /// <param name="childCount">The child count.</param>
        /// <param name="childAge">The child age.</param>        
        public static void AgesAndCounts(int[] adultCount, int[] childCount, int[,] childAge)
        {
            if (adultCount[0] == 0)
            {
                throw Validate.InvalidInputFault(Messages.MissingOccupancy);
            }

            // TODO: Get the below biz rule values from config
            ValidateChildAge(adultCount, childCount, childAge, 12, 6);
            ValidateAdultCounts(adultCount, 6);
            ValidateChildCounts(childCount, 5);
        }

        /// <summary>
        /// Validateion of arrival and departure dates
        /// </summary>
        /// <param name="arrivalDay">Arrival Day</param>
        /// <param name="arrivalMonth">Arrival Month</param>
        /// <param name="arrivalYear">Arrival year</param>
        /// <param name="departureDay">Departure Day</param>
        /// <param name="departureMonth">Departure Month</param>
        /// <param name="departureYear">Departure Year</param>
        public static void Dates(
            string arrivalDay,
            string arrivalMonth,
            string arrivalYear,
            string departureDay,
            string departureMonth,
            string departureYear)
        {
            DateTime arrivalDate = default(DateTime);
            DateTime departureDate = default(DateTime);

            ValidateDateParts(arrivalDay, arrivalMonth, arrivalYear, true);
            ValidateDateParts(departureDay, departureMonth, departureYear, false);

            try
            {
                arrivalDate = new DateTime(Convert.ToInt32(arrivalYear), Convert.ToInt32(arrivalMonth), Convert.ToInt32(arrivalDay));
            }
            catch
            {
                throw Validate.InvalidInputFault(Messages.InvalidArrivalDate);
            }

            try
            {
                departureDate = new DateTime(Convert.ToInt32(departureYear), Convert.ToInt32(departureMonth), Convert.ToInt32(departureDay));
            }
            catch
            {
                throw Validate.InvalidInputFault(Messages.InvalidDepartureDate);
            }

            if ((arrivalDate < DateTime.Now.Date) || (arrivalDate > DateTime.Now.AddYears(2).AddDays(-1)))
            {
                throw Validate.InvalidInputFault(Messages.InvalidArrivalDate);
            }

            if ((departureDate < DateTime.Now.Date) || (departureDate > DateTime.Now.AddYears(2)))
            {
                throw Validate.InvalidInputFault(Messages.InvalidDepartureDate);
            }

            if (departureDate.Equals(arrivalDate))
            {
                throw Validate.InvalidInputFault(Messages.InvalidDepartureDate);
            }

            if (departureDate < arrivalDate)
            {
                throw Validate.InvalidInputFault(Messages.DepartureBeforeArrival);
            }

            if (departureDate > arrivalDate.AddDays(99))
            {
                throw Validate.InvalidInputFault(Messages.DepartureTooLate);
            }
        }

        /// <summary>
        /// Validates the booking code.
        /// </summary>
        /// <param name="bookingCode">The booking code</param>
        /// <param name="apiKey">The API key</param>
        /// <param name="raiseFault">raiseFault value</param>
        /// <returns>True if valid, False otherwise</returns>
        public static bool BookingCode(string bookingCode, string apiKey, bool raiseFault)
        {
            List<string> promoCodes = PartnerDetails(apiKey).PromoCodes;
            bool isValid = promoCodes.Contains(bookingCode);
            if (!isValid && raiseFault)
            {
                throw Validate.InvalidInputFault(Messages.InvalidBookingCode);
            }

            return isValid;
        }

        /// <summary>
        /// Validates the opera availability.
        /// </summary>
        public static void ValidateOwsAvailability()
        {
            if (!AdminBusinessFactory.CreateInstance(string.Empty).IsOwsAvailabilityServiceUp)
            {
                throw Validate.ServerUnavailableFault();
            }
        }

        /// <summary>
        /// Validates the date parts in request
        /// </summary>
        /// <param name="day">Day part of the date</param>
        /// <param name="month">Month part of the date</param>
        /// <param name="year">Year part of the date</param>
        /// <param name="arrival">Indicates if the check is for arrival date</param>
        private static void ValidateDateParts(string day, string month, string year, bool arrival)
        {
            int numDay, numMonth, numYear;
            bool isValid = true;
            bool isDateOutOfRange = false;
            int maxAllowedDays = 396;
            string maxAllowedDaysConfigured = string.Empty;
            if (string.IsNullOrEmpty(day) || string.IsNullOrEmpty(month) || string.IsNullOrEmpty(year))
            {
                if (arrival)
                {
                    throw MissingInputFault(Messages.MissingArrivalDate);
                }
                else
                {
                    throw MissingInputFault(Messages.MissingDepartureDate);
                }
            }

            if (int.TryParse(day, out numDay))
            {
                isValid = numDay > 0 && numDay < 32;
            }
            else
            {
                isValid = false;
            }

            if (!isValid)
            {
                if (arrival)
                {
                    throw InvalidInputFault(Messages.InvalidArrivalDay);
                }
                else
                {
                    throw InvalidInputFault(Messages.InvalidDepartureDay);
                }
            }

            if (int.TryParse(month, out numMonth))
            {
                isValid = numMonth > 0 && numMonth < 13;
            }
            else
            {
                isValid = false;
            }

            if (!isValid)
            {
                if (arrival)
                {
                    throw InvalidInputFault(Messages.InvalidArrivalMonth);
                }
                else
                {
                    throw InvalidInputFault(Messages.InvalidDepartureMonth);
                }
            }

            if (int.TryParse(year, out numYear))
            {
                DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
                maxAllowedDaysConfigured = ConfigHelper.GetValue(ConfigKeys.AllowedDaysForDateOfArrival);

                if (!string.IsNullOrEmpty(maxAllowedDaysConfigured))
                    maxAllowedDays = Convert.ToInt32(maxAllowedDaysConfigured);

                if (arrival)
                {
                    isValid = dt < DateTime.Today.AddDays(maxAllowedDays);
                    if (!isValid)
                        isDateOutOfRange = true;
                }
                else
                {
                    isValid = dt <= DateTime.Today.AddDays(maxAllowedDays);
                    if (!isValid)
                        isDateOutOfRange = true;
                }
            }
            else
            {
                isValid = false;
            }

            if (!isValid)
            {
                if (arrival)
                {
                    if (isDateOutOfRange)
                        throw InvalidInputFault(string.Format(Messages.InvalidArrivalDateRange, Convert.ToInt32(maxAllowedDaysConfigured) - 1));
                    else
                        throw InvalidInputFault(Messages.InvalidArrivalYear);
                }
                else
                {
                    if (isDateOutOfRange)
                        throw InvalidInputFault(string.Format(Messages.InvalidDepartureDateRange, maxAllowedDaysConfigured));
                    else
                        throw InvalidInputFault(Messages.InvalidDepartureYear);
                }
            }
        }

        /// <summary>
        /// Validates the continuity.
        /// </summary>
        /// <param name="counts">The array to be validated for continuity</param>
        /// <param name="max">Upper bound for values in this array</param>        
        private static void ValidateAdultCounts(int[] counts, int max)
        {
            bool isContinuos = true;
            for (int iterator = 0; iterator < counts.Length; iterator++)
            {
                if (counts[iterator] > max)
                {
                    throw Validate.InvalidInputFault(Messages.AdultCountExceeded);
                }

                if (isContinuos)
                {
                    isContinuos = counts[iterator] != 0;
                }
                else
                {
                    if (counts[iterator] != 0)
                    {
                        throw Validate.MissingInputFault(Messages.MissingOccupancy);
                    }
                }
            }
        }

        /// <summary>
        /// Validates the continuity.
        /// </summary>
        /// <param name="counts">The array to be validated for continuity</param>
        /// <param name="max">Upper bound for values in this array</param>       
        private static void ValidateChildCounts(int[] counts, int max)
        {
            for (int iterator = 0; iterator < counts.Length; iterator++)
            {
                if (counts[iterator] > max)
                {
                    throw Validate.InvalidInputFault(Messages.ChildCountExceeded);
                }
            }
        }

        /// <summary>
        /// Validates the child age.
        /// </summary>
        /// <param name="adultCounts">The adult counts.</param>
        /// <param name="childCount">The child counts.</param>
        /// <param name="childAges">The child ages.</param>
        /// <param name="maxAge">The max age.</param>
        /// <param name="maxAdults">The max adults.</param>     
        private static void ValidateChildAge(int[] adultCounts, int[] childCount, int[,] childAges, int maxAge, int maxAdults)
        {
            for (int room = 0; room < childAges.GetLength(0); room++)
            {
                for (int age = 0; age < childAges.GetLength(1); age++)
                {
                    if (childAges[room, age] > maxAge)
                    {
                        if (adultCounts[room] < maxAdults)
                        {
                            adultCounts[room]++;
                            if (childCount[room] > 0)
                            {
                                childCount[room]--;
                            }

                            childAges[room, age] = -1;
                        }
                        else
                        {
                            throw Validate.InvalidInputFault(Messages.AdultCountExceeded);
                        }
                    }
                }
            }
        }
    }
}
