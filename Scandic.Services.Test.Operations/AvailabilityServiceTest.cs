﻿// <copyright file="AvailabilityServiceTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.Operations
{
    using System;    
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Operations;    
    
    /// <summary>
    /// This is a test class for AvailabilityServiceTest and is intended
    /// to contain all AvailabilityServiceTest Unit Tests
    /// </summary>
    [TestClass]
    public class AvailabilityServiceTest
    {
        /// <summary>
        /// Declare testContextInstance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }     
        
        /// <summary>
        /// A Test for GetRoomAndRates
        /// </summary>
        [TestMethod]
        public void GetRoomAndRatesTest()
        {
            AvailabilityService target = new AvailabilityService();
            string hotelId = "830";
            string ad = "28";
            string am = "08";
            string ay = "2011";
            string dd = "29";
            string dm = "08";
            string dy = "2011";
            string an1 = "2";
            string an2 = "2";
            string an3 = null;
            string an4 = null;
            string cn1 = "2";
            string cn2 = "2";
            string cn3 = null;
            string cn4 = null;
            string language = "en";
            string bookingCode = string.Empty;
            int expected = 830;
            int actual = target.GetRoomAndRates(hotelId, ad, am, ay, dd, dm, dy, an1, an2, an3, an4, cn1, cn2, cn3, cn4, language, bookingCode).HotelId;
           Assert.AreEqual(expected, actual);
        }
    }
}
