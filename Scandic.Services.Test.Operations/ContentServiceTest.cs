﻿// <copyright file="ContentServiceTest.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Test.Operations
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Operations;

    /// <summary>
    /// Holds Testmethods for ContentService
    /// </summary>
    [TestClass]
    public class ContentServiceTest
    {
        /// <summary>
        /// field used for TestContext property
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the ContentServiceTest class.
        /// </summary>
        public ContentServiceTest()
        {
             // TODO: Add constructor logic here            
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        /// <summary>
        /// A test for GetCountryList
        /// </summary>
        [TestMethod]
        public void GetCountryListTest()
        {
            ContentService target = new ContentService();    
            CountryList expected = new CountryList { Language = "en" };
            CountryList actual = target.GetCountryList("en");
            Assert.AreEqual(expected.Language, actual.Language);
        }

        /// <summary>
        /// A test for GetCityList
        /// </summary>        
        [TestMethod]
        public void GetCityListTest()
        {
            ContentService target = new ContentService();
            string country = "SE";
            string language = "en";
            var actual = target.GetCityList(country, language);
            Assert.IsInstanceOfType(actual, typeof(CityList));
        }

        /// <summary>
        /// A test for GetHotelInformation
        /// </summary>
        [TestMethod]
        public void GetHotelInformationTest()
        {
            ContentService target = new ContentService();
            string hotelID = "810";
            string language = "en";
            HotelInformation expected = new HotelInformation { Language = language };
            HotelInformation actual;
            actual = target.GetHotelInformation(hotelID, language);
            Assert.AreEqual(1, 1); // change this
        }

        /// <summary>
        /// A test for GetHotelList
        /// </summary>
        [TestMethod]
        public void GetHotelListTest()
        {
            ContentService target = new ContentService();
            string city = "SE1";
            string language = "en";
            HotelList actual = target.GetHotelList(city, language);
            Assert.IsInstanceOfType(actual, typeof(HotelList));
        }

        /// <summary>
        /// A test for ContentService Constructor
        /// </summary>
        [TestMethod]
        public void ContentServiceConstructorTest()
        {           
            Assert.IsInstanceOfType(new ContentService(), typeof(ContentService));
        }

        /// <summary>
        /// A test for GetCountryList
        /// </summary>
        [TestMethod]
        public void GetCountryListTestForEmptyResultFault()
        {
            try
            {
                ContentService target = new ContentService();
                string language = "xx";
                CountryList actual = target.GetCountryList(language);
                Assert.Fail("An exception was expected but didn't occur.");
            }
            catch
            {
                Assert.IsTrue(true, "Invalid language should result in an exception");
            }
        }

        /// <summary>
        /// A test for GetCityList
        /// </summary>
        [TestMethod]
        public void GetCityListTestForEmptyResult()
        {
            try
            {
                ContentService target = new ContentService();
                var actual = target.GetCityList("XX", string.Empty);
                Assert.Fail("An exception was expected but didn't occur.");
            }
            catch
            {
                Assert.IsTrue(true, "Invalid country should result in an exception");
            }
        }

        /// <summary>
        /// A test for GetHotelList
        /// </summary>
        [TestMethod]
        public void GetHotelListTestForEmptyResult()
        {
            try
            {
                ContentService target = new ContentService();
                var actual = target.GetHotelList("XX", string.Empty);
                Assert.Fail("An exception was expected but didn't occur.");
            }
            catch
            {
                Assert.IsTrue(true, "Invalid city should result in an exception");
            }
        }
    }
}
