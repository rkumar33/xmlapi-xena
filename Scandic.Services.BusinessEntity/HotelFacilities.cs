﻿// <copyright file="HotelFacilities.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;    

    /// <summary>
    /// This class has fields that holds the facilities available int he  of the hotel
    /// </summary>
    [Serializable]
    public class HotelFacilities
    {
        /// <summary> 
        /// Gets or sets the NumberOfRooms 
        /// </summary>  
        public string NumberOfRooms { get; set; }

        /// <summary>     
        /// Gets or sets the NonSmokingRooms 
        /// </summary>  
        public string NonsmokingRooms { get; set; }

          /// <summary>
          /// field for roomsForDisabled
          /// </summary>
          private string roomsForDisabled;

        /// <summary>     
        /// Gets or sets the RoomsForDisabled 
        /// </summary>  
          public string RoomsForDisabled
          {
              get
              {
                  return string.IsNullOrEmpty(roomsForDisabled) ? "false" : roomsForDisabled;
              }
              set
              {
                  roomsForDisabled = value;
              }
          }

          /// <summary>
          /// field for relaxCenter
          /// </summary>
          private string relaxCenter;

        /// <summary>     
        /// Gets or sets the RelaxCenter 
        /// </summary>  
          public string RelaxCenter
          {
              get
              {
                  return string.IsNullOrEmpty(relaxCenter) ? "false" : relaxCenter;
              }
              set
              {
                  relaxCenter = value;
              }
          }

        /// <summary>
        /// field for RestaurantOrBar 
        /// </summary>
        private string restaurantOrBar;

        /// <summary>     
        /// Gets or sets the RestaurantOrBar 
        /// </summary>  
        public string RestaurantOrBar
        {
            get
            {
                return string.IsNullOrEmpty(restaurantOrBar) ? "false" : restaurantOrBar;
            }
            set
            {
                restaurantOrBar = value;
            }
        }

        /// <summary>
        /// field for garage 
        /// </summary>
        private string garage;

        /// <summary>           
        /// Gets or sets the Garage 
        /// </summary>  
        public string Garage
        {
            get
            {
                return string.IsNullOrEmpty(garage) ? "false" : garage;
            }
            set
            {
                garage = value;
            }
        }

        /// <summary>
        /// field for outdoorParking 
        /// </summary>
        private string outdoorParking;

        /// <summary>           
        /// Gets or sets the OutdoorParking 
        /// </summary>  
        public string OutdoorParking
        {
            get
            {
                return string.IsNullOrEmpty(outdoorParking) ? "false" : outdoorParking;
            }
            set
            {
                outdoorParking = value;
            }
        }

        /// <summary>
        /// field for shop 
        /// </summary>
        private string shop;

        /// <summary>           
        /// Gets or sets the Shop 
        /// </summary>  
        public string Shop
        {
            get
            {
                return string.IsNullOrEmpty(shop) ? "false" : shop;
            }
            set
            {
                shop = value;
            }
        }

        /// <summary>
        /// field for meetingFacilities 
        /// </summary>
        private string meetingFacilities;

        /// <summary>           
        /// Gets or sets the MeetingFacilities 
        /// </summary>  
        public string MeetingFacilities
        {
            get
            {
                return string.IsNullOrEmpty(meetingFacilities) ? "false" : meetingFacilities;
            }           
                set
            {
                meetingFacilities = value;
            }
        }

        /// <summary>           
        /// Gets or sets the EcoLabeledHotel 
        /// </summary>  
        public string EcoLabeledHotel { get; set; }

        /// <summary>           
        /// Gets or sets the DistanceToCityCenterInKm 
        /// </summary>  
        public string DistanceToCityCenterInKM { get; set; }

        /// <summary>           
        /// Gets or sets the DistanceToTrainStationInKm 
        /// </summary>  
        public string DistanceToTrainStationInKM { get; set; }

        /// <summary>
        /// field for wirelessInternetAccess 
        /// </summary>
        private string wirelessInternetAccess;

        /// <summary>           
        /// Gets or sets the WirelessInternetAccess 
        /// </summary>  
        public string WirelessInternetAccess
        {
            get
            {
                return string.IsNullOrEmpty(wirelessInternetAccess) ? "false" : wirelessInternetAccess;
            }
            set
            {
                wirelessInternetAccess = value;
            }
        }

        /// <summary>           
        /// Gets or sets the AirportName 
        /// </summary>  
        public string AirportName { get; set; }

        /// <summary>           
        /// Gets or sets the DistanceToAirportInKm 
        /// </summary>  
        public string DistanceToAirportInKM { get; set; }      
    }
}
