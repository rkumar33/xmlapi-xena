﻿// <copyright file="RoomRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Runtime.Serialization;   

    /// <summary>
    /// This class has the fields to hold the RoomRate
    /// </summary>
    [Serializable]
    [DataContract(
         Name = "RoomRate",
         Namespace = @"http://api.scandichotels.com/schemas")]
   public class RoomRate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomRate"/> class.
        /// </summary>
        public RoomRate()
        {
            this.FromRate = new FromRate();
        }

        /// <summary>
        /// Gets or sets RoomType
        /// </summary>
           [DataMember(
            Name = "RoomType",
            Order = 1,
            IsRequired = true)]
        public string RoomType { get; set; }

           /// <summary>
           /// Gets or sets the FromRate
           /// </summary>
         [DataMember(
            Name = "FromRate",
            Order = 2,
            IsRequired = true)]          
        public FromRate FromRate { get;  set; }
    }
}
