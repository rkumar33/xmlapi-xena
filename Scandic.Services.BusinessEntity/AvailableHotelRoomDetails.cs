﻿// <copyright file="AvailableHotelRoomDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This class holds the details of the room available in a hotel
    /// </summary>
    [Serializable]
    public class AvailableHotelRoomDetails
    {
        /// <summary>
        /// Initializes a new instance of the AvailableHotelRoomDetails class.
        /// </summary>
        public AvailableHotelRoomDetails()
        {
            this.RoomRates = new List<RoomRate>();
        }

        /// <summary>
        /// Gets the room rates.
        /// </summary>
        /// <value>
        /// The room rates.
        /// </value>
        public IList<RoomRate> RoomRates { get; private set; }
    }
}
