﻿// <copyright file="HotelDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// This holds the details of the hotel
    /// </summary>
    [Serializable]
    public class HotelDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HotelDetails"/> class.
        /// </summary>
        public HotelDetails()
        {
            this.HotelAddress = new HotelAddress();
            this.HotelCoordinate = new HotelCoordinates();
            this.HotelDescription = new HotelDescription();
            this.HotelFacilities = new HotelFacilities();
            this.RoomFacilitiesList = new List<RoomFacilities>();
            this.AlternateHotels = new List<string>();
            this.Images = new List<Image>();
            this.HotelBookingImage = new Image();
        }

        /// <summary>
        /// Gets or sets the Language
        /// </summary>         
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>        
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Continent 
        /// </summary>            
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the HotelId
        /// </summary>          
        public string HotelId { get; set; }

        /// <summary>
        /// Gets or sets the HotelPageReferenceId
        /// </summary>          
        public string HotelPageReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Hotel 
        /// </summary>          
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the List of Alternate Hotel names configured in CMS for the specified hotel.
        /// </summary> 
        public string HotelUrl { get; set; }  

        /// <summary>
        /// Gets or sets the Address of the Hotel 
        /// </summary>          
        public HotelAddress HotelAddress { get; set; }

        /// <summary>
        /// Gets or sets the Coordinates of the Hotel 
        /// </summary>  
        public HotelCoordinates HotelCoordinate { get; set; }

        /// <summary>
        /// Gets or sets the HotelPhone 
        /// </summary>  
        public string HotelPhone { get; set; }

        /// <summary>
        /// Gets or sets the HotelFax 
        /// </summary>  
        public string HotelFax { get; set; }

        /// <summary>
        /// Gets or sets the HotelEmail 
        /// </summary>  
        public string HotelEmail { get; set; }

        /// <summary>
        /// Gets or sets the HotelDescription 
        /// </summary>  
        public HotelDescription HotelDescription { get; set; }

        /// <summary>
        /// Gets or sets the HotelFacilities 
        /// </summary> 
        public HotelFacilities HotelFacilities { get; set; }

        /// <summary>
        /// Gets or sets the RoomFacilities 
        /// </summary> 
        public IList<RoomFacilities> RoomFacilitiesList { get; set; }

        /// <summary>
        /// Gets or sets the Images 
        /// </summary> 
        public IList<Image> Images { get; set; }

        /// <summary>
        /// Gets or sets the HotelBookingImage 
        /// </summary> 
        public Image HotelBookingImage { get; set; }

        /// <summary>
        /// Gets the List of Alternate Hotel names configured in CMS for the specified hotel.
        /// </summary> 
        public IList<string> AlternateHotels { get; private set; }

        /// <summary>
        /// Gets the Tax Rates of the hotel
        /// </summary>
        public double APITaxRates { get; set; }
    }        
}
