﻿// <copyright file="LowestRateLogDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
   using System;

    /// <summary>
    /// Meta for LowestRateLogDetails
    /// </summary>
   public class LowestRateLogDetails
    {               
        /// <summary>
        /// Gets or sets ID the Arrival date for which the search is performed
        /// </summary>
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// Gets or sets Name of the city
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the Rate Name available for occupancy
        /// </summary>
        public string RateName { get;  set; }

        /// <summary>
        /// Gets or sets the Room Name available for occupancy
        /// </summary>
        public string RoomType { get;  set; }

        /// <summary>
        /// Gets or sets the Currency in which rate is provided
        /// </summary>
        public string Currency { get;  set; }

        /// <summary>
        /// Gets or sets the price per night
        /// </summary>
        public string PricePerNight { get;  set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(
                            "{0} | {1} | {2} | {3} | {4}\n",                            
                            this.HotelName,
                            this.RateName,
                            this.RoomType,
                            this.Currency,
                            this.PricePerNight);
        }
    }
}
