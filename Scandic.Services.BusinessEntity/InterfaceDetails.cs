﻿// <copyright file="InterfaceDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;

    /// <summary>
    /// class to get interface details
    /// </summary>  
    [Serializable]
    public class InterfaceDetails
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value> Gets or sets the Interface Name. </value>     
        public string Name { get; set; }
    }
}
