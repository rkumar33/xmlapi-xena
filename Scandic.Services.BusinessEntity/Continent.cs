﻿// <copyright file="Continent.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Continent entity
    /// </summary>
    [Serializable]
    public class Continent
    {
        /// <summary>
        /// Initializes a new instance of the Continent class
        /// </summary>
        public Continent()
        {
            this.Countries = new List<Country>();
        }

        /// <summary>
        /// Gets or sets ID of the continent
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the continent
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the list of cities in the country 
        /// </summary>
        public IList<Country> Countries { get; private set; }
    }
}
