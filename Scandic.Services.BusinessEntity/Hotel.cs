﻿// <copyright file="Hotel.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Hotel entity
    /// </summary>
    [Serializable]
    public class Hotel
    {
        /// <summary>
        /// Gets or sets the ID of the hotel
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the Name of the hotel
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Type of the hotel
        /// </summary>
        public string PropertyType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the hotel is open for booking
        /// </summary>
        public bool IsBookable { get; set; }

        /// <summary>
        /// Gets or sets the Postal City of the hotel
        /// </summary>
        public string PostalCity { get; set; }

        /// <summary>
        /// Gets the Tax Rates of the hotel
        /// </summary>
        public double APITaxRates { get; set; }
        
    }

    public class HotelComparer : IEqualityComparer<Hotel>
    {
        // Hotels are equal if their names and hotel Ids are equal.
        public bool Equals(Hotel hotel1, Hotel hotel2)
        {
            //Check whether the compared objects reference the same data.
            if (Hotel.ReferenceEquals(hotel1, hotel2)) return true;

            //Check whether any of the compared objects is null.
            if (Hotel.ReferenceEquals(hotel1, null) || Hotel.ReferenceEquals(hotel2, null))
                return false;

            //Check whether the hotels' properties are equal.
            return hotel1.Id == hotel2.Id && hotel1.Name == hotel2.Name;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.
        public int GetHashCode(Hotel hotel)
        {
            //Check whether the object is null
            if (Hotel.ReferenceEquals(hotel, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashHotelName = hotel.Name == null ? 0 : hotel.Name.GetHashCode();

            //Get hash code for the Id field.
            int hashHotelId = hotel.Id.GetHashCode();

            //Calculate the hash code for the hotel.
            return hashHotelName ^ hashHotelId;
        }
    }
}
