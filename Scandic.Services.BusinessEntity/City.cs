﻿// <copyright file="City.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Continent entity
    /// </summary>
    [Serializable]
    public class City
    {
        /// <summary>
        /// Initializes a new instance of the City class
        /// </summary>
        public City()
        {
            this.Hotels = new List<Hotel>();
        }

        /// <summary>
        /// Gets or sets ID of the city
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the city
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the list of hotels in the city 
        /// </summary>
        public IList<Hotel> Hotels { get; private set; }
    }
}
