﻿// <copyright file="Room.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// This class has the fields to hold the Room Details
    /// </summary>
    [Serializable]
    [DataContract(
  Name = "Room",
  Namespace = @"http://api.scandichotels.com/schemas")]
   public class Room
    {
        /// <summary>
        /// Gets or sets the RoomCode
        /// </summary>        
        public string RoomCode { get; set; }

        /// <summary>
        /// Gets or sets the RoomType
        /// </summary>
              [DataMember(
            Name = "RoomType",
            Order = 1,
            IsRequired = true)]
        public string RoomType { get; set; }

        /// <summary>
        /// Gets or sets the RoomTypeDescription
        /// </summary>
          [DataMember(
            Name = "RoomTypeDescription",
            Order = 2,
            IsRequired = true)]
        public string RoomTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the RoomImage
        /// </summary>
          [DataMember(
            Name = "RoomImage",
            Order = 3,
            IsRequired = true)]
        public RoomImage RoomImage { get; set; }
    }
}
