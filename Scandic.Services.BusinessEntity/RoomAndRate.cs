﻿// <copyright file="RoomAndRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization; 
    using System.Text;

    /// <summary>
    /// This class has the fields to hold the RoomAndRate of the hotel
    /// </summary>    
    public class RoomAndRate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomAndRate"/> class.
        /// </summary>
        public RoomAndRate()
       {
           this.RoomRates = new List<RoomRate>();
       }

         /// <summary>
         /// Gets or sets RoomRates
         /// </summary>
        public IList<RoomRate> RoomRates { get; set; }             
       
        /// <summary>
        /// Gets or sets CurrencyCode
        /// </summary>     
        public string CurrenyCode { get; set; }

        /// <summary>
        /// Gets or sets TotalPricePerStay
        /// </summary>          
        public double TotalPricePerStay { get; set; }

        /// <summary>
        /// Gets or sets TotalPricePerNight
        /// </summary>        
        public double TotalPricePerNight { get; set; }

        /// <summary>
        /// Gets or sets DeepLinkUrl
        /// </summary>      
        public string DeepLinkUrl { get; set; }     
    }
}
