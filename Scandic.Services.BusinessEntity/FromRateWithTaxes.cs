﻿// <copyright file="FromRateWithTaxes.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// This class has the fields to hold the FromRate
    /// </summary>
    [Serializable]
    [DataContract(
       Name = "FromRate",
       Namespace = @"http://api.scandichotels.com/schemas")]
    public class FromRateWithTaxes : FromRate
    {
        /// <summary>
        /// Gets or sets TotalPricePerStay
        /// </summary>   
        [DataMember(
        Name = "BasePricePerNight",
        Order = 1)]
        public double BasePricePerNight { get; set; }


        /// <summary>
        /// Gets or sets TotalPricePerNight
        /// </summary>        

        [DataMember(
         Name = "BasePricePerStay",
         Order = 2,
         IsRequired = true)]
        public double BasePricePerStay { get; set; }

        /// <summary>
        /// Gets or Sets Taxes 
        /// </summary>
        [DataMember(
        Name = "Taxes",
        Order = 3,
        IsRequired = true)]
        public IList<Tax> Taxes { get; set; }
    }
}
