﻿// <copyright file="FromRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// This class has the fields to hold the FromRate
    /// </summary>
    [Serializable]
    [DataContract(
       Name = "FromRate",
       Namespace = @"http://api.scandichotels.com/schemas")]
    public class FromRate
    {
        /// <summary>
        /// Gets or sets the PricePerNight
        /// </summary>
        [DataMember(
         Name = "PricePerNight",
         Order = 1,
         IsRequired = true)]
        public double PricePerNight { get; set; }

        /// <summary>
        /// Gets or sets the PricePerNightCurrencyCode
        /// </summary>        
        public string PricePerNightCurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the PricePerStay
        /// </summary>
        [DataMember(
         Name = "PricePerStay",
         Order = 2,
         IsRequired = true)]
        public double PricePerStay { get; set; }

        /// <summary>
        /// Gets or sets the PricePerStayCurrencyCode
        /// </summary>
        public string PricePerStayCurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets PricePerNight
        /// </summary>
        [DataMember(
         Name = "RateName",
         Order = 3,
         IsRequired = true)]
        public string RateName { get; set; }

        /// <summary>
        /// Gets or sets PricePerNight
        /// </summary>
        [DataMember(
         Name = "RateDescription",
         Order = 7,
         IsRequired = true)]
        public string RateDescription { get; set; }

        /// <summary>
        /// Gets or sets the deep link URL.
        /// </summary>
        /// <value>
        /// The deep link URL.
        /// </value>
        public string DeepLinkUrlSelectRatePage { get; set; }
		
        /// <summary>
        /// Gets or sets BreakfastIncluded
        /// </summary>
        [DataMember(
            Name = "BreakfastIncluded",
            Order = 8,
            IsRequired = true)]
        public bool BreakfastIncluded { get; set; }

        /// <summary>
        /// Gets or sets FreeCancellation
        /// </summary>
        [DataMember(
            Name = "FreeCancellation",
            Order = 9,
            IsRequired = true)]
        public bool FreeCancellation { get; set; }

        /// <summary>
        /// Gets or sets GuaranteeType
        /// </summary>
        [DataMember(
            Name = "Payment",
            Order = 10,
            IsRequired = true)]
        public string Payment { get; set; }
    }
}
