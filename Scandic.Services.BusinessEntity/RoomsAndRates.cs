﻿// <copyright file="RoomsAndRates.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    
    /// <summary>
    /// This class has the fields to hold the Rooms and Rates Combination available
    /// </summary>
    [Serializable]
   public class RoomsAndRates
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomsAndRates"/> class.
        /// </summary>
        public RoomsAndRates()
        {
            this.RoomAndRates = new List<RoomAndRate>();
            this.Rooms = new List<Room>();
        }

        /// <summary>
        /// Gets or sets the HotelId
        /// </summary>
        public int HotelId { get; set; }

        /// <summary>
        /// Gets or sets the HotelName
        /// </summary>
        public string HotelName { get; set; }

        
        public string DeeplinkURLHotelPage { get; set; }


        /// <summary>
        /// Gets or sets the Language
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Operator
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Continent
        /// </summary>
        public string Continent { get; set; }

        /// <summary>
        /// Gets list of Rooms
        /// </summary>
        public IList<Room> Rooms { get; private set; }      

        /// <summary>
        /// Gets the list of RoomAndRate
        /// </summary>
        public IList<RoomAndRate> RoomAndRates { get; private set; }
    }
}
