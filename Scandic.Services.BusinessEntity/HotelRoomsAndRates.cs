﻿// <copyright file="HotelRoomsAndRates.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

using System;
using System.Collections.Generic;

namespace Scandic.Services.BusinessEntity
{
    /// <summary>
    /// This class has the fields to hold the Rooms and Rates Combination available for all Hotels
    /// </summary>
    [Serializable]
    public class HotelRoomsAndRates
    {
        /// <summary>
        /// Gets the list of RoomAndRate
        /// </summary>
        public List<HotelRoomRates> Hotel { get; set; }
    }
}
