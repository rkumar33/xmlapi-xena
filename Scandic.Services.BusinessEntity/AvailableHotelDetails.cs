﻿// <copyright file="AvailableHotelDetails.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.CmsEntity;
    using Scandic.Services.ServiceAgents.OwsEntity;
    using proxy = Scandic.Services.ServiceAgents.OwsProxy.Availability;
    #endregion

    /// <summary>
    /// This class holds the details of the hotel availability
    /// </summary>
    [Serializable]
    public class AvailableHotelDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableHotelDetails"/> class.
        /// </summary>
        public AvailableHotelDetails()
        {
            this.AvailableHotelRoomDetails = new List<AvailableHotelRoomDetails>();
            this.AvailableHotelRoomFacilities = new List<RoomFacilities>();
            this.HotelAddress = new HotelAddress();
            this.HotelDescription = new BusinessEntity.HotelDescription();
            this.HotelImages = new List<Image>();
            this.HotelFromRate = new FromRate();
            this.HotelFromRateWithTaxes = new FromRateWithTaxes();
            this.IsHotelAvailable = true;
            this.Rooms = new List<OwsRoom>();
            this.RateNetwork = new RateTypes();
            this.RoomNetwork = new RoomTypes();
            this.IsTrueBlockRates = false;
            this.HasPromotionalRates = false;


        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableHotelDetails"/> class.
        /// </summary>
        /// <param name="roomStay">The room stay.</param>
        /// <param name="roomNetwork">The room network</param>
        /// <param name="rateNetwork">The rate network</param>
        /// <param name="isHotelAvailable">Is Hotel Available</param>
        /// <param name="currentSearchType">The Current SearchType</param>
        /// <param name="campaignCode">The Campaign Code</param>
        public AvailableHotelDetails(List<proxy.RoomStay> roomStay, RoomTypes roomNetwork, RateTypes rateNetwork, bool isHotelAvailable, SearchType currentSearchType, string campaignCode, int roomsPerNight)
        {
            this.AvailableHotelRoomDetails = new List<AvailableHotelRoomDetails>();
            this.AvailableHotelRoomFacilities = new List<RoomFacilities>();
            this.HotelAddress = new HotelAddress();
            this.HotelDescription = new BusinessEntity.HotelDescription();
            this.HotelImages = new List<Image>();
            this.HotelFromRate = new FromRate();
            this.HotelFromRateWithTaxes = new FromRateWithTaxes();
            this.IsHotelAvailable = isHotelAvailable;
            this.Rooms = new List<OwsRoom>();
            this.RoomNetwork = roomNetwork;
            this.RateNetwork = rateNetwork;
            this.IsTrueBlockRates = false;
            this.HasPromotionalRates = false;
            if (isHotelAvailable && roomStay.Count > 0)
            {
                if (currentSearchType == SearchType.REGULAR)
                {
                    if (string.IsNullOrEmpty(campaignCode))
                    {
                        //if (!isRoomRateService)
                        this.SetUpAvailableHotelDetailsFromOpera(roomStay);
                        //else
                        //    this.SetUpAvailableHotelDetailsForRoomsAndRatesFromOpera(roomStay);
                        if (this.Rooms == null || this.Rooms.Count == 0)
                        {
                            this.IsHotelAvailable = false;
                        }
                    }
                    else
                    {
                        //if (!isRoomRateService)
                        this.SetUpAvailableHotelDetailsForPromoCodes(roomStay, campaignCode);
                        //else
                        //    this.SetUpAvailableHotelRoomRatesDetailsForPromoCodes(roomStay, campaignCode);
                        if (!this.HasPromotionalRates)
                        {
                            this.IsHotelAvailable = false;
                        }
                    }
                }
                else if (currentSearchType == SearchType.CORPORATE)
                {
                    this.SetUpAvailableHotelDetailsForBlockCodes(roomStay, roomsPerNight);
                    if (this.Rooms == null || this.Rooms.Count == 0)
                    {
                        this.IsHotelAvailable = false;
                    }
                }
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the hotel is open for booking
        /// </summary>
        public bool IsHotelAvailable { get; set; }

        /// <summary>
        /// Gets or sets the CityId
        /// </summary>
        public string CityId { get; set; }

        /// <summary>
        /// Gets or sets the MessageonOnSingleHotelSearchNotAvailable.
        /// </summary>
        public string MessageOnSingleHotelSearchNotAvailable { get; set; }

        /// <summary>
        /// Gets or sets the Continent.
        /// </summary>   
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the hotel id.
        /// </summary>      
        public string HotelId { get; set; }

        /// <summary>
        /// Gets or sets the name of the hotel.
        /// </summary>
        /// <value>
        /// The name of the hotel.
        /// </value>
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the hotel address.
        /// </summary>
        /// <value>
        /// The hotel address.
        /// </value>
        public HotelAddress HotelAddress { get; set; }

        /// <summary>
        /// Gets or sets the hotel description.
        /// </summary>
        /// <value>
        /// The hotel description.
        /// </value>
        public HotelDescription HotelDescription { get; set; }

        /// <summary>
        /// Gets the hotel images.
        /// </summary>       
        public IList<Image> HotelImages { get; private set; }

        /// <summary>
        /// Gets or sets the hotel booking Image.
        /// </summary>
        public Image HotelBookingImage { get; set; }

        /// <summary>
        /// Gets the room availability details.
        /// </summary>
        public IList<AvailableHotelRoomDetails> AvailableHotelRoomDetails { get; private set; }

        /// <summary>
        /// Gets the available hotel room facilities.
        /// </summary>
        public IList<RoomFacilities> AvailableHotelRoomFacilities { get; private set; }

        /// <summary>
        /// Gets or sets the hotel from rate.
        /// </summary>        
        public FromRate HotelFromRate { get; set; }

        /// <summary>
        /// Gets or sets the hotel from rate.
        /// </summary>        
        public FromRateWithTaxes HotelFromRateWithTaxes { get; set; }

        /// <summary>
        /// Gets the rooms.
        /// </summary>
        public IList<OwsRoom> Rooms { get; private set; }

        /// <summary>
        /// Gets the room network.
        /// </summary>
        public RoomTypes RoomNetwork { get; private set; }

        /// <summary>
        /// Gets the rate network.
        /// </summary>
        public RateTypes RateNetwork { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is true block rates.
        /// </summary>
        ///  <value>
        /// <c>true</c> if this instance is true block rates; otherwise, <c>false</c>.
        /// </value>
        public bool IsTrueBlockRates { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance has promotional rates.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has promotional rates; otherwise, <c>false</c>.
        /// </value>
        public bool HasPromotionalRates { get; private set; }

        /// <summary>
        /// Gets or sets the deeplink URL hotel page.
        /// </summary>
        /// <value>
        /// The deeplink URL hotel page.
        /// </value>
        public string Deeplinkurlhotellangingpage { get; set; }

        /// <summary>
        /// Gets the Tax Rates of the hotel
        /// </summary>
        public double APITaxRates { get; set; }

        /// <summary>
        /// Fills the availability details.
        /// </summary>
        /// <param name="hotelDetails">The Hotel Details</param>
        /// <param name="campaignCode">The campaign code.</param>
        public void FillMultipleRoomRatesAvailabilityDetails(HotelDetails hotelDetails, string campaignCode)
        {
            if (this.IsHotelAvailable)
            {
                this.FillHotelDetailsForRoomsAndRates(hotelDetails, campaignCode);

                for (int roomIterator = 0; roomIterator < this.Rooms.Count; roomIterator++)
                {
                    AvailableHotelRoomDetails roomAvailability = new AvailableHotelRoomDetails();
                    for (int roomRateIterator = 0; roomRateIterator < this.Rooms[roomIterator].RoomRates.Count; roomRateIterator++)
                    {
                        if (this.Rooms[roomIterator].RoomRates[roomRateIterator] != null)
                        {
                            RoomRate roomRates = new RoomRate();
                            RateCategory rateCategory = null;
                            OwsRoomRate owsRoomRate = this.Rooms[roomIterator].RoomRates[roomRateIterator];
                            RateCode ratePlanCodePage = null;
                            var fromRateWithTaxes = new FromRateWithTaxes();

                            roomRates.RoomType = this.GetRoomCategoryByRoomCode(owsRoomRate.RoomTypeCode) != null ?
                                this.GetRoomCategoryByRoomCode(owsRoomRate.RoomTypeCode).RoomCategoryName : string.Empty;
                            
                            if (!this.IsTrueBlockRates)
                            {
                                rateCategory = this.GetRateCategoryByRateCode(owsRoomRate.RatePlanCode);
                                fromRateWithTaxes.RateName = rateCategory != null ? rateCategory.RateCategoryName : string.Empty;
                                fromRateWithTaxes.RateDescription = rateCategory != null ? rateCategory.RateCategoryDescription : string.Empty;

                                ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(owsRoomRate.RatePlanCode);
                                fromRateWithTaxes.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); // ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                                fromRateWithTaxes.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                                if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                                    fromRateWithTaxes.Payment = rateCategory.GuaranteeType; //GetPaymentType(rateCategory.GuaranteeType);
                                else
                                    fromRateWithTaxes.Payment = PaymentTypes.Postpaid;
                            }
                            else
                            {
                                fromRateWithTaxes.RateName = string.Empty;
                                fromRateWithTaxes.RateDescription = string.Empty;
                                rateCategory = this.GetRateCategoryForTrueBlockCode(campaignCode);
                                if (rateCategory != null) 
                                {
                                    fromRateWithTaxes.RateName = rateCategory.RateCategoryId;
                                    fromRateWithTaxes.RateDescription = rateCategory.RateCategoryDescription;

                                    ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(owsRoomRate.RatePlanCode);
                                    fromRateWithTaxes.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                                    fromRateWithTaxes.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                                    if(rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                                        fromRateWithTaxes.Payment = rateCategory.GuaranteeType; //GetPaymentType(rateCategory.GuaranteeType);
                                    else
                                        fromRateWithTaxes.Payment = PaymentTypes.Postpaid;
                                }
                            }

                            fromRateWithTaxes.PricePerNight = owsRoomRate.BaseRate;
                            fromRateWithTaxes.PricePerStay = owsRoomRate.TotalRate;
                            fromRateWithTaxes.PricePerNightCurrencyCode = owsRoomRate.BaseRateCurrency;

                            fromRateWithTaxes.Taxes = new List<Tax>();
                            Tax tax = new Tax();

                            LogHelper.LogInfo("FillMultipleRoomRatesAvailabilityDetails: APITaxRates: " + hotelDetails.APITaxRates + " HotelID: " + hotelDetails.HotelId, LogCategory.OWSRoomsRate);

                            if (hotelDetails.APITaxRates > 0)
                                tax.Amount = Convert.ToDouble(String.Format("{0:0.00}", fromRateWithTaxes.PricePerStay - Convert.ToDouble(String.Format("{0:0.00}", (fromRateWithTaxes.PricePerStay / (1 + (hotelDetails.APITaxRates / 100)))))));
                            else
                                tax.Amount = 0.00;

                            tax.Type = "VAT";

                            fromRateWithTaxes.Taxes.Add(tax);
                            if (hotelDetails.APITaxRates > 0)
                                fromRateWithTaxes.BasePricePerNight = fromRateWithTaxes.PricePerNight - (fromRateWithTaxes.PricePerNight - Convert.ToDouble(String.Format("{0:0.00}", (fromRateWithTaxes.PricePerNight / (1 + hotelDetails.APITaxRates / 100)))));
                            else
                                fromRateWithTaxes.BasePricePerNight = fromRateWithTaxes.PricePerNight;

                            fromRateWithTaxes.BasePricePerNight = Convert.ToDouble(String.Format("{0:0.00}", fromRateWithTaxes.BasePricePerNight));
                            fromRateWithTaxes.BasePricePerStay = fromRateWithTaxes.PricePerStay - Convert.ToDouble(String.Format("{0:0.00}", tax.Amount));
                            fromRateWithTaxes.BasePricePerStay = Convert.ToDouble(String.Format("{0:0.00}", fromRateWithTaxes.BasePricePerStay));
                            roomRates.FromRate = fromRateWithTaxes;
                            roomAvailability.RoomRates.Add(roomRates);
                        }
                    }

                    this.AvailableHotelRoomDetails.Add(roomAvailability);
                }
            }
        }


        /// <summary>
        /// Fills the availability details.
        /// </summary>
        /// <param name="hotelDetails">The Hotel Details</param>
        /// <param name="campaignCode">The campaign code.</param>
        public void FillAvailabilityDetails(HotelDetails hotelDetails, string campaignCode)
        {
            if (this.IsHotelAvailable)
            {
                this.FillHotelDetails(hotelDetails, campaignCode);

                for (int roomIterator = 0; roomIterator < this.Rooms.Count; roomIterator++)
                {
                    AvailableHotelRoomDetails roomAvailability = new AvailableHotelRoomDetails();
                    for (int roomRateIterator = 0; roomRateIterator < this.Rooms[roomIterator].RoomRates.Count; roomRateIterator++)
                    {
                        if (this.Rooms[roomIterator].RoomRates[roomRateIterator] != null)
                        {
                            RoomRate roomRates = new RoomRate();
                            RateCategory rateCategory = null;
                            OwsRoomRate owsRoomRate = this.Rooms[roomIterator].RoomRates[roomRateIterator];
                            RateCode ratePlanCodePage = null;

                            roomRates.RoomType = this.GetRoomCategoryByRoomCode(owsRoomRate.RoomTypeCode) != null ?
                                this.GetRoomCategoryByRoomCode(owsRoomRate.RoomTypeCode).RoomCategoryName : string.Empty;
                            if (!this.IsTrueBlockRates)
                            {
                                rateCategory = this.GetRateCategoryByRateCode(owsRoomRate.RatePlanCode);
                                roomRates.FromRate.RateName = rateCategory != null ? rateCategory.RateCategoryName : string.Empty;
                                roomRates.FromRate.RateDescription = rateCategory != null ? rateCategory.RateCategoryDescription : string.Empty;

                                ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(owsRoomRate.RatePlanCode);
                                roomRates.FromRate.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                                roomRates.FromRate.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;
                                
                                if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                                    roomRates.FromRate.Payment = rateCategory.GuaranteeType; //GetPaymentType(rateCategory.GuaranteeType);
                                else
                                    roomRates.FromRate.Payment = PaymentTypes.Postpaid;
                            }
                            else
                            {
                                roomRates.FromRate.RateName = string.Empty;
                                roomRates.FromRate.RateDescription = string.Empty;
                                rateCategory = this.GetRateCategoryForTrueBlockCode(campaignCode);
                                if (rateCategory != null)
                                {
                                    roomRates.FromRate.RateName = rateCategory.RateCategoryId;
                                    roomRates.FromRate.RateDescription = rateCategory.RateCategoryDescription;

                                    ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(owsRoomRate.RatePlanCode);
                                    roomRates.FromRate.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                                    roomRates.FromRate.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                                    if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                                        roomRates.FromRate.Payment = rateCategory.GuaranteeType; //GetPaymentType(rateCategory.GuaranteeType);
                                    else
                                        roomRates.FromRate.Payment = PaymentTypes.Postpaid;
                                }
                            }

                            roomRates.FromRate.PricePerNight = owsRoomRate.BaseRate;
                            roomRates.FromRate.PricePerStay = owsRoomRate.TotalRate;

                            roomRates.FromRate.PricePerNightCurrencyCode = owsRoomRate.BaseRateCurrency;
                            roomAvailability.RoomRates.Add(roomRates);
                        }
                    }

                    this.AvailableHotelRoomDetails.Add(roomAvailability);
                }
            }
        }



        /// <summary>
        /// Gets the promotion rates.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="promotionCode">The promotion code.</param>
        /// <returns>Returns the Promotional Rates</returns>
        private static List<string> GetPromotionRates(IList<proxy.RoomStay> listRoomStay, string promotionCode)
        {
            List<string> rateCodes = new List<string>();

            foreach (proxy.RoomStay roomStay in listRoomStay)
            {
                proxy.RatePlan[] ratePlans = roomStay.RatePlans;

                if (null != ratePlans)
                {
                    for (int i = 0; i < ratePlans.Length; i++)
                    {
                        proxy.RatePlan ratePlan = ratePlans[i];
                        if (promotionCode == ratePlan.promotionCode)
                        {
                            rateCodes.Add(ratePlan.ratePlanCode);
                        }
                    }
                }
            }

            return rateCodes;
        }

        /// <summary>
        /// Gets the bookable status for block code.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <returns>Checks the Bookable Status for Block Code.</returns>
        private static bool GetBookableStatusForBlockCode(List<proxy.RoomStay> listRoomStay)
        {
            bool returnValue = false;
            foreach (proxy.RoomStay roomStay in listRoomStay)
            {
                proxy.RatePlan[] plans = roomStay.RatePlans;
                int planLength = plans.Length;
                for (int counter = 0; counter < planLength; counter++)
                {
                    proxy.AdditionalDetail[] details = plans[counter].AdditionalDetails;
                    int detailCount = details.Length;
                    for (int count = 0; count < detailCount; count++)
                    {
                        if (details[count].detailType.ToString() == AppConstants.MISC)
                        {
                            returnValue = true;
                            break;
                        }
                    }

                    // If inner loop finds the keyword then do not loop further for the outer loop.
                    if (returnValue)
                    {
                        break;
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Fills the hotel details.
        /// </summary>
        /// <param name="hotelDetails">The available hotel details.</param>
        /// <param name="campaignCode">The campaign code.</param>
        private void FillHotelDetails(HotelDetails hotelDetails, string campaignCode)
        {
            this.Continent = hotelDetails.Continent;
            this.HotelId = hotelDetails.HotelId;
            this.HotelName = hotelDetails.HotelName;
            this.HotelAddress = hotelDetails.HotelAddress;
            this.HotelDescription = hotelDetails.HotelDescription;
            this.HotelBookingImage = hotelDetails.HotelBookingImage;
            this.HotelImages = hotelDetails.Images;
            this.AvailableHotelRoomFacilities = hotelDetails.RoomFacilitiesList;
            OwsRoomRate lowestRoomRate = this.GetLowestRateForHotel();
            if (lowestRoomRate != null)
            {
                this.HotelFromRate.PricePerNight = lowestRoomRate.BaseRate;
                this.HotelFromRate.PricePerNightCurrencyCode = lowestRoomRate.BaseRateCurrency;
                this.HotelFromRate.PricePerStay = lowestRoomRate.TotalRate;
                this.HotelFromRate.PricePerStayCurrencyCode = lowestRoomRate.TotalRateCurrency;
                RateCategory rateCategory = null;
                RateCode ratePlanCodePage = null;
                if (!this.IsTrueBlockRates)
                {
                    rateCategory = this.GetRateCategoryByRateCode(lowestRoomRate.RatePlanCode);
                    this.HotelFromRate.RateName = rateCategory != null ? rateCategory.RateCategoryName : string.Empty;
                    this.HotelFromRate.RateDescription = rateCategory != null ? rateCategory.RateCategoryDescription : string.Empty;

                    ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(lowestRoomRate.RatePlanCode);
                    this.HotelFromRate.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                    this.HotelFromRate.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                    if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                        this.HotelFromRate.Payment = rateCategory.GuaranteeType; //GetPaymentType(rateCategory.GuaranteeType);
                    else
                        this.HotelFromRate.Payment = PaymentTypes.Postpaid;
                }
                else
                {
                    this.HotelFromRate.RateName = string.Empty;
                    this.HotelFromRate.RateDescription = string.Empty;
                    rateCategory = this.GetRateCategoryForTrueBlockCode(campaignCode);
                    if (rateCategory != null)
                    {
                        this.HotelFromRate.RateName = rateCategory.RateCategoryId;
                        this.HotelFromRate.RateDescription = rateCategory.RateCategoryDescription;

                        ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(lowestRoomRate.RatePlanCode);
                        this.HotelFromRate.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                        this.HotelFromRate.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                        if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                            this.HotelFromRate.Payment = rateCategory.GuaranteeType;//GetPaymentType(rateCategory.GuaranteeType);
                        else
                            this.HotelFromRate.Payment = PaymentTypes.Postpaid;
                    }
                }
            }
        }

        private void FillHotelDetailsForRoomsAndRates(HotelDetails hotelDetails, string campaignCode)
        {
            this.Continent = hotelDetails.Continent;
            this.HotelId = hotelDetails.HotelId;
            this.HotelName = hotelDetails.HotelName;
            this.HotelAddress = hotelDetails.HotelAddress;
            this.HotelDescription = hotelDetails.HotelDescription;
            this.HotelBookingImage = hotelDetails.HotelBookingImage;
            this.HotelImages = hotelDetails.Images;
            this.AvailableHotelRoomFacilities = hotelDetails.RoomFacilitiesList;
            this.APITaxRates = hotelDetails.APITaxRates;
            OwsRoomRate lowestRoomRate = this.GetLowestRateForHotel();
            if (lowestRoomRate != null)
            {
                this.HotelFromRateWithTaxes.PricePerNight = lowestRoomRate.BaseRate;
                this.HotelFromRateWithTaxes.PricePerNightCurrencyCode = lowestRoomRate.BaseRateCurrency;
                this.HotelFromRateWithTaxes.PricePerStay = lowestRoomRate.TotalRate;
                this.HotelFromRateWithTaxes.PricePerStayCurrencyCode = lowestRoomRate.TotalRateCurrency;
                //SCANMOD-714
                this.HotelFromRateWithTaxes.Taxes = new List<Tax>();
                Tax tax = new Tax();

                LogHelper.LogInfo("FillHotelDetailsForRoomsAndRates: APITaxRates: " + this.APITaxRates, LogCategory.OWSRoomsRate);

                if (this.APITaxRates > 0)
                    tax.Amount = Convert.ToDouble(String.Format("{0:0.00}", this.HotelFromRateWithTaxes.PricePerStay - Convert.ToDouble(String.Format("{0:0.00}", (this.HotelFromRateWithTaxes.PricePerStay / (1 + (this.APITaxRates / 100)))))));
                else
                    tax.Amount = 0.00;
                tax.Type = "VAT";
                this.HotelFromRateWithTaxes.Taxes.Add(tax);

                this.HotelFromRateWithTaxes.BasePricePerStay = this.HotelFromRateWithTaxes.PricePerStay - Convert.ToDouble(String.Format("{0:0.00}", tax.Amount));
                this.HotelFromRateWithTaxes.BasePricePerStay = Convert.ToDouble(String.Format("{0:0.00}", this.HotelFromRateWithTaxes.BasePricePerStay));

                if (this.APITaxRates > 0)
                    this.HotelFromRateWithTaxes.BasePricePerNight = this.HotelFromRateWithTaxes.PricePerNight - (this.HotelFromRateWithTaxes.PricePerNight - Convert.ToDouble(String.Format("{0:0.00}", (this.HotelFromRateWithTaxes.PricePerNight / (1 + (this.APITaxRates / 100))))));
                else
                    this.HotelFromRateWithTaxes.BasePricePerNight = this.HotelFromRateWithTaxes.PricePerNight;
                this.HotelFromRateWithTaxes.BasePricePerNight = Convert.ToDouble(String.Format("{0:0.00}", this.HotelFromRateWithTaxes.BasePricePerNight));

                RateCategory rateCategory = null;
                RateCode ratePlanCodePage = null;
                if (!this.IsTrueBlockRates)
                {
                    rateCategory = this.GetRateCategoryByRateCode(lowestRoomRate.RatePlanCode);
                    this.HotelFromRateWithTaxes.RateName = rateCategory != null ? rateCategory.RateCategoryName : string.Empty;
                    this.HotelFromRateWithTaxes.RateDescription = rateCategory != null ? rateCategory.RateCategoryDescription : string.Empty;

                    ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(lowestRoomRate.RatePlanCode);
                    this.HotelFromRateWithTaxes.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                    this.HotelFromRateWithTaxes.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                    if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                        this.HotelFromRateWithTaxes.Payment = rateCategory.GuaranteeType;//GetPaymentType(rateCategory.GuaranteeType);
                    else
                        this.HotelFromRateWithTaxes.Payment = PaymentTypes.Postpaid;
                }
                else
                {
                    this.HotelFromRateWithTaxes.RateName = string.Empty;
                    this.HotelFromRateWithTaxes.RateDescription = string.Empty;
                    rateCategory = this.GetRateCategoryForTrueBlockCode(campaignCode);
                    if (rateCategory != null)
                    {
                        this.HotelFromRateWithTaxes.RateName = rateCategory.RateCategoryId;
                        this.HotelFromRateWithTaxes.RateDescription = rateCategory.RateCategoryDescription;

                        ratePlanCodePage = this.GetRatePlanCodePageByRatePlanCode(lowestRoomRate.RatePlanCode);
                        this.HotelFromRateWithTaxes.BreakfastIncluded = IsBreakFastIncluded(hotelDetails.HotelId); //ratePlanCodePage != null ? ratePlanCodePage.BreakfastIncluded : false;
                        this.HotelFromRateWithTaxes.FreeCancellation = ratePlanCodePage != null ? ratePlanCodePage.IsCancellable : false;

                        if (rateCategory != null && !string.IsNullOrEmpty(rateCategory.GuaranteeType))
                            this.HotelFromRateWithTaxes.Payment = rateCategory.GuaranteeType;//GetPaymentType(rateCategory.GuaranteeType);
                        else
                            this.HotelFromRateWithTaxes.Payment = PaymentTypes.Postpaid;
                    }
                }
            }
        }


        /// <summary>
        /// Method to Get the LowestRateForHotel
        /// </summary>        
        /// <returns>OwsRoomRate object</returns>
        private OwsRoomRate GetLowestRateForHotel()
        {
            OwsRoomRate lowestRoomRate = null;
            List<OwsRoomRate> minRatesFromEachRoom = new List<OwsRoomRate>();
            if (this.Rooms.Count > 0)
            {
                for (int roomIterator = 0; roomIterator < this.Rooms.Count; roomIterator++)
                {
                    if (this.Rooms[roomIterator].RoomRates.Count > 0)
                    {
                        OwsRoomRate lowestRoomRateForEachRoom = this.Rooms[roomIterator].RoomRates.OrderBy(r => r.BaseRate).First();
                        minRatesFromEachRoom.Add(lowestRoomRateForEachRoom);
                    }
                }

                if (minRatesFromEachRoom.Count > 0)
                {
                    lowestRoomRate = minRatesFromEachRoom.OrderBy(r => r.BaseRate).First();
                }
            }

            return lowestRoomRate;
        }

        /// <summary>
        /// Gets the rate name by rate code.
        /// </summary>       
        /// <param name="rateCode">The roomcode.</param>
        /// <returns>
        /// RateCategory of the rate code passed in
        /// </returns>
        private RateCategory GetRateCategoryByRateCode(string rateCode)
        {
            try
            {
                if (rateCode != null)
                {
                    string rateCategoryName = this.RateNetwork.RateTypeMap[rateCode];
                    if (!string.IsNullOrEmpty(rateCategoryName))
                    {
                        return this.RateNetwork.RateCategoryCollection.Find(delegate(RateCategory rate)
                        {
                            return rate.RateCategoryName.Equals(rateCategoryName);
                        });
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch
            {

            }

            return null;
        }

        /// <summary>
        /// Gets the rateplancode page by rateplan code.
        /// </summary>       
        /// <param name="ratePlanCode">The roomcode.</param>
        /// <returns>
        /// RatePlanCodePage of the ratePlancode passed in
        /// </returns>
        private RateCode GetRatePlanCodePageByRatePlanCode(string ratePlanCode)
        {
            try
            {
                //if (ratePlanCode != null)
                //{
                //    string rateCategoryName = this.RateNetwork.RateTypeMap[rateCode];
                    if (!string.IsNullOrEmpty(ratePlanCode))
                    {
                        return this.RateNetwork.RatePlanCodes.Find(delegate(RateCode rate)
                        {
                            return rate.OperaRateId.Equals(ratePlanCode);
                        });
                    }
                    else
                    {
                        return null;
                    }
                //}
            }
            catch
            {

            }

            return null;
        }
        /// <summary>
        /// Gets the block code description for true block codes..
        /// </summary>       
        /// <param name="rateCode">The roomcode.</param>
        /// <returns>
        /// RateCategory of the rate code passed in
        /// </returns>
        private RateCategory GetRateCategoryForTrueBlockCode(string blockCodeName)
        {
            try
            {
                if (!string.IsNullOrEmpty(blockCodeName))
                {
                    return this.RateNetwork.RateCategoryCollection.Find(
                        rate => rate.OperaId != null && rate.OperaId.Equals(blockCodeName));
                }
                else
                {
                    return null;
                }
            }
            catch
            {

            }

            return null;
        }


        /// <summary>
        /// Gets the room name by room code.
        /// </summary>       
        /// <param name="roomCode">The roomcode.</param>
        /// <returns>
        /// roomCategory of the room code passed in
        /// </returns>
        private RoomCategory GetRoomCategoryByRoomCode(string roomCode)
        {
            try
            {
                string roomCategoryName = this.RoomNetwork.RoomTypeMap[roomCode];
                if (!string.IsNullOrEmpty(roomCategoryName))
                {
                    return this.RoomNetwork.RoomCategoryCollection.Find(delegate(RoomCategory room)
                    {
                        return room.RoomCategoryName.Equals(roomCategoryName);
                    });
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Sets up available hotel details from opera.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>       
        private void SetUpAvailableHotelDetailsFromOpera(List<proxy.RoomStay> listRoomStay)
        {
            this.Rooms = new List<OwsRoom>();
            List<string> bonusChequeRates = ContentAccess.GetBonusChequeRateCodes();
            if (listRoomStay != null && listRoomStay.Count > 0)
            {
                foreach (proxy.RoomStay roomStay in listRoomStay)
                {
                    if (roomStay != null)
                    {
                        OwsRoom room = new OwsRoom();
                        if (null != roomStay.RoomRates)
                        {
                            foreach (proxy.RoomRate roomRate in roomStay.RoomRates)
                            {
                                if (this.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                                        this.IsRoomCodeInCMS(roomRate.roomTypeCode) &&
                                       !bonusChequeRates.Contains(roomRate.ratePlanCode))
                                {
                                    // string rateCategoryName =this.GetRateCategoryByRateCode(roomRate.ratePlanCode).RateCategoryName;
                                    room.RoomRates.Add(this.GetRoomRate(roomRate));
                                }
                            }
                        }

                        if (room.RoomRates.Count > 0)
                        {
                            this.Rooms.Add(room);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets up available hotel details from opera.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>       
        //private void SetUpAvailableHotelDetailsForRoomsAndRatesFromOpera(List<proxy.RoomStay> listRoomStay)
        //{
        //    this.Rooms = new List<OwsRoom>();
        //    List<string> bonusChequeRates = ContentAccess.GetBonusChequeRateCodes();
        //    if (listRoomStay != null && listRoomStay.Count > 0)
        //    {
        //        foreach (proxy.RoomStay roomStay in listRoomStay)
        //        {
        //            if (roomStay != null)
        //            {
        //                OwsRoom room = new OwsRoom();
        //                if (null != roomStay.RoomRates)
        //                {
        //                    foreach (proxy.RoomRate roomRate in roomStay.RoomRates)
        //                    {
        //                        if (this.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
        //                                this.IsRoomCodeInCMS(roomRate.roomTypeCode) &&
        //                               !bonusChequeRates.Contains(roomRate.ratePlanCode))
        //                        {
        //                            // string rateCategoryName =this.GetRateCategoryByRateCode(roomRate.ratePlanCode).RateCategoryName;
        //                            room.RoomRates.Add(this.GetRoomRate(roomRate));
        //                        }
        //                    }
        //                }

        //                if (room.RoomRates.Count > 0)
        //                {
        //                    this.Rooms.Add(room);
        //                }
        //            }
        //        }
        //    }
        //}


        /// <summary>
        /// Sets up available hotel details for block codes.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        private void SetUpAvailableHotelDetailsForBlockCodes(List<proxy.RoomStay> listRoomStay, int roomsPerNight)
        {

            this.IsTrueBlockRates = GetBookableStatusForBlockCode(listRoomStay);
            List<string> bonusChequeRates = ContentAccess.GetBonusChequeRateCodes();

            this.Rooms = new List<OwsRoom>();

            foreach (proxy.RoomStay roomStay in listRoomStay)
            {
                OwsRoom room = new OwsRoom();
                List<string> roomTypeToRemove = GetUnavailableRoomType(roomStay, roomsPerNight);
                if (null != roomStay.RoomRates)
                {
                    foreach (proxy.RoomRate roomRate in roomStay.RoomRates)
                    {
                        string ratePlanCode = roomRate.ratePlanCode;
                        if (this.IsTrueBlockRates)
                        {
                            if (this.IsRoomCodeInCMS(roomRate.roomTypeCode)
                                && !bonusChequeRates.Contains(roomRate.ratePlanCode))
                            {
                                if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                                {
                                    OwsRoomRate roomRateEntity = this.GetRoomRate(roomRate);
                                    room.RoomRates.Add(roomRateEntity);
                                }
                            }
                        }
                        else if (this.IsRatePlanCodeInCMS(ratePlanCode) &&
                                this.IsRoomCodeInCMS(roomRate.roomTypeCode) &&
                             !bonusChequeRates.Contains(roomRate.ratePlanCode))
                        {
                            if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                            {
                                OwsRoomRate roomRateEntity = this.GetRoomRate(roomRate);
                                room.RoomRates.Add(roomRateEntity);
                            }
                        }
                    }
                }

                if (room.RoomRates.Count > 0)
                {
                    this.Rooms.Add(room);
                }
            }
        }

        /// <summary>
        /// Sets up available hotel details for promo codes.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="campaignCode">The campaign code.</param>
        private void SetUpAvailableHotelDetailsForPromoCodes(List<proxy.RoomStay> listRoomStay, string campaignCode)
        {
            this.Rooms = new List<OwsRoom>();
            List<string> bonusChequeRates = ContentAccess.GetBonusChequeRateCodes();
            List<string> promoCodes = GetPromotionRates(listRoomStay, campaignCode);
            if (listRoomStay != null && listRoomStay.Count > 0)
            {
                foreach (proxy.RoomStay roomStay in listRoomStay)
                {
                    if (roomStay != null)
                    {
                        OwsRoom room = new OwsRoom();
                        if (null != roomStay.RoomRates)
                        {
                            foreach (proxy.RoomRate roomRate in roomStay.RoomRates)
                            {
                                if (this.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                                        this.IsRoomCodeInCMS(roomRate.roomTypeCode) &&
                                       !bonusChequeRates.Contains(roomRate.ratePlanCode))
                                {
                                    ////string rateCategoryName = this.GetRateCategoryByRateCode(roomRate.ratePlanCode).RateCategoryName;
                                    if (promoCodes != null && promoCodes.Contains(roomRate.ratePlanCode))
                                    {
                                        this.HasPromotionalRates = true;
                                        OwsRoomRate roomRateDetails = this.GetRoomRate(roomRate);
                                        room.RoomRates.Add(roomRateDetails);
                                    }
                                }
                            }
                        }

                        this.Rooms.Add(room);
                    }
                }
            }
        }

        /// <summary>
        /// Sets up available hotel details for promo codes.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="campaignCode">The campaign code.</param>
        private void SetUpAvailableHotelRoomRatesDetailsForPromoCodes(List<proxy.RoomStay> listRoomStay, string campaignCode)
        {
            this.Rooms = new List<OwsRoom>();
            List<string> bonusChequeRates = ContentAccess.GetBonusChequeRateCodes();
            List<string> promoCodes = GetPromotionRates(listRoomStay, campaignCode);
            if (listRoomStay != null && listRoomStay.Count > 0)
            {
                foreach (proxy.RoomStay roomStay in listRoomStay)
                {
                    if (roomStay != null)
                    {
                        OwsRoom room = new OwsRoom();
                        if (null != roomStay.RoomRates)
                        {
                            foreach (proxy.RoomRate roomRate in roomStay.RoomRates)
                            {
                                if (this.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                                        this.IsRoomCodeInCMS(roomRate.roomTypeCode) &&
                                       !bonusChequeRates.Contains(roomRate.ratePlanCode))
                                {
                                    ////string rateCategoryName = this.GetRateCategoryByRateCode(roomRate.ratePlanCode).RateCategoryName;
                                    if (promoCodes != null && promoCodes.Contains(roomRate.ratePlanCode))
                                    {
                                        this.HasPromotionalRates = true;
                                        OwsRoomRate roomRateDetails = this.GetRoomRate(roomRate);
                                        room.RoomRates.Add(roomRateDetails);
                                    }
                                }
                            }
                        }

                        this.Rooms.Add(room);
                    }
                }
            }
        }


        /// <summary>
        /// Gets the room rate.
        /// </summary>
        /// <param name="roomRate">The room rate.</param>
        /// <returns>Returns the OwsRoomRate</returns>
        private OwsRoomRate GetRoomRate(proxy.RoomRate roomRate)
        {
            OwsRoomRate owsRoomRate = new OwsRoomRate()
            {
                RoomTypeCategory = (roomRate.roomTypeCode != null && this.GetRoomCategoryByRoomCode(roomRate.roomTypeCode) != null) ? this.GetRoomCategoryByRoomCode(roomRate.roomTypeCode).RoomCategoryName : string.Empty,
                RateTypeCategory = (roomRate.ratePlanCode != null && this.GetRateCategoryByRateCode(roomRate.ratePlanCode) != null) ? this.GetRateCategoryByRateCode(roomRate.ratePlanCode).RateCategoryName : string.Empty,
                BaseRate = (roomRate.Rates != null && roomRate.Rates[0] != null) ? Convert.ToDouble(roomRate.Rates[0].Base.Value) : 0,
                BaseRateCurrency = (roomRate.Rates != null && roomRate.Rates[0] != null) ? roomRate.Rates[0].Base.currencyCode : string.Empty,
                TotalRate = (roomRate != null && roomRate.Total != null) ? roomRate.Total.Value : 0,
                TotalRateCurrency = (roomRate != null && roomRate.Total != null) ? roomRate.Total.currencyCode : string.Empty,
                RatePlanCode = (roomRate != null) ? roomRate.ratePlanCode : string.Empty,
                RoomTypeCode = (roomRate != null) ? roomRate.roomTypeCode : string.Empty,
            };

            return owsRoomRate;
        }

        /// <summary>
        /// Determines whether [is rate plan code in CMS] [the specified rateCode].
        /// </summary>
        /// <param name="rateCode">rate Code for which the Rate category has to be retrieved</param>
        /// <returns>boolean indicating whether the specified RatePlancode is configured in CMS</returns>
        private bool IsRatePlanCodeInCMS(string rateCode)
        {
            bool isInCMS = true;
            if (!this.RateNetwork.RateTypeMap.Keys.Contains(rateCode))
            {
                isInCMS = false;
            }

            return isInCMS;
        }

        /// <summary>
        /// Determines whether [is room plan code in CMS] [the specified roomCode].
        /// </summary>
        /// <param name="roomCode">room Code for which the Room category has to be retrieved</param>
        /// <returns>boolean indicating whether the specified roomCode is configured in CMS</returns>
        private bool IsRoomCodeInCMS(string roomCode)
        {
            bool isInCMS = true;
            if (!this.RoomNetwork.RoomTypeMap.Keys.Contains(roomCode))
            {
                isInCMS = false;
            }

            return isInCMS;
        }

        private List<string> GetUnavailableRoomType(proxy.RoomStay roomStay, int numberOfRoomPerNight)
        {
            List<string> roomToRemove = new List<string>();
            if (null != roomStay)
            {
                int roomTypeLength = roomStay.RoomTypes.Length;
                for (int count = 0; count < roomTypeLength; count++)
                {
                    string roomTypeCode = roomStay.RoomTypes[count].roomTypeCode;
                    if (IsRoomCodeInCMS(roomTypeCode))
                    {
                        if (roomStay.RoomTypes[count].numberOfUnits < numberOfRoomPerNight)
                        {
                            roomToRemove.Add(roomTypeCode);
                        }
                    }
                }
            }
            if (null != roomStay.RoomRates)
            {
                int roomRateLength = roomStay.RoomRates.Length;
                for (int count = 0; count < roomRateLength; count++)
                {
                    string roomTypeCode = roomStay.RoomRates[count].roomTypeCode;
                    if (IsRoomCodeInCMS(roomTypeCode))
                    {
                        if ((roomStay.RoomRates[count].Rates == null))
                        {
                            roomToRemove.Add(roomTypeCode);
                        }
                    }
                }
            }
            return roomToRemove;
        }

        private static string GetPaymentType(string guaranteeType)
        {
            string result = PaymentTypes.Postpaid;
            string[] guaranteeTypes = null;
            string commaDelimitedGuaranteeTypes = ConfigHelper.GetValue(ConfigKeys.PrepaidGuaranteeTypes);
            if (!string.IsNullOrEmpty(commaDelimitedGuaranteeTypes))
            {
                guaranteeTypes = commaDelimitedGuaranteeTypes.Split(',');
                result = guaranteeTypes.Contains(guaranteeType) ? PaymentTypes.Prepaid : PaymentTypes.Postpaid;
            }
            return result;
        }

        private static bool IsBreakFastIncluded(string hotelID)
        {
            bool result = true;
            string[] hotelIds = null;
            string commaDelimitedHotelIDs = ConfigHelper.GetValue(ConfigKeys.XenaBreakfastNotIncludedHotels);
            if (!string.IsNullOrEmpty(commaDelimitedHotelIDs))
            {
                hotelIds = commaDelimitedHotelIDs.Split(',');
                result = hotelIds.Contains(hotelID) ? false : true;
            }
            return result;
        }


    }
}
