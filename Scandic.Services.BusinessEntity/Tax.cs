﻿// <copyright file="Tax.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Scandic.Services.BusinessEntity
{
    [Serializable]
    [DataContract(
      Name = "Tax",
      Namespace = @"http://api.scandichotels.com/schemas")]
    public class Tax
    {
        /// <summary>
        /// Amount of Tax
        /// </summary>
        [DataMember(
        Name = "Amount",
        Order = 0,
        IsRequired = false)]
        public double Amount { get; set; }

        /// <summary>
        /// Type of Tax
        /// </summary>
        [DataMember(
        Name = "Type",
        Order = 1,
        IsRequired = false)]
        public string Type { get; set; }

    }
}
