﻿// <copyright file="AvailableHotelDetailsList.cs" company="Scandic Hotels">
//     Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Scandic.Services.ServiceAgents;   

    /// <summary>
    /// AvailableHotelDetailsList class
    /// </summary>
    [Serializable]
    public class AvailableHotelDetailsList
    {
        /// <summary>
        /// Initializes a new instance of the AvailableHotelDetailsList class.
        /// </summary>
        /// <param name="hotelList">list of AvailableHotelDetails</param>
        public AvailableHotelDetailsList(List<AvailableHotelDetails> hotelList)
        {
            this.AvailableHotels = new List<AvailableHotelDetails>();
            if (hotelList != null && hotelList.Count > 0)
            {
                this.AvailableHotels = hotelList.OrderBy(r => r.HotelFromRate.PricePerNight).ToList<AvailableHotelDetails>();
            }
        }

        /// <summary>
        /// Gets or sets the language in which data is to be filled (default: English)
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the property provider name (default: Scandic)
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Gets or sets the continent.
        /// </summary>
        /// <value>
        /// The continent.
        /// </value>
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets the messageon on single hotel search not available.
        /// </summary>
        /// <value>
        /// The messageon on single ho  tel search not available.
        /// </value>
        public string MessageOnSingleHotelSearchNotAvailable { get; set; }

        /// <summary>
        /// Gets or sets the city Id
        /// </summary>
        public string CityId { get; set; }

        /// <summary>
        /// Gets or sets the deep link URL.
        /// </summary>
        /// <value>
        /// The deep link URL.
        /// </value>
        public string DeeplinkUrlHotelList { get; set; }

        /// <summary>
        /// Gets or sets the deep link URL find hotel page.
        /// </summary>
        /// <value>
        /// The deep link URL.
        /// </value>
        public string DeepLinkUrlFindHotelPage { get; set; }

        /// <summary>
        /// Gets the list of HotelDetails where provider operates
        /// </summary>
        public IList<AvailableHotelDetails> AvailableHotels { get; private set; }       
    }
}
