﻿// <copyright file="HotelSearchEntity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// The enumerated type containing the different values
    /// for 5 different kinds of search user can do in the site    
    /// </summary>
    public enum SearchType
    {
        /// <summary>
        /// The Regular
        /// </summary>
        REGULAR,

        /// <summary>
        /// The Corporate 
        /// </summary>
        CORPORATE,
    }

    /// <summary>
    /// Basic information on a HotelSearchEntity.
    /// </summary>
    public class HotelSearchEntity
    {
        ////public HotelSearchEntity()
        ////{
        ////   ListRooms = new List<HotelSearchRoomEntity>();
        ////}

        /// <summary>
        /// Initializes a new instance of the HotelSearchEntity class
        /// </summary>
        public HotelSearchEntity()
        {
            this.SelectedHotelCode = new List<string>();
            this.ListRooms = new List<HotelSearchRoomEntity>();
        }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the searchedfor.
        /// </summary>
        public SearchedForEntity SearchedFor { get; set; }

        /// <summary>
        /// Gets or sets the arrival date.
        /// </summary>
        /// <value>The arrival date.</value>       
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// Gets or sets the departure date
        /// </summary>
        /// <value>The departure date.</value>       
        public DateTime DepartureDate { get; set; }

        /// <summary>           
        /// Gets the NoOfNights 
        /// </summary> 
        public int NoOfNights
        {
            get
            {
                if (this.ArrivalDate != default(DateTime)
                    && this.DepartureDate != default(DateTime)
                    && this.DepartureDate > this.ArrivalDate)
                {
                    return (this.DepartureDate - this.ArrivalDate).Days;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets the rooms per night.
        /// </summary>
        /// <value>The rooms per night.</value>       
        public int RoomsPerNight { get; set; }

        /// <summary>
        /// Gets the selected hotel code.
        /// </summary>
        /// <value>The selected hotel code.</value>       
        public IList<string> SelectedHotelCode { get; private set; }

        /// <summary>
        /// Gets the list of rooms.
        /// </summary>
        /// <value>The list rooms.</value>        
        public IList<HotelSearchRoomEntity> ListRooms { get; private set; }

        /// <summary>
        /// Gets or sets the hotel country code.
        /// </summary>
        /// <value>The hotel country code.</value>      
        public string HotelCityId { get; set; }

        /// <summary>
        /// Gets or sets the search type object
        /// containing the type of search user has done.
        /// </summary>
        public SearchType SearchingType { get; set; }

        /// <summary>
        /// Gets or sets the Campaign code valid for the corresponding type of search user has done
        /// If Regular Search/Booking => Promotion code enter by user need to be set
        /// If Negotiated Search/Booking => D-Number/Corporate code entered by user need to be set
        /// If Voucher Search/Booking => The Voucher special code to be set
        /// </summary>
        public string CampaignCode { get; set; }

        /// <summary>
        /// Gets or sets the Qualifying Type
        /// </summary>
        public string QualifyingType { get; set; }
    }
}