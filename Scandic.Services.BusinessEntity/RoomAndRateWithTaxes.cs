﻿// <copyright file="RoomAndRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// This class has the fields to hold the RoomAndRate of the hotel
    /// </summary>    
    public class RoomAndRateWithTaxes : RoomAndRate
    {
        /// <summary>
        /// Gets or sets TotalPricePerStay
        /// </summary>          
        public double TotalBaseRatePerStay { get; set; }

        /// <summary>
        /// Gets or sets TotalPricePerNight
        /// </summary>        
        public double TotalBaseRatePerNight { get; set; }

        /// <summary>
        /// Gets or Sets Taxes 
        /// </summary>
        public List<Tax> Taxes { get; set; }

    }
}
