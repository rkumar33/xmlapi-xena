﻿// <copyright file="HotelCoordinates.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;

    /// <summary>
    /// This class has fields that holds the coordinates of the hotel
    /// </summary>
    [Serializable]
    public class HotelCoordinates
    {
        /// <summary>
        /// Gets or sets the Longitude
        /// </summary>         
        public string Longitude { get; set; }

        /// <summary>
        /// Gets or sets the Latitude 
        /// </summary>         
        public string Latitude { get; set; }
    }
}
