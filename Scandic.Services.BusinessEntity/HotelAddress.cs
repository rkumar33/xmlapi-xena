﻿// <copyright file="HotelAddress.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;

    /// <summary>
    /// This class has fields that holds the address of the hotel
    /// </summary>
    [Serializable]
    public class HotelAddress
    {
        /// <summary>
        /// Gets or sets the streetaddress of the hotel 
        /// </summary>         
        public string StreetAddress { get; set; }

        /// <summary>
        /// Gets or sets the Postal Code of the hotel
        /// </summary>         
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the postalcity of the city 
        /// </summary>         
        public string PostalCity { get; set; }

        /// <summary>
        /// Gets or sets the MarketingCityName 
        /// </summary>         
        public string MarketingCityName { get; set; }

        /// <summary>
        /// Gets or sets the Name of the country 
        /// </summary>         
        public string Country { get; set; }
    }
}
