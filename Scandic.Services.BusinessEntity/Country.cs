﻿// <copyright file="Country.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Continent entity
    /// </summary>
    [Serializable]
    public class Country
    {
        /// <summary>
        /// Initializes a new instance of the Country class
        /// </summary>
        public Country()
        {
            this.Cities = new List<City>();
        }

        /// <summary>
        /// Gets or sets ID of the country
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the country
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Name of the Continent the country belongs to
        /// </summary>
        public string Continent { get; set; }

        /// <summary>
        /// Gets the list of cities in the country 
        /// </summary>
        public IList<City> Cities { get; private set; }

        /// <summary>
        /// Gets the Tax Rates of the hotel
        /// </summary>
        public double APITaxRates { get; set; }
    }
}
