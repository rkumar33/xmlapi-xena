﻿// <copyright file="RateTypes.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Scandic.Services.ServiceAgents.CmsEntity;

    /// <summary>
    /// Entire meta of the Rate Types
    /// </summary>
    [Serializable]
    public class RateTypes
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateTypes"/> class.
        /// </summary>
        public RateTypes()
        {
            this.RateCategoryCollection = new List<RateCategory>();
            this.RateTypeMap = new Dictionary<string, string>();
            this.RatePlanCodes = new List<RateCode>();
        }

        /// <summary>
        /// Gets or sets the language in which data is to be filled (default: English)
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets the rate category collection.
        /// </summary>
        public List<RateCategory> RateCategoryCollection { get; private set; }

        /// <summary>
        /// Gets the rate type map.
        /// </summary>
        public IDictionary<string, string> RateTypeMap { get; private set; }

        /// <summary>
        /// Gets the rateplan codes.
        /// </summary>
        public List<RateCode> RatePlanCodes { get; private set; }
    }
}
