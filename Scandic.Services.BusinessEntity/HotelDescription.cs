﻿// <copyright file="HotelDescription.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Text.RegularExpressions;

    /// <summary>
    /// This class has fields that holds the description of the hotel
    /// </summary>
    [Serializable]
    public class HotelDescription
    {
        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        private static Regex htmlRemover = new Regex(@"(\<.*?\>|&lt;.*?\>)", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        private static Regex nbspHtmlRemover = new Regex(@"(\&nbsp;|&ampnbsp;|&amp;nbsp;)", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        private static Regex ampHtmlRemover = new Regex(@"(\&amp;)", RegexOptions.Compiled);

        /// <summary>
        /// Private variable for HotelIntro
        /// </summary>
        private string hotelIntro;

        /// <summary>
        /// Private variable for HotelFacilitiesDescription
        /// </summary>
        private string hotelFacilitiesDescription;

        /// <summary>
        /// Gets or sets the HotelIntro 
        /// </summary>  
        public string HotelIntro
        {
            get
            {
                return this.hotelIntro;
            }

            set
            {
                this.hotelIntro = htmlRemover.Replace(value, string.Empty);
                this.hotelIntro = nbspHtmlRemover.Replace(this.hotelIntro, " ");
                this.hotelIntro = ampHtmlRemover.Replace(this.hotelIntro, "&");
            }
        }

        /// <summary>
        /// Gets or sets the HotelFacilitiesDescription 
        /// </summary>  
        public string HotelFacilitiesDescription
        {
            get
            {
                return this.hotelFacilitiesDescription;
            }

            set
            {
                this.hotelFacilitiesDescription = htmlRemover.Replace(value, string.Empty);
                this.hotelFacilitiesDescription = nbspHtmlRemover.Replace(this.hotelFacilitiesDescription, " ");
                this.hotelFacilitiesDescription = ampHtmlRemover.Replace(this.hotelFacilitiesDescription, "&");
            }
        }
    }
}
