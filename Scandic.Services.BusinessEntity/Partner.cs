﻿// <copyright file="Partner.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using Scandic.Services.Framework;

    /// <summary>
    /// Entity class for partner
    /// </summary>    
    [Serializable]
    public class Partner
    {
        /// <summary>
        /// Initializes a new instance of the Partner class.
        /// </summary>        
        public Partner()
        {
             this.Operator = "Scandic";
             this.IsPartnerEnabled = false;
             this.InterfaceList = new List<InterfaceDetails>();
             this.ArbCodes = new List<string>();
             this.ArbCodesMapWithOperaId = new Dictionary<string, string>();
             this.CorporateCodes = new List<string>();
             this.PromoCodes = new List<string>();
             this.TravelAgentCodes = new List<string>();
        }
        
        /// <summary>
        /// Gets or sets the operator.
        /// </summary>        
        public string Operator { get; set; }   

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value> Gets or Sets the partner name. </value>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the API key.
        /// </summary>
        /// <value>The API key.</value>       
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the interface list.
        /// </summary>
        /// <value>The interface list.</value>        
        public List<InterfaceDetails> InterfaceList { get; set; }

        /// <summary>
        /// Gets or sets the validity start date.
        /// </summary>
        /// <value>The validity start date.</value>        
        public DateTime ValidityStartDate { get; set; }

        /// <summary>
        /// Gets or sets the validity end date.
        /// </summary>
        /// <value>The validity end date.</value>     
        public DateTime ValidityEndDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the tracking code.
        /// </summary>
        /// <value>The tracking code.</value>      
        public string TrackingCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is tracking code disabled.
        /// </summary>
        /// <value><c>true</c> if this instance is tracking code disabled; otherwise, <c>false</c>.</value>        
        public bool IsTrackingCodeDisabled { get; set; }

        /// <summary>
        /// Gets or sets the ARB codes.
        /// </summary>
        /// <value>The ARB codes.</value>       
        public List<string> ArbCodes { get; set; }

        /// <summary>
        /// Gets or sets the OperaIds with ARB codes.
        /// </summary>
        /// <value>The ARB codes, OperaIds.</value>       
        public Dictionary<string, string> ArbCodesMapWithOperaId { get; set; }

        /// <summary>
        /// Gets or sets the promocodes.
        /// </summary>
        /// <value>The promocodes.</value>        
        public List<string> PromoCodes { get; set; }

        /// <summary>
        /// Gets or sets the corporate codes.
        /// </summary>
        /// <value>The corporate codes.</value>        
        public IList<string> CorporateCodes { get; set; }

        /// <summary>
        /// Gets or sets the travel agent codes.
        /// </summary>
        /// <value>The travel agent codes.</value>      
        public IList<string> TravelAgentCodes { get; set; }

        /// <summary>
        /// Gets or sets the partner profile code.
        /// </summary>
        /// <value>The partner profile code.</value>      
        public string PartnerProfileCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is partner enabled.
        /// </summary>
        /// <value><c>true</c> if this instance is partner enabled; otherwise, <c>false</c>.</value>      
        public bool IsPartnerEnabled { get; set; }

        /// <summary>
        /// Determines whether partner has access to the specified key.
        /// </summary>
        /// <param name="apiName">Name of the API.</param>
        /// <returns><c>true</c> if [has partner access to] [the specified key]; otherwise, <c>false</c>.</returns>       
        public bool HasAccessTo(string apiName)
        {
            return this.InterfaceList.Exists(delegate(InterfaceDetails interfaceDetails)
            {                
                return interfaceDetails.Name.ToUpperInvariant().Trim().Contains(apiName.Trim().ToUpperInvariant());
            });
        }

        /// <summary>
        /// Determines whether [has access to promo code] [the specified booking code].
        /// </summary>
        /// <param name="bookingCode">The booking code.</param>
        /// <returns>
        ///   <c>true</c> if [has access to promo code] [the specified booking code]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasAccessToPromoCode(string bookingCode)
        {
            return this.PromoCodes.Exists(delegate(string code)
            {
                return code.Equals(bookingCode, StringComparison.OrdinalIgnoreCase);
            });
       }

        /// <summary>
        /// Determines whether [has access to block code] [the specified block code].
        /// </summary>
        /// <param name="blockCode">The block code.</param>
        /// <returns>
        ///   <c>true</c> if [has access to block code] [the specified block code]; otherwise, <c>false</c>.
        ///   Please ensure to add validation to pick published rate codes.
        /// </returns>
        public bool HasAccessToBlockCode(string blockCode)
        {
            string bookingCode = blockCode;

            if (this.ArbCodesMapWithOperaId.ContainsKey(blockCode))
            {
                bookingCode = this.ArbCodesMapWithOperaId[blockCode];
            }

            return this.ArbCodes.Exists(delegate(string code)
            {
                return code.Equals(bookingCode, StringComparison.OrdinalIgnoreCase);
            });

        }
    }
}
