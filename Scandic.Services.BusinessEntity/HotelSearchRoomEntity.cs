﻿// <copyright file="HotelSearchRoomEntity.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Number of Adults per room user has selected
    /// </summary>
    public class HotelSearchRoomEntity
    {
        /// <summary>
        /// Initializes a new instance of the HotelSearchRoomEntity class.
        /// </summary>
        public HotelSearchRoomEntity()
        {
            this.Listchildern = new List<ChildEntity>();
        }

        /// <summary>
        /// Gets or sets the adults per room.
        /// </summary>
        /// <value>The adults per room.</value>        
        public int AdultsPerRoom { get; set; }

        /// <summary>
        /// Gets or sets the children per room.
        /// </summary>
        /// <value>The children per room.</value>            
        public int ChildrenPerRoom { get; set; }

        /// <summary>
        /// Gets the list childern.
        /// </summary>
        public IList<ChildEntity> Listchildern { get; private set; }

        /// <summary>
        /// Gets the children occupancy per room.
        /// </summary>
        /// <value> The children occupancy per room. </value>     
        public int ChildrenOccupancyPerRoom
        {
            get
            {
                return this.SetChildrenAccupancyDetails();
            }
        }

        /// <summary>
        /// Sets the children accupancy details.
        /// </summary>        
        /// <returns>The count of numer of occupants</returns>
        private int SetChildrenAccupancyDetails()
        {
            int count = 0;
            int adultCount = this.AdultsPerRoom;
            foreach (ChildEntity ch in this.Listchildern)
            {  //// To set the occupancy in other cases.
                if ((ch.Age > 5 && ch.Age <= 12) || ch.Age < 0)
                {
                    count++;
                }
                else
                {
                    adultCount--;
                    if (adultCount < 0)
                    {
                        count++;
                    }
                }
            }

            count += this.ChildrenPerRoom - this.Listchildern.Count;
            return count;
        }
    }
}