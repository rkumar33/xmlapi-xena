﻿// <copyright file="HotelDetailsList.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Entire meta of the HotelDetails information
    /// </summary>
   [Serializable]
    public class HotelDetailsList
    {
        /// <summary>
        /// Initializes a new instance of the HotelDetailsList class
        /// </summary>
       public HotelDetailsList()
       {
           this.Hotels = new List<HotelDetails>();
       }

        /// <summary>
        /// Gets or sets the language in which data is to be filled (default: English)
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the property provider name (default: Scandic)
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Gets the list of HotelDetails where provider operates
        /// </summary>
        public IList<HotelDetails> Hotels { get; private set; }
    }
}
