﻿// <copyright file="RoomTypes.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Scandic.Services.ServiceAgents.CmsEntity;

    /// <summary>
    /// Entire meta of the Room Types
    /// </summary>
    [Serializable]
    public class RoomTypes
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomTypes"/> class.
        /// </summary>
        public RoomTypes()
        {
            this.RoomCategoryCollection = new List<RoomCategory>();
            this.RoomTypeMap = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets the language in which data is to be filled (default: English)
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets the room category collection.
        /// </summary>
        public List<RoomCategory> RoomCategoryCollection { get; private set; }

        /// <summary>
        /// Gets the room type map.
        /// </summary>
        public IDictionary<string, string> RoomTypeMap { get; private set; }
    }
}
