﻿// <copyright file="RoomFacilities.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Text.RegularExpressions;

    /// <summary>
    /// This class has the fields to hold the facilities available in  hotel rooms
    /// </summary>
    [Serializable]
    public class RoomFacilities
    {
        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        private static Regex htmlRemover = new Regex(@"(\<.*?\>|&lt;.*?\>)", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        private static Regex nbspHtmlRemover = new Regex(@"(\&nbsp;)", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        private static Regex ampHtmlRemover = new Regex(@"(amp;)", RegexOptions.Compiled);

        /// <summary>           
        /// Variable to hold the Description 
        /// </summary>  
        private string description;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoomFacilities"/> class.
        /// </summary>
        public RoomFacilities()
        {
            this.Image = new RoomImage();
        }

        /// <summary>           
        /// Gets or sets the Name 
        /// </summary>  
        public string Name { get; set; }
        
        /// <summary>           
        /// Gets or sets the Description 
        /// </summary>  
        public string Description
        {
            get
            {
                return this.description;
            }

            set
            {
                this.description = ampHtmlRemover.Replace(value, string.Empty);
                this.description = htmlRemover.Replace(this.description, string.Empty);
                this.description = nbspHtmlRemover.Replace(this.description, " ");
            }
        }

        /// <summary>           
        /// Gets or sets the Image details 
        /// </summary>  
        public RoomImage Image { get; set; }

        /// <summary>
        /// Gets or sets the Page Reference ID of the corresponding room category under structured information.
        /// </summary>
        public string RoomCategoryPageReferenceId { get; set; }
    }
}
