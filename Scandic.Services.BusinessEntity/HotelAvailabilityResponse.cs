﻿// <copyright file="HotelAvailabilityResponse.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;

    /// <summary>
    /// This class has fields that holds the Availability Response against the corresponding hotel Id
    /// </summary>
    [Serializable]
    public class HotelAvailabilityResponse
    {
        /// <summary>
        /// Gets or sets the Id of the hotel 
        /// </summary>  
        public string HotelId { get; set; }

        /// <summary>
        /// Gets or sets the AvailabilityResponse for the hotel 
        /// </summary>  
        public List<AvailabilityResponse> Responses { get; set; }
    }
}
