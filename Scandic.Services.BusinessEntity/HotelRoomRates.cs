﻿// <copyright file="HotelRoomRates.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

using System.Runtime.Serialization;
namespace Scandic.Services.BusinessEntity
{
    /// <summary>
    /// Gets or Sets Taxes 
    /// </summary>
    [DataContract(Name = "Hotel", Namespace = @"http://api.scandichotels.com/schemas")]
    public class HotelRoomRates
    {
        public string Id { get; set; }
        public RoomsAndRates RoomsAndRates { get; set; }
    }
}
