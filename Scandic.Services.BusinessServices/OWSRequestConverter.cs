// <copyright file="OwsRequestConverter.cs" company="Scandic Hotels">
// Copyright � Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Text;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;

    #endregion

    /// <summary>
    /// OwsRequestConverter class
    /// </summary>
    public class OwsRequestConverter
    {
        #region Availability Request Getters

        /// <summary>
        /// Gethotels the reference.
        /// </summary>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>       
        /// <returns>HotelReference object</returns>
        public static HotelReference GethotelReference(HotelSearchEntity hotelSearchEntity)
        {
            string chainCode = ConfigHelper.GetValue(ConfigKeys.OwSChainCode);
            HotelReference hotelRef = new HotelReference();
            hotelRef.chainCode = chainCode;
            hotelRef.hotelCode = hotelSearchEntity.SelectedHotelCode[0];
            return hotelRef;          
        }

        /// <summary>
        /// Gets the general availability request.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="roomSearch">The room search.</param>        
        /// <returns>AvailabilityRequest object</returns>
        public static AvailabilityRequest GetGeneralAvailabilityRequest(HotelSearchEntity hotelSearch, HotelSearchRoomEntity roomSearch)
        {
            /*******************************************************************************************
            * Get the Request Object for General availability
            *******************************************************************************************/
            HotelReference hotelReference = GethotelReference(hotelSearch);
            AvailRequestSegment requestSegment = new AvailRequestSegment();
            requestSegment.availReqType = AvailRequestType.Room;
            requestSegment.numberOfRoomsSpecified = true;
            requestSegment.numberOfRooms = 1;

            requestSegment.totalNumberOfGuestsSpecified = true;
            if (roomSearch != null)
            {
                requestSegment.totalNumberOfGuests = roomSearch.AdultsPerRoom;
            }

            requestSegment.numberOfChildrenSpecified = true;

            if (roomSearch != null)
            {
                requestSegment.numberOfChildren = roomSearch.ChildrenOccupancyPerRoom;
            }

            Scandic.Services.ServiceAgents.OwsProxy.Availability.TimeSpan dateRange = new Scandic.Services.ServiceAgents.OwsProxy.Availability.TimeSpan();
            dateRange.StartDate = hotelSearch.ArrivalDate;
            dateRange.Item = hotelSearch.DepartureDate;
            requestSegment.StayDateRange = dateRange;

            requestSegment.HotelSearchCriteria = new HotelSearchCriterion[1];
            requestSegment.HotelSearchCriteria[0] = new HotelSearchCriterion();
            requestSegment.HotelSearchCriteria[0].HotelRef = hotelReference;

            AvailabilityRequest request = new AvailabilityRequest();
            request.summaryOnly = true;
            request.AvailRequestSegment = new AvailRequestSegment[1];
            request.AvailRequestSegment[0] = requestSegment;

            RatePlanCandidate ratePlanCandidate = new RatePlanCandidate();
            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                    {
                        ratePlanCandidate.promotionCode = hotelSearch.CampaignCode;
                        requestSegment.RatePlanCandidates = new RatePlanCandidate[] { ratePlanCandidate };
                    }

                    break;

                case SearchType.CORPORATE:
                    if (AppConstants.BLOCKCODEQUALIFYINGTYPE == hotelSearch.QualifyingType)
                    {
                        RoomStayCandidate roomStayCandidate = new RoomStayCandidate();
                        roomStayCandidate.invBlockCode = hotelSearch.CampaignCode;
                        requestSegment.RoomStayCandidates = new RoomStayCandidate[] { roomStayCandidate };
                    }

                    break;
            }

            return request;
        }
        #endregion
    }
}
