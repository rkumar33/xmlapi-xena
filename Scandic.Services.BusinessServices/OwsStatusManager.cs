﻿// <copyright file="OwsStatusManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;

    #endregion

    /// <summary>
    /// class for monitoring Opera availabilty.
    /// </summary>
    public class OwsStatusManager : EntityManagerBase
    {
        /// <summary>
        /// Inherit the OWS availability, for API availability
        /// </summary>
        private const string INHERIT = "INHERIT";

        /// <summary>
        /// Set the API as unavailable
        /// </summary>
        private const string NO = "NO";

        /// <summary>
        /// Initializes a new instance of the OwsStatusManager class.
        /// </summary>
        /// <param name="forceRefreshCache">This parameter if set to true 
        /// will ensure ensure the OWS Status cache is refreshed.</param>
        public OwsStatusManager(bool forceRefreshCache = false)
        {
            this.CacheKey = "OwsMonitorValue";
            this.IsOwsAvailabilityServiceUp = true;            
            this.SetManagedEntity();
            this.SetApiAvailability();
            
        }

        /// <summary>
        /// Gets a value indicating whether this instance is ows availability service up.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is ows availability service up; otherwise, <c>false</c>.
        /// </value>
        public bool IsOwsAvailabilityServiceUp { get; private set; }

        /// <summary>
        /// Gets or sets the Cached value of the managed entity
        /// </summary>
        public new object CachedValue
        {
            get
            {
                return CacheHelper.Get(this.CacheKey);
            }

            set
            {
                this.ExpireCache();
                CacheHelper.Put(this.CacheKey, value, Int32.Parse(ConfigHelper.GetValue(ConfigKeys.OwsSheduledRefresh)));
            }
        }

        /// <summary>
        /// Fills the managed object. This call does not
        /// consider Cache freshness
        /// </summary>
        /// <typeparam name="T">Indicates that the return type Generic</typeparam>
        /// <param name="FillOnline"></param>
        /// <returns>
        /// The entity with data filled
        /// </returns>
        public override T RefreshEntity<T>()
        {
            if (this.IsCached)
            {
                this.ExpireCache();
                this.CachedValue = this.IsOwsAvailabilityServiceUp;
            }

            return default(T);
        }

        /// <summary>
        /// Sets the ows availability.
        /// </summary>
        public override void SetManagedEntity()
        {
            if (this.IsCacheValid)
            {
                this.IsOwsAvailabilityServiceUp = Convert.ToBoolean(this.CachedValue);
            }
            else
            {
                this.RefreshEntity<bool>();
            }
        }

        /// <summary>
        /// Sets the API availability.
        /// </summary>
        private void SetApiAvailability()
        {
            bool isOperaAvailable;
            string apiServiceAvailability = ConfigHelper.GetValue(ConfigKeys.ApiServiceVisibility);
            if (apiServiceAvailability.ToUpper().Equals(INHERIT))
            {
                isOperaAvailable = this.GetOperaAvailability();
            }
            else if (apiServiceAvailability.ToUpper().Equals(NO))
            {
                isOperaAvailable = false;
            }
            else
            {
                isOperaAvailable = true;
            }

            this.IsOwsAvailabilityServiceUp = isOperaAvailable;
        }

        /// <summary>
        /// Gets the opera availability.
        /// </summary>
        /// <returns>Returns a Bool value indicating availability for a hotelsearch in opera</returns>
        private bool GetOperaAvailability()
        {
            HotelSearchEntity hotelSearchEntity = this.CreateSearchEntityForOwsMonitor();
            HotelSearchRoomEntity room = hotelSearchEntity.ListRooms[0];
            AvailabilityRequest request = OwsRequestConverter.GetGeneralAvailabilityRequest(hotelSearchEntity, room);
            return AvailabilityAccess.GetAvailabilityForOWSMonitor(request);
        }

        /// <summary>
        /// Creates the search entity for ows monitor.
        /// </summary>
        /// <returns>Returns the hotel Search entity</returns>
        private HotelSearchEntity CreateSearchEntityForOwsMonitor()
        {
            HotelSearchEntity hotelSearch = new HotelSearchEntity();
            HotelSearchRoomEntity room = new HotelSearchRoomEntity();
            hotelSearch.SelectedHotelCode.Add(ConfigHelper.GetValue(ConfigKeys.HotelIdForOwsMonitor));
            hotelSearch.RoomsPerNight = OwsMonitorConstants.ROOMPERNIGHT;
            hotelSearch.ArrivalDate = DateTime.Today.AddDays(1);
            hotelSearch.DepartureDate = DateTime.Today.AddDays(2);
            room.AdultsPerRoom = OwsMonitorConstants.ADULTSPERROOM;
            room.ChildrenPerRoom = OwsMonitorConstants.CHILDRENPERROOM;
            hotelSearch.ListRooms.Add(room);
            return hotelSearch;
        }
    }
}
