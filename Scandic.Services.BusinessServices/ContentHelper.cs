﻿// <copyright file="ContentHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Linq;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using XenaEntity = Scandic.Services.Xena.BusinessEntity;

    /// <summary>
    /// ContentHelper class
    /// </summary>
    public class ContentHelper
    {
        /// <summary>
        /// Gets the property value.
        /// </summary>
        /// <param name="page">Name of the page.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>the value of the property name specified from the page passed In</returns>
        public static string GetMasterValue(RawPage page, string propertyName)
        {
            int masterId = Int32.Parse(GetPropertyValue(page, "PageLink"));
            string masterLang = !string.IsNullOrEmpty(ContentHelper.GetPropertyValue(page, "PageMasterLanguageBranch")) ?
                ContentHelper.GetPropertyValue(page, "PageMasterLanguageBranch") : "en";
            RawPage masterPage = ContentAccess.GetPage(masterId, masterLang);
            string masterValue = string.Empty;
            if (masterPage != null)
            {
                RawProperty property = masterPage.Property.ToList<RawProperty>().Find(delegate(RawProperty p)
                {
                    return p.Name.Equals(propertyName);
                });

                if (property != null)
                {
                    if (!string.IsNullOrEmpty(property.Value))
                    {
                        masterValue = property.Value;
                    }
                }
            }

            LogHelper.LogInfo(string.Format("Returned Master {0,20} : {1,20}", propertyName, masterValue), LogCategory.Content);
            return masterValue;
        }


        /// <summary>
        /// Gets the property value Not Master Value.
        /// </summary>
        /// <param name="page">Name of the page.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>the value of the property name specified from the page passed In</returns>
        public static string GetPropertyValueNotMasterValue(RawPage page, string propertyName)
        {
            string propertyValue = string.Empty;
            if (page != null)
            {
                RawProperty property = page.Property.ToList<RawProperty>().Find(delegate(RawProperty p)
                {
                    return p.Name.Equals(propertyName);
                });

                if (property != null)
                {
                    if (!string.IsNullOrEmpty(property.Value))
                    {
                        propertyValue = property.Value;
                    }
                }
            }

            LogHelper.LogInfo(string.Format("Returned {0,20} : {1,20}", propertyName, propertyValue), LogCategory.Content);
            return propertyValue;
        }
        /// <summary>
        /// Gets the property value.
        /// </summary>
        /// <param name="page">Name of the page.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>the value of the property name specified from the page passed In</returns>
        public static string GetPropertyValue(RawPage page, string propertyName)
        {
            string propertyValue = string.Empty;
            if (page != null)
            {
                RawProperty property = page.Property.ToList<RawProperty>().Find(delegate(RawProperty p)
                {
                    return p.Name.Equals(propertyName);
                });

                if (property != null)
                {
                    if (!string.IsNullOrEmpty(property.Value))
                    {
                        propertyValue = property.Value;
                    }
                    else
                    {
                        propertyValue = GetMasterValue(page, propertyName);
                    }
                }
            }

            LogHelper.LogInfo(string.Format("Returned {0,20} : {1,20}", propertyName, propertyValue), LogCategory.Content);
            return propertyValue;
        }

        /// <summary>
        /// Determines whether specified page is published in CMS or not.
        /// </summary>
        /// <param name="page">The page to be verified.</param>
        /// <returns>
        ///   <c>true</c> if [is page published] [the specified page]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPagePublished(RawPage page)
        {
            bool isPagePublished = false;

            if (!string.IsNullOrEmpty(GetPropertyValue(page, "PageStartPublish")))
            {
                DateTime pageStartPublish = Convert.ToDateTime(GetPropertyValue(page, "PageStartPublish"));
                isPagePublished = pageStartPublish <= DateTime.Now;
            }

            if (!string.IsNullOrEmpty(GetPropertyValue(page, "PageStopPublish")))
            {
                DateTime pageStopPublish = Convert.ToDateTime(GetPropertyValue(page, "PageStopPublish"));
                isPagePublished = pageStopPublish >= DateTime.Now;
            }

            return isPagePublished;
        }
        /// <summary>
        /// Determines whether specified page is published in CMS or not.
        /// </summary>
        /// <param name="page">The page to be verified.</param>
        /// <returns>
        ///   <c>true</c> if [is page published] [the specified page]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPagePublished(string ValidityStartDate,string ValidityEndDate)
        {
            bool isPagePublished = false;

            if (!string.IsNullOrEmpty(ValidityStartDate))
            {
                DateTime pageStartPublish = Convert.ToDateTime(ValidityStartDate);
                isPagePublished = pageStartPublish <= DateTime.Now;
            }

            if (!string.IsNullOrEmpty(ValidityEndDate))
            {
                DateTime pageStopPublish = Convert.ToDateTime(ValidityEndDate);
                isPagePublished = pageStopPublish >= DateTime.Now;
            }

            return isPagePublished;
        }
    }
}
