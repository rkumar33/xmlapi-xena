﻿// <copyright file="ContentBusinessService.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using System;

    /// <summary>
    /// Implements methods related to CMS data fetch
    /// </summary>
    public class ContentBusinessService : IContentBusinessContract
    {
        /// <summary>
        /// Gets or sets the language in which entities
        /// are to be returned, where applicable
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the provider for which entities
        /// are to be returned, where applicable
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Get the whole hotel network meta
        /// </summary>
        /// <returns>Hotel Network as an object tree</returns>
        public HotelNetwork GetHotelNetwork()
        {
            return new HotelNetworkManager(this.Language, this.Provider).Network;
        }

        /// <summary>
        /// Get list of hotels and details
        /// </summary>
        /// <returns>List of hotels and details</returns>
        public HotelDetailsList GetHotelDetails()
        {
            return new HotelDetailsManager(this.Language, this.Provider).HotelCollection;
        }

        /// <summary>
        /// Returns details of one partner, identified by API Key
        /// </summary>
        /// <param name="apiKey">API Key of the partner</param>
        /// <returns>Details of the partner</returns>
        public Partner GetPartnerDetails(string apiKey)
        {
            return new PartnerManager().GetPartnerDetails(apiKey);
        }

        /// <summary>
        /// Gets the rate details.
        /// </summary>
        /// <returns>List of the Rate categories and Rate Code configured in CMS</returns>
        public RateTypes GetRateTypes()
        {
            return new RateTypeManager(this.Language).RateTypes;
        }

        /// <summary>
        /// Gets the rate details.
        /// </summary>
        /// <returns>List of the Room categories and Room Codes configured in CMS</returns>
        public RoomTypes GetRoomTypes()
        {
            return new RoomTypeManager(this.Language).RoomTypes;
        }

        /// <summary>
        /// Refreshes all cached entities
        /// </summary>
        public void RefreshCache()
        {
            string[] languages = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).Split(',');
            string provider = ConfigHelper.GetValue(ConfigKeys.Provider);
            
            foreach (string language in languages)
            {
                try
                {
                    LogHelper.LogInfo(
                    string.Format("Preloading Cache: Network | {0} | {1}", provider, language),
                    LogCategory.Service);
                    new HotelNetworkManager(language, provider);
                }
                catch (Exception ex)
                {

                    LogHelper.LogException(ex, LogCategory.Service);
                }

                try
                {
                    LogHelper.LogInfo(
                    string.Format("Preloading Cache: Hotels | {0} | {1}", provider, language),
                    LogCategory.Service);
                    new HotelDetailsManager(language, provider);
                }
                catch (Exception ex)
                {

                    LogHelper.LogException(ex, LogCategory.Service);
                }
            }
             
        }
    }
}
