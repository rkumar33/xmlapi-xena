﻿// <copyright file="HotelNetworkHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Scandic.Services.BusinessEntity;

    /// <summary>
    /// HotelNetworkHelper class
    /// </summary>
    public class HotelNetworkHelper
    {
        #region Constants
        /// <summary>
        /// Language constant for default
        /// </summary>
        private const string LANG = "en";

        /// <summary>
        /// Provider constatnt for default
        /// </summary>
        private const string PROVIDER = "Scandic";
        #endregion

        #region Master Lists
        /// <summary>
        /// Gets a list of all continents
        /// </summary>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all continents</returns>
        public static IEnumerable<Continent> AllContinents(string language, string provider = PROVIDER)
        {
            return new HotelNetworkManager(language, provider)
                .Network
                .Continents;
        }

        /// <summary>
        /// Gets a list of all countries
        /// </summary>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all countries</returns>
        public static IEnumerable<Country> AllCountries(string language, string provider = PROVIDER)
        {
            return AllContinents(language, provider)
                .SelectMany(continent => continent.Countries);
        }

        /// <summary>
        /// Gets a list of all cities
        /// </summary>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all cities</returns>
        public static IEnumerable<City> AllCities(string language, string provider = PROVIDER)
        {
            return AllCountries(language, provider)
                .SelectMany(ctry => ctry.Cities);
        }

        /// <summary>
        /// Gets a list of all hotels
        /// </summary>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all hotels</returns>
        public static IEnumerable<Hotel> AllHotels(string language = LANG, string provider = PROVIDER)
        {
            return AllCities(language, provider)
                .SelectMany(hotel => hotel.Hotels);
        }
        #endregion

        #region Filtered Lists
        /// <summary>
        /// Gets a list of all cities in a country
        /// </summary>
        /// <param name="country">Country to filter by</param>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all cities in a country</returns>
        public static IEnumerable<City> CitiesInCountry(string country, string language = LANG, string provider = PROVIDER)
        {
            return AllCountries(language, provider)
                .Where(ctry => ctry.Id == country)
                .SelectMany(cty => cty.Cities);
        }

        /// <summary>
        /// Gets a list of all hotels in a city
        /// </summary>
        /// <param name="city">City to filter by</param>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all hotels in a city</returns>
        public static IEnumerable<Hotel> HotelsInCity(string city, string language = LANG, string provider = PROVIDER)
        {
            return AllCities(language, provider)
                .Where(cty => cty.Id == city)
                .SelectMany(hotel => hotel.Hotels);
        }

        /// <summary>
        /// Gets a list of all bookable hotels
        /// </summary>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all bookable hotels</returns>
        public static IEnumerable<Hotel> BookableHotels(string language = LANG, string provider = PROVIDER)
        {
            return AllHotels(language, provider)
                .Where(hotel => hotel.IsBookable == true);
        }

        /// <summary>
        /// Gets a list of all bookable hotels in a city
        /// </summary>
        /// <param name="city">City to filter by</param>
        /// <param name="language">Language to get the list in, default to en</param>
        /// <param name="provider">Provider for which the list is, defaults to Scandic</param>
        /// <returns>List of all bookable hotels in a city</returns>
        public static IEnumerable<Hotel> BookableHotelsInCity(string city, string language = LANG, string provider = PROVIDER)
        {
            return HotelsInCity(city, language, provider)
                .Where(hotel => hotel.IsBookable == true);
        }
        #endregion

        #region Is Valid

        /// <summary>
        /// IsContinentValid method
        /// </summary>
        /// <param name="continent">continent value</param>
        /// <returns>Indicate continent validity</returns>
        public static bool IsContinentValid(string continent, string language)
        {
            return AllContinents(language).Where(ctry => ctry.Id == continent).Count() > 0;
        }

        /// <summary>
        /// IsCountryValid method
        /// </summary>
        /// <param name="country">country value</param>
        /// <returns>Indicate country validity</returns>
        //public static bool IsCountryValid(string country, string language)
        //{
        //    return AllCountries(language).Where(ctry => ctry.Id == country).Count() > 0;
        //}

        /// <summary>
        /// IsCityValid method
        /// </summary>
        /// <param name="city">city value</param>
        /// <returns>Indicate city validity</returns>
        //public static bool IsCityValid(string city, string language)
        //{
        //    return AllCities(language).Where(cty => cty.Id == city).Count() > 0;
        //}

        /// <summary>
        /// IsHotelValid method
        /// </summary>
        /// <param name="hotel">hotel value</param>
        /// <returns>Indicate hotel validity</returns>
        //public static bool IsHotelValid(string hotel)
        //{
        //    return AllHotels().Where(htl => htl.Id == hotel).Count() > 0;
        //}

        /// <summary>
        /// IsHotelValidAndBookable method
        /// </summary>
        /// <param name="hotel">hotel value</param>
        /// <returns>Indicate hotel validity and bookable</returns>
        public static bool IsHotelValidAndBookable(string hotel)
        {
            return BookableHotels().Where(htl => htl.Id == hotel).Count() > 0;
        }
        #endregion

        #region Specific Lists

        /// <summary>
        /// GetContinentForCountry method
        /// </summary>
        /// <param name="country">country value</param>
        /// <returns>continent for country</returns>
        //public static string GetContinentForCountry(string country)
        //{
        //    Country selectedCountry = AllCountries().ToList<Country>().Find(delegate(Country cn)
        //    {
        //        return cn.Id.Equals(country);
        //    });

        //    if (selectedCountry != null)
        //    {
        //        return selectedCountry.Continent;
        //    }
        //    else
        //    {
        //        return string.Empty;
        //    }
        //}

        /// <summary>
        /// GetContinentForCity method
        /// </summary>
        /// <param name="city">city value</param>
        /// <returns>continent for city</returns>
        public static string GetContinentForCity(string city, string language)
        {

            var continent = (from cnt in AllContinents(language)
                             from country in cnt.Countries
                             from ct in country.Cities
                             where string.Equals(ct.Id, city, StringComparison.InvariantCultureIgnoreCase)
                             select cnt).FirstOrDefault();


            return continent != null ? continent.Name : "Europe";
        }

        /// <summary>
        /// Get alternative hotels for a given hotel
        /// </summary>
        /// <param name="hotelId">Originally requested hotel</param>
        /// <param name="language">Language (default: en)</param>
        /// <param name="provider">Provider (default:Scandic)</param>
        /// <returns>List of alternative hotels</returns>
        public static IEnumerable<string> GetAlternativeHotels(string hotelId, string language = LANG, string provider = PROVIDER)
        {
            HotelDetailsList hotelCollection = new HotelDetailsManager(language, provider).HotelCollection;
            return hotelCollection.Hotels.Where(h => h.HotelId == hotelId).SelectMany(h => h.AlternateHotels).ToList<string>();
        }

        /// <summary>
        /// Gets the country code for hotel.
        /// </summary>
        /// <param name="hotelId">The hotel id.</param>
        /// <returns>Country Code to which the hotel belongs</returns>
        public static string GetCountryCodeForHotel(string hotelId, string language)
        {
            return AllCountries(language)
                .Where(cn => cn.Cities.SelectMany(hts => hts.Hotels).Select(ht => ht.Id == hotelId).Count() > 0)
                .Select(cnt => cnt.Id).First();
        }


        public static double GetCountryVATForHotel(string hotelId, string language)
        {
            List<Country> listCountry = AllCountries(language).ToList();
            double Tax = 0;
            foreach (var item in listCountry)
            {
                foreach (var city in item.Cities)
                {
                    foreach (var hotel in city.Hotels)
                    {
                        if (hotel.Id == hotelId)
                        {
                            Tax = item.APITaxRates;
                            break;
                        }
                    }
                }
            }
            //if (string.IsNullOrEmpty(strTax))
            //    strTax = "0"; 
            return Tax;
        }


        /// <summary>
        /// return continent for a hotel
        /// </summary>
        /// <param name="hotelId">ID of the hotel</param>
        /// <returns>Name of Continent in which the hotel is</returns>
        public static string GetContinentForHotel(string hotelId, string language)
        {
            var continent = (from cnt in AllContinents(language)
                             from country in cnt.Countries
                             from city in country.Cities
                             from hotel in city.Hotels
                             where string.Equals(hotel.Id, hotelId, StringComparison.InvariantCultureIgnoreCase)
                             select cnt).FirstOrDefault();

            return continent != null ? continent.Name : "Europe";
        }

        #endregion
    }
}
