﻿// <copyright file="HotelDetailsManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading.Tasks;
    using System.Xml;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using Scandic.Services.ServiceAgents.CmsEntity;
    using XenaEntity = Scandic.Services.Xena.BusinessEntity;
    using System.Linq;
    using System.Net;
    using System.Text;
    using Newtonsoft.Json;
    #endregion

    /// <summary>
    /// Manages the HotelDetailsCollection data
    /// </summary>    
    public class HotelDetailsManager : EntityManagerBase
    {
        /// <summary>
        /// Lock object
        /// </summary>
        private static Object lockObject = new Object();

        /// <summary>
        /// Holds the count of the Images configured
        /// </summary>
        private const int ImageCount = 10;


        private static string IVImageToken;

        /// <summary>
        /// Initializes a new instance of the HotelDetailsManager class
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="provider">The provider.</param>      
        public HotelDetailsManager(string language, string provider)
        {
            this.HotelCollection = new HotelDetailsList { Language = language, Provider = provider };
            this.Language = language;
            this.CacheKey = string.Format(
                CultureInfo.InvariantCulture,
                "{0}-{1}-{2}",
                "HotelDetailsManager",
                this.HotelCollection.Provider,
                this.HotelCollection.Language);
            this.Initialize();
        }

        /// <summary>
        /// Gets the hotelDetails collection to be populated and managed
        /// </summary>       
        public HotelDetailsList HotelCollection { get; private set; }

        /// <summary>
        /// Helper method to fill the HotelDetails Collection
        /// </summary>
        public override void SetManagedEntity()
        {
            bool refreshEntity = !(this.IsCacheValid);
            if (!refreshEntity)
            {
                this.HotelCollection = this.CachedValue as HotelDetailsList;
            }

            refreshEntity = refreshEntity || this.HotelCollection == null;

            // If an entity needs to be refreshed i.e.
            // it is not in cache but needs to be picked
            // from the offline file.            
            if (refreshEntity)
            {
                // There can be more than one thread trying to refresh
                // So, allow only one thread to pick data from file.
                lock (lockObject)
                {
                    if (this.CachedValue == null)
                    {
                        var entity = this.RefreshEntity<HotelDetailsList>();
                        if (!(entity == default(HotelDetailsList)))
                        {
                            this.HotelCollection = entity;
                        }

                        if (this.IsCached)
                        {
                            this.CachedValue = this.HotelCollection;
                        }
                    }
                    else
                    {
                        this.HotelCollection = this.CachedValue as HotelDetailsList;
                        LogHelper.LogInfo("HotelDetailsList already loaded by another thread", LogCategory.Content);
                    }
                }
            }
        }

        #region Helpers
        /// <summary>
        /// Gets the value of a node
        /// </summary>
        /// <param name="parent">parent node in which to search</param>
        /// <param name="node">node name to search</param>
        /// <param name="numeric">indicates if the node is expected to be numeric default : false</param>
        /// <returns>cleaned up value of the node</returns>
        private static string GetNodeValue(XmlNode parent, string node, bool numeric = false)
        {
            if (parent.SelectSingleNode(node) != null)
            {
                return parent.SelectSingleNode(node).InnerText;
            }
            else
            {
                if (numeric)
                {
                    return "0";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Fills the images.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        private static void FillImages(HotelDetails hotel, RawPage hotelPage)
        {
            // hotel.Images = new List<Image>();
            string path;
            string title;

            // Populate GeneralImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "GeneralImage" + i.ToString();
                title = "GeneralImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            // Populate RoomImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "RoomImage" + i.ToString();
                title = "RoomImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            // Populate LeisureImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "LeisureImage" + i.ToString();
                title = "LeisureImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            // Populate RetuarantBarImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "RestBarImage" + i.ToString();
                title = "RestaurantBarImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            if (hotel.Images.Count > 0)
            {
                hotel.Images[0].IsMainImage = "true";
            }
        }


        private static List<IVHotelImage> GetImageFromImageVault(string imgID)
        {
            string imgURLResp = string.Empty;
            string URL = ConfigHelper.GetValue(ConfigKeys.XenaImageVaultMediaServiceURL);

            if (string.IsNullOrEmpty(IVImageToken))
                IVImageToken = GetTokenForImageFromImageVault();

            StringBuilder postBody = new StringBuilder();
            postBody.Append("{ \r\n Filter: { Id: [" + imgID + "]}, \r\n  Populate: { \r\n MediaFormats: \r\n [ \r\n { \r\n $type: \"" + ConfigHelper.GetValue(ConfigKeys.XenaImageVaultMediaFormatType) + "\",");
            postBody.Append("\r\n Effects:[{$type: \"" + ConfigHelper.GetValue(ConfigKeys.XenaImageVaultMediaFormatEffectType) + "\",Width:" + ConfigHelper.GetValue(ConfigKeys.XenaImageVaultMediaFormatWidth));
            postBody.Append(",Height:" + ConfigHelper.GetValue(ConfigKeys.XenaImageVaultMediaFormatHeight) + ",ResizeMode:'ScaleToFill'}] \r\n } \r\n ], \r\n");
            postBody.Append("PublishIdentifier: \"" + ConfigHelper.GetValue(ConfigKeys.XenaImageVaultPublishIdentifier) + "\"\r\n } \r\n }");

            string authorizaton = "Bearer " + IVImageToken;
            string contentType = "application/json";
            HttpStatusCode statusCode;
            imgURLResp = GetRequestResponse(URL, postBody.ToString(), authorizaton, contentType, out statusCode);

            List<IVHotelImage> result = null;
            if (!string.IsNullOrEmpty(imgURLResp))
                result = JsonConvert.DeserializeObject<List<IVHotelImage>>(imgURLResp);
            else if (statusCode == HttpStatusCode.Unauthorized)
            {
                IVImageToken = GetTokenForImageFromImageVault();
                authorizaton = "Bearer " + IVImageToken;
                imgURLResp = GetRequestResponse(URL, postBody.ToString(), authorizaton, contentType, out statusCode);
                if (!string.IsNullOrEmpty(imgURLResp))
                    result = JsonConvert.DeserializeObject<List<IVHotelImage>>(imgURLResp);
            }
            return result;
        }


        private static string GetTokenForImageFromImageVault()
        {
            string token = string.Empty;
            string authTokenIV = string.Empty;
            string URL = ConfigHelper.GetValue(ConfigKeys.XenaImageVaultAuthTokenURL);
            var postBody = "grant_type=client_credentials";

            string username = ConfigHelper.GetValue(ConfigKeys.XenaImageVaultUserName);
            string password = ConfigHelper.GetValue(ConfigKeys.XenaImageVaultPassword);
            string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            string authorizaton = "Basic " + encoded;
            string contentType = "application/x-www-form-urlencoded";
            HttpStatusCode statusCode;
            token = GetRequestResponse(URL, postBody.ToString(), authorizaton, contentType, out statusCode);

            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(token);

            if (result != null && result.Count > 0)
                authTokenIV = Convert.ToString(result["access_token"]);

            return authTokenIV;
        }


        private static string GetRequestResponse(string URL, string postBody, string authorizaton, string contentType, out HttpStatusCode statusCode)
        {
            statusCode = HttpStatusCode.OK;
            string response = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = contentType;
            request.Headers.Add("Authorization", authorizaton);
            using (Stream stream = request.GetRequestStream())
            {
                byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                stream.Write(content, 0, content.Length);
            }
            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                response = responseReader.ReadToEnd();
                responseReader.Close();
            }

            catch (System.Net.WebException we)
            {
                statusCode = ((System.Net.HttpWebResponse)(we.Response)).StatusCode;
                Console.Out.WriteLine("Exception Occured " + we.Message);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine("Exception Occured " + e.Message);
            }
            return response;
        }


        /// <summary>
        /// Fills the images for Xena Hotels.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        private static void FillImagesXena(HotelDetails hotel, XenaEntity.Hotel hotelPage)
        {
            if (hotelPage.HotelExteriorImageId.HasValue)
            {
                hotel.Images = new List<Image>();

                List<IVHotelImage> listHotelImages = GetImageFromImageVault(Convert.ToString(hotelPage.HotelExteriorImageId.Value));
                string hotelImg = string.Empty;
                if (listHotelImages != null && listHotelImages.Count > 0)
                {
                    int imgCount = 0;
                    bool mainImg = true;
                    foreach (var img in listHotelImages)
                    {
                        if (img.MediaConversions != null && img.MediaConversions.Count > 0)
                        {
                            imgCount++;
                            hotelImg = img.MediaConversions[0].Url;
                            if (!string.IsNullOrEmpty(hotelImg))
                                hotelImg = hotelImg.Substring(hotelImg.IndexOf("publishedmedia")); //ConfigHelper.GetValue(ConfigKeys.XenaImageVaultPublishIdentifier) + "imagevault/" + 
                            if (imgCount > 1)
                                mainImg = false;
                            Image image = new Image
                            {
                                ImageUrl = hotelImg, //ContentHelper.GetPropertyValue(hotelPage, path),
                                Title = "", //ContentHelper.GetPropertyValue(hotelPage, title),
                                IsMainImage = mainImg == true ? "true" : "false"
                            };
                            if ((!string.IsNullOrEmpty(image.ImageUrl))) // || (!string.IsNullOrEmpty(image.Title)))
                            {
                                hotel.Images.Add(image);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Fills the room facilties.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        /// <param name="language">The language.</param>
        private static void FillRoomFacilties(HotelDetails hotel, RawPage hotelPage, string language)
        {
            int hotelReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(hotelPage, "PageLink"));
            string roomFacilitiesPageTypeID = ConfigHelper.GetValue(ConfigKeys.HotelRoomDescriptionPageTypeID);
            RawPage[] roomFacilitiesPages = ContentAccess.GetPagesWithPageTypeID(roomFacilitiesPageTypeID, hotelReferenceID, language);
            foreach (RawPage roomFacilitiesPage in roomFacilitiesPages)
            {
                if (ContentHelper.IsPagePublished(roomFacilitiesPage))
                {
                    RoomFacilities roomFacilities = new RoomFacilities();
                    roomFacilities.Name = ContentHelper.GetPropertyValue(roomFacilitiesPage, "PageName");

                    // Abhishek:artf1250482 : Fallback description isn't sent start
                    // Get the HotelSpecific RoomDescription in a Locale.
                    string roomDescription = ContentHelper.GetPropertyValueNotMasterValue(roomFacilitiesPage, "RoomDescription");

                    // If RoomDescription is Null then get it from RoomCategory in StructuredNode
                    if (string.IsNullOrEmpty(roomDescription))
                    {
                        RoomCategory roomCategory = new RoomTypeManager(language).RoomTypes.RoomCategoryCollection.Find(delegate(RoomCategory room)
                                {
                                    return room.RoomCategoryName.Trim().ToLower().Equals(roomFacilities.Name.Trim().ToLower());
                                });

                        roomDescription = roomCategory != null ? roomCategory.RoomCategoryDescription : string.Empty;
                    }
                    // or Get the MaterValue in English.
                    if (string.IsNullOrEmpty(roomDescription))
                    {
                        roomDescription = ContentHelper.GetPropertyValue(roomFacilitiesPage, "RoomDescription");
                    }
                    //Abhishek:artf1250482 : Fallback description isn't sent end
                    roomFacilities.RoomCategoryPageReferenceId = ContentHelper.GetPropertyValue(roomFacilitiesPage, "RoomCategory");
                    roomFacilities.Description = roomDescription;
                    roomFacilities.Image.ImageUrl = ContentHelper.GetPropertyValue(roomFacilitiesPage, "RoomImage");
                    hotel.RoomFacilitiesList.Add(roomFacilities);
                }
            }
        }

        /// <summary>
        /// Fills the room facilties for Xena.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        /// <param name="language">The language.</param>
        private void FillRoomFaciltiesXena(HotelDetails hotel, XenaEntity.Hotel hotelPage, string language)
        {
            DataAccessFacade daf = new DataAccessFacade();
            List<XenaEntity.RoomCategoryConnection> roomCategories = daf.GetEntityDataInList<XenaEntity.RoomCategoryConnection>(ConfigHelper.GetValue(ConfigKeys.XenaRoomCategories), Convert.ToString(hotelPage.Id), language);

            foreach (XenaEntity.RoomCategoryConnection roomFacilitiesPage in roomCategories)
            {
                RoomFacilities roomFacilities = new RoomFacilities();
                roomFacilities.Name = roomFacilitiesPage.RoomCategory != null ? roomFacilitiesPage.RoomCategory.Name : string.Empty; ; //ContentHelper.GetPropertyValue(roomFacilitiesPage, "PageName");

                // Get the HotelSpecific RoomDescription in a Locale.
                string roomDescription = roomFacilitiesPage.Description;

                // If RoomDescription is Null then get it from RoomCategory in StructuredNode
                if (string.IsNullOrEmpty(roomDescription))
                {
                    RoomCategory roomCategory = new RoomTypeManager(language).RoomTypes.RoomCategoryCollection.Find(delegate(RoomCategory room)
                    {
                        return room != null ? room.RoomCategoryName.Trim().ToLower().Equals(roomFacilities.Name.Trim().ToLower()) : false;
                    });

                    roomDescription = roomCategory != null ? roomCategory.RoomCategoryDescription : string.Empty;
                }
                roomFacilities.Description = !string.IsNullOrEmpty(roomDescription) ? roomDescription : string.Empty;

                if (roomFacilitiesPage.MediaIds != null && roomFacilitiesPage.MediaIds.Count() > 0)
                {
                    IEnumerable<int> roomImage = roomFacilitiesPage.MediaIds;

                    foreach (var item in roomImage.ToList())
                    {
                        roomFacilities.Image.ImageUrl = GetXENAImagesFromCMS(Convert.ToString(item));
                    }

                }
                else
                    roomFacilities.Image.ImageUrl = "";

                hotel.RoomFacilitiesList.Add(roomFacilities);
            }
        }

        /// <summary>
        /// Fills the alternative hotels.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        /// <param name="language">The language.</param>
        private static void FillAlternativeHotels(HotelDetails hotel, RawPage hotelPage, string language)
        {
            int hotelReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(hotelPage, "PageLink"));
            string alternativeHotelPageTypeID = ConfigHelper.GetValue(ConfigKeys.AlternativeHotelPageTypeID);
            RawPage[] alternativeHotelPages = ContentAccess.GetPagesWithPageTypeID(alternativeHotelPageTypeID, hotelReferenceID, language);
            foreach (RawPage alternativeHotelPage in alternativeHotelPages)
            {
                if (ContentHelper.IsPagePublished(alternativeHotelPage))
                {
                    if (!string.IsNullOrEmpty(ContentHelper.GetPropertyValue(alternativeHotelPage, "AlternativeHotel")))
                    {
                        hotel.AlternateHotels.Add(ContentHelper.GetPropertyValue(alternativeHotelPage, "AlternativeHotel"));
                    }
                }
            }
        }

        /// <summary>
        /// Fills the alternative hotels for Xena.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        /// <param name="language">The language.</param>
        private void FillAlternativeHotelsXena(HotelDetails hotel, XenaEntity.Hotel hotelPage, string language)
        {
            DataAccessFacade daf = new DataAccessFacade();
            List<XenaEntity.NearbyHotel> nearByHotels = daf.GetEntityDataInList<XenaEntity.NearbyHotel>(ConfigHelper.GetValue(ConfigKeys.XenaNearByHotels), Convert.ToString(hotelPage.Id), language);
            if (nearByHotels != null && nearByHotels.Count > 0)
            {
                foreach (XenaEntity.NearbyHotel alternativeHotelPage in nearByHotels)
                {
                    if (!string.IsNullOrEmpty(alternativeHotelPage.Name))
                        hotel.AlternateHotels.Add(alternativeHotelPage.Name);
                }
            }
        }

        /// <summary>
        /// Fills the hotel details collection.
        /// </summary>
        public void DownloadHotelPages()
        {
            this.HotelCollection = new HotelDetailsList { Language = this.Language, Provider = this.HotelCollection.Provider };
            RawPage[] hotelPages = ContentAccess.GetPagesWithPageTypeID(
                ConfigHelper.GetValue(ConfigKeys.HotelPageTypeID), // This is the Page Type for Hotels
                Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.CountryContainerPageReferenceID)), // This is reference to the Container Page
                this.Language);
            this.ConvertPagesToEntity(hotelPages);
            Serializer.ObjectToFile(this.OfflineFile, this.HotelCollection);
        }

        private Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

        /// <summary>
        /// Fills the hotel details collection.
        /// </summary>
        public void DownloadXenaHotelPages()//HttpClient _httpClient)
        {
            this.HotelCollection = new HotelDetailsList { Language = this.Language, Provider = this.HotelCollection.Provider };
            DataAccessFacade daf = new DataAccessFacade();
            List<XenaEntity.Hotel> hotelPages = daf.GetContentsInListByFilter<XenaEntity.Hotel>(ConfigHelper.GetValue(ConfigKeys.XenaAPIHotels), "language=" + this.Language);
            //List<XenaEntity.Hotel> hotelPages = new List<XenaEntity.Hotel>();
            //XenaEntity.Hotel hotel = daf.GetContentsByID<XenaEntity.Hotel>(ConfigHelper.GetValue(ConfigKeys.XenaAPIHotels), "2189", "language=" + this.Language);
            //hotelPages.Add(hotel);
            this.ConvertXenaPagesToEntity(hotelPages);
            Serializer.ObjectToFile(this.OfflineFile, this.HotelCollection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelPages"></param>
        private void ConvertXenaPagesToEntity(IEnumerable<XenaEntity.Hotel> hotelPages)
        {
            Stopwatch watch = new Stopwatch();
            this.HotelCollection = new HotelDetailsList();
            watch.Start();

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                //List<Exception> exceptions = null;
                Parallel.ForEach(
                    hotelPages,
                    hotelPage =>
                    {
                        //try
                        //{
                        if (hotelPage.IsPublished)
                        {
                            HotelDetails hotelDetails = new HotelDetails();

                            hotelDetails.HotelId = Convert.ToString(hotelPage.OperaId); //ContentHelper.GetPropertyValue(hotelPage, "OperaID");
                            //hotelDetails.HotelPageReferenceId = ""; //ContentHelper.GetPropertyValue(hotelPage, "PageLink");
                            hotelDetails.HotelName = hotelPage.Name; //ContentHelper.GetPropertyValue(hotelPage, "Heading");
                            hotelDetails.HotelUrl = hotelPage.HotelPageUrl; //ContentHelper.GetPropertyValue(hotelPage, "PageLinkURL");

                            hotelDetails.HotelAddress.Country = "";//hotelPage.Address.City //ContentHelper.GetPropertyValue(hotelPage, "Country");
                            hotelDetails.HotelAddress.MarketingCityName = hotelPage.Address != null ? hotelPage.Address.City : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "City");
                            hotelDetails.HotelAddress.PostalCity = hotelPage.Address != null ? hotelPage.Address.City : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "PostalCity");
                            hotelDetails.HotelAddress.PostalCode = hotelPage.Address != null ? hotelPage.Address.PostCode : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "PostCode");
                            hotelDetails.HotelAddress.StreetAddress = hotelPage.Address != null ? hotelPage.Address.Street : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "StreetAddress");

                            hotelDetails.HotelCoordinate.Latitude = hotelPage.Location != null ? Convert.ToString(hotelPage.Location.Latitude) : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "GeoY");
                            hotelDetails.HotelCoordinate.Longitude = hotelPage.Location != null ? Convert.ToString(hotelPage.Location.Longitude) : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "GeoX");

                            hotelDetails.HotelFax = hotelPage.Fax;
                            hotelDetails.HotelPhone = hotelPage.Phone;
                            hotelDetails.HotelEmail = hotelPage.Email;

                            hotelDetails.HotelDescription.HotelFacilitiesDescription = ""; //ContentHelper.GetPropertyValue(hotelPage, "FacilitiesDescription");
                            hotelDetails.HotelDescription.HotelIntro = !string.IsNullOrEmpty(hotelPage.ContentDescriptionShort) ? hotelPage.ContentDescriptionShort : String.Empty;

                            hotelDetails.HotelFacilities = FillHotelFacilitiesXENA(hotelPage);

                            if (hotelPage.HotelExteriorImageId != null && hotelPage.HotelExteriorImageId.HasValue)
                                hotelDetails.HotelBookingImage.ImageUrl = GetXENAImagesFromCMS(Convert.ToString(hotelPage.HotelExteriorImageId.Value));

                            HotelNetwork hn = new HotelNetwork();
                            hotelDetails.APITaxRates = hn.GetVatForHotelFromCountry(hotelDetails.HotelId);

                            FillImagesXena(hotelDetails, hotelPage);
                            FillRoomFaciltiesXena(hotelDetails, hotelPage, this.Language);
                            FillAlternativeHotelsXena(hotelDetails, hotelPage, this.Language);
                            this.HotelCollection.Hotels.Add(hotelDetails);
                        }
                        //}

                        //catch (Exception ex)
                        //{
                        //    if (exceptions == null)
                        //    {
                        //        exceptions = new List<Exception>();
                        //    }
                        //    exceptions.Add(ex);
                        //}
                        //finally
                        //{
                        //    if (exceptions != null) throw new AggregateException(exceptions);
                        //}
                    });
            }

            watch.Stop();
            PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
        }

        /// <summary>
        /// method to return Xena Hotels image
        /// </summary>
        /// <param name="imageID">Xena Hotel Image ID</param>
        /// <returns>Image Path</returns>
        private string GetXENAImagesFromCMS(string imageID)
        {
            List<IVHotelImage> listHotelImages = GetImageFromImageVault(imageID);
            string hotelImg = string.Empty;
            if (listHotelImages != null && listHotelImages.Count > 0)
            {
                if (listHotelImages[0].MediaConversions != null && listHotelImages[0].MediaConversions.Count > 0)
                {
                    hotelImg = listHotelImages[0].MediaConversions[0].Url;
                    if (!string.IsNullOrEmpty(hotelImg))
                        hotelImg = hotelImg.Substring(hotelImg.IndexOf("publishedmedia"));//ConfigHelper.GetValue(ConfigKeys.XenaImageVaultPublishIdentifier) + "imagevault/" + 
                }

            }
            return hotelImg;

        }

        /// <summary>
        /// method to return Xena hotels facilities 
        /// </summary>
        /// <param name="hotelPage">Xena Hotel entity</param>
        /// <returns></returns>
        private HotelFacilities FillHotelFacilitiesXENA(XenaEntity.Hotel hotelPage)
        {
            HotelFacilities facilities = new HotelFacilities();
            DataAccessFacade daf = new DataAccessFacade();

            string airportDistance;
            string trainDistance;

            facilities.AirportName = GetAirportAndTrainDataFromHotelXENA(Convert.ToString(hotelPage.Id), out airportDistance, out trainDistance); //ContentHelper.GetPropertyValue(hotelPage, "Airport1Name");
            facilities.DistanceToAirportInKM = airportDistance; //ContentHelper.GetPropertyValue(hotelPage, "Airport1Distance");
            facilities.DistanceToCityCenterInKM = !string.IsNullOrEmpty(Convert.ToString(hotelPage.DistanceToCityCentre)) ? Convert.ToString(hotelPage.DistanceToCityCentre / 1000) : string.Empty; //ContentHelper.GetPropertyValue(hotelPage, "CityCenterDistance");
            facilities.DistanceToTrainStationInKM = trainDistance; //ContentHelper.GetPropertyValue(hotelPage, "DistanceTrain");

            if (hotelPage.EcoLabels != null)
            {
                string ecoLabel = string.Empty;
                if (hotelPage.EcoLabels.NordicEcoLabel)
                    ecoLabel = "Nordic Eco Label,";
                else if (hotelPage.EcoLabels.EuEcoLabel)
                    ecoLabel += "Eu Eco Label,"; //ContentHelper.GetPropertyValue(hotelPage, "EcoLabeled");
                else if (hotelPage.EcoLabels.GreenGlobeLabel)
                    ecoLabel += "Green Globe Label,";

                if (ecoLabel.Length > 0)
                    ecoLabel = ecoLabel.Remove(ecoLabel.Length - 1);

                facilities.EcoLabeledHotel = ecoLabel;
            }

            facilities.NonsmokingRooms = hotelPage.NumberOfRooms != null ? hotelPage.NumberOfRooms.NonSmoking.HasValue ? hotelPage.NumberOfRooms.NonSmoking.Value.ToString() : "0" : "0";
            facilities.NumberOfRooms = hotelPage.NumberOfRooms != null ? hotelPage.NumberOfRooms.Total.HasValue ? hotelPage.NumberOfRooms.Total.Value.ToString() : "0" : "0";
            facilities.RoomsForDisabled = hotelPage.NumberOfRooms != null ? hotelPage.NumberOfRooms.ForDisabled != null ? hotelPage.NumberOfRooms.ForDisabled.Value > 0 ? true.ToString() : false.ToString() : false.ToString() : false.ToString();
            facilities.MeetingFacilities = Convert.ToString(IsMeetingFacilityAvailable(Convert.ToString(hotelPage.Id)));
            facilities.Garage = Convert.ToString(IsGarageOrParkingAvailable(Convert.ToString(hotelPage.Id), "GARAGE"));
            facilities.OutdoorParking = Convert.ToString(IsGarageOrParkingAvailable(Convert.ToString(hotelPage.Id), "OUTPARKING"));
            facilities.RelaxCenter = Convert.ToString(IsRelaxCenterAvailable(Convert.ToString(hotelPage.Id)));
            facilities.RestaurantOrBar = Convert.ToString(IsRestaurantOrBarOrShopAvailable(Convert.ToString(hotelPage.Id), "RESTAURANTBAR"));
            facilities.Shop = Convert.ToString(IsRestaurantOrBarOrShopAvailable(Convert.ToString(hotelPage.Id), "SHOP")); ;
            facilities.WirelessInternetAccess = "True";
            return facilities;
        }

        private string GetAirportAndTrainDataFromHotelXENA(string hotelID, out string airportDistance, out string trainDistance)//, out string citycenterDistance
        {
            DataAccessFacade daf = new DataAccessFacade();
            List<XenaEntity.POIConnection> data = daf.GetEntityDataInList<XenaEntity.POIConnection>(ConfigHelper.GetValue(ConfigKeys.XenaPOIConnection), hotelID, this.Language);
            string result = string.Empty;
            airportDistance = string.Empty;
            trainDistance = string.Empty;
           
            List<KeyValuePair<string, string>> airport = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> train = new List<KeyValuePair<string, string>>();
            //Airport
            if (data != null && data.Count > 0)
            {
                foreach (var item in data)
                {
                    foreach (var cat in item.POI.Categories)
                    {
                        if (cat.CategoryGroup == XenaEntity.Enums.POICategoryGroup.Airport)
                        {
                            airport.Add(new KeyValuePair<string, string>(item.POI.Name, Convert.ToString(item.Distance)));
                        }
                        if (cat.CategoryGroup == XenaEntity.Enums.POICategoryGroup.Trains)
                        {
                            train.Add(new KeyValuePair<string, string>(item.POI.Name, Convert.ToString(item.Distance)));
                        }
                    }
                }
            }
            if (airport != null && airport.Count > 0)
            {
                var airportKeyVal = airport.OrderBy(o => o.Value).First();
                result = airportKeyVal.Key;
            }
            if (train != null && train.Count > 0)
            {
                var trainKeyVal = train.OrderBy(o => o.Value).First();
                trainDistance = trainKeyVal.Value;
            }
            return result;
        }

        private bool IsMeetingFacilityAvailable(string hotelID)
        {
            bool retVal = false;
            DataAccessFacade daf = new DataAccessFacade();
            XenaEntity.Meeting meeting = daf.GetEntityData<XenaEntity.Meeting>(ConfigHelper.GetValue(ConfigKeys.XenaMeetingFacility), hotelID, this.Language);
            if (meeting != null && meeting.MeetingRooms != null && meeting.MeetingRooms.Count > 0)
                retVal = true;
            meeting = null;
            return retVal;
        }

        private bool IsRelaxCenterAvailable(string hotelID)
        {
            bool retVal = false;
            DataAccessFacade daf = new DataAccessFacade();
            XenaEntity.Facilities hotelFacility = daf.GetEntityData<XenaEntity.Facilities>(ConfigHelper.GetValue(ConfigKeys.XenaFacilities), hotelID, this.Language);
            if (hotelFacility != null && hotelFacility.RelaxFacility != null)
                retVal = true;
            hotelFacility = null;
            return retVal;
        }

        private bool IsRestaurantOrBarOrShopAvailable(string hotelID, string condition)
        {

            bool retVal = false;
            DataAccessFacade daf = new DataAccessFacade();
            //try
            //{
            List<XenaEntity.Restaurant> restaurant = daf.GetEntityDataInList<XenaEntity.Restaurant>(ConfigHelper.GetValue(ConfigKeys.XenaRestaurantBar), hotelID, this.Language);
            if (restaurant != null && restaurant.Count > 0)
            {

                if (condition == "SHOP")
                {
                    XenaEntity.Restaurant shop = restaurant.Where(r => r.RestaurantType == XenaEntity.Enums.RestaurantType.Restaurant).FirstOrDefault();
                    if (shop != null && shop.RestaurantType == XenaEntity.Enums.RestaurantType.Other)
                        retVal = true;
                }
                if (condition == "RESTAURANTBAR")
                {
                    XenaEntity.Restaurant rest = restaurant.Where(r => r.RestaurantType == XenaEntity.Enums.RestaurantType.Restaurant).FirstOrDefault();
                    XenaEntity.Restaurant bar = restaurant.Where(r => r.RestaurantType == XenaEntity.Enums.RestaurantType.Bar).FirstOrDefault();
                    if (rest != null && rest.RestaurantType == XenaEntity.Enums.RestaurantType.Restaurant || bar != null && bar.RestaurantType == XenaEntity.Enums.RestaurantType.Bar)
                        retVal = true;
                }
            }
            restaurant = null;
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.Message);
            //}

            return retVal;
        }

        private bool IsGarageOrParkingAvailable(string hotelID, string condition)
        {
            bool retVal = false;
            DataAccessFacade daf = new DataAccessFacade();
            XenaEntity.Parking parking = daf.GetEntityData<XenaEntity.Parking>(ConfigHelper.GetValue(ConfigKeys.XenaParking), hotelID, this.Language);
            if (parking != null)
            {
                if (condition == "GARAGE")
                    if (parking.GarageParking != null)
                        retVal = true;
                if (condition == "OUTPARKING")
                    if (parking.OutdoorParking != null)
                        retVal = true;
            }
            parking = null;
            return retVal;
        }


        /// <summary>
        /// Fills the hotel details.
        /// </summary>
        /// <param name="hotelPages">The hotel pages.</param>
        private void ConvertPagesToEntity(RawPage[] hotelPages)
        {
            Stopwatch watch = new Stopwatch();
            this.HotelCollection = new HotelDetailsList();
            watch.Start();

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                List<Exception> exceptions = null;
                Parallel.ForEach(
                    hotelPages,
                    hotelPage =>
                    {
                        try
                        {
                            if (ContentHelper.IsPagePublished(hotelPage))
                            {
                                HotelDetails hotelDetails = new HotelDetails();

                                hotelDetails.HotelId = ContentHelper.GetPropertyValue(hotelPage, "OperaID");
                                hotelDetails.HotelPageReferenceId = ContentHelper.GetPropertyValue(hotelPage, "PageLink");
                                hotelDetails.HotelName = ContentHelper.GetPropertyValue(hotelPage, "Heading");
                                hotelDetails.HotelUrl = ContentHelper.GetPropertyValue(hotelPage, "PageLinkURL");

                                hotelDetails.HotelAddress.Country = ContentHelper.GetPropertyValue(hotelPage, "Country");
                                hotelDetails.HotelAddress.MarketingCityName = ContentHelper.GetPropertyValue(hotelPage, "City");
                                hotelDetails.HotelAddress.PostalCity = ContentHelper.GetPropertyValue(hotelPage, "PostalCity");
                                hotelDetails.HotelAddress.PostalCode = ContentHelper.GetPropertyValue(hotelPage, "PostCode");
                                hotelDetails.HotelAddress.StreetAddress = ContentHelper.GetPropertyValue(hotelPage, "StreetAddress");

                                hotelDetails.HotelCoordinate.Latitude = ContentHelper.GetPropertyValue(hotelPage, "GeoY");
                                hotelDetails.HotelCoordinate.Longitude = ContentHelper.GetPropertyValue(hotelPage, "GeoX");

                                hotelDetails.HotelFax = ContentHelper.GetPropertyValue(hotelPage, "Fax");
                                hotelDetails.HotelPhone = ContentHelper.GetPropertyValue(hotelPage, "Phone");
                                hotelDetails.HotelEmail = ContentHelper.GetPropertyValue(hotelPage, "Email");

                                hotelDetails.HotelDescription.HotelFacilitiesDescription = ContentHelper.GetPropertyValue(hotelPage, "FacilitiesDescription");
                                hotelDetails.HotelDescription.HotelIntro = ContentHelper.GetPropertyValue(hotelPage, "HotelIntro");

                                hotelDetails.HotelFacilities.AirportName = ContentHelper.GetPropertyValue(hotelPage, "Airport1Name");
                                hotelDetails.HotelFacilities.DistanceToAirportInKM = ContentHelper.GetPropertyValue(hotelPage, "Airport1Distance");
                                hotelDetails.HotelFacilities.DistanceToCityCenterInKM = ContentHelper.GetPropertyValue(hotelPage, "CityCenterDistance");
                                hotelDetails.HotelFacilities.DistanceToTrainStationInKM = ContentHelper.GetPropertyValue(hotelPage, "DistanceTrain");
                                hotelDetails.HotelFacilities.EcoLabeledHotel = ContentHelper.GetPropertyValue(hotelPage, "EcoLabeled");
                                hotelDetails.HotelFacilities.Garage = ContentHelper.GetPropertyValue(hotelPage, "Garage");
                                hotelDetails.HotelFacilities.MeetingFacilities = ContentHelper.GetPropertyValue(hotelPage, "MeetingFacilities");
                                hotelDetails.HotelFacilities.NonsmokingRooms = ContentHelper.GetPropertyValue(hotelPage, "NonSmokingRooms");
                                hotelDetails.HotelFacilities.NumberOfRooms = ContentHelper.GetPropertyValue(hotelPage, "NoOfRooms");
                                hotelDetails.HotelFacilities.OutdoorParking = ContentHelper.GetPropertyValue(hotelPage, "OutdoorParking");
                                hotelDetails.HotelFacilities.RelaxCenter = ContentHelper.GetPropertyValue(hotelPage, "RelaxCenter");
                                hotelDetails.HotelFacilities.RestaurantOrBar = ContentHelper.GetPropertyValue(hotelPage, "RestaurantBar");
                                hotelDetails.HotelFacilities.RoomsForDisabled = ContentHelper.GetPropertyValue(hotelPage, "RoomsForDisabled");
                                hotelDetails.HotelFacilities.Shop = ContentHelper.GetPropertyValue(hotelPage, "Shop");
                                hotelDetails.HotelFacilities.WirelessInternetAccess = ContentHelper.GetPropertyValue(hotelPage, "WirelessInternet");
                                hotelDetails.HotelBookingImage.ImageUrl = ContentHelper.GetPropertyValue(hotelPage, "HotelBookingImage");
                                //hotelDetails.APITaxRates = ContentHelper.GetPropertyValue(hotelPage, "APITaxRates");
                                HotelNetwork hn = new HotelNetwork();
                                hotelDetails.APITaxRates = hn.GetVatForHotelFromCountry(hotelDetails.HotelId);
                                FillImages(hotelDetails, hotelPage);
                                FillRoomFacilties(hotelDetails, hotelPage, this.Language);
                                FillAlternativeHotels(hotelDetails, hotelPage, this.Language);
                                this.HotelCollection.Hotels.Add(hotelDetails);
                            }
                        }

                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }
                    });
            }

            watch.Stop();
            PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
        }
        #endregion
    }


    public class MediaConversion
    {
        public string type { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public double AspectRatio { get; set; }
        public int FormatWidth { get; set; }
        public int FormatHeight { get; set; }
        public double FormatAspectRatio { get; set; }
        public string MediaFormatName { get; set; }
        public int Id { get; set; }
        public int MediaFormatId { get; set; }
        public string Url { get; set; }
        public string Html { get; set; }
        public string ContentType { get; set; }
        public string Name { get; set; }
    }

    public class IVHotelImage
    {
        public string type { get; set; }
        public int Id { get; set; }
        public int VaultId { get; set; }
        public string Name { get; set; }
        public List<MediaConversion> MediaConversions { get; set; }
        public string DateAdded { get; set; }
        public string AddedBy { get; set; }
    }


}
