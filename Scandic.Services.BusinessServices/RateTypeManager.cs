﻿// <copyright file="RateTypeManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using Scandic.Services.ServiceAgents.CmsEntity;
    using XenaEntity = Scandic.Services.Xena.BusinessEntity;
    using System.Text;
    using System.Linq;
    #endregion

    /// <summary>
    /// Manages the RateType and RateCategory data
    /// </summary>
    public class RateTypeManager : EntityManagerBase
    {
        /// <summary>
        /// Lock object
        /// </summary>
        private static Object lockObject = new Object();

        /// <summary>
        /// Initializes a new instance of the <see cref="RateTypeManager"/> class.
        /// </summary>
        /// <param name="language">The language.</param>
        public RateTypeManager(string language)
        {
            this.RateTypes = new RateTypes { Language = language };
            this.Language = language;
            this.CacheKey = string.Format(
                CultureInfo.InvariantCulture,
                "{0}-{1}",
                "RateTypeManager",
                this.RateTypes.Language);
            this.TimeStampFile = Path.Combine(ConfigHelper.GetValue(ConfigKeys.AppPath), ConfigHelper.GetValue(ConfigKeys.TimeStampFileFrequent));
            this.Initialize();
        }

        /// <summary>
        /// Gets or sets the rate network.
        /// </summary>
        /// <value>
        /// The rate network.
        /// </value>
        public RateTypes RateTypes { get; set; }

        /// <summary>
        /// Determines whether [is rate plan code in CMS] [the specified rateCode].
        /// </summary>
        /// <param name="rateCode">rate Code for which the Rate category has to be retrieved</param>
        /// <returns>boolean indicating whether the specified RatePlancode is configured in CMS</returns>
        public bool IsRatePlanCodeInCMS(string rateCode)
        {
            bool isInCMS = false;
            string rateCategoryName = this.RateTypes.RateTypeMap[rateCode];
            if (!string.IsNullOrEmpty(rateCategoryName))
            {
                isInCMS = true;
            }

            return isInCMS;
        }

        /// <summary>
        /// Fills the rate type map.
        /// </summary>
        public override void SetManagedEntity()
        {
            bool refreshEntity = !(this.IsCacheValid);

            if (!refreshEntity)
            {
                this.RateTypes = this.CachedValue as RateTypes;
            }

            refreshEntity = refreshEntity || this.RateTypes == null;

            // If an entity needs to be refreshed i.e.
            // it is not in cache but needs to be picked
            // from the offline file.            
            if (refreshEntity)
            {
                // There can be more than one thread trying to refresh
                // So, allow only one thread to pick data from file.
                lock (lockObject)
                {
                    if (this.CachedValue == null)
                    {
                        var entity = this.RefreshEntity<RateTypes>();
                        if (!(entity == default(RateTypes)))
                        {
                            this.RateTypes = entity;
                        }

                        if (this.IsCached)
                        {
                            this.CachedValue = this.RateTypes;
                        }
                    }
                    else
                    {
                        this.RateTypes = this.CachedValue as RateTypes;
                        LogHelper.LogInfo("RateTypes already loaded by another thread", LogCategory.Content);
                    }
                }
            }
        }

        /// <summary>
        /// Fills the rate type details.
        /// </summary>
        public void DownloadRateTypeDetails()
        {
            Stopwatch watch = new Stopwatch();
            this.RateTypes = new RateTypes { Language = this.Language };
            string rateCategoryPageTypeID = ConfigHelper.GetValue(ConfigKeys.RateCategoryPageTypeID);
            string rateTypePageTypeID = ConfigHelper.GetValue(ConfigKeys.RateTypePageTypeID);
            int rateTypeContainerPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.RateContainerID));

            // Fetch all Rate Categories configured in CMS
            if (PerformanceHelper.Enabled)
            {
                watch.Start();
            }

            RawPage[] rateCategoryPages = ContentAccess.GetPagesWithPageTypeID(rateCategoryPageTypeID, rateTypeContainerPageReferenceID, this.RateTypes.Language);
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                List<Exception> exceptions = null;

                Parallel.ForEach(
                    rateCategoryPages,
                    rawPage =>
                    {
                        try
                        {
                            if (ContentHelper.IsPagePublished(rawPage))
                            {
                                RateCategory rateCategory = new RateCategory();
                                string rateCategoryName = ContentHelper.GetPropertyValue(rawPage, "PageName");
                                rateCategory.RateCategoryName = rateCategoryName;
                                rateCategory.RateCategoryId = ContentHelper.GetPropertyValue(rawPage, "RateCategoryID");
                                rateCategory.RateCategoryDescription = ContentHelper.GetPropertyValue(rawPage, "RateCategoryDescription");
                                rateCategory.GuaranteeType = ContentHelper.GetPropertyValue(rawPage, "GuaranteeType");

                                if (!this.RateTypes.RateCategoryCollection.Contains(rateCategory))
                                {
                                    this.RateTypes.RateCategoryCollection.Add(rateCategory);
                                }

                                // Fill a collection with the ratecodes configured in CMS and its corresponding ratecategory.
                                int pageReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(rawPage, "PageLink"));
                                RawPage[] rateTypePages = ContentAccess.GetPagesWithPageTypeID(rateTypePageTypeID, pageReferenceID, this.RateTypes.Language);
                                Parallel.ForEach(
                                    rateTypePages,
                                    rateTypePage =>
                                    {
                                        try
                                        {
                                            if (ContentHelper.IsPagePublished(rateTypePage))
                                            {
                                                string rateCode = ContentHelper.GetPropertyValue(rateTypePage, "PageName");
                                                if (!string.IsNullOrEmpty(rateCode) && !string.IsNullOrEmpty(rateCategoryName) && !this.RateTypes.RateTypeMap.ContainsKey(rateCode))
                                                {
                                                    this.RateTypes.RateTypeMap.Add(rateCode.Trim(), rateCategoryName);
                                                    RateCode rtCode = new RateCode();
                                                    rtCode.OperaRateId = ContentHelper.GetPropertyValue(rateTypePage, "OperaID").Trim();
                                                    rtCode.BreakfastIncluded = string.IsNullOrEmpty(ContentHelper.GetPropertyValue(rateTypePage, "BreakfastNotIncluded"));
                                                    rtCode.IsCancellable = !string.IsNullOrEmpty(ContentHelper.GetPropertyValue(rateTypePage, "IsCancellable"));
                                                    this.RateTypes.RatePlanCodes.Add(rtCode);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            if (exceptions == null)
                                            {
                                                exceptions = new List<Exception>();
                                            }
                                            exceptions.Add(ex);
                                        }
                                        finally
                                        {
                                            if (exceptions != null) throw new AggregateException(exceptions);
                                        }
                                    });
                            }
                        }
                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }
                    });


                int blockContainerPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.BlockContainerPageReferenceID));
                string blockRatePageTypeID = ConfigHelper.GetValue(ConfigKeys.BlockRatePageTypeID);
                RawPage[] blockRatePages = ContentAccess.GetPagesWithPageTypeID(blockRatePageTypeID, blockContainerPageReferenceID, this.RateTypes.Language);

                Parallel.ForEach(
                    blockRatePages,
                    blockRatePage =>
                    {
                        try
                        {
                            if (ContentHelper.IsPagePublished(blockRatePage))
                            {
                                RateCategory rateCategory = new RateCategory();
                                string rateCategoryName = ContentHelper.GetPropertyValue(blockRatePage, "PageTypeName");
                                string rateType = ContentHelper.GetPropertyValue(blockRatePage, "PageName");
                                rateCategory.RateCategoryId = ContentHelper.GetPropertyValue(blockRatePage, "PageName");
                                rateCategory.OperaId = ContentHelper.GetPropertyValue(blockRatePage, "OperaID").Trim();
                                rateCategory.RateCategoryDescription = ContentHelper.GetPropertyValue(blockRatePage, "BlockDescription");
                                rateCategory.RateCategoryName = rateCategoryName;
                                if (!this.RateTypes.RateCategoryCollection.Contains(rateCategory))
                                {
                                    this.RateTypes.RateCategoryCollection.Add(rateCategory);
                                }

                                if (!string.IsNullOrEmpty(rateType) && !string.IsNullOrEmpty(rateCategoryName) && !this.RateTypes.RateTypeMap.ContainsKey(rateType))
                                {
                                    this.RateTypes.RateTypeMap.Add(rateType.Trim(), rateCategoryName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }
                    });

            }

            Serializer.ObjectToFile(this.OfflineFile, this.RateTypes);
            if (PerformanceHelper.Enabled)
            {
                watch.Stop();
                PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
            }
        }

        /// <summary>
        /// Downloads all the Rate Type Details of Xena
        /// </summary>
        public void DownloadRateTypeDetailsXena()
        {
            Stopwatch watch = new Stopwatch();
            this.RateTypes = new RateTypes { Language = this.Language };
            // Fetch all Rate Categories configured in CMS
            if (PerformanceHelper.Enabled)
            {
                watch.Start();
            }
            DataAccessFacade daf = new DataAccessFacade();
            List<XenaEntity.RateGroup> rateCategoryPages = daf.GetContentsInListByFilter<XenaEntity.RateGroup>(ConfigHelper.GetValue(ConfigKeys.XenaAPIRates), "lang=" + this.Language);

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                List<Exception> exceptions = null;
                Parallel.ForEach(rateCategoryPages, rateCategoryPage =>
                {
                    //try
                    //{
                    if (rateCategoryPage.IsPublished)
                    {
                        RateCategory rateCategory = new RateCategory();
                        string rateCategoryName = rateCategoryPage.Header;
                        rateCategory.RateCategoryName = rateCategoryName;
                        rateCategory.RateCategoryId = rateCategoryName;

                        IEnumerable<string> rateCatDesc = rateCategoryPage.GeneralTerms;
                        StringBuilder sbRCD = new StringBuilder();
                        if (rateCatDesc != null)
                        {
                            sbRCD.Append("<ul>");
                            foreach (var item in rateCatDesc)
                            {
                                sbRCD.Append(string.Format("<li>{0}</li>", item));
                            }
                            sbRCD.Append("</ul>");
                        }
                        rateCategory.RateCategoryDescription = sbRCD.ToString();

                        if (rateCategoryPage.CancellationRule == XenaEntity.Enums.CancellationRule.CancellableBefore6PM)
                            rateCategory.GuaranteeType = "Postpaid";
                        else
                            rateCategory.GuaranteeType = "Prepaid";

                        if (!RateTypes.RateCategoryCollection.Contains(rateCategory))
                        {
                            this.RateTypes.RateCategoryCollection.Add(rateCategory);
                        }

                        // Fill a collection with the ratecodes configured in CMS and its corresponding ratecategory.
                        //Rate Codes
                        IEnumerable<string> rateCodes = rateCategoryPage.RateCodes;
                        StringBuilder sb = new StringBuilder();
                        try
                        {
                            foreach (var item in rateCodes)
                            {
                                if (!this.RateTypes.RateTypeMap.ContainsKey(item))
                                {
                                    this.RateTypes.RateTypeMap.Add(item, rateCategoryName);
                                }
                                RateCode rtCode = new RateCode();
                                rtCode.OperaRateId = item;
                                rtCode.BreakfastIncluded = true; //rateCategoryPage.BreakfastIncluded;

                                if (rateCategoryPage.CancellationRule == XenaEntity.Enums.CancellationRule.NotCancellable)
                                    rtCode.IsCancellable = false;
                                else
                                    rtCode.IsCancellable = true;

                                if (!this.RateTypes.RatePlanCodes.Contains(rtCode))
                                {
                                    this.RateTypes.RatePlanCodes.Add(rtCode);
                                }
                                sb.Append(item + ",");
                            }
                            sb = sb.Remove(sb.Length - 1, 1);
                        }
                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }

                        LogHelper.LogWarning(string.Format("RateCategory: {0} | RateCode:{1}", rateCategoryName, sb.ToString()), LogCategory.Content);

                    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    if (exceptions == null)
                    //    {
                    //        exceptions = new List<Exception>();
                    //    }
                    //    exceptions.Add(ex);
                    //}
                    //finally
                    //{
                    //    if (exceptions != null) throw new AggregateException(exceptions);
                    //}
                });


                /*int blockContainerPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.BlockContainerPageReferenceID));
                string blockRatePageTypeID = ConfigHelper.GetValue(ConfigKeys.BlockRatePageTypeID);
                RawPage[] blockRatePages = ContentAccess.GetPagesWithPageTypeID(blockRatePageTypeID, blockContainerPageReferenceID, this.RateTypes.Language);

                Parallel.ForEach(
                    blockRatePages,
                    blockRatePage =>
                    {
                        try
                        {
                            if (ContentHelper.IsPagePublished(blockRatePage))
                            {
                                RateCategory rateCategory = new RateCategory();
                                string rateCategoryName = ContentHelper.GetPropertyValue(blockRatePage, "PageTypeName");
                                string rateType = ContentHelper.GetPropertyValue(blockRatePage, "PageName");
                                rateCategory.RateCategoryId = ContentHelper.GetPropertyValue(blockRatePage, "PageName");
                                rateCategory.OperaId = ContentHelper.GetPropertyValue(blockRatePage, "OperaID").Trim();
                                rateCategory.RateCategoryDescription = ContentHelper.GetPropertyValue(blockRatePage, "BlockDescription");
                                rateCategory.RateCategoryName = rateCategoryName;
                                if (!this.RateTypes.RateCategoryCollection.Contains(rateCategory))
                                {
                                    this.RateTypes.RateCategoryCollection.Add(rateCategory);
                                }

                                if (!string.IsNullOrEmpty(rateType) && !string.IsNullOrEmpty(rateCategoryName) && !this.RateTypes.RateTypeMap.ContainsKey(rateType))
                                {
                                    this.RateTypes.RateTypeMap.Add(rateType.Trim(), rateCategoryName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }
                    });*/

            }

            Serializer.ObjectToFile(this.OfflineFile, this.RateTypes);
            if (PerformanceHelper.Enabled)
            {
                watch.Stop();
                PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
            }
        }
    }
}
