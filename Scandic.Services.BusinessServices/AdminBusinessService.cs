﻿// <copyright file="AdminBusinessService.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System.Collections.Generic;
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using System;

    /// <summary>
    /// Implements methods related to CMS data fetch
    /// </summary>    
    public class AdminBusinessService : IAdminBusinessContract
    {
        /// <summary>
        /// Gets a value indicating whether the OWS availability
        /// </summary>
        public bool IsOwsAvailabilityServiceUp
        {
            get
            {
                return new OwsStatusManager().IsOwsAvailabilityServiceUp;
            }
        }

        /// <summary>
        /// Gets the partner details.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        /// <returns>returns partner details </returns>       
        public Partner GetPartnerDetails(string apiKey)
        {
            return new PartnerManager().GetPartnerDetails(apiKey);
        }

        /// <summary>
        /// Gets a list of all partners
        /// </summary>
        /// <returns>List of partners</returns>
        public IList<Partner> PartnerList()
        {
            return new PartnerManager().Partners;
        }

        /// <summary>
        /// Refreshes all cached entities in this service
        /// </summary>
        public void RefreshCache()
        {
            try
            {
                new OwsStatusManager(true);
            }
            catch (Exception ex)
            {

                LogHelper.LogException(ex, LogCategory.Service);
            }

            try
            {
                new PartnerManager(true);
            }
            catch (Exception ex)
            {

                LogHelper.LogException(ex, LogCategory.Service);
            }
          
        }
    }
}
