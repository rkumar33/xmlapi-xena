﻿// <copyright file="EntityManagerBase.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Collections.Generic;    
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;   

    /// <summary>
    /// Base class for all business entities
    /// </summary>
    [Serializable]
    public abstract class EntityManagerBase : IEntityManager
    {
        /// <summary>
        /// Initializes a new instance of the EntityManagerBase class
        /// </summary>
        public EntityManagerBase()
        {
            this.CacheKey = "BaseEntity";
        }
        
        /// <summary>
        /// Gets or sets the timestamp file for  in which entity is managed
        /// </summary>
        protected string TimeStampFile = Path.Combine(ConfigHelper.GetValue(ConfigKeys.AppPath),ConfigHelper.GetValue(ConfigKeys.TimeStampFileRegular));

        /// <summary>
        /// Gets or sets Language in which entity is managed
        /// </summary>
        public string Language { get; protected set; }

        /// <summary>
        /// Gets or sets the Cache Key for this Entity
        /// </summary>
        public string CacheKey { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether this entity
        /// is cached / should be cached
        /// </summary>
        public bool IsCached
        {
            get
            {
                return ConfigHelper.GetValue(ConfigKeys.IsCached).ToUpperInvariant().Equals("TRUE", StringComparison.OrdinalIgnoreCase);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this entity
        /// is cached and not expired in cache
        /// </summary>
        /// <returns></returns>
        public bool IsCacheValid
        {
            get { return CacheHelper.IsAvailable(this.CacheKey) && this.IsCached; }
        }

        /// <summary>
        /// Gets or sets Current offline file
        /// </summary>
        public string OfflineFile { get; protected set; }

      
        /// <summary>
        /// Gets a value indicating whether this entity
        /// is cached and not expired in cache
        /// </summary>
        /// <returns></returns>
        public bool IsOfflineValid
        {
            get { return bool.Parse(ConfigHelper.GetValue(ConfigKeys.UseOfflineContent)) && (File.Exists(this.OfflineFile)); }
        }

        /// <summary>
        /// Gets or sets the Cached value of the managed entity
        /// </summary>
        public object CachedValue
        {
            get
            {
                return CacheHelper.Get(this.CacheKey);
            }

            set
            {
                this.ExpireCache();
                CacheHelper.Put(this.CacheKey, value, this.TimeStampFile);
            }
        }

        /// <summary>
        /// Initialize the entity manager
        /// </summary>
        public void Initialize()
        {
            this.OfflineFile = string.Format(CultureInfo.InvariantCulture, "{0}{1}", ConfigHelper.GetValue("AppPath"),this.CacheKey);
            this.SetManagedEntity();
        }

        /// <summary>
        /// Expires any cached entries for this entity
        /// </summary>
        public void ExpireCache()
        {
            CacheHelper.Clear(this.CacheKey);
        }

        /// <summary>
        /// Refresh the managed entity
        /// </summary>
        /// <typeparam name="T">Indicates that the return type Generic </typeparam>
        /// <returns>The entity with data filled</returns>
        public virtual T RefreshEntity<T>()
        {
            T managedEntity = default(T);
            if (this.IsOfflineValid)
            {
                managedEntity = this.GetOfflineEntity<T>();                
            }          

            return managedEntity;
        }

        #region Unimplemented - Lower Member Specific
        /// <summary>
        /// Sets value for managed entity
        /// </summary>
        public abstract void SetManagedEntity();
        #endregion

        /// <summary>
        /// Gets the partner list from CMS.
        /// </summary>
        /// <typeparam name="T">Indicates a Type place holder </typeparam>
        /// <returns>Gets an offline entity</returns>
        protected T GetOfflineEntity<T>()
        {
            T offline = default(T);
            try
            {
                if (File.Exists(this.OfflineFile))
                {
                    offline = Serializer.FileToObject<T>(this.OfflineFile);
                }                
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, LogCategory.Availability);
            }

            return offline;
        }
    }
}
