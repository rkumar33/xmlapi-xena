﻿// <copyright file="IEntityManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;

    /// <summary>
    /// Interface to be implemented by all
    /// cached business entities
    /// </summary>
    public interface IEntityManager
    {
        /// <summary>
        /// Gets Cache Key for this Entity
        /// </summary>
        string CacheKey { get; }

        /// <summary>
        /// Gets a value indicating whether this entity
        /// is cached / should be cached
        /// </summary>
        bool IsCached { get; }

        /// <summary>
        /// Gets a value indicating whether this entity
        /// is cached validly
        /// </summary>
        bool IsCacheValid { get; }

        /// <summary>
        /// Gets a value indicating whether this entity
        /// is cached validly
        /// </summary>
        bool IsOfflineValid { get; }

        /// <summary>
        /// Gets Current offline file
        /// </summary>
        string OfflineFile { get; }

        /// <summary>
        /// Gets Language in which this entity is managed
        /// </summary>
        string Language { get; }

        /// <summary>
        /// Gets or sets the Cached value of the managed entity
        /// </summary>
        object CachedValue { get; set; }

        /// <summary>
        /// Expires any cached entries for this entity
        /// </summary>
        void ExpireCache();

        /// <summary>
        /// Fills the managed object. This call does not
        /// consider Cache freshness
        /// </summary>
        /// <typeparam name="T">Indicates the param is a Generic Type PlaceHolder</typeparam>
        /// <returns>Returns the managed object</returns>
        T RefreshEntity<T>();

        /// <summary>
        /// Sets the managed object using RefreshEntity,
        /// only if the one in cache has expired
        /// </summary>
        /// objects.</param>
        void SetManagedEntity();
    }
}
