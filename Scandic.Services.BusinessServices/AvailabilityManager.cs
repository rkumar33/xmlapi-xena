﻿// <copyright file="AvailabilityManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.CmsEntity;
    using Scandic.Services.ServiceAgents.OwsEntity;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;

    /// <summary>
    /// class to manage availabilty calls.
    /// </summary>
    public class AvailabilityManager
    {
        /// <summary>
        /// Initializes a new instance of the AvailabilityManager class.
        /// </summary>
        /// <param name="language">language value</param>
        /// <param name="provider">provider value </param>   
        /// <param name="parterApiKey">Partner Api Key used to uniquely identitfy the partner</param>
        public AvailabilityManager(string language, string provider, string parterApiKey)
        {
            this.Language = language;
            this.Provider = provider;
            this.PartnerApiKey = parterApiKey;
        }

        /// <summary>
        /// Gets or sets the language in which entities
        /// are to be returned, where applicable
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the provider for which entities
        /// are to be returned, where applicable
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Gets or sets the partner API key.
        /// </summary>
        /// <value>
        /// The partner API key.
        /// </value>
        public string PartnerApiKey { get; set; }

        /// <summary>
        /// Gets the bookable hotels list from network.
        /// </summary>
        /// <param name="cityId">cityId value</param>
        /// <param name="language">language value</param>
        /// <param name="provider">provider value</param>
        /// <returns>list of hotel</returns>
        public static List<Hotel> GetBookableHotelsListFromNetwork(string cityId, string language, string provider)
        {
            HotelNetwork network = new HotelNetworkManager(language, provider).Network;
            List<Hotel> hotelList = network.Continents
                   .SelectMany(continent => continent.Countries)
                   .SelectMany(ctry => ctry.Cities)
                   .Where(cty => cty.Id == cityId)
                   .SelectMany(hotelCity => hotelCity.Hotels)
                   .Where(hotelCity => hotelCity.IsBookable.Equals(true)).ToList<Hotel>();
            return hotelList;
        }

        /// <summary>
        /// Gets the bookable hotel.
        /// </summary>
        /// <param name="hotelId">The hotel id.</param>
        /// <param name="language">language value</param>
        /// <param name="provider">provider value</param>
        /// <returns>Hotel which are bookable</returns>
        public static Hotel GetBookableHotel(string hotelId, string language, string provider)
        {
            HotelNetwork network = new HotelNetworkManager(language, provider).Network;
            Hotel bookableHotel = network.Continents
                     .SelectMany(continent => continent.Countries)
                     .SelectMany(ctry => ctry.Cities)
                     .SelectMany(hotelCity => hotelCity.Hotels)
                     .Single(hotel => hotel.Id.Equals(hotelId) && hotel.IsBookable.Equals(true));
            return bookableHotel;
        }

        /// <summary>
        /// Gets the available hotel list.
        /// </summary>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="logCategory">The log category.</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// AvailableHotelDetailsList object
        /// </returns>
        public AvailableHotelDetailsList GetAvailabilityMultipleHotels(HotelSearchEntity hotelSearchEntity, LogCategory logCategory, string id)
        {
            bool isException = false;
            DateTime startDateTime = DateTime.Now;
            List<AvailableHotelDetails> hotelList = new List<AvailableHotelDetails>();
            List<AvailabilityRequest> requests = new List<AvailabilityRequest>();
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();

            //RK[14/11/2011]: This try catch is put in place just to make sure OWSPerformance logs are tracked before exception is 
            //propogated.
            try
            {
                //create availability requests for all hotels             
                requests = GetAvailabilityRequestsForMultipleHotels(hotelSearchEntity);

                //Get Responses for all availability request.
                hotelList = GetAvailabilityReponseForMultipleHotels(requests, hotelSearchEntity, logCategory, id);
            }
            catch (Exception ex)
            {
                isException = true;
                throw ex;
            }
            finally
            {
                DateTime endDateTime = DateTime.Now;
                string logMessage = string.Format("{0},Total OWS time, GetAvailabilityMultipleHotels ,{1},{2},{3}", id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                LogHelper.LogInfo(logMessage, logCategory);
            }
            string hotelContinent = HotelNetworkHelper.GetContinentForCity(hotelSearchEntity.HotelCityId, this.Language);
            AvailableHotelDetailsList hotelDetailsList = new AvailableHotelDetailsList(hotelList)
            {
                Continent = hotelContinent,
                Provider = this.Provider,
                Language = this.Language
            };

            return hotelDetailsList;
        }

        /// <summary>
        /// Gets the availability requests for multiple hotels.
        /// </summary>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <returns>List of AvailabilityRequest</returns>
        public List<AvailabilityRequest> GetAvailabilityRequestsForMultipleHotels(HotelSearchEntity hotelSearchEntity)
        {
            List<AvailabilityRequest> requests = new List<AvailabilityRequest>();
            var hotels = hotelSearchEntity.SelectedHotelCode.ToList<string>();

            foreach (string hotel in hotels)
            {
                hotelSearchEntity.SelectedHotelCode.Clear();
                hotelSearchEntity.SelectedHotelCode.Add(hotel);
                this.UpdateAvailabilityRequests(requests, hotelSearchEntity, hotel);
            }

            return requests;
        }

        /// <summary>
        /// Gets the availability reponse for multiple hotels.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="logCategory">The log category.</param>
        /// <param name="id">The id of the request</param>
        /// <returns>List of AvailableHotelDetails</returns>
        public List<AvailableHotelDetails> GetAvailabilityReponseForMultipleHotels(List<AvailabilityRequest> requests, HotelSearchEntity hotelSearchEntity, LogCategory logCategory, string id = "")
        {
            List<AvailableHotelDetails> hotelList = new List<AvailableHotelDetails>();
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();
            //Get Responses for all availability request.
            if (requests.Count > 0)
            {
                responses = AvailabilityAccess.GetAvailabilityForMultipleHotels(requests, logCategory, id);
            }

            if (responses != null && responses.Count > 0)
            {
                //Group response based on hotelid 
                var hotelResponseGroups = from rs in responses
                                          where rs.Result.resultStatusFlag != ResultStatusFlag.FAIL
                                          group rs by rs.AvailResponseSegments[0].RoomStayList[0].HotelReference.hotelCode into grp
                                          select new HotelAvailabilityResponse { HotelId = grp.Key, Responses = grp.ToList<AvailabilityResponse>() };

                //Process Hotel Details for each hotel's response.
                foreach (var hotelResponse in hotelResponseGroups)
                {
                    hotelSearchEntity.SelectedHotelCode.Clear();
                    hotelSearchEntity.SelectedHotelCode.Add(hotelResponse.HotelId);
                    var hotelDetails = this.ProcessHotelResponses(hotelResponse.Responses, hotelSearchEntity, hotelResponse.HotelId);
                    if (hotelDetails.IsHotelAvailable)
                    {
                        //Generate HotelList
                        hotelList.Add(hotelDetails);
                    }
                }
            }

            return hotelList;
        }


        /// <summary>
        /// Gets the availability reponse for multiple hotels rooms and rates.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="logCategory">The log category.</param>
        /// <param name="id">The id of the request</param>
        /// <returns>List of AvailableHotelDetails</returns>
        public List<AvailableHotelDetails> GetAvailabilityReponseForMultipleHotelsRoomsAndRates(List<AvailabilityRequest> requests, HotelSearchEntity hotelSearchEntity, LogCategory logCategory, string id = "")
        {
            List<AvailableHotelDetails> hotelList = new List<AvailableHotelDetails>();
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();
            //Get Responses for all availability request.
            if (requests.Count > 0)
            {
                responses = AvailabilityAccess.GetAvailabilityForMultipleHotels(requests, logCategory, id);
            }

            if (responses != null && responses.Count > 0)
            {
                //Group response based on hotelid 
                var hotelResponseGroups = from rs in responses
                                          where rs.Result.resultStatusFlag != ResultStatusFlag.FAIL
                                          group rs by rs.AvailResponseSegments[0].RoomStayList[0].HotelReference.hotelCode into grp
                                          select new HotelAvailabilityResponse { HotelId = grp.Key, Responses = grp.ToList<AvailabilityResponse>() };

                //Process Hotel Details for each hotel's response.
                foreach (var hotelResponse in hotelResponseGroups)
                {
                    hotelSearchEntity.SelectedHotelCode.Clear();
                    hotelSearchEntity.SelectedHotelCode.Add(hotelResponse.HotelId);
                    var hotelDetails = this.ProcessMultipleHotelResponsesForRoomsAndRates(hotelResponse.Responses, hotelSearchEntity, hotelResponse.HotelId);

                    if (hotelDetails.IsHotelAvailable)
                    {
                        //Generate HotelList
                        hotelList.Add(hotelDetails);
                    }
                }
            }

            return hotelList;
        }

        /// <summary>
        /// Gets the available hotel details.
        /// </summary>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="hotelId">ID of the hotel, passed separately to unwrap (in thread)</param>
        /// <returns>AvailableHotelDetails objects</returns>
        public AvailableHotelDetails GetAvailabilitySingleHotel(HotelSearchEntity hotelSearchEntity, string hotelId, LogCategory logCategory, string id)
        {
            bool isException = false;
            bool isHotelAvailable = true;
            AvailableHotelDetails availableHotelDetails = null;
            DateTime startDateTime = DateTime.Now;
            List<RoomStay> listRoomStay = new List<RoomStay>();
            List<AvailabilityRequest> requests = new List<AvailabilityRequest>();
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();

            if (hotelSearchEntity.ListRooms != null && hotelSearchEntity.ListRooms.Count > 0)
            {
                try
                {
                    foreach (HotelSearchRoomEntity room in hotelSearchEntity.ListRooms)
                    {
                        AvailabilityRequest request = OwsRequestConverter.GetGeneralAvailabilityRequest(hotelSearchEntity, room);
                        request.AvailRequestSegment[0].HotelSearchCriteria[0].HotelRef.hotelCode = hotelId;
                        requests.Add(request);
                    }

                    if (requests.Count > 0)
                    {
                        responses = AvailabilityAccess.GetAvailabilityResponse(requests, logCategory, id);

                        if (responses != null && responses.Count > 0)
                        {
                            foreach (var response in responses)
                            {
                                RoomStay roomStay = default(RoomStay);
                                roomStay = GetRoomStayFromResponse(response);
                                listRoomStay.Add(roomStay);
                                LogHelper.LogInfo(string.Format("Room{0,11}{1,10}", hotelId, hotelSearchEntity.ArrivalDate), LogCategory.Service);
                            }
                        }
                        else
                        {
                            isHotelAvailable = false;
                        }
                    }
                    else
                    {
                        isHotelAvailable = false;
                    }
                }
                catch (Exception ex)
                {
                    isException = true;
                    throw ex;
                }
                finally
                {
                    DateTime endDateTime = DateTime.Now;
                    string logMessage = string.Format("{0},Total OWS time, GetAvailabilitySingleHotel ,{1},{2},{3}", id, startDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), endDateTime.ToString("MM/dd/yyyy HH:mm:ss.fff"), isException.ToString());
                    LogHelper.LogInfo(logMessage, logCategory);
                }
            }

            if (isHotelAvailable)
            {
                RoomTypes roomTypes = new RoomTypeManager(this.Language).RoomTypes;
                RateTypes rateTypes = new RateTypeManager(this.Language).RateTypes;
                availableHotelDetails = new AvailableHotelDetails(listRoomStay, roomTypes, rateTypes, isHotelAvailable, hotelSearchEntity.SearchingType, hotelSearchEntity.CampaignCode, hotelSearchEntity.RoomsPerNight);
                HotelDetailsList hotels = new HotelDetailsManager(this.Language, this.Provider).HotelCollection;

                //Added by Abhishek- Applied check for BookingCode and Response doesn't Contain Miscellinous Element which is Identified by IstrueBlockRate -Start
                if (hotelSearchEntity.QualifyingType != null && hotelSearchEntity.QualifyingType.Equals(AppConstants.BLOCKCODEQUALIFYINGTYPE)
                    && !availableHotelDetails.IsTrueBlockRates)
                {
                    availableHotelDetails = new AvailableHotelDetails();
                    availableHotelDetails.IsHotelAvailable = false;
                }
                //Abhsihek-end
                else if (hotels.Hotels.Count > 0)
                {
                    HotelDetails currentHotelDetails = hotels.Hotels.Single(htl => htl.HotelId == hotelId);
                    string continent = HotelNetworkHelper.GetContinentForHotel(hotelId, this.Language);
                    currentHotelDetails.Continent = (continent != null) ? continent : string.Empty;
                    availableHotelDetails.FillAvailabilityDetails(currentHotelDetails, hotelSearchEntity.CampaignCode);
                }
            }
            else
            {
                availableHotelDetails = new AvailableHotelDetails();
                availableHotelDetails.IsHotelAvailable = false;
            }

            return availableHotelDetails;
        }


        public RoomsAndRates GetRoomsandRates(HotelSearchEntity hotelSearch, string id)
        {
            string hotelId = hotelSearch.SelectedHotelCode[0];
            AvailableHotelDetails availableHotelDetails = this.GetAvailabilitySingleHotel(hotelSearch, hotelSearch.SelectedHotelCode[0], LogCategory.OWSRoomsRate, id);
            List<List<string>> hotelRoomTypeList = new List<List<string>>();
            RoomsAndRates roomsAndRates = new RoomsAndRates();
            NameValueCollection roomTypeMap = new NameValueCollection();
            if (availableHotelDetails != null && availableHotelDetails.AvailableHotelRoomDetails.Count > 0)
            {
                for (int roomIterator = 0; roomIterator < availableHotelDetails.AvailableHotelRoomDetails.Count; roomIterator++)
                {
                    // Select distinct Room Types for one requested room
                    List<string> roomTypeList = availableHotelDetails.AvailableHotelRoomDetails[roomIterator].RoomRates
                                   .OrderBy(h => h.FromRate.PricePerNight)
                                   .GroupBy(g => g.RoomType)
                                   .Select(f => f.First().RoomType).ToList<string>();
                    if (roomTypeList != null && roomTypeList.Count > 0)
                    {
                        hotelRoomTypeList.Add(roomTypeList);
                        foreach (string roomType in roomTypeList)
                        {
                            if (string.IsNullOrEmpty(roomTypeMap[roomType]))
                            {
                                roomTypeMap.Add(roomType, (roomTypeMap.Count + 1).ToString());
                            }
                        }
                    }
                }

                Dictionary<string, List<string>> roomCombinations = GetRoomCombinationList(hotelRoomTypeList, roomTypeMap);

                // Build the response
                roomsAndRates.HotelId = Convert.ToInt32(availableHotelDetails.HotelId);
                roomsAndRates.HotelName = availableHotelDetails.HotelName;
                //XENA
                //roomsAndRates.DeeplinkURLHotelPage = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.HotelLandingPage, availableHotelDetails.HotelId, this.PartnerApiKey);
                roomsAndRates.DeeplinkURLHotelPage = "Xena.com";
                PopulateRooms(availableHotelDetails, roomsAndRates, roomTypeMap, this.Language);
                PopulateRoomRates(hotelSearch, roomCombinations, availableHotelDetails, roomsAndRates, this.PartnerApiKey);
            }

            return roomsAndRates;
        }


        /// <summary>
        /// This method returns the Rooms and rates available for booking in a hotel
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="id">The id of the request</param>
        /// <returns>
        /// Rooms and Rates
        /// </returns>
        public HotelRoomsAndRates GetMultipleRoomsandRates(HotelSearchEntity hotelSearch, string id)
        {
            HotelRoomsAndRates hotelRoomsAndRates = new HotelRoomsAndRates();
            hotelRoomsAndRates.Hotel = new List<HotelRoomRates>();
            List<AvailableHotelDetails> hotelList = new List<AvailableHotelDetails>();
            List<AvailabilityRequest> requests = new List<AvailabilityRequest>();
            List<AvailabilityResponse> responses = new List<AvailabilityResponse>();

            try
            {
                if (hotelSearch.SelectedHotelCode != null && hotelSearch.SelectedHotelCode.Count > 0)
                {
                    requests = GetAvailabilityRequestsForMultipleHotels(hotelSearch);

                    hotelList = GetAvailabilityReponseForMultipleHotelsRoomsAndRates(requests, hotelSearch, LogCategory.OWSRoomsRate, id);
                }
            }
            catch { }

            if (hotelList != null && hotelList.Count > 0)
                for (int i = 0; i < hotelList.Count; i++)
                {
                    HotelRoomRates hotelRoomRates = new HotelRoomRates();
                    hotelRoomRates.RoomsAndRates = new RoomsAndRates();
                    string hotelId = hotelList[i].HotelId;// hotelSearch.SelectedHotelCode[i];

                    AvailableHotelDetails availableHotelDetails = hotelList[i];
                    List<List<string>> hotelRoomTypeList = new List<List<string>>();

                    NameValueCollection roomTypeMap = new NameValueCollection();
                    if (availableHotelDetails != null && availableHotelDetails.AvailableHotelRoomDetails != null && availableHotelDetails.AvailableHotelRoomDetails.Count > 0)
                    {
                        for (int roomIterator = 0; roomIterator < availableHotelDetails.AvailableHotelRoomDetails.Count; roomIterator++)
                        {
                            // Select distinct Room Types for one requested room
                            List<string> roomTypeList = availableHotelDetails.AvailableHotelRoomDetails[roomIterator].RoomRates
                                           .OrderBy(h => h.FromRate.PricePerNight)
                                           .GroupBy(g => g.RoomType)
                                           .Select(f => f.First().RoomType).ToList<string>();
                            if (roomTypeList != null && roomTypeList.Count > 0)
                            {
                                hotelRoomTypeList.Add(roomTypeList);
                                foreach (string roomType in roomTypeList)
                                {
                                    if (string.IsNullOrEmpty(roomTypeMap[roomType]))
                                    {
                                        roomTypeMap.Add(roomType, (roomTypeMap.Count + 1).ToString());
                                    }
                                }
                            }
                        }

                        Dictionary<string, List<string>> roomCombinations = GetRoomCombinationList(hotelRoomTypeList, roomTypeMap);

                        // Build the response
                        hotelRoomRates.RoomsAndRates.HotelId = Convert.ToInt32(availableHotelDetails.HotelId);
                        hotelRoomRates.RoomsAndRates.HotelName = availableHotelDetails.HotelName;
                        //hotelRoomRates.RoomsAndRates.DeeplinkURLHotelPage = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.HotelLandingPage, availableHotelDetails.HotelId, this.PartnerApiKey);
                        hotelRoomRates.RoomsAndRates.DeeplinkURLHotelPage = DeepLinkHelper.GetXenaDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.HotelLandingPage, availableHotelDetails.HotelId, this.PartnerApiKey);
                        PopulateRooms(availableHotelDetails, hotelRoomRates.RoomsAndRates, roomTypeMap, this.Language);
                        PopulateMultipleRoomRates(hotelSearch, hotelId, roomCombinations, availableHotelDetails, hotelRoomRates.RoomsAndRates, this.PartnerApiKey);

                        hotelRoomsAndRates.Hotel.Add(hotelRoomRates);
                        hotelRoomsAndRates.Hotel[i].Id = availableHotelDetails.HotelId;
                    }


                }

            return hotelRoomsAndRates;
        }

        #region Helpers

        /// <summary>
        /// Gets the room stay from response.
        /// </summary>
        /// <param name="response">The request.</param>  
        /// <returns>RoomStay object</returns>
        private static RoomStay GetRoomStayFromResponse(AvailabilityResponse response)
        {
            RoomStay roomStay = null;
            if (response != null && response.AvailResponseSegments != null && response.AvailResponseSegments.Length > 0 && response.AvailResponseSegments[0] != null && response.AvailResponseSegments[0].RoomStayList != null && response.AvailResponseSegments[0].RoomStayList.Length > 0 && response.AvailResponseSegments[0].RoomStayList[0] != null)
            {
                roomStay = response.AvailResponseSegments[0].RoomStayList[0];
            }

            return roomStay;
        }

        /// <summary>
        /// Gets the room combination list.
        /// </summary>
        /// <param name="hotelRoomTypeList">The hotel room type list.</param>
        /// <param name="roomTypeMap">The room type map.</param>
        /// <returns>a dictionary with possible Roomrate combination as values</returns>
        private static Dictionary<string, List<string>> GetRoomCombinationList(List<List<string>> hotelRoomTypeList, NameValueCollection roomTypeMap)
        {
            Dictionary<string, List<string>> roomRateCombination = new Dictionary<string, List<string>>();
            for (int roomListIterator = 0; roomListIterator < hotelRoomTypeList.Count; roomListIterator++)
            {
                if (hotelRoomTypeList.Count > 0 && hotelRoomTypeList[0].Count > 0)
                {
                    for (int room1Iterator = 0;
                        room1Iterator < (hotelRoomTypeList.Count <= 2 ? hotelRoomTypeList[0].Count : hotelRoomTypeList.Count == 3 ? Math.Min(4, hotelRoomTypeList[0].Count) : Math.Min(3, hotelRoomTypeList[0].Count));
                        room1Iterator++)
                    {
                        if (hotelRoomTypeList.Count > 1 && hotelRoomTypeList[1].Count > 0)
                        {
                            for (int room2Iterator = 0;
                               room2Iterator < (hotelRoomTypeList.Count <= 2 ? hotelRoomTypeList[1].Count : hotelRoomTypeList.Count == 3 ? Math.Min(4, hotelRoomTypeList[1].Count) : Math.Min(3, hotelRoomTypeList[1].Count));
                                // room2Iterator < hotelRoomTypeList[1].Count; 
                                room2Iterator++)
                            {
                                if (hotelRoomTypeList.Count > 2 && hotelRoomTypeList[2].Count > 0)
                                {
                                    for (int room3Iterator = 0;
                                        room3Iterator < (hotelRoomTypeList.Count <= 2 ? hotelRoomTypeList[2].Count : hotelRoomTypeList.Count == 3 ? Math.Min(4, hotelRoomTypeList[2].Count) : Math.Min(3, hotelRoomTypeList[2].Count));
                                        //room3Iterator < hotelRoomTypeList[2].Count; 
                                        room3Iterator++)
                                    {
                                        if (hotelRoomTypeList.Count > 3 && hotelRoomTypeList[3].Count > 0)
                                        {
                                            for (int room4Iterator = 0;
                                                room4Iterator < (hotelRoomTypeList.Count <= 2 ? hotelRoomTypeList[3].Count : hotelRoomTypeList.Count == 3 ? Math.Min(4, hotelRoomTypeList[3].Count) : Math.Min(3, hotelRoomTypeList[3].Count));
                                                //room4Iterator < hotelRoomTypeList[3].Count;
                                                room4Iterator++)
                                            {
                                                List<string> roomTypeList = new List<string>();
                                                roomTypeList.Add(hotelRoomTypeList[0][room1Iterator]);
                                                roomTypeList.Add(hotelRoomTypeList[1][room2Iterator]);
                                                roomTypeList.Add(hotelRoomTypeList[2][room3Iterator]);
                                                roomTypeList.Add(hotelRoomTypeList[3][room4Iterator]);
                                                string key = string.Format(
                                                    CultureInfo.InvariantCulture,
                                                    "{0}{1}{2}{3}",
                                                    roomTypeMap[hotelRoomTypeList[0][room1Iterator]],
                                                    roomTypeMap[hotelRoomTypeList[1][room2Iterator]],
                                                    roomTypeMap[hotelRoomTypeList[2][room3Iterator]],
                                                    roomTypeMap[hotelRoomTypeList[3][room4Iterator]]);
                                                if (!roomRateCombination.Keys.Contains(key))
                                                {
                                                    roomRateCombination.Add(key, roomTypeList);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            List<string> roomTypeList = new List<string>();
                                            roomTypeList.Add(hotelRoomTypeList[0][room1Iterator]);
                                            roomTypeList.Add(hotelRoomTypeList[1][room2Iterator]);
                                            roomTypeList.Add(hotelRoomTypeList[2][room3Iterator]);
                                            string key = string.Format(
                                                   CultureInfo.InvariantCulture,
                                                   "{0}{1}{2}",
                                                   roomTypeMap[hotelRoomTypeList[0][room1Iterator]],
                                                   roomTypeMap[hotelRoomTypeList[1][room2Iterator]],
                                                   roomTypeMap[hotelRoomTypeList[2][room3Iterator]]);
                                            if (!roomRateCombination.Keys.Contains(key))
                                            {
                                                roomRateCombination.Add(key, roomTypeList);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    List<string> roomTypeList = new List<string>();
                                    roomTypeList.Add(hotelRoomTypeList[0][room1Iterator]);
                                    roomTypeList.Add(hotelRoomTypeList[1][room2Iterator]);
                                    string key = string.Format(
                                                  CultureInfo.InvariantCulture,
                                                  "{0}{1}",
                                                  roomTypeMap[hotelRoomTypeList[0][room1Iterator]],
                                                  roomTypeMap[hotelRoomTypeList[1][room2Iterator]]);
                                    if (!roomRateCombination.Keys.Contains(key))
                                    {
                                        roomRateCombination.Add(key, roomTypeList);
                                    }
                                }
                            }
                        }
                        else
                        {
                            List<string> roomTypeList = new List<string>();
                            roomTypeList.Add(hotelRoomTypeList[0][room1Iterator]);
                            string key = roomTypeMap[hotelRoomTypeList[0][room1Iterator]];
                            if (!roomRateCombination.Keys.Contains(key))
                            {
                                roomRateCombination.Add(key, roomTypeList);
                            }
                        }
                    }
                }
            }

            return roomRateCombination;
        }


        /// <summary>
        /// Populates the room rates.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="roomRateCombination">The room rate combination.</param>
        /// <param name="roomRatesList">The room rates list.</param>
        /// <param name="roomsAndRates">The rooms and rates.</param>
        /// <param name="partnerKey">The Partner API Key used to uniquely identify the partner</param>
        private static void PopulateRoomRates(HotelSearchEntity hotelSearch, Dictionary<string, List<string>> roomRateCombination, AvailableHotelDetails availableHotelDetails, RoomsAndRates roomsAndRates, string partnerKey)
        {
            //XENA
            //string deepLinkUrl = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.SelectRateRegular, hotelSearch.SelectedHotelCode[0], partnerKey);
            string deepLinkUrl = "Xena.com";
            if (roomRateCombination != null && roomRateCombination.Count > 0)
            {
                List<RoomAndRate> roomRateList = new List<RoomAndRate>();

                List<List<Scandic.Services.BusinessEntity.RoomRate>> roomRatesList = new List<List<Scandic.Services.BusinessEntity.RoomRate>>();
                foreach (AvailableHotelRoomDetails availableHotelRoomDetails in availableHotelDetails.AvailableHotelRoomDetails)
                {
                    roomRatesList.Add(GetMinRoomRatesForEachRoomType(availableHotelRoomDetails));
                }

                foreach (List<string> roomTypes in roomRateCombination.Values)
                {
                    StringBuilder roomDeeplink = new StringBuilder();
                    int roomNumber = 1;
                    RoomAndRate roomAndRate = new RoomAndRate();
                    for (int iterator = 0; iterator < roomTypes.Count; iterator++)
                    {
                        Scandic.Services.BusinessEntity.RoomRate roomRate = new Scandic.Services.BusinessEntity.RoomRate();

                        var selectedRoomRate = from s in roomRatesList[iterator]
                                               orderby s.FromRate.PricePerNight ascending
                                               where (s.RoomType == roomTypes[iterator])
                                               select s;

                        List<Scandic.Services.BusinessEntity.RoomRate> selectedRoomRates = selectedRoomRate.ToList<Scandic.Services.BusinessEntity.RoomRate>();

                        roomAndRate.RoomRates.Add(selectedRoomRates[0]);
                        roomAndRate.CurrenyCode = selectedRoomRates[0].FromRate.PricePerNightCurrencyCode;
                        roomAndRate.TotalPricePerNight += Convert.ToDouble(selectedRoomRates[0].FromRate.PricePerNight);
                        roomAndRate.TotalPricePerStay += Convert.ToDouble(selectedRoomRates[0].FromRate.PricePerStay);

                        roomDeeplink.Append(
                                    string.Format(
                                        CultureInfo.InvariantCulture,
                                        "&RO{0}={1}&RA{2}={3}",
                                        roomNumber,
                                        roomTypes[iterator],
                                        roomNumber,
                                        selectedRoomRates[0].FromRate.RateName));
                        roomNumber++;
                    }

                    roomAndRate.DeepLinkUrl = string.Format(
                                        CultureInfo.InvariantCulture,
                                        "{0}{1}",
                                        deepLinkUrl,
                                        roomDeeplink);
                    roomRateList.Add(roomAndRate);
                }

                roomRateList = roomRateList.OrderBy(r => r.TotalPricePerNight).ToList<RoomAndRate>();

                if (roomRateList.Count > 0)
                {
                    foreach (RoomAndRate roomAndRate in roomRateList)
                    {
                        roomsAndRates.RoomAndRates.Add(roomAndRate);
                    }
                }
            }
        }


        /// <summary>
        /// Populates the room rates.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="roomRateCombination">The room rate combination.</param>
        /// <param name="roomRatesList">The room rates list.</param>
        /// <param name="roomsAndRates">The rooms and rates.</param>
        /// <param name="partnerKey">The Partner API Key used to uniquely identify the partner</param>
        private static void PopulateMultipleRoomRates(HotelSearchEntity hotelSearch, string hotelID, Dictionary<string, List<string>> roomRateCombination, AvailableHotelDetails availableHotelDetails, RoomsAndRates roomsAndRates, string partnerKey)
        {
            //string deepLinkUrl = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.SelectRateRegular, hotelID, partnerKey);
            string deepLinkUrl = DeepLinkHelper.GetXenaDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.SelectRateRegular, hotelID, partnerKey);
            if (roomRateCombination != null && roomRateCombination.Count > 0)
            {
                List<RoomAndRateWithTaxes> roomRateList = new List<RoomAndRateWithTaxes>();

                List<List<Scandic.Services.BusinessEntity.RoomRate>> roomRatesList = new List<List<Scandic.Services.BusinessEntity.RoomRate>>();
                foreach (AvailableHotelRoomDetails availableHotelRoomDetails in availableHotelDetails.AvailableHotelRoomDetails)
                {
                    roomRatesList.Add(GetMinRoomRatesForEachRoomType(availableHotelRoomDetails));
                }

                foreach (List<string> roomTypes in roomRateCombination.Values)
                {
                    StringBuilder roomDeeplink = new StringBuilder();
                    int roomNumber = 1;
                    RoomAndRateWithTaxes roomAndRate = new RoomAndRateWithTaxes();
                    for (int iterator = 0; iterator < roomTypes.Count; iterator++)
                    {
                        Scandic.Services.BusinessEntity.RoomRate roomRate = new Scandic.Services.BusinessEntity.RoomRate();

                        var selectedRoomRate = from s in roomRatesList[iterator]
                                               orderby s.FromRate.PricePerNight ascending
                                               where (s.RoomType == roomTypes[iterator])
                                               select s;

                        List<Scandic.Services.BusinessEntity.RoomRate> selectedRoomRates = selectedRoomRate.ToList<Scandic.Services.BusinessEntity.RoomRate>();

                        roomAndRate.RoomRates.Add(selectedRoomRates[0]);
                        roomAndRate.CurrenyCode = selectedRoomRates[0].FromRate.PricePerNightCurrencyCode;
                        roomAndRate.TotalPricePerNight += Convert.ToDouble(selectedRoomRates[0].FromRate.PricePerNight);
                        roomAndRate.TotalPricePerStay += Convert.ToDouble(selectedRoomRates[0].FromRate.PricePerStay);

                        //Rakesh: SCANMOD:714
                        roomAndRate.Taxes = new List<Tax>();
                        Tax tax = new Tax();
                        if (availableHotelDetails.APITaxRates > 0)
                        {
                            tax.Amount += Convert.ToDouble(String.Format("{0:0.00}", roomAndRate.TotalPricePerStay - Convert.ToDouble(String.Format("{0:0.00}",(roomAndRate.TotalPricePerStay / (1 + (availableHotelDetails.APITaxRates / 100)))))));
                        }
                        else
                            tax.Amount = 0.00;
                        tax.Type = "VAT";
                        roomAndRate.Taxes.Add(tax);

                        if (availableHotelDetails.APITaxRates > 0)
                            roomAndRate.TotalBaseRatePerNight = roomAndRate.TotalPricePerNight - (roomAndRate.TotalPricePerNight - Convert.ToDouble(String.Format("{0:0.00}",(roomAndRate.TotalPricePerNight / (1 + (availableHotelDetails.APITaxRates / 100))))));
                        else
                            roomAndRate.TotalBaseRatePerNight = roomAndRate.TotalPricePerNight;
                        roomAndRate.TotalBaseRatePerNight = Convert.ToDouble(String.Format("{0:0.00}", roomAndRate.TotalBaseRatePerNight));
                        roomAndRate.TotalBaseRatePerStay = roomAndRate.TotalPricePerStay - Convert.ToDouble(tax.Amount);
                        roomAndRate.TotalBaseRatePerStay = Convert.ToDouble(String.Format("{0:0.00}", roomAndRate.TotalBaseRatePerStay));

                        roomDeeplink.Append(
                                    string.Format(
                                        CultureInfo.InvariantCulture,
                                        "&RO{0}={1}&RA{2}={3}",
                                        roomNumber,
                                        roomTypes[iterator],
                                        roomNumber,
                                        selectedRoomRates[0].FromRate.RateName));
                        roomNumber++;
                    }

                    roomAndRate.DeepLinkUrl = string.Format(CultureInfo.InvariantCulture,
                                        "{0}{1}",
                                        deepLinkUrl,
                                        roomDeeplink);
                    roomRateList.Add(roomAndRate);
                }

                roomRateList = roomRateList.OrderBy(r => r.TotalPricePerNight).ToList<RoomAndRateWithTaxes>();

                if (roomRateList.Count > 0)
                {
                    foreach (RoomAndRateWithTaxes roomAndRate in roomRateList)
                    {
                        roomsAndRates.RoomAndRates.Add(roomAndRate);
                    }
                }
            }
        }

        /// <summary>
        /// Populates the rooms.
        /// </summary>
        /// <param name="availableHotelDetails">The available hotel details.</param>
        /// <param name="roomsAndRates">The rooms and rates.</param>
        /// <param name="roomTypeMap">The room type map.</param>
        /// <param name="language">Language of this availability manager</param>
        private static void PopulateRooms(AvailableHotelDetails availableHotelDetails, RoomsAndRates roomsAndRates, NameValueCollection roomTypeMap, string language)
        {
            if (roomTypeMap.Count > 0)
            {
                List<string> roomsToBeFetched = new List<string>();
                List<RoomCategory> roomCategoryCollection = new RoomTypeManager(language).RoomTypes.RoomCategoryCollection;
                try
                {
                    foreach (string roomtype in roomTypeMap.Keys)
                    {
                        roomsToBeFetched.Add(roomtype);
                        //XENA
                        //string roomCategoryPageReferenceId = roomCategoryCollection.First(roomcat => roomcat.RoomCategoryName.Trim() == roomtype.Trim()).PageReferenceId.ToString();
                        string roomCategoryName = roomCategoryCollection.First(roomcat => roomcat.RoomCategoryName.Trim() == roomtype.Trim()).RoomCategoryName.ToString();// .PageReferenceId.ToString();
                        List<Room> rooms = availableHotelDetails.AvailableHotelRoomFacilities
                              //.Where(room => room.RoomCategoryPageReferenceId.Trim() == roomCategoryPageReferenceId.Trim())
                              .Where(room => room.Name.Trim() == roomCategoryName.Trim())
                              .Select(room => new Scandic.Services.BusinessEntity.Room()
                              {
                                  RoomType = roomtype,
                                  RoomTypeDescription = room.Description,
                                  RoomImage = new RoomImage
                                  {
                                      ImageUrl = room.Image.ImageUrl,
                                  }
                              }).ToList<Room>();
                        if (rooms != null && rooms.Count > 0 && !roomsAndRates.Rooms.Contains(rooms[0]))
                        {
                            roomsAndRates.Rooms.Add(rooms[0]);
                            roomsToBeFetched.Remove(roomtype);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                if (roomsToBeFetched.Count > 0)
                {
                    foreach (string roomtype in roomsToBeFetched)
                    {
                        List<Room> rooms = roomCategoryCollection
                              .Where(room => room.RoomCategoryName.Trim().ToLower() == roomtype.Trim().ToLower())
                              .Select(room => new Scandic.Services.BusinessEntity.Room()
                              {
                                  RoomType = room.RoomCategoryName,
                                  RoomTypeDescription = room.RoomCategoryDescription
                              }).ToList<Room>();
                        if (rooms != null && rooms.Count > 0 && !roomsAndRates.Rooms.Contains(rooms[0]))
                        {
                            roomsAndRates.Rooms.Add(rooms[0]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the type of the min room rates for each room.
        /// </summary>
        /// <param name="availableHotelDetails">The available hotel details.</param>
        /// <returns>List of RoomRate with minimum rate for each room type</returns>
        private static List<Scandic.Services.BusinessEntity.RoomRate> GetMinRoomRatesForEachRoomType(AvailableHotelRoomDetails availableHotelRoomDetails)
        {
            List<Scandic.Services.BusinessEntity.RoomRate> roomRatesList = new List<BusinessEntity.RoomRate>();
            var minRoomRates = from avail in availableHotelRoomDetails.RoomRates
                               group avail by avail.RoomType into typeGroup
                               join room in availableHotelRoomDetails.RoomRates
                                        on typeGroup.Key equals room.RoomType
                               where room.FromRate.PricePerNight == typeGroup.Min(r => r.FromRate.PricePerNight)
                               orderby room.FromRate.PricePerNight
                               select room;
            roomRatesList = minRoomRates
                .GroupBy(g => g.RoomType)
                .Select(f => f.First()).ToList();
            return roomRatesList;
        }

        /// <summary>
        /// Updates the availability requests.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="hotelId">The hotel id.</param>
        private void UpdateAvailabilityRequests(List<AvailabilityRequest> requests, HotelSearchEntity hotelSearchEntity, string hotelId)
        {
            if (requests == null)
            {
                requests = new List<AvailabilityRequest>();
            }

            if (hotelSearchEntity.ListRooms != null && hotelSearchEntity.ListRooms.Count > 0)
            {
                foreach (HotelSearchRoomEntity room in hotelSearchEntity.ListRooms)
                {
                    AvailabilityRequest request;
                    request = OwsRequestConverter.GetGeneralAvailabilityRequest(hotelSearchEntity, room);
                    request.AvailRequestSegment[0].HotelSearchCriteria[0].HotelRef.hotelCode = hotelId;
                    requests.Add(request);
                }
            }
        }

        /// <summary>
        /// Processes the hotel responses.
        /// </summary>
        /// <param name="responses">The responses.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="hotelId">The hotel id.</param>
        /// <returns>Available HotelDetails</returns>
        private AvailableHotelDetails ProcessHotelResponses(List<AvailabilityResponse> responses, HotelSearchEntity hotelSearchEntity, string hotelId)
        {
            bool isHotelAvailable = true;
            AvailableHotelDetails availableHotelDetails = null;
            List<RoomStay> listRoomStay = new List<RoomStay>();

            if (responses != null && responses.Count > 0)
            {
                foreach (var response in responses)
                {
                    if (response.Result.resultStatusFlag != ResultStatusFlag.FAIL)
                    {
                        RoomStay roomStay = default(RoomStay);
                        roomStay = GetRoomStayFromResponse(response);
                        listRoomStay.Add(roomStay);
                    }
                    else
                    {
                        isHotelAvailable = false;
                        break;
                    }
                }
            }
            else
            {
                isHotelAvailable = false;
            }

            if (isHotelAvailable)
            {
                RoomTypes roomTypes = new RoomTypeManager(this.Language).RoomTypes;
                RateTypes rateTypes = new RateTypeManager(this.Language).RateTypes;
                availableHotelDetails = new AvailableHotelDetails(listRoomStay, roomTypes, rateTypes, isHotelAvailable, hotelSearchEntity.SearchingType, hotelSearchEntity.CampaignCode, hotelSearchEntity.RoomsPerNight);
                HotelDetailsList hotels = new HotelDetailsManager(this.Language, this.Provider).HotelCollection;

                //Added by Abhishek- Applied check for BookingCode and Response doesn't Contain Miscellinous Element which is Identified by IstrueBlockRate -Start
                if (hotelSearchEntity.QualifyingType != null && hotelSearchEntity.QualifyingType.Equals(AppConstants.BLOCKCODEQUALIFYINGTYPE)
                    && !availableHotelDetails.IsTrueBlockRates)
                {
                    availableHotelDetails = new AvailableHotelDetails();
                    availableHotelDetails.IsHotelAvailable = false;
                }
                //Abhsihek-end
                else if (hotels.Hotels.Count > 0)
                {
                    HotelDetails currentHotelDetails = hotels.Hotels.Single(htl => htl.HotelId == hotelId);
                    string continent = HotelNetworkHelper.GetContinentForHotel(hotelId, this.Language);
                    currentHotelDetails.Continent = (continent != null) ? continent : string.Empty;
                    availableHotelDetails.FillAvailabilityDetails(currentHotelDetails, hotelSearchEntity.CampaignCode);
                }
            }
            else
            {
                availableHotelDetails = new AvailableHotelDetails();
                availableHotelDetails.IsHotelAvailable = false;
                availableHotelDetails.HotelId = hotelId;
            }

            return availableHotelDetails;
        }

        /// <summary>
        /// Processes the multiple hotel responses.
        /// </summary>
        /// <param name="responses">The responses.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="hotelId">The hotel id.</param>
        /// <returns>Available HotelDetails</returns>
        private AvailableHotelDetails ProcessMultipleHotelResponsesForRoomsAndRates(List<AvailabilityResponse> responses, HotelSearchEntity hotelSearchEntity, string hotelId)
        {
            bool isHotelAvailable = true;
            AvailableHotelDetails availableHotelDetails = null;
            List<RoomStay> listRoomStay = new List<RoomStay>();
            try
            {
                if (responses != null && responses.Count > 0)
                {
                    foreach (var response in responses)
                    {
                        if (response.Result.resultStatusFlag != ResultStatusFlag.FAIL)
                        {
                            RoomStay roomStay = default(RoomStay);
                            roomStay = GetRoomStayFromResponse(response);
                            listRoomStay.Add(roomStay);
                        }
                        else
                        {
                            isHotelAvailable = false;
                            break;
                        }
                    }
                }
                else
                {
                    isHotelAvailable = false;
                }

                if (isHotelAvailable)
                {
                    RoomTypes roomTypes = new RoomTypeManager(this.Language).RoomTypes;
                    RateTypes rateTypes = new RateTypeManager(this.Language).RateTypes;
                    availableHotelDetails = new AvailableHotelDetails(listRoomStay, roomTypes, rateTypes, isHotelAvailable, hotelSearchEntity.SearchingType, hotelSearchEntity.CampaignCode, hotelSearchEntity.RoomsPerNight);
                    HotelDetailsList hotels = new HotelDetailsManager(this.Language, this.Provider).HotelCollection;

                    //Added by Abhishek- Applied check for BookingCode and Response doesn't Contain Miscellinous Element which is Identified by IstrueBlockRate -Start
                    if (hotelSearchEntity.QualifyingType != null && hotelSearchEntity.QualifyingType.Equals(AppConstants.BLOCKCODEQUALIFYINGTYPE)
                        && !availableHotelDetails.IsTrueBlockRates)
                    {
                        availableHotelDetails = new AvailableHotelDetails();
                        availableHotelDetails.IsHotelAvailable = false;
                    }
                    //Abhsihek-end
                    else if (hotels.Hotels.Count > 0)
                    {
                        HotelDetails currentHotelDetails = hotels.Hotels.Single(htl => htl.HotelId == hotelId);
                        string continent = HotelNetworkHelper.GetContinentForHotel(hotelId, this.Language);
                        currentHotelDetails.APITaxRates = HotelNetworkHelper.GetCountryVATForHotel(hotelId, this.Language);

                        LogHelper.LogInfo("ProcessMultipleHotelResponsesForRoomsAndRates: APITaxRates: " + currentHotelDetails.APITaxRates + " HotelID: " + hotelId, LogCategory.OWSRoomsRate);

                        currentHotelDetails.Continent = (continent != null) ? continent : string.Empty;
                        availableHotelDetails.FillMultipleRoomRatesAvailabilityDetails(currentHotelDetails, hotelSearchEntity.CampaignCode);
                    }
                }
                else
                {
                    availableHotelDetails = new AvailableHotelDetails();
                    availableHotelDetails.IsHotelAvailable = false;
                    availableHotelDetails.HotelId = hotelId;
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, LogCategory.OWSRoomsRate);
            }
            return availableHotelDetails;
        }




        #endregion
    }
}
