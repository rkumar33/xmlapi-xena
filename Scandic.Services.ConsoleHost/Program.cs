﻿// <copyright file="Program.cs" company="Scandic Hotels">
// Copyright © Scandic   
// </copyright>

namespace Scandic.Services.ConsoleHost
{
    using System;
    using System.ServiceModel.Web;
    using Scandic.Services.Framework;
    using Scandic.Services.Operations;
    using Scandic.Services.Operations.Extensions;  

    /// <summary>
    /// Hosting program to host the services
    /// </summary>
    public static class MainClass
    {
        /// <summary>
        /// Entry point for the console program
        /// </summary>
        /// <param name="args">Command line arguments</param>
        public static void Main(string[] args)
        {
            // Setup Content Service
            var contentHoster = new WebServiceHost(typeof(ContentService));
            contentHoster.Opened += (objSender, objArgs) => Console.WriteLine("Opened: Content Services");
            contentHoster.Closed += (objSender, objArgs) => Console.WriteLine("Closed: Content Services");
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.KeyValidation)))
            {
                contentHoster.Description.Behaviors.Add(new ValidateApiKey());
            }

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.Tracking)))
            {
                contentHoster.Description.Behaviors.Add(new SendRequestToSiteCatalyst());
            }

            // Setup Admin Service
            var adminHoster = new WebServiceHost(typeof(AdministrationService));
            adminHoster.Opened += (objSender, objArgs) => Console.WriteLine("Opened: Admin Services");
            adminHoster.Closed += (objSender, objArgs) => Console.WriteLine("Closed: Admin Services");
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.KeyValidation)))
            {
                adminHoster.Description.Behaviors.Add(new ValidateApiKey());
            }

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.Tracking)))
            {
                adminHoster.Description.Behaviors.Add(new SendRequestToSiteCatalyst());
            }

            // Setup Availabilty Service
            var availabilityHoster = new WebServiceHost(typeof(AvailabilityService));
            availabilityHoster.Opened += (objSender, objArgs) => Console.WriteLine("Opened: Availability Services");
            availabilityHoster.Closed += (objSender, objArgs) => Console.WriteLine("Closed: Availability Services");
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.KeyValidation)))
            {
                availabilityHoster.Description.Behaviors.Add(new ValidateApiKey());
            }

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.Tracking)))
            {
                availabilityHoster.Description.Behaviors.Add(new SendRequestToSiteCatalyst());
            }

            // Open Services
            contentHoster.Open();
            adminHoster.Open();
            availabilityHoster.Open();

            // Wait for user input to close
            Console.WriteLine("Press <ENTER> to shut down the host.");
            Console.ReadLine();
            contentHoster.Close();
            adminHoster.Close();
            availabilityHoster.Close();
        }
    }
}